#include<bits/stdc++.h>
using namespace std;

double p[3], q[3], d[3], c[3], r;


bool predicate(double x){
	double dirRatio[3];
	double newq[3];
	newq[0] = q[0]+d[0]*x;
	newq[1] = q[1]+d[1]*x;
	newq[2] = q[2]+d[2]*x;
	dirRatio[0] = p[0] - newq[0];
	dirRatio[1] = p[1] - newq[1];
	dirRatio[2] = p[2] - newq[2];
	double lambda = (dirRatio[0]*(c[0] - p[0]) + dirRatio[1]*(c[1] - p[1]) + dirRatio[2]*(c[2] - p[2]))/(dirRatio[0]*dirRatio[0]+dirRatio[1]*dirRatio[1]+dirRatio[2]*dirRatio[2]);
	double pt[3];
	//cout<<"lambda   " << lambda << endl;
	pt[0] = lambda * dirRatio[0] + p[0]; 
	pt[1] = lambda * dirRatio[1] + p[1]; 
	pt[2] = lambda * dirRatio[2] + p[2];

	double distsq = (pt[0]-c[0])*(pt[0]-c[0]) + (pt[1]-c[1])*(pt[1]-c[1]) + (pt[2]-c[2])* (pt[2]-c[2]);
	//cout << distsq << endl;
	//cout << r << endl;
	//cout << distsq - r*r<<endl;
	if(distsq - r*r < 0.0000000000000000){
		return false;
	}
	return true;
}



int main(){
	int t;
	cin >> t;
	while(t--){
	  cin >> p[0] >> p[1] >> p[2] >> q[0] >> q[1] >> q[2] >> d[0] >> d[1] >> d[2] >> c[0] >> c[1] >> c[2] >> r;	
	  double lo, hi, mid;
	  lo = 0;
	  hi = 2.000000000000000*1e9;	
		
	  int x =400;	
      while(x--){
      	//cout<<"Dsfd";
	      mid = lo + (hi-lo)/2.0;
	      //cout << mid << " ";
	      //cout << predicate(mid)<<" ";
	      if (predicate(mid) == true)
	         hi = mid;
	      else lo = mid;
	   }
	   cout << setprecision(10);
	   cout << fixed;
	   cout << lo << endl;

	 }
}