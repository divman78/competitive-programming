#include<bits/stdc++.h>
using namespace std;
#define ll long long


int pf(long long n) 
{  
    long long cnt = 0; 
  
    for (long long i = 1; i <= sqrt(n); i = i+2) 
    { 
        if (n%i == 0) 
        { 
        	if(n/i == i) cnt++; 
        	else cnt+=2;
        } 
    }
    return cnt%2; 
    // return n == sqrt(n);

}

main(){
	long long t;
	cin >> t;
	while(t--){
		long long n;
		cin >> n;
		ll arr[n];
		for (long long i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}
		long long cnt = 0;
		//cout << endl;
		for (long long i = 0; i < n; ++i)
		{ 
			if(arr[i]%2 && pf(arr[i])){
				cnt++;
			}

		}
		cout << cnt << endl;
	}
}