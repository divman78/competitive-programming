#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}

bool fun(const pair<int,int>& a, const pair<int,int>& b){
	if(a.first<b.first){
		return true;
	}
	else if(a.first==b.first){
		return (a.second<b.second);
	}
	return false;
}

signed main(){
	init();

	TEST_CASES
	{
		int n; cin>>n;
		string s; cin >> s;
		pair<int,int> arr[n];
		for (int i = 0; i < n; ++i)
		{
			arr[i].first = s[i]-48;
			arr[i].second = i;
		}
		sort(arr, arr+n, fun);
		for (int i = 0; i < n; ++i)
		{
			cout << arr[i].first << " "<< arr[i].second << endl;
		}
		bool flag = true;
		for (int i = 1; i < n; ++i)
		{
			if(s[i]>=s[i-1]) continue;
			flag = false;
			break;
		}

		if(flag){
			for (int i = 0; i < n; ++i)
			{
				cout<< 1 ;
			}
			cout << endl;
		}else{
			flag = true;
			int cnt = 0;
			for (int i = 1; i < n; ++i)
			{
				if(s[i] >= s[i-1]) cnt=0;
				else if(cnt == 1){
					flag = false;
					break;
				}
				else{
					cnt++;
				}
			}
			if(!flag){
				cout << "-\n" << endl;
			}
			else{
				int aans[n] = {0};
				int ind = n-1;
				aans[arr[0].second] = 1;
				for (int i = 1; i < n; ++i)
				{
					if(arr[i].second>arr[i-1].second){
						aans[arr[i].second] = 1;
					}
				}
				for (int i = 0; i < n; ++i)
				{
					if (aans[i])
					{
						cout << aans[i];
					}
					else{
						cout << 2;
					}
				}
				cout << endl;
			}
		}
	}


	




	return 0;
}