#include<bits/stdc++.h>
#define mx 4100
using namespace std;
int dp[mx][105];
int freq[mx];
main(){
	ios_base::sync_with_stdio(false);cin.tie(nullptr);cout.tie(nullptr);cerr.tie(nullptr);
	//freopen("input.txt","r",stdin);
	int n,m,q;
	cin>>n>>m>>q;
	int arr[n];
	for(int i=0;i<n;i++){
		cin>>arr[i];
	}
	reverse(arr,arr+n);
	for(int i=0;i<m;i++){
		int mask = 0;
		string str;
		cin>>str;
		int count = 0;
		for(int j=n-1;j>=0;j--){
			mask += (str[j]-'0')*pow(2,count++);
		}
		freq[mask]++;
	}
	for(int i=0;i<mx;i++){
		for(int j=0;j<mx;j++){
			int sum = 0;
			int p = i;
			int q = j;
			for(int k=0;k<n;k++){
				if(p % 2 == q % 2){
					sum += arr[k];
				}
				p = p / 2;
				q = q / 2;
			}
			if(sum <= 100){
				dp[i][sum] += freq[j];
			}			
		}
	}
	for(int i=0;i<mx;i++){
		for(int j=1;j<=100;j++){
			dp[i][j] += dp[i][j-1];
		}
	}
	while(q--){
		string str;
		cin>>str;
		int num;
		cin>>num;
		int mask = 0;
		int count = 0;
		for(int j=n-1;j>=0;j--){
			mask += (str[j]-'0')*pow(2,count++);
		}
		cout<<dp[mask][num]<<"\n";
		
	}
	
	
}