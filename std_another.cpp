#include<bits/stdc++.h>
using namespace std;

//save time
#define ll long long int

//If using cin and cout
#define ios ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);

//Optimizations
#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")

//fast I/O
#ifndef _WIN32
#define getchar getchar_unlocked
#define putchar putchar_unlocked
#endif
#define gc getchar
#define pc putchar
#define scan getFoo

//Constants
#define PI   3.141592653593
#define MOD  1000000007
#define EPS  0.000000001
 
//loops
#define REP(i,n) 	for(ll i=0;i<(n);++i)
#define FOR(i,a,b) 	for(ll i=(a);i<(b);++i)
#define DFOR(i,a,b) for(ll i=(a);i>=(b);--i)
 
//vectors
#define vi vector<int>
#define vll vector<ll>
#define vii vector<pair<int,int> >
#define pb 	push_back
 
//pairs
#define pi pair<int,int>
#define pll pair<ll,ll>
#define mp make_pair
#define F first
#define S second

//queue
#define di deque<int>
#define qi queue<int>
#define qii queue<pair<int,int>>
#define PQ priority_queue

//general
#define E empty()

//Input methods
template <typename T>
void getFoo(T &x){
  x=0;
  register char c=gc();
  for(;c<48 || c>57;c=gc());
  for(;c>47 && c<58;c=gc())
    x=(x<<1)+(x<<3)+c-48;
}
 
template <typename T1,typename T2>
void getFoo(T1 &x,T2 &y){
    getFoo(x);
    getFoo(y);
}
 
template <typename T1,typename T2,typename T3>
void getFoo(T1 &x,T2 &y,T3 &z){
    getFoo(x);
    getFoo(y);
    getFoo(z);
}

map<ll,ll> mpp;
inline void solve()
{
	ios
	ll t,n,x;
	scan(t);
	while(t--)
	{
	    
	    mpp.clear();
	    scan(n);
	    for(int i=0;i<n;i++)
	    {
	        scan(x);
	        mpp[x]++;
	    }
	    
	    map<ll,ll>::iterator it,it1;
	    ll cc=0;
	    
	    for(it=mpp.begin();it!=mpp.end();it++)
	    {
	        auto itr=mpp.upper_bound(it->second);
	       for(it1=mpp.begin();it1!=itr;it1++)
	       {
	           if(it1->second >= it->first)cc++;
	       }
	    }
	    
	    printf("%lld\n",cc);
	} 
}

int main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
    solve();
    return 0;
}


