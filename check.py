from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import sys
import math
from nearpy import Engine
from nearpy.hashes import RandomBinaryProjections

# L = np.array([[[1,2,3], [4, 5, 6]],[[7,8,9], [10,11,12]]])


# F1 = np.array([1,2,3,4,5])
# F2 = np.array([1,1,1,1,1])
# print(L[0:2, 1:3].flatten())
# print(F1-F2)

# def fspecial_gauss(size, sigma):
#     """Function to mimic the 'fspecial' gaussian MATLAB function
#     """
#     x, y = np.mgrid[-size//2 + 1:size//2 + 1, -size//2 + 1:size//2 + 1]
#     g = np.exp(-((x**2 + y**2)/(2.0*sigma**2)))
#     return g/g.sum()



# aa = fspecial_gauss(5,1)
# print(aa)

# from numpy import random
# import pykgraph

# dataset = random.rand(1000000, 16)
# query = random.rand(1000, 16)

# index = pykgraph.KGraph(dataset, 'euclidean')  # another option is 'angular'
# index.build(reverse=-1)                        #
# index.save("index_file");
# # load with index.load("index_file");

# knn = index.search(query, K=10)                       # this uses all CPU threads
# knn = index.search(query, K=10, threads=1)            # one thread, slower
# knn = index.search(query, K=1000, P=100)              # search for 1000-nn, no need to recompute index.

from annoy import AnnoyIndex
import random

f = 40
t = AnnoyIndex(f)  # Length of item vector that will be indexed
for i in xrange(1000):
    v = [random.gauss(0, 1) for z in xrange(f)]
    t.add_item(i, v)

t.build(10) # 10 trees
t.save('test.ann')

# ...

u = AnnoyIndex(f)
u.load('test.ann') # super fast, will just mmap the file
print(u.get_nns_by_item(0, 1000)) # will find the 1000 nearest neighbors