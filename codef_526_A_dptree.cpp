#include<bits/stdc++.h>
using namespace std;
#define pb(x) push_back(x)
#define mp(x, y) make_pair(x, y)
#define int long long

signed main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int n;
	cin >> n;
	vector<map<int,int> >adj(n);
	int w[n][3];
	int nbrs[n];
	memset(nbrs, 0, sizeof(nbrs));
	memset(w, -1, sizeof(w));
	for (int i = 0; i < n; ++i)
	{
		cin >> w[i][0];
	}
	for(int i = 0; i < n-1; i++){
		int u, v, wt;
		cin >>  u >> v >> wt;
		u--, v--;
		adj[u][v] = wt;
		adj[v][u] = wt;
		nbrs[u]++;
		nbrs[v]++;
	}
	//cout << "heloo";

	// for(int i = 0; i < n; i++){
	// 	cout << i << "->" << endl;
	// 	for(map<int,int>::iterator itr = adj[i].begin(); itr!=adj[i].end(); itr++){
	// 		cout << itr->first << " " << itr->second << endl;
	// 	}
	// }

	queue<int> q;
	for(int i = 0; i < n; i++){
		if(nbrs[i] <= 1){
			q.push(i);
		}
	}

	while(1){
		int u = q.front(); q.pop();
		//cout << u;
		if(nbrs[u] == 0){
			//cout << w[u] << endl;
			break;
		}
		nbrs[u]--;
		int v = adj[u].begin() -> first;
		adj[u].erase(adj[u].find(v));
		nbrs[v]--;
		////////////////
		int temp;
		if(w[v][1] == -1){
			temp = w[v][0] - adj[v][u] + max(w[u][0], w[u][1]);
			if(temp > w[v][0]){
				w[v][1] = temp;
			}
		}
		else{
			temp = w[v][1] - adj[v][u] + max(w[u][0], w[u][1]);
			if(temp > w[v][1]){
				if(temp > w[v][2]){
					w[v][2] = temp;
				}
			}
			temp = w[v][0] - adj[v][u] + max(w[u][0], w[u][1]);
			if(temp > w[v][0]){
				w[v][1] = temp;
			}
		}

		////////////////
		adj[v].erase(adj[v].find(u));
		//cout << u+1 << " " << v+1 << " " << w[v] << endl;
		if(nbrs[v] == 1){
			q.push(v);
		}
		else if(nbrs[v] == 0){
			//cout << max(w[u],w[v]) << endl;
			break;
		}
	}
	int ans = 0;
	for (int i = 0; i < n; ++i)
	{
		for(int j = 0; j < 3; j++)
			ans = max(ans, w[i][j]);
	}

	cout << ans << endl;
}
