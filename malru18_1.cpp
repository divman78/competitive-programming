#include<bits/stdc++.h>
using namespace std;
#define int long long
#define mp(x, y) make_pair(x, y)
#define N 100005
int n;
int childs[N];

int dfs(int v, vector<vector<int> >& ch){
	if(ch[v].size() == 0){
		childs[v] = 1;
		return 1;
	}
	for (int i = 0; i < ch[v].size(); ++i)
	{
		childs[v] += dfs(ch[v][i], ch);
	}
	return childs[v];
}

signed main(){
	cin >> n;
	int pr[n];
	vector<vector<int> >ch(n);
	for (int i = 0; i < n; ++i)
	{
		if(i == 0) pr[0] = 0;
		else{
			int x;
			cin >> x;
			pr[i] = x-1;
			ch[x-1].push_back(i);
		}
	}
	dfs(0,ch);
	vector<pair<int, int> > vec;
	for(int i = 0; i  < n; i++){
		vec.push_back(mp(childs[i], i));
	}

	sort(vec.begin(), vec.end());

	int colors = 1;

	for (int i = 0; i < n; ++i)
	{
		//cout << vec[i].second << " " << vec[i].first << endl;
		int v = vec[i].second;
		if(ch[v].size() == 0){
			cout << 1 << " ";
			continue;
		}
		int numchilds = vec[i].first;
		int diff = numchilds - colors;
		colors += diff;
		cout << colors << " ";

	}

	cout << endl;



}


/* stuff you should look for
    * int overflow, array bounds
    * special cases (n=1?), set tle
    * do smth instead of nothing and stay organized
*/