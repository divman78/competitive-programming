#include<bits/stdc++.h>
using namespace std;

#define int long long

vector<int> bin(int n){
	vector<int> vec;
	while(n){
		vec.push_back(n%2);
		n/=2;
	}
	return vec;
}

void rec(vector<int>& ans, int cnt, int p){
	if(p == 0){
		return;
	}
	if(cnt == 0){
		return;
	}

}

int vpn(int n){
	int cnt = 0;
	while(n){
		if(n%2 == 1){
			cnt++;
		}
	}
	return cnt;
}


signed main(){
	freopen("input.txt","r",stdin);
	// int N = 100000;
	// int pow2[n];
	// pow2[0] = 0;

	int n, k;
	cin >> n >> k;
	vector<int> vec = bin(n);
	int cnt = 0;

	for (int i = 0; i < vec.size(); ++i)
	{
		if(vec[i] == 1){
			cnt++;
		}
	}

	if(cnt > k){
		cout << "No";
	}

	else if(k > n){
		cout << "No";
	}

	else{
		cout << "Yes"<<endl;
		std::vector<int> ans;
		while(vpn(n) != k){
			n--;
			k--;
			ans.push_back(1);		
		}
		std::vector<int> temp = bin(n);
		for(int i=0;i<temp.size();i++){
			if(temp[i] == 1){
				int p = 2 << (i-1);
				ans.push_back(p);
			}
		}
		for(int i=0;i<ans.size();i++){
			cout<<ans[i]<<" ";
		}

	}



}