#include<bits/stdc++.h>
using namespace std;
#define N 300005
#define endl "\n"
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0)
#define int long long

int n;
int  arr[N][3], prefsum[N][3], prefsop[N][3], sufsop[N][3];

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif

	cin >> n;

	memset(prefsum, 0, sizeof(prefsum));
	memset(prefsop, 0, sizeof(prefsum));
	memset(sufsop, 0, sizeof(prefsum));

	for (int j = 1; j <= 2; ++j)
	{
		for (int i = 1; i <= n; ++i)
		{
			cin >> arr[i][j];
		}
	}

	for (int i = 1; i <= n; ++i)
	{
		for (int j = 1; j <= 2; ++j)
		{
			prefsum[i][j] += prefsum[i-1][j] + arr[i][j];
			// cout << prefsum[i][j] << " ";
		}
		// cout << endl;
	}

	for (int i = 1; i <= n; ++i)
	{
		for (int j = 1; j <= 2; ++j)
		{
			prefsop[i][j] += prefsop[i-1][j] + i*arr[i][j];
			// cout << prefsop[i][j] << " ";
		}
		// cout << endl;
	}

	for (int i = n; i >= 1; --i)
	{
		for (int j = 1; j <= 2; ++j)
		{
			sufsop[i][j] += sufsop[i+1][j] + (n-i+1)*arr[i][j];
			// cout << sufsop[i][j] << " ";

		}
		// cout << endl;
	}

	// for(int j = 1; j <= 2; j++){
	// 	for (int i = 1; i <= n; ++i)
	// 	{
	// 		cout << prefsum[i][j] << " ";
	// 	}
	// 	cout << endl;
	// }

	// for(int j = 1; j <= 2; j++){
	// 	for (int i = 1; i <= n; ++i)
	// 	{
	// 		cout << prefsop[i][j] << " ";
	// 	}
	// 	cout << endl;
	// }

	// for(int j = 1; j <= 2; j++){
	// 	for (int i = 1; i <= n; ++i)
	// 	{
	// 		cout << sufsop[i][j] << " ";
	// 	}
	// 	cout << endl;
	// }	


	int sum = 0;
	int res = 0;
	int t = -1;

	for(int i = 1; i <= n; i++){

		if(i&1){
			int diff1 = prefsop[n][1] - prefsop[i-1][1];
			int diff2 = prefsum[n][1] - prefsum[i-1][1];
			int x = (i-1)*2 - i;
			int prod1 = diff1 + x*diff2;
			int diff3 = sufsop[i][2];
			int diff4 = prefsum[n][2] - prefsum[i-1][2];
			int y = n-1+i-1;
			int prod2 = diff3 + y*diff4;
			res = max(prod1+prod2+sum, res);
			// cout << diff1<< " " << diff2 << " " << prod1 << " " <<" ";
			// cout <<diff3 << " "<<diff4 << " " << prod2 << " " << x << " " << y <<" "<<sum<<" "<< prod1+prod2+sum << endl;
			sum += (t+1)*arr[i][1]+(t+2)*arr[i][2];

		}
		else{
			int diff1 = prefsop[n][2] - prefsop[i-1][2];
			int diff2 = prefsum[n][2] - prefsum[i-1][2];
			int x = (i-1)*2 - i;
			int prod1 = diff1 + x*diff2;
			int diff3 = sufsop[i][1];
			int diff4 = prefsum[n][1] - prefsum[i-1][1];
			int y = n-1+i-1;
			int prod2 = diff3 + y*diff4;
			res = max(prod1+prod2+sum, res);
			// cout << diff1<< " " << diff2 << " " << prod1 << " " <<" ";
			// cout <<diff3 << " "<<diff4 << " " << prod2 << " " << x << " " << y <<" "<<sum<< " "<< prod1+prod2+sum <<endl;
			sum += (t+1)*arr[i][2]+(t+2)*arr[i][1];
		}
		t+=2;

	}

	cout << res << endl;


}