#include <bits/stdc++.h>
using namespace std;

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  if (p1==p2) { p2=(p1=buf)+fread(buf,1,100000,stdin); if (p1==p2) return EOF; }
  return *p1++;
}

inline int getint(){
  int x;
  char c=nc(),b=1;
  if(c == -1)
	return 0;
  for (;!(c>='0' && c<='9');c=nc()) if (c=='-') b=-1;
  for (x=0;c>='0' && c<='9';x=x*10+c-'0',c=nc()); x*=b;
  return x;
}

#define N 101010

map <int, int> mp[N<<2];
int a[N], b[N];
int ans[N<<2], to[N<<2];

void build(int L, int R, int id) {
	mp[id].clear();
	for (int i = L; i <= R; i ++) mp[id][b[i]] ++;
	ans[id] = 0;
	to[id] = 0;
	for (int i = L; i <= R; i ++) if (a[i] == b[i]) ans[id] ++;
	if (L == R) return;
	int mid = (L + R) >> 1;
	build(L, mid, id << 1);
	build(mid + 1, R, (id << 1) | 1);
}

void set_to(int id, int c) {
	to[id] = c;
	ans[id] = mp[id].count(c) ? mp[id][c] : 0;
}

void push_down(int id) {
	if (to[id]) {
		set_to(id << 1, to[id]);
		set_to((id << 1) | 1, to[id]);
		to[id] = 0;
	}
}

void push_up(int id) {
	ans[id] = ans[id<<1] + ans[(id<<1)|1];
}

void modify(int le, int ri, int c, int L, int R, int id) {
	if (le > R || L > ri) return;
	if (le <= L && R <= ri) {
		set_to(id, c);
		return;
	}
	push_down(id);
	int mid = (L + R) >> 1;
	modify(le, ri, c, L, mid, id << 1);
	modify(le, ri, c, mid + 1, R, (id << 1) | 1);
	push_up(id);
}

int main() {
	int T;
	scanf("%d", &T);
	while (T --) {
		int n, m;
		n = getint(), m = getint();
		for (int i = 1; i <= n; i ++) a[i] = getint();
		for (int i = 1; i <= n; i ++) b[i] = getint();
		build(1, n, 1);
		cerr << "build" << endl;
		int rlt = ans[1];
		int le, ri, c;
		while (m --) {
			le = getint(), ri = getint(), c = getint();
			le ^= rlt, ri ^= rlt, c ^= rlt;
			modify(le, ri, c, 1, n, 1);
			rlt = ans[1];
			printf("%d\n", rlt);
		}
		cerr << "end" << endl;
	}

	return 0;
}