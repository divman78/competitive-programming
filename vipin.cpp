#include<bits/stdc++.h>
#define mx 50
#define mod 1000000007
#define int long long
using namespace std;
map<int,int> mp;
vector<int> fact[mx];
int ans[mx];
map<int,int> visited; 
int arr[mx];
int anss = 1;
vector<int> primeF[mx];
vector<int> factor[mx];

int rec(int n){
	if(visited[n] == 1){
		return ans[n] % mod;
	}
	if(mp[n] == 0){
		return 0; 
	}
	else{
	//	cout<<"n = "<<n<<endl;
		visited[n] = 1;
		ans[n] = (ans[n] + 1) % mod;
		for(int i=0;i<fact[n].size();i++){
			ans[fact[n][i]] = rec(fact[n][i]) % mod;
			ans[n] = (ans[n] + ans[fact[n][i]]) % mod;
		}
		return ans[n] % mod;
	}
}

vector<int> ret;
void combinationUtil(vector<long long> arr, long long data[], long long start, long long end, long long index, long long r)
{
    if (index == r)
    {
    	long long val = 1;
        for (long long j=0; j<r; j++)
            val *= data[j];
        ret.push_back(val);
        return;
    }
 
    for (long long i=start; i<=end && end-i+1 >= r-index; i++)
    {
        data[index] = arr[i];
        combinationUtil(arr, data, i+1, end, index+1, r);
    }
} 

void Combination(vector<long long> arr, long long r)
{
	long long n = arr.size();
    long long data[r];
    combinationUtil(arr, data, 0, n-1, 0, r);
}

int primes[mx];
void preprocess(){
	primeF[1].push_back(1);
	for(int i=2;i<mx;i++){
		if(primes[i] == 0)
		{
			for(int j=i;j<mx;j+=i){
				primes[j] = 1;
				int temp = j;
				while(temp % i == 0){
					primeF[j].push_back(i);
					temp = temp/i;
				} 
			}
		}
	}
	for(int i=1;i<mx;i++){
		ret.resize(0);
		for(int j=0;j<primeF[i].size();j++){
			Combination(primeF[i],j+1);
		}
		factor[i] = ret;
	}
}

main(){
	preprocess();
	for(int i=1;i<=20;i++){
		cout<<i<<" => ";
		for(int j=0;j<factor[i].size();j++){
			cout<<factor[i][j]<<" ";
		}
		cout<<endl;
	}
	//freopen("input.txt","r",stdin);
	/*
	for(int i=1;i<mx;i++){
		for(int j=1;j<=sqrt(i);j++){
			if(i%j == 0){
				if(j == i/j){
					fact[i].push_back(j);
				}
				else{
				fact[i].push_back(j);
				fact[i].push_back(i/j);
				}
			}
		}
		sort(fact[i].begin(),fact[i].end());
		fact[i].pop_back();
		reverse(fact[i].begin(),fact[i].end());
	}
	*/
	/*
	for(int i=1;i<=10;i++){
		for(int j=0;j<fact[i].size();j++){
			cout<<fact[i][j]<<" ";
		}
		cout<<endl;
	}*/
	//cout<<"Hello"<<endl;
	/*
	int t;
	cin>>t;
	while(t--){
		for(int i=1;i<mx;i++){
			ans[i] = 0;
		}
		mp.clear();
		visited.clear();
		int n;
		cin>>n;
		for(int i=1;i<=n;i++){
			cin>>arr[i];
			mp[arr[i]]++;
		}
		sort(arr+1,arr+n+1);
		reverse(arr+1,arr+n+1);
		
		for(int i=1;i<=n;i++){
			if(visited[arr[i]] == 0){
				ans[arr[i]] = rec(arr[i]) % mod;
			}
		}
		int count = 0;
		for(int i=1;i<=n;i++){
			count = (count + ans[arr[i]])%mod;
		}
		cout<<count<<endl;


	}
	*/
}