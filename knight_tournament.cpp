#include<bits/stdc++.h>
using namespace std;
#define N 300010
#define left (2*idx+1)
#define right (2*idx+2)
#define mid ((l+r)>>1)


int n, m;

int seg[4*N];

void update(int idx, int l, int r, int x, int y, int val){
	if(seg[idx] != 0){
		if(l!=r){
			if(seg[left]==0)
				seg[left] = seg[idx];
			if(seg[right]==0)
				seg[right] = seg[idx];
		}
	}
	if(l > r || l > y || r < x){
		return;
	}
	if(l >= x && r <= y){
		if(seg[idx]==0)
			seg[idx]=val;
		return;
	}
	update(left, l, mid, x, y, val);
	update(right, mid+1, r, x, y, val);
}

int ans;
void query(int idx, int l, int r, int udx){
	if(seg[idx] != 0){
		if(l!=r){
			if(seg[left]==0)
				seg[left] = seg[idx];
			if(seg[right]==0)
				seg[right] = seg[idx];
		}
	}
	if(l > r || l > udx || r < udx){
		return;
	}
	if(l == r){
		ans = seg[idx];
		return;
	}
	query(left, l, mid, udx);
	query(right, mid+1, r, udx);

}


signed main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	cin >> n >> m;

	for (int i = 0; i < m; ++i)
	{
		//cout << i << " -> \n";
		int l, r, x;
		cin >> l >> r >> x;
		l--; r--;
		int md = x-2;
		if(md >= l && md <= r){
			update(0, 0, n-1, l, md, x);
		}
		if(md >= l-2 && md <= r-2){
			update(0, 0, n-1, md+2, r, x);
		}
	}

	for (int i = 0; i < n; ++i)
	{
		query(0, 0, n-1, i);
		cout << ans << " ";
	}
}