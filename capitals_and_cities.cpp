#include<bits/stdc++.h>
using namespace std;
#define int long long

signed main(){
	int n, k;
	cin >> n >> k;
	pair<int,int> arr[n];
	int total = 0;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i].first;
		arr[i].second = i;
		total+=arr[i].first;
	}
	sort(arr, arr+n);
	
	int prev = 0;
	int nxt = total;
	int ans=LLONG_MAX;
	int ind = INT_MAX;
	for (int i = 0; i < n; ++i){
		prev+=(i>0)?arr[i-1].first:0;
		nxt-=arr[i].first;
		int sum = (arr[i].first*i-prev) + (nxt - arr[i].first*(n-i-1)) - k;
		if(sum<0){
			sum = k%2;
		}
		if(ans>sum){
			ind = arr[i].second;
			ans = sum;
		}
		else if(ans == sum && ind > arr[i].second){
		    ind = arr[i].second;
		}
	}
	cout << ind+1 << " " << ans << endl;
}