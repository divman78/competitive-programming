#include<bits/stdc++.h>
using namespace std;


void seive(){

}

int main()
{
	#ifndef ONLINE_JUDGE
	//freopen("input.txt","r",stdin);
	#endif

	int t;
	cin >> t;
	//t = 1;
	while(t--){
		int n;
		cin >> n;
		int arr[n];
		for(int i = 0; i < n; i++){
			cin >> arr[i];
		}
		vector<int> v[n];
		for(int i = 0; i < n; i++){
			for(int j = 1; j*j < arr[i]; j++){
				if(arr[i]%j == 0){
					v[i].push_back(j);
					v[i].push_back(arr[i]/j);
				}
			}
			int sq = sqrt(arr[i]);
			if(sq*sq == arr[i]){
				v[i].push_back(sq);
			}
		}

		int ans[n];
		memset(ans, 0, sizeof(ans));

		for(int i = 0; i < n; i++){
			for(int j = 0; j < v[i].size(); j++){
				if(v[i][j] > i+1) continue;
				int divi = v[i][j];
				ans[divi] = ans[divi-1]; 
			}
		}
		for (int i = 0; i < n; ++i)
		{
			cout << ans[i] << " ";
		}
		//cout << ans[10000];
		cout << endl;

	} 

	return 0;
}