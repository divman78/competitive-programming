#include<bits/stdc++.h>
using namespace std;
#define N 3009
#define M 3009


long long party[N], bribe[M];
long long vote[M];


main(){

	long long n, m;
	cin >> n >> m;
	
	for (long long i = 0; i < n; ++i)
	{                                           
		cin >> party[i] >> bribe[i];
		party[i]--;
	}

    long long mxp = 0;
    for (long long i = 0; i < m; ++i)
    {
    	vote[i] = 0;
    }
	for (long long i = 0; i < n; ++i)
	{
		vote[party[i]]++;
		if(vote[mxp] < vote[party[i]]){
			mxp = party[i];
		}
	}

	// for (long long i = 0; i < m; ++i)
 //    {
 //    	cout << vote[i] << " ";
 //    }
	cout << mxp <<endl;

	long long ans = 0;
	long long t = 10;
	while(t--){
        //cout <<"Df";
		if(vote[mxp] == vote[0]){
			bool flag = false;
			for (long long i = 1; i < m; ++i)
			{
				if(vote[mxp] == vote[i]){
					flag = true;
					break;
				}
			}
			if(!flag) {
				cout <<"ans = ";
				cout << ans << endl;
				break;
			} 
		}

		long long temp = INT_MAX;
		long long index = -1;
		for (long long i = 0; i < n; ++i)
		{
			if(vote[party[i]]==vote[mxp]  && party[i]){
				if(bribe[i] < temp){
					temp = min(bribe[i], temp);
					index = i;
				}
			}
		}


		long long min1 = INT_MAX, min2 = INT_MAX;
		long long index1 = -1, index2 = -1;
		long long cnt = 0;

		for (long long i = 0; i < n; ++i)
		{
			if(vote[party[i]]!=vote[mxp] && party[i]){
				if(bribe[i] < min1){
					min1 = min(bribe[i], min1);
					index1 = i;
					cnt = 1;
				}
			}
		}
        
        for (long long i = 0; i < n; ++i)
		{
			if(vote[party[i]]!=vote[mxp] && party[i]){
				if(bribe[i] < min2 && i != index1){
					min2 = min(bribe[i], min2);
					index2 = i;
					cnt = 2;
				}
			}
		}
 
		if(vote[0] == vote[mxp] && min1 < temp){
			ans += min1;
			vote[0]++;
			vote[party[index1]]--;
			party[index1] = 0;
		}

		else if(cnt == 2 && min1 + min2 < temp){
			ans += min1+min2;
			vote[0]+=2;
			vote[party[index1]]--;
			vote[party[index2]]--;
			party[index1] = 0;
			party[index2] = 0;
		}
		else{
			ans += temp;
			vote[0]++;
			vote[party[index]]--;
			party[index] = 0;
		}

		for (long long i = 0; i < m; ++i)
		{
			if(vote[i] > vote[mxp]){
				mxp = i;
			}
		}
		cout << temp << " " << min1 << " " << min2 << " "<<ans <<" "<<index <<" "<<index1 <<" "<<index2<<" "<<vote[0] <<" "<<vote[mxp]<<endl;
	}






}