#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include<unistd.h>

// A normal C function that is executed as a thread 
// when its name is specified in pthread_create()
void *myThreadFun(void *vargp)
{
    sleep(1);
    printf("Printing GeeksQuiz from Thread %d \n", *(int*)vargp);
    return NULL;
}

void *myThreadFun1(void *vargp)
{
    sleep(1);
    printf("Printing GeeksQuiz from Thread %d \n", *(int*)vargp);
    return NULL;
}

  
int main()
{
    pthread_t thread_id1, thread_id2;
    printf("Before Thread\n");
    int x = 10;
    pthread_create(&thread_id1, NULL, myThreadFun, &x);
    pthread_create(&thread_id2, NULL, myThreadFun1, &x);
    printf("%d\n", (int)thread_id1);
    pthread_join(thread_id1, NULL);
    pthread_join(thread_id2, NULL);
    printf("After Thread\n");
    exit(0);
}