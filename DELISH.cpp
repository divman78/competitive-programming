#include<bits/stdc++.h>
using namespace std;

int main(){
	int t;
	//freopen("input.txt","r",stdin);
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		long long arr[n];
		for (int i = 0; i < n; ++i){
			cin >> arr[i];
		}
		long long premax[n], premin[n], sufmax[n], sufmin[n];
		//prefix for maximum sum till every index
		premax[0] = arr[0];
		for (int i = 1; i < n; ++i){
			long long newMax = premax[i-1] + arr[i];
			if(newMax > arr[i]){
				premax[i] = newMax;
			}
			else{
				premax[i] = arr[i];
			}
		}
		//sufmax for maximum sum till every index
		sufmax[n - 1] = arr[n - 1];
		for (int i = n - 2; i >= 0; --i){
			long long newMax = sufmax[i+1] + arr[i];
			if(newMax > arr[i]){
				sufmax[i] = newMax;
			}
			else{
				sufmax[i] = arr[i];
			}
		}
        //premin for minimum sum till every index
		premin[0] = arr[0];
		for (int i = 1; i < n; ++i){
			long long newmin = premin[i-1] + arr[i];
			if(newmin < arr[i]){
				premin[i] = newmin;
			}
			else{
				premin[i] = arr[i];
			}
		}
		//sufmin for minimum sum till every index
		sufmin[n - 1] = arr[n - 1];
		for (int i = n - 2; i >= 0; --i){
			long long newmin = sufmin[i+1] + arr[i];
			if(newmin < arr[i]){
				sufmin[i] = newmin;
			}
			else{
				sufmin[i] = arr[i];
			}
		}
		for (int i = 1; i < n; ++i){
			premin[i] = min(premin[i], premin[i-1]);
			premax[i] = max(premax[i], premax[i-1]);
		}
		for (int i = n-2; i >=0; --i){
			sufmin[i] = min(sufmin[i], sufmin[i+1]);
			sufmax[i] = max(sufmax[i], sufmax[i+1]);
		}

		// for (int i = 0; i < n; ++i){
		// 	cout << "pmin = " << premin[i] <<" "<< "smin = " << sufmin[i] <<" "<< "pmax = " << premax[i] <<" "<< "smax = " << sufmax[i] <<" "<<endl;
		// }
		long long ans = 0;

		for (int i = 1; i < n; ++i){
			ans = max(ans, abs(sufmax[i]-premin[i-1]));
			ans = max(ans, abs(sufmin[i]-premax[i-1]));
		}
		cout << ans << endl;

	}
}