#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define N 100005

int pr[N];
int su[N];
vector<int> childs[N];
int val[N];


long long ans = 0;

void dfs(int v, int par){
	//cout << v << " ";
	if(su[v] == -1){
		int mini = INT_MAX;
		for (int i = 0; i < childs[v].size(); ++i)
		{
			mini = min(su[childs[v][i]], mini);
			if(su[childs[v][i]] < su[par]){
				cout << -1 << endl;
				exit(0);
			}
		}
		if(mini == INT_MAX){
			su[v] = su[par];
			val[v] = 0;
		}

		else{
			su[v] = mini;
			val[v] = mini-su[par];
		}
	}
	else{
		if(v == par){
			val[v] = su[v];
		}
		else{
			val[v] = su[v]-su[par];
			//cout << su[v]<<" "<<su[par]<<endl;
		}
	}
	//cout << val[v] << " ";
	//cout << endl;
	ans+=val[v];

	for (int i = 0; i < childs[v].size(); ++i)
	{
		dfs(childs[v][i], v);
	}
}









signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif

	int n;
	cin >> n;
	for (int i = 2; i <= n; ++i)
	{
		cin >> pr[i];
	}

	for (int i = 2; i <= n; ++i)
	{
		childs[pr[i]].push_back(i);
	}

	for (int i = 1; i <= n; ++i)
	{
		cin >> su[i];
	}

	dfs(1, 1);
	cout << ans << endl;
	

	

	



	return 0;
}