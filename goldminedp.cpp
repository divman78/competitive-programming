#include<bits/stdc++.h>
using namespace std;

int main(){
	freopen("input.txt","r",stdin);
	long long n, m;
	cin >> n >> m;
	long long arr[n+1][m+1];
	memset(arr,0,sizeof(arr));
	for (long long i = 1; i <= n; ++i)
	{
		for (long long j = 1; j <= m; ++j)
		{
			cin >> arr[i][j];
		}
	}

	for (long long i = 1; i <= n; ++i)
	{
		for (long long j = 2; j <= m; ++j)
		{
			arr[i][j] += arr[i][j-1];
		}
	}

	for (long long i = 2; i <= n; ++i)
	{
		for (long long j = 1; j <= m; ++j)
		{
			arr[i][j] += arr[i-1][j];
		}
	}

	long long q;
	cin >> q;
	while(q--){
		long long x1, y1, x2, y2;
		if(x1*y1 > x2*y2){
			swap(x1,x2);
			swap(y1,y2);
		}
		cin >> x1 >> y1 >> x2 >> y2;
		long long ans = arr[x2][y2] + arr[x1-1][y1-1] - arr[x1-1][y2] - arr[x2][y1-1];
		cout << ans << endl;
	}
}

