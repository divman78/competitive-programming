#include <bits/stdc++.h>
#pragma GCC optimize("Ofast,no-stack-protector,unroll-loops")
#pragma GCC target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#define ll          long long
#define pb          push_back
#define eb 			emplace_back
#define mp 			make_pair
#define pii         pair<ll,ll>
#define vi          vector<ll>
#define vii         vector<pii>
#define mi          map<ll,ll>
#define mii         map<pii,ll>
#define all(a)      (a).begin(),(a).end()
#define fi          first
#define si          second
#define sz(x)       (ll)x.size()
#define endl        '\n'
#define checkbit(n, b) ((n >> b) & 1)
#define mod        1000000007
#define puts(n)		cout<<n;
#define rep(i,a,b)  for(ll i=a;i<b;i++)
#define mset(m,v) memset(m,v,sizeof(m))
using namespace std;
ll posx[]={1,-1,0,0};
ll posy[]={0,0,1,-1};
 
void solve()
{
    ll n,m,k,i,j;
    cin>>n>>m>>k;
    ll a[n];
    for(i=0;i<n;i++)
    cin>>a[i];
    ll dp[n][m];
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        dp[i][j]=-1ll*mod*10000000;
    }
    dp[0][0]=(a[0]-k);
    for(i=1;i<n;i++)
    {
        if(dp[i-1][m-1]>0)
        dp[i][0]=dp[i-1][m-1]+(a[i]-k);
        else
        dp[i][0]=(a[i]-k);
        for(j=1;j<m;j++)
        {
            dp[i][j]=dp[i-1][j-1]+(a[i]);
        }
    }
    ll ans=0;
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        ans=max(ans,dp[i][j]);
    }
    cout<<ans;
 
}
 
signed main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int t=1;
	//cin>>t;
	while(t--)
	{
		solve();
	}
	return 0;
}