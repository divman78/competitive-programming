#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}
		sort(arr, arr+n);
		double ans = (double)arr[n -1]/2.0 + (double)arr[n-2]/2.0;
		for(int i = n - 3; i >= 0; i--){
			ans = (ans/2.0) + ((double)arr[i]/2.0);
		}
		cout << fixed << setprecision(10);
		cout << ans << endl;

	}
	return 0;
}