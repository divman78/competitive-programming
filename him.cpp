#include<bits/stdc++.h>
using namespace std;
int main()
{
    int t,n;
    cin>>t;
    while(t--)
    {
        cin>>n;
        int b[n];
        int a[1000001]={0};
        int k;
        int evencount=0;
        int oddcount=0;
        for(int i=0;i<n;i++)
        {
            cin>>k;
            b[i]=k;
            a[k]++;
            if(k%2!=0)
                oddcount++;
            else 
                evencount++;
        }
        long long pairs1=(oddcount*(oddcount-1))/2;
        long long pairs2=(evencount*(evencount-1))/2;
        int count=0;
        for(int i=0;i<n;i++)
        {
        	if(a[b[i]]>=2)
			count+=(a[b[i]]*(a[b[i]]-1))/2;
		}
        for(int i=0;i<n;i++)
        {
            //count+=a[b[i]]/2;
            int temp=b[i];
            int count1=0;
            int count2=0;
            
            if((temp-2)>=0&&temp^(temp-2)==2)
            {
                count1=a[temp];
                count2=a[temp-2];
                count+=count1*count2;
                a[temp]=0;
                a[temp-2]=0;
            }
            else if(temp^(temp+2)==2)
            {
                count1=a[temp];
                count2=a[temp+2];
                count+=count1*count2;
                a[temp]=0;
                a[temp+2]=0;
            }
        }
        cout<<pairs1+pairs2-count<<endl;
    }
    return 0;
}