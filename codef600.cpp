#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}

signed main(){
	init();

	//TEST_CASES
	{
		int n, m;
		cin >> n >> m;

		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}

		sort(arr, arr+n);
		reverse(arr, arr+n);

		
		int num = 0;
		int sum[m] = {0};
		
		for (int i = n-1; i >= 0; --i)
		{
			sum[i%m]+=arr[i];

			num += arr[i]*((i/m)+1);
		}


		
		vector<int> ans;

		for (int i = 0; i < n; ++i)
		{
			
			ans.push_back(num);

			num -= sum[i%m];

			sum[i%m]-=arr[i];

		}

		reverse(ans.begin(), ans.end());
            
		for (int i = 0; i < n; ++i)
		{
			cout << ans[i] << " ";
		}

		cout << endl;

		
		
	}


	




	return 0;
}