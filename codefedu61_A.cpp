#include<bits/stdc++.h>
#include<vector>
#include<set>
#include<string>
#include<map>
#include<queue>
#include<stack>

#define int long long
#define endl "\n"

const int mod=(int)1e9+7;
const int inf=(int)1e18;
const int mx=1e5+5;

using namespace std;

signed main(){
	ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int n;
	cin>>n;
	int q;
	cin>>q;
	pair<int,int> arr[q];
	for(int i=0;i<q;i++){
		cin>>arr[i].first>>arr[i].second;
	}
	int pref[n+1] = {0};
	for(int i=0;i<q;i++){
		int a = arr[i].first;
		pref[a]++;
		int b = arr[i].second;
		b++;
		pref[b]--;
	}
	int one[n+1] = {0};
	int two[n+1] = {0};
	int a = 0;
	int b = 0;
	int count = 0;
	for(int i=1;i<=n;i++){
		pref[i] = pref[i] + pref[i-1];
		if(pref[i] == 1){
			a++;
		}
		if(pref[i] == 2){
			b++;
		}
		if(pref[i] != 0){
			count++;
		}
		one[i] = a;
		two[i] = b;
	}
	int mini = inf;
	for(int i=0;i<q;i++){
		for(int j=i+1;j<q;j++){
			vector<pair<int,int> > v;
			v.push_back(arr[i]);
			v.push_back(arr[j]);
			sort(v.begin(),v.end());
			if(v[0].second >= v[1].first){
				int a = v[0].first;
				int b = v[1].first - 1;
				int t = 0;
				if(b >= a){
					t += one[b] - one[a-1];
				}
				a = v[1].first;
				b = min(v[0].second,v[1].second);
				if(b >= a){
					t += two[b] - two[a-1];
				}
				a = b + 1;
				b = max(v[0].second,v[1].second);
				if(b >= a){
					t += one[b] - one[a-1];
				}
				if(t == 0){
					cout << i << " " << j << endl;
					cout<<count;
					#ifndef ONLINE_JUDGE
					clk = clock() - clk;
					cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
					#endif
					return 0;
				}
				mini = min(t,mini);
			}
			else{
				int a = arr[i].first;
				int b = arr[i].second;
				int c = arr[j].first;
				int d = arr[j].second;
				int t = one[b] - one[a-1];
				t += (one[d] - one[c-1]);
				if(t == 0){
					cout << i << " " << j << endl;
					cout<<count;
					//return 0;
					#ifndef ONLINE_JUDGE
					clk = clock() - clk;
					cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
					#endif
					return 0;
				}
				mini = min(t,mini);
			}
			
		}
	}
	cout<<count-mini;

	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
		
	
	return 0;
}