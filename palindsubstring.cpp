// This code is adopted from following link
// http://www.leetcode.com/2011/11/longest-palindromic-substring-part-i.html
 
#include <stdio.h>
#include <string.h> 

int longestPalSubstr( char *str )
{
    int n = strlen( str );

    int table[n][n];
    memset(table, 0, sizeof(table));
    int maxLength = 1;
    for (int i = 0; i < n; ++i)
        table[i][i] = 1;
    int start = 0;
    for (int i = 0; i < n-1; ++i)
    {
        if (str[i] == str[i+1])
        {
            table[i][i+1] = 1;
            start = i;
            maxLength = 2;
        }
    }
 

    for (int k = 3; k <= n; ++k)
    {
        for (int i = 0; i < n-k+1 ; ++i)
        {
            int j = i + k - 1;

            if (table[i+1][j-1] && str[i] == str[j])
            {
                table[i][j] = 1;
 
                if (k > maxLength)
                {
                    start = i;
                    maxLength = k;
                }
            }
        }
    }

 
    return maxLength; 
}

int notprime[100001];

void seive(){
    notprime[1] = 1;
    for (int i = 2; i*i < 100001; ++i)
     {
        if(!notprime[i])
        {
            for (int j = 2*i; j < 100001; j+=i)
            {
                notprime[j] = 1;
            }
        }
    } 
}
 
int main()
{
    seive();
    int t;
    scanf("%d",&t);
    while(t--){
        char str[100000];
        scanf("%s", str);
        if(notprime[(longestPalSubstr( str ))])
            printf("%s", "NOT ");
        printf("%s", "PRIME");
        printf("\n");

    }
    
    return 0;
}