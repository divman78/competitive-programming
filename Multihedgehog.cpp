#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","r",stdin);
	int n, k;
	cin >> n >> k;
	vector<set<int> > adj(n);
	for(int i = 0; i < n-1; i++){
		int x, y;
		cin >> x >> y;
		adj[x-1].insert(y-1);
		adj[y-1].insert(x-1);
	}

	// for(int i = 0; i < n; i++){
	// 	cout << i + 1<< "- > ";
	// 	for(set<int>::iterator itr = adj[i].begin(); itr != adj[i].end(); itr++){
	// 		cout << (*itr) + 1 << " ";
	// 	}
	// 	cout << "\n";
	// }

	queue<int> q;
	int count[n];
	int level[n];
	int visited[n];
	memset(count, 0, sizeof(count));
	memset(level, 0, sizeof(level));
	memset(visited, 0, sizeof(visited));

	vector<int> vec;

	for(int i = 0; i < n; i++){
		if(adj[i].size()==1){
			visited[i] = 1;
			vec.push_back(i);
		}
	}

	for(auto i : vec){
		if(adj[i].size()==1){
			int temp = *(adj[i].begin());
			adj[i].erase(temp);
			adj[temp].erase(i);
			if(!visited[temp]){
				q.push(temp);
				visited[temp] = 1;
				level[temp] = 1;
			}
			count[temp]++;
		}
	}

	// for(int i = 0; i < n; i++){
	// 	cout << i + 1<< "- > ";
	// 	for(set<int>::iterator itr = adj[i].begin(); itr != adj[i].end(); itr++){
	// 		cout << (*itr) + 1 << " ";
	// 	}
	// 	cout << "\n";
	// }

	bool valid = true;

	while(!q.empty()){

		int temp = q.front();
		q.pop();
		//cout << temp+1 << "\n";
		if(adj[temp].size()!=1){
			if(valid) valid = false;
			else cout << "NO\n", exit(0);
		}
		if(count[temp]<3){
			//cout << adj[temp].size() << " " << count[temp] << " ";
			cout << "NO\n"; exit(0);
		}
		int temp1 = *(adj[temp].begin());
		adj[temp].erase(temp1);
		adj[temp1].erase(temp);
		if(!visited[temp1]){
			q.push(temp1);
			visited[temp1] = 1;
			level[temp1] = level[temp]+1;
		}
		count[temp1]++;
	}

	int mx = INT_MIN;
	for(int i = 0; i < n; i++){
		mx = max(mx, level[i]);
		//cout << level[i] << " ";
	}
	int cnt = 0;
	for(int i = 0; i < n; i++){
		if(level[i]==mx) cnt++;
	}
	if(mx != k || cnt!=1){
		cout << "NO\n";
	}
	else{
		cout << "YES\n";
	}
}