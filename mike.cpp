#include<bits/stdc++.h>
using namespace std;

int main(){
	freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		double ru , rq;
		ru = 1400.0;
		cin >> rq;
		cout << fixed << setprecision(0);
		cout << rq << " -> ";
		cout << fixed << setprecision(5);
		cout << (1.0/(1.0+ pow(10.0, (rq-ru)/400))) << endl;

	}
}
//output
// 1400 -> 0.50000
// 1600 -> 0.24025
// 1800 -> 0.09091
// 2000 -> 0.03065
// 2200 -> 0.00990
// 2500 -> 0.00178
// 2800 -> 0.00032
// 3200 -> 0.00003

//input
// 8
// 1400 
// 1600
// 1800
// 2000
// 2200
// 2500
// 2800
// 3200