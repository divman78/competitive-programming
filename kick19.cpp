#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000
#define printans(t, res) cout << "Case #" << t << ": " << res << endl; 
#define R 256

void preprocess(){
	
}

int r, c;
string arr[R];
int vis[R][R];
int filli[R][R];

signed main(){
	FAST_IO;
	// #ifndef ONLINE_JUDGE
	 freopen("/home/divakar/programs/input.txt","r",stdin);
	// //freopen("/home/divakar/programs/output.txt","w",stdout);
	// #endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	cin >> t; 
	int tt = t;
	while(t--){

		cin >> r >> c;

		pair<int,int> p;
		queue<pair<int,int> > q;
		int mx = 0;
		int fi = 1;


		for(int i = 1; i <= r; i++){
			cin >> arr[i];
			arr[i] = " " + arr[i];
		}
		for (int i = 1; i <= r; ++i)
		{
			for (int j = 1; j <= c; ++j)
			{
				vis[i][j] = 0;
			}
		}

		for (int i = 1; i <= r; ++i)
		{
			for (int j = 1; j <= c; ++j)
			{
				filli[i][j] = INT_MAX;
			}
		}

		int cnt = 0;

		for (int i = 1; i <= r; ++i)
		{
			for (int j = 1; j <= c; ++j)
			{
				if(arr[i][j] == '1'){
					cnt++;
				}
			}
		}


		if(cnt == r*c){
			printans(tt-t,0);
			continue;
		}

		int ans = INT_MAX;

		for(int rows = 1; rows <= r; rows++){
			for(int cols = 1; cols <= c; cols++){
				if(arr[rows][cols] == '0'){

					arr[rows][cols] = '1';

					for (int i = 1; i <= r; ++i)
					{
						for (int j = 1; j <= c; ++j)
						{
							vis[i][j] = 0;
						}
					}

					for (int i = 1; i <= r; ++i)
					{
						for (int j = 1; j <= c; ++j)
						{
							filli[i][j] = INT_MAX;
						}
					}

					for (int i = 1; i <= r; ++i)
					{
						for (int j = 1; j <= c; ++j)
						{
							if(arr[i][j] == '1'){
								q.push(mp(i,j));
								vis[i][j] = 1;
								filli[i][j] = 0;
							}
						}
					}

					fi = 1;

					while(!q.empty()){
						queue<pair<int,int> > qq;

						while(!q.empty()){

							p = q.front();
							q.pop();

							if(p.first-1 >= 1 && !vis[p.first-1][p.second]){
								qq.push(mp(p.first-1,p.second));
								filli[p.first-1][p.second] = fi;
								vis[p.first-1][p.second] = 1;
							}
							if(p.first+1 <= r && !vis[p.first+1][p.second]){
								qq.push(mp(p.first+1,p.second));
								filli[p.first+1][p.second] = fi;
								vis[p.first+1][p.second] = 1;
							}
							if(p.second-1 >= 1 && !vis[p.first][p.second-1]){
								qq.push(mp(p.first,p.second-1));
								filli[p.first][p.second-1] = fi;
								vis[p.first][p.second-1] = 1;
							}
							if(p.second+1 <= c && !vis[p.first][p.second+1]){
								qq.push(mp(p.first,p.second+1));
								filli[p.first][p.second+1] = fi;
								vis[p.first][p.second+1] = 1;
							}

						}
						fi++;

						q = qq;
					}

					mx = 0;

					for (int i = 1; i <= r; ++i)
					{
						for (int j = 1; j <= c; ++j)
						{
							//cout << filli[i][j] << " ";
							if(filli[i][j] > mx){
								mx = filli[i][j];
								p = mp(i,j);
							}
						}
						//cout << "\n";
					}

					ans = min(mx, ans);

					arr[rows][cols] = '0';

					//cout << mx << endl;
				}


			}
		}

		






		printans(tt-t,ans);


        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	// #ifndef ONLINE_JUDGE
	// clk = clock() - clk;
	// cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	// #endif
	return 0;
}




















