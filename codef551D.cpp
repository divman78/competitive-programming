#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000


int n;
vector<int> adj[N];
int ismax[N];
int dp[N];
int leaves[N];

int dfs(int u=0){
	if(adj[u].size()==0){
		dp[u]=1;
		leaves[u] = 1;
		return dp[u];
	}
	for(auto v: adj[u]){
		dfs(v);
		leaves[u] += leaves[v];
	}
	if(ismax[u]){
		for (auto v: adj[u]){
			dp[u] = max(dp[u], leaves[u]-leaves[v]+dp[v]);
		}
	}
	else{
		int x = 0;
		for(auto v: adj[u]){
			x += (dp[v]-1);
		}
		dp[u]=x+1;
	}
	return dp[u];
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	//freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();


	int t=1;
	//cout << " dsf ";
	//cin >> t; 
	while(t--){

		cin >> n;
		for (int i = 0; i < n; ++i)
		{
			cin >> ismax[i];
		}

		for (int i = 1; i < n; ++i)
		{
			int x;
			cin >> x;
			adj[x-1].push_back(i);
		}

		cout << dfs() << endl;



		


        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















