#include <bits/stdc++.h>
using namespace std;
#define MAXN 100000
#define rint register int
inline int rf(){int r;int s=0,c;for(;!isdigit(c=getchar());s=c);for(r=c^48;isdigit(c=getchar());(r*=10)+=c^48);return s^45?r:-r;}
int n, L, A[MAXN+5];
int main()
{
	n = rf();
	L = (int)sqrt(n);
	for(rint i = 1, o = n, j; i <= n; i += L)
		for(j = min(i+L-1,n); j >= i; A[j--] = o--);
	for(rint i = 1; i <= n; i++)
		printf("%d%c",A[i],"\n "[i<n]);
	return 0;
}