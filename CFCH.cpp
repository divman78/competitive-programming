#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define MX 100000000000000000

int n, k;

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif

	int t=1;
	cin >> t; 
	while(t--){

		cin >> n >> k;

		int arr[n];

		int mx = INT_MIN;
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
			mx = max(arr[i], mx);
		}

		vector<int> vec[mx+1];

		for (int i = 0; i < n; ++i)
		{
			vec[arr[i]].push_back(i+1);
		}

		int ans = MX;

		for (int i = 1; i <= mx; ++i)
		{
			if(vec[i].size() >=  k){

				int pre[vec[i].size()+1];
				pre[0] = vec[i][0];
				for (int j = 1; j < vec[i].size(); ++j)
				{
					pre[j] = pre[j-1] + vec[i][j];
				}

				int sumr, suml;
				int l = 0, r = k-1;
				for (int j = 0; j < vec[i].size(); ++j)
				{	

					while(r+1 <= vec[i].size()-1)
					{
						if(abs(vec[i][j] - vec[i][r+1]) < (vec[i][j] -vec[i][l]))
						{
							l++;
							r++;
						}
						else
						{
							break;
						}
					}

					int cntl = j-l;
					int cntr = r-j;

					if(l-1>=0)suml = pre[j]-pre[l-1];
					else suml = pre[j];
					if(j>=0)sumr = pre[r]-pre[j];
					else sumr = pre[r];
					suml -= vec[i][j];
 
					int temp = (abs(sumr-cntr*vec[i][j]) - (((cntr)*(cntr+1))/2)) + (abs(suml-cntl*vec[i][j]) - (((cntl)*(cntl+1))/2)) ;
					ans = min(temp, ans);
				}
			}
		}

		if(ans == MX){
			cout << -1 << endl;
		}


		else cout << ans << endl;
	}

	return 0;
}