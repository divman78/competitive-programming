#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define MO 1000000007
#define N 100011

void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
int n1, n2;
string s1, s2;
int power10[2*N];
int fx[N][10];
int pref[N];
int t;


void preprocess(){
	power10[0] = 1;
	for (int i = 1; i < 2*N; ++i)
	{
		power10[i] = (power10[i-1]*10)%MO;
	}


	for (int i = 0; i < 10; ++i){
		fx[0][i] = i;
		if(i>0)fx[0][i] += fx[0][i-1];
	}

	for (int i = 1; i < N; ++i){
		int temp = (-power10[2*i-2]+power10[2*i]+MO)%MO;
		for (int j = 0; j < 10; ++j)
		{
			fx[i][j] = (fx[i-1][9]+j*temp)%MO;
			if(j>0) fx[i][j] = (fx[i][j] + fx[i][j-1])%MO; 
		}
	}
}

int strToInt(string& s){
	int ret = 0;
	for (int i = 0; i < s.length(); ++i)
	{
		ret = (ret + (s[i]-48)*power10[i])%MO;
	}
	return ret;
}



int naive(string& s){
	int ret = 0;
	int num = strToInt(s);
	while(num){
		int num2 = num;
		int cnt = 0;
		while(num2){
			int dig = num2%10;
			num2/=10;
			if(num2==0 || num2%10!=dig){
				ret = (ret+dig*power10[cnt])%MO;
			}
			cnt++;
		}
		num--;
	}
	return ret;
}

int pre(string& s, int i){
	int dig = s[i]-48;
	int ret = 0, more = 0;
	if(dig>0) ret = fx[i][dig-1];
	if(i>0){
		more = pre(s,i-1);
		ret = (ret + more)%MO;
		ret = (ret + ((pref[i-1]*power10[i])%MO)*dig)%MO;
		if(s[i]==s[i-1])
			if(i!=1)ret = (((ret-((pref[i-2]*power10[i-1])%MO)*dig)%MO)+MO)%MO;
		    else ret = (ret-dig+MO)%MO;
		else if(s[i]<s[i-1])
			ret = (((ret-((power10[i-1]*power10[i-1])%MO)*dig)%MO)+MO)%MO;
	}
	else{
		ret = (dig+ret)%MO;
	}
	return ret;
}

int calc(string& s){
	int num = 0;
	for (int i = 0; i < s.length(); ++i)
	{
		num = (num + (s[i]-48)*power10[i])%MO;
		pref[i] = (num+1)%MO;
	}
	return pre(s, s.length()-1);
}

void solve(){
	cin >> n1 >> s1;
	cin >> n2 >> s2;
	reverse(s1.begin(), s1.end());
	reverse(s2.begin(), s2.end());
	for (int i = 0; i < n1; ++i)
	{
		if(s1[i]=='0') s1[i] = '9';
		else {
			s1[i]--; 
			break;
		}
	}
	cout << (calc(s2)-calc(s1)+MO)%MO << endl;
}

signed main(){
	clock_t clk = clock();
	read_file();
	preprocess();

	cin >> t;
	while(t--){
		solve();
	}


	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}