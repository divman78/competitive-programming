#include<bits/stdc++.h>
using namespace std;
#define MO 1000000007

int main(){
	
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		
		int a[n];
		
		for(int i=0; i<n; i++)
		cin>>a[i];
		
		map <int, int,greater<int> > m;
		
		for(int i=0; i<n; i++)
		 m[a[i]]++;
		 
		map<int, int> :: iterator itr;
		
		
		int carry = 0; 
		long long int total = 1;
		
			for(itr = m.begin(); itr!=m.end(); itr++)
			{
				int temp = itr->second;     // frequecey of a number
				
				if(temp == 1)
				{
					carry = (carry+1)%2;
					continue;
				}
				
				if(carry)
				total = total*temp;
				
				
				int comb = ((temp-carry)*(temp-1-carry))/2;
				if(comb != 0){
					
					total = total*comb;
					cout<<total<<endl;
				}
				
				
				carry = (carry+temp)%2;
				
			}
		
		cout<<total<<endl;		
	}
	
	return 0;
}