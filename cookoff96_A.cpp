#include<bits/stdc++.h>
using namespace std;

int arr[1042][1042];
int ans[1042][1042];
int row[1042];
int col[1042];

main(){
	//freopen("input.txt", "r", stdin);
	int t;
	cin >> t;
	while(t--){
		int n, m;
		cin >> n >> m;
		for (int i = 0; i < n; ++i)
		{
			row[i] = 0;
			for (int j = 0; j < m; ++j)
			{
				col[j] = 0;
				ans[i][j] = -1;
				
			}
		}
		std::queue<pair<int,int> > v;
		for(int i = 0; i  < n; i++){
			string s;
			cin >> s;
			for(int j = 0 ; j < m ; j++){
				arr[i][j] = s[j] - '0';
				if(arr[i][j]){
					v.push(make_pair(i,j));
					ans[i][j] = 0;
				}
			}
		}

		while(!v.empty()){
			pair<int, int> temp = v.front();
			v.pop();
			//cout << row[temp.first] << col[temp.first] << endl;
			if(!row[temp.first]){
				for(int i = 0;  i< m; i++){
					if(arr[temp.first][i] == 0){
						arr[temp.first][i] = 1;
						ans[temp.first][i] = ans[temp.first][temp.second] + 1;
						v.push(make_pair(temp.first,i));
					}
				}
				row[temp.first] = 1;
			}
			if(!col[temp.second]){
				for(int i = 0;  i< n; i++){
					if(arr[i][temp.second] == 0){
						arr[i][temp.second] = 1;
						ans[i][temp.second] = ans[temp.first][temp.second] + 1;
						v.push(make_pair(i,temp.second));
					}
				}
				col[temp.second] = 1;
			}

// cout << temp.first << " "<<temp.second << endl;
// 			for (int i = 0; i < n; ++i)
// 		{
// 			for (int j = 0; j < m; ++j)
// 			{
// 				cout << ans[i][j] << " ";
// 			}
// 			cout << endl;
// 		}

		}
		
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < m; ++j)
			{
				cout << ans[i][j] << " ";
			}
			cout << endl;
		}

	}

}