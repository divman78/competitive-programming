#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	//cin >> t; 
	while(t--){

		int n, k;
		cin >> n >> k;
		int arr[n+1];
		arr[0] = 0;
		int total = 0;
		for (int i = 1; i <= n; ++i)
		{
			cin >> arr[i];
			total += arr[i];
		}
		int pref[n+1] = {0};
		multiset<pair<int,int> > ss;
		for (int i = 1; i <= n; ++i)
		{
			pref[i] = arr[i]+pref[i-1];
			if(i<=n-k+1)ss.insert(mp(pref[i], i));
		}

		int minus = 0;
		int curr = 0;
		int ans = 0;

		int cnt = 1;

		while(cnt <= k){
			//cout << cnt << "->"<< endl;
			pair<int,int> p = *ss.begin();
			int sum = 0;
			int cl = p.second;
			//cout << cl << endl;
			for(int j = cl; j >= curr; j--){
				ss.erase(mp(pref[j], j));
			}
			curr = cl+1;
			sum = p.first - minus;
			if(cnt == k) ans += k*(total-minus);
			else ans += cnt*sum;
			minus += sum;
			//cout << k*(total-minus) << " " << minus << endl;
			ss.insert(mp(pref[n-k+cnt+1], n-k+cnt+1));
			cnt++;

		}

		cout << ans << endl;








        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















