#include<bits/stdc++.h>
using namespace std;


const int N = int(1e6) + 1;
int big[N];
int cnt[N];
int col[N];
int sz[N];
vector<int> adj[N];
int n, maxx;

void szcalc(int v, int par = -1){
	sz[v]++;
	for(auto u: adj[v]){
		if(u!=p)
			szcalc(u,v), sz[v]+=sz[u];
	}
}

void add(int v, int p, int x){
	cnt[col[v]] += x;
	if(cnt[col[v]] > cnt[maxx]){
		maxx = col[v];
	}
	for (auto u: adj[v]){
		if(u!=p && !big[u]){
			add(u,v,x);
		}
	}
}

int res[N];

void dfs(int v, int par = -1, int keep = -1){
	int mx = -1, biggest = -1;
	for (auto u: adj[v]){
		if(u!=p && mx < sz[u]){
			biggest = u;
			mx = sz[u];
		}
	}
	for (auto u: adj[v])  
		if(u!=p && u!=biggest) dfs(u,v,0);

	maxx = 0;

	if(biggest != -1) dfs(biggest, v, 1), big[biggest] = 1;
	
	add(v,p,1);
	res[v] = maxx;
	if(biggest!=-1) biggest = -1;
	add(v,p,-1);
}

int main(){
	cin >> n;
	for (int i = 0; i < n; ++i){
		cin >> col[i];
	}
	for (int i = 0; i < n - 1; ++i){
		int x, y;
		cin >> x >> y;
		x--, y--;
		adj[x].push_back(x);
		adj[y].push_back(x);
	}
	szcalc(0);
	dfs(0);

	for (int i = 0; i < n; ++i)
	{
		cout << res[i] << " ";
	}
	cout << endl;

}