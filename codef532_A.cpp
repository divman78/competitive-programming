#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 300005

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	double n, r;
	cin >> n >> r;
	double PI = 22.0/7.0;

	double c = 1.0/cos((PI/2.0)-((2.0*PI)/(2.0*n)));

	double ans = r/(c-1.0);
	cout << ans  << endl;


	return 0;
}