#include<bits/stdc++.h>
using namespace std;


const int N = int(1e6) + 1;
long long big[N];
long long cnt[N];
long long col[N];
long long sz[N];
vector<long long> adj[N];
long long n, maxx;
long long sum;

void szcalc(long long v, long long p = -1){
	sz[v]++;
	//cout << v << " => ";
	for(auto u: adj[v]){
		//cout << u <<endl;
		if(u!=p)
			szcalc(u,v), sz[v]+=sz[u];
	}
}

void add(long long v, long long p, long long x){
	cnt[col[v]] += x;

	if(x>0){
		if(cnt[col[v]] > maxx){
			maxx = cnt[col[v]];
			sum = col[v];
		}
		else if(cnt[col[v]] == maxx){
			sum += col[v];
		}
	}
	for (auto u: adj[v])
		if(u!=p && !big[u])
			add(u,v,x);
}

long long res[N];

void dfs(long long v, long long p = -1, long long keep = -1){
	long long mx = -1, biggest = -1;
	// for (auto u: adj[v]){
	// 	if(u!=p && mx < sz[u]){
	// 		biggest = u;
	// 		mx = sz[u];
	// 	}
	// }
	for (auto u: adj[v])  
		if(u!=p && u!=biggest) dfs(u,v,0);

	if(biggest != -1) 
		dfs(biggest, v, 1), big[biggest] = 1;
	
	add(v,p,1);
	res[v] = sum;
	if(biggest != -1) 
		big[biggest] = 0;
	if(keep == 0) 
		add(v,p,-1), maxx = 0, sum = 0;
}

int main(){
	cin >> n;
	for (long long i = 0; i < n; ++i){
		cin >> col[i];
	}
	for (long long i = 0; i < n - 1; ++i){
		long long x, y;
		cin >> x >> y;
		x--, y--;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	szcalc(0);
	dfs(0);

	for (long long i = 0; i < n; ++i)
	{
		cout << res[i] << " ";
	}
	cout << endl;
}