//Hashs struct taken from user "ashishgup" codeforces modules

#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000
#define P1 31
#define MOD1 1000000007

void preprocess(){
	
}

struct Hashs 
{
	vector<int> hashs;
	vector<int> pows;
	int P;
	int MOD;

	Hashs() {}

	Hashs(string &s, int P, int MOD) : P(P), MOD(MOD) 
	{
		int n = s.length();
		pows.resize(n+1, 0);
		hashs.resize(n+1, 0);
		pows[0] = 1;
		for(int i=n-1;i>=0;i--) 
		{
			hashs[i]=(1LL * hashs[i+1] * P + s[i] - 'a' + 1) % MOD;
			pows[n-i]=(1LL * pows[n-i-1] * P) % MOD;
		}
		pows[n] = (1LL * pows[n-1] * P)%MOD;
	}
	int get_hash(int l, int r) 
	{
		int ans=hashs[l] + MOD - (1LL*hashs[r+1]*pows[r-l+1])%MOD;
		ans%=MOD;
		return ans;
	}
};

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","r",stdin);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	cin >> t; 
	while(t--){

		int n;
		cin >> n;
		string s;
		cin >> s;
		Hashs h1 = Hashs(s, P1, MOD1);
		int cnt[n+1];
		memset(cnt, 0, sizeof(cnt));
		for (int i = 1; i <= n; ++i)
		{
			int x = h1.get_hash(0, i-1);
			//cout << i << "  " << x << " -> ";
			for (int j = 1; j < n; j++)
			{
				if(j+i-1 >= n){
					break;
				}

				//cout << h1.get_hash(j, j+i-1) << " ";
				if(h1.get_hash(j, j+i-1) == x){
					cnt[i]++;
				}
			}	
			//cout << endl; 
		}

		int mx = INT_MIN;
		int ind = -1;
		for (int i = 1; i <= n; ++i)
		{
			if(mx < cnt[i]){
				mx = cnt[i];
			}

			//cout << cnt[i] << " ";
		}

		for (int i = n; i >= 1; --i)
		{
			if(mx == cnt[i]){
				for(int j = 0; j < i; j++){
					cout << s[j];
				}
				cout <<  endl;
				break;
			}

		}
		
	}

	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif




	return 0;
}




