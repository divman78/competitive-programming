#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	//read_file();
}

signed main(){
	init();

	//TEST_CASES
	{

		int n;
		cin >> n;
		int  arr[n];

		int gcd = 1;
		cin >> arr[0];
		std::map<int, int> mm;
		mm[arr[0]]++;
		for (int i = 1; i < n; ++i)
		{
			mm[arr[i]]++;
			cin >> arr[i];
		}
		sort(arr, arr+n);
		reverse(arr, arr+n);
		if(mm.size()==1){
			cout << 0 << " " << 0 << endl;
			exit(0);
		}

		int arr2[n];
		gcd = arr[0]-arr[1];
		int ans = 0;
		for (int i = 2; i < n; ++i)
		{
			gcd = __gcd(arr[0]-arr[i],gcd);
		}

		for (int i = 0; i < n; ++i)
		{
			ans += (arr[0]-arr[i])/gcd;
		}


		
        cout << ans <<  " " << gcd << endl;


		


	}


	




	return 0;
}