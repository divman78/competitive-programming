#include<bits/stdc++.h>
using namespace std;

int main(){
	int n;
	cin>> n;
	vector<int> vec[n+1];
	for (int i = 0; i < n - 1; ++i)
	{
		int o, m;
		cin >> o >> m;
		vec[o].push_back(m);
		vec[m].push_back(o);
	}

	int arr[n];
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}

	set<int> ss[n+1];
	queue<int> 	qq;
	if(arr[0] != 1){
		cout << "no", exit(0);
	}

	int curr = arr[0];
	int par[n+1];
	for (int i = 0; i < vec[curr].size(); ++i)
	{

		ss[curr].insert(vec[curr][i]);
		par[vec[curr][i]] = curr;
	}

	for (int i = 1; i < n; ++i)
	{
		if(ss[curr].empty()){
			//cout <<"sdfs" << i << " ";
			curr = qq.front();
			qq.pop();
		}
		if(ss[curr].find(arr[i]) == ss[curr].end()){
			cout << "no";
			//cout << i << " " << curr << endl ;
			exit(0);
		}
		ss[curr].erase(arr[i]);
		
		for (int j = 0; j < vec[arr[i]].size(); ++j)
		{
			if(vec[arr[i]][j] != par[arr[i]]){
				ss[arr[i]].insert(vec[arr[i]][j]);
				par[vec[arr[i]][j]] = arr[i];
			}
		}
	}

	cout << "yes";
}