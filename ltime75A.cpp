#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define N 1000001

#define int long long
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)



void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}


signed main(){
	init();

	int t = 1;
	cin >> t;

	while(t--){
		int n, k;
		cin >> n >> k;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}

		map<int,int> mm;

		for (int i = 0; i < n; ++i)
		{
			mm[arr[i]]++;
		}

		int flag = true;

		for (int i = 0; i < n; ++i)
		{
			if(mm[arr[i]] < n/k){
				flag = false;
			}
		}
		if(flag == false){
			cout << "NO\n";
			continue;
		}
		pair<int,int> pr[mm.size()];
		int x = 0;
		iterm(mm)
		{
			pr[x++]=make_pair(itr->second, itr->first);
		}

		sort(pr, pr+mm.size());
	    reverse(pr, pr+mm.size());
		x = 0;

		for (int i = 0; i < n; ++i)
		{
			if(pr[x].first == 0){
				flag = false;
				break;
			}
			arr[i] = pr[x].second;
			pr[x].first--;
			x++;
			if(x == mm.size()) x = 0;
		}

		if(flag==false){
			cout << "NO\n";
			continue;
		}

   		int sum = 0;
		for (int i = 0; i < k; ++i)
		{
			sum += arr[i];
		}

		for (int i = k; i < n; ++i)
		{
			int su = sum-arr[i-k]+arr[i];
			if(sum != su){
				flag = false;
				break;
			}
			sum = su;
		}
		if(flag==false){
			cout << "NO\n";
			continue;
		}



		cout << "YES\n";
		for (int i = 0; i < n; ++i)
		{
			cout << arr[i] << " ";
		}
		cout << endl;

	}


	




	return 0;
}