#include<bits/stdc++.h>
using namespace std;
#define MO 1000000007

int main(){
	
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		
		int a[n];
		
		for(int i=0; i<n; i++)
		cin>>a[i];
		
		map <int, int,greater<int> > m;
		
		for(int i=0; i<n; i++)
		 m[a[i]]++;
		 
		map<int, int> :: iterator itr;
		
		
		int carry = 0; 
		long long int total = 1;
		
			for(itr = m.begin(); itr!=m.end(); itr++)
			{
				int temp = itr->second;     // frequecey of a number
				
				if(temp == 1)
				{
					carry = (((carry+1)%MO)%2)%MO;
					continue;
				}
				
				if(carry)
				total = (((total)%MO)*(temp%MO))%MO;
				
				
				int comb = ((((((temp-carry+MO)%MO)*(((temp-1-carry+MO))%MO)))%MO)*500000004)%MO;
				if(comb != 0){
					
					total = (((total)%MO)*((comb)%MO))%MO;
					//cout<<total<<endl;
				}
				
				
				carry = (((carry%MO)+(temp%MO))%MO)%2;
				
			}
		
		cout<<total<<endl;		
	}
	
	return 0;
}