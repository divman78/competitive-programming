/*input
3
3 3
101
000
101
1 2
11
5 5
10001
00000
00000
00000
10001
*/
#include<bits/stdc++.h>
using namespace std;
const int N=251;
const int mod=1e9 + 7;
#define int long long
const int inf=1e18;
#define pii pair<int, int>
#define f first
#define s second 
#define mp make_pair
int grid[N][N], r, c, t;
queue< pii > q;
vector< pii > els;int temp[N][N], start[N][N], fin[N][N];
void check(int i, int j, int dist)
{
    if(i>r||i<1||j>c||j<1) return;
    if(grid[i][j]<=dist) return;
    grid[i][j]=dist;q.push(mp(i, j));
}
bool p(int x)
{
    els.clear();
    for(int i=1;i<=r;i++)
    {
        for(int j=1;j<=c;j++)
        {
            if(grid[i][j]>x) els.push_back(mp(i, j));
            start[i][j]=fin[i][j]=temp[i][j]=0;
        }
    }
    int req=els.size();
    for(auto cur:els)
    {

        if(x == 1) cout << cur.f << "-" <<cur.s << endl;
        for(int i=max(cur.f - x, (int)1);i<=min(cur.f + x, r);i++)
        {
            int rem=x - abs(cur.f - i);
            int j=max(cur.s - rem, (int)1);start[i][j]++;
            j=min(cur.s + rem, c);fin[i][j]++;
        }

    }
    cout << "\n\n\n\n\nx -> " << x << " "<< "req -> " << req << endl;
    for (int i = 1; i <= r ; ++i)
    {
        for (int j = 1; j <= c; ++j)
        {
            cout << start[i][j] << " ";
        }
        cout << "\t";

        for (int j = 1; j <= c; ++j)
        {
            cout << fin[i][j] << " ";
        }

        cout << endl;
    }

    cout << "\n\n\n";
    for(int i=1;i<=r;i++)
    {
        int cur=0;
        for(int j=1;j<=c;j++)
        {
            cur+=start[i][j];temp[i][j]=cur;
            cur-=fin[i][j];
            cout << temp[i][j] << " ";
        }
        cout << endl;
    }

    for(int i=1;i<=r;i++)
    {
        for(int j=1;j<=c;j++)
        {
          if(temp[i][j]==req) return true;
        }
    }
    return false;
}

signed main()
{
    ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
    cin>>t;
    for(int k=1;k<=t;k++)
    {
        cin>>r>>c;
        for(int i=0;i<=r+1;i++)
        {
            grid[i][c+1]=grid[i][0]=0;
        }
        for(int i=0;i<=c+1;i++)
        {
            grid[r+1][i]=grid[0][i]=0;
        }
        for(int i=1;i<=r;i++)
        {
            string st;cin>>st;
            for(int j=1;j<=c;j++)
            {
                grid[i][j]=inf;
                if(st[j-1]=='1') {grid[i][j]=0; q.push(mp(i, j));}
            }
            
        }
        while(!q.empty()){
            pii cur=q.front();q.pop();
            check(cur.f + 1, cur.s, grid[cur.f][cur.s] + 1);
            check(cur.f, cur.s + 1, grid[cur.f][cur.s] + 1);
            check(cur.f - 1, cur.s, grid[cur.f][cur.s] + 1);
            check(cur.f, cur.s - 1, grid[cur.f][cur.s] + 1);
        }
        int low=0, high=510;
        while(low<high)
        {
            int mid=(low + high)/2;
            if(p(mid)) high=mid;
            else low=mid+1;
        }

        cout << "\n\ngrid array : ";

        for (int i = 1; i <= r ; ++i)
        {
            for (int j = 1; j <= c; ++j)
            {
                cout << grid[i][j] << " ";
            }
            cout << endl;
        }
        cout<<"Case #"<<k<<": "<<low<<endl;
    }
}