#include<bits/stdc++.h>
#define int long long
#define MAXN 100005
#define SQRSIZE  320
using namespace std;

int arr[MAXN];
int brr[MAXN];                
int block[SQRSIZE];   
int lazy[SQRSIZE]; 
int blk_sz;
int n;
int ans = 0;

map<int,int> mpB[SQRSIZE];
 
void update(int l, int r,int c) 
{ 
    while (l<r and l%blk_sz!=0 and l!=0) 
    {  
    	if(lazy[l/blk_sz] != -1){
    		arr[l] = lazy[l/blk_sz];
    	}
    	if(arr[l] == brr[l] && arr[l] != c){
    		block[l/blk_sz]--;
    		ans--;
    	}
    	else if(arr[l] != brr[l] && brr[l] == c){
    		block[l/blk_sz]++;
    		ans++;
    	}
        arr[l] = c; 
        l++; 
    } 
    while (l+blk_sz <= r) 
    {
    	ans -= block[l/blk_sz];
    	block[l/blk_sz] = mpB[l/blk_sz][c]; 
    	ans += block[l/blk_sz];
        lazy[l/blk_sz] = c; 
        l += blk_sz; 
    } 
    while (l<=r) 
    { 
    	if(lazy[l/blk_sz] != -1){
    		arr[l] = lazy[l/blk_sz];
    	}
    	if(arr[l] == brr[l] && arr[l] != c){
    		block[l/blk_sz]--;
    		ans--;
    	}
    	else if(arr[l] != brr[l] && brr[l] == c){
    		block[l/blk_sz]++;
    		ans++;
    	}
        arr[l] = c; 
        l++; 
    } 
}

void preprocess(){
	int blk_idx = -1; 
    blk_sz = sqrt(n); 
    //cout<<"blk_sz "<<blk_sz<<endl;
    for (int i=0; i<n; i++) 
    { 
    	if (i%blk_sz == 0) 
        { 
            blk_idx++; 
        } 
        if(arr[i] == brr[i]){
        	block[blk_idx]++;
        	ans++;
        } 
        mpB[blk_idx][brr[i]]++; 
    }
}

signed main(){
	freopen("input.txt","r",stdin);
	int t;
	cin>>t;
	while(t--){
		ans = 0;
		for(int i=0;i<SQRSIZE;i++){
			lazy[i] = -1;
			block[i] = 0;
			mpB[i].clear();
		}	
		int q;
		cin>>n>>q;
		for(int i=0;i<n;i++){
			cin>>arr[i];
		}
		for(int i=0;i<n;i++){
			cin>>brr[i];
		}
		preprocess();
		while(q--){
			int l,r,c;
			cin>>l>>r>>c;
			l = l ^ ans; 
			r = r ^ ans;
			c = c ^ ans;
			l--;
			r--;
	//		cout<<"l = "<<l<<" r = "<<r<<" c "<<c<<endl;
			update(l,r,c);
			cout<<ans<<endl;
		}


	}
	return 0;
}