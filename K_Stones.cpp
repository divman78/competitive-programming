#include<bits/stdc++.h>
using namespace std;
#define N 100
#define K 100005

int n, k;
int arr[N];

int dp[K];

int calc(int ki){
	//cout << ki << " ";
	if(dp[ki]!=-1){
		return dp[ki];
	}
	dp[ki] = 0;
	for (int i = 0; i < n; ++i){
		if(dp[ki]==0){
			if(ki-arr[i]>=0) dp[ki] = 1-calc(ki-arr[i]);
		}
		else{
			return dp[ki];
		}
	}
	return dp[ki];
}

int main(){
	freopen("input.txt","r",stdin);
	cin >> n >> k;	

	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}

	memset(dp, -1, sizeof(dp));


	string ans = calc(k) ? "First":"Second";
	cout << ans << endl; 


}