// In the name of God.
// We're nothing and you're everything.
// Ya Ali!
 

//arpa's solution is wrong

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int maxn = 1e5 + 14;


struct Dsu{
	int par[maxn], his[2 * maxn], sz;
	int root(int v){
		return par[v] < 0 ? v : par[v] = root(par[v]);
	}
	bool fri(int v, int u){
		return root(v) == root(u);
	}
	ll merge(int v, int u){
		if((v = root(v)) == (u = root(u)))
			return 0;
		his[sz++] = v;
		his[sz++] = u;
		ll ret = (ll) -par[v] * -par[u];
		par[u] += par[v];
		par[v] = u;
		return ret;
	}
	void reset(){
		while(sz)
			par[ his[--sz] ] = -1;
	}
}  dsu;
bool impo[maxn];
ll dp[maxn];
int n;
vector<pair<int, int> > vec[maxn];
void solve(){
	cin >> n;
	for(int i = 0; i < n - 1; i++){
		int v, u, w;
		cin >> v >> u;
		v--, u--;
		cin >> w;
		vec[w].push_back({v, u});
	}
	ll ans = 0;
	for(int i = maxn - 1; i >= 1; i--){
		dsu.reset();
		for(int j = i; j < maxn; j += i){
			for(auto &e : vec[j])
				dp[i] += dsu.merge(e.first, e.second);
			if(j > i)
				dp[i] -= dp[j];
		}
		if(i<=20)cout << dp[i] << " " ;
		ans += i * dp[i];
	}
	cout << endl;
	cout << ans << '\n';
}
int main(){
	ios::sync_with_stdio(0), cin.tie(0);
	freopen("input.txt","r",stdin);
	freopen("output2.txt","w",stdout);
	memset(dsu.par, -1, sizeof dsu.par);
	fill(impo + 1, impo + maxn, 1);
	for(int i = 1; i * i < maxn; i++)
		for(int j = i * i; j < maxn; j += i * i)
			impo[j] = 0;
	int t;
	cin >> t;
	while(t--){
		solve();
		fill(dp, dp + maxn, 0);
		fill(vec, vec + maxn, vector<pair<int, int>> ());
	}
}