#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(unordered_map<int,vector<int> >::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}

bool fun(pair<int,int>& pp1, pair<int,int>& pp2){
	if(pp1.first!=pp2.first){
		return pp1.first>pp2.first;
	}
	return pp1.second>pp2.second; 
}

signed main(){
	init();
	clock_t clk = clock();
	
	TEST_CASES
	{
		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}
		int m;
		cin >> m;
		vector<pair<int,int> > vec(m);
		for (int i = 0; i < m; ++i)
		{
			cin >> vec[i].first >> vec[i].second;
		}

		sort(vec.begin(), vec.end(), fun);

		vector<pair<int,int> > vec1;
		int prev = -1;
		for (int i = 0; i < m; ++i)
		{
			if(vec[i].first!=prev){
				vec1.push_back(vec[i]);
				prev=vec[i].first;
			}
		}

		vector<pair<int,int> > vec2;
		vec2.push_back(vec1[0]);
		for (int i = 1; i < vec1.size(); ++i)
		{
			if(vec2[vec2.size()-1].second < vec1[i].second){
				vec2.push_back(vec1[i]);
			}
		}



		reverse(vec2.begin(), vec2.end());

		for (int i = 0; i < vec2.size(); ++i)
		{
			//cout << vec2[i].first << " " << vec2[i].second <<endl;
		}

		int powr = -1;
		int enr = -1;
		int ans = 0;
		int cnt = 0;
		int reqPow = -1;
		for (int i = 0; i < n; ++i)
		{
			cnt++;
			enr--;
			reqPow = max(reqPow, arr[i]);

			//cout << i << " "  << reqPow << " " << ans << " " << powr << " " << enr << " "<< cnt << endl;
			if(enr < 0){
			    powr = -1;
		        enr = vec2[0].second;		
		        cnt = 1;	
		        reqPow = arr[i];
		        ans++;
			}
			if(powr<reqPow){
				int ind =upper_bound(vec2.begin(), vec2.end(), make_pair(reqPow,-1LL))-vec2.begin();
				if(ind == vec2.size()) {
					//cout << "fo\n";
					ans = -2; 
					break;
				}
				else
				{
					powr = vec2[ind].first;
					enr = vec2[ind].second-cnt;

					//cout << "hi" << powr << " " << enr << endl;
					if(enr<0){
						cnt=1;
						ans++;
						reqPow = arr[i];
						int ind = upper_bound(vec2.begin(), vec2.end(), make_pair(reqPow,-1LL))-vec2.begin();
						if(ind==vec2.size()) {
					        ans = -2; 
				         	break;
				        }
				        powr = vec2[ind].first;
					    enr = vec2[ind].second-cnt;

					    if(enr<0){
					    	//cout << ind << " " <<powr << " " << enr << " fo1\n";
					    	ans = -2;
					    	break;
					    }
					}
				}
			}
		}
		//cout << "ans = ";

		if(ans<=0){
			cout << -1 << endl;
		}
		else{
			cout << ans << endl;
		}	



		
	}


	

	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif


	return 0;
}