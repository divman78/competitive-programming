#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) //cout << arr[i] << " "; //cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) //cout << "#debug " << x << endl;
#define print2D(arr, n) //cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){//cout << arr[i][j] << " ";}//cout << endl;}//cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) //cout << itr->first << "->" << itr->second << " "; //cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO //cout << "NO"; return 0;
#define MX 310
#define MO 1000000007
#define N 400001
//#define N 16
#define left (2*idx+1)
#define right (2*idx+2)
#define mid ((l+r)>>1)


int n, q;
int spf[MX];
int arr[N];

mii seg[4*N];
mii lazy[4*N];



void preprocess(){
	for(int i = 2; i  < MX; i++){
		if(spf[i]==0){
			for(int j = i; j < MX; j+=i){
				if(spf[j] == 0)
					spf[j] = i;
			}
		}
	}
}

void pfs(int x, mii& mp){
	while(x!=1){
		mp[spf[x]]++;
		x/=spf[x];
	}
}

int power(int num,int n){
  if(n==0) return 1;
  int x=power(num,n/2);
  x=x*x%MO;
  if(n%2==1) x=x*num%MO;
  return x;
}

int MODinverse(int num){
  return power(num,MO-2)%MO;
}



void build(int idx=0, int l=0, int r=n-1){
	// //cout << idx << " " << l << " " << r << endl;
	if(l==r){
		pfs(arr[l],seg[idx]);
		return;
	}
	if(l>r){
		return;
	}

	build(left, l, mid);
	build(right, mid+1, r);
	iterm(seg[left]){
		seg[idx][itr->first] = itr->second;
	}
	iterm(seg[right]){
		seg[idx][itr->first] += itr->second;
	}
}

void update(int idx, int l, int r, int x, int y, mii& mp){
	if(lazy[idx].size()){
		iterm(lazy[idx]){
			seg[idx][itr->first] += itr->second*(r-l+1);
		}
		if(l!=r){
			iterm(lazy[idx]){
				lazy[left][itr->first] += lazy[idx][itr->first];
			}
			iterm(lazy[idx]){
				lazy[right][itr->first] += lazy[idx][itr->first];
			}
		}
		lazy[idx].clear();
	}
	if(l > y || r < x || l > r){
		return;
	}
	//cout<<"update : "   << idx << " "<< l << " " << r << " " << x << " " << y <<   "\n";
	if(l >= x && r <= y){
		iterm(mp){
			seg[idx][itr->first] += itr->second*(r-l+1);
		}
		if(l!=r){
			iterm(mp){
				lazy[left][itr->first] += itr->second;
			}
			iterm(mp){
				lazy[right][itr->first] += itr->second;
			}
		}
		return;
	}

	update(left, l, mid, x, y, mp);
	update(right, mid+1, r, x, y, mp);

    seg[idx].clear();
	iterm(seg[left]){
		seg[idx][itr->first] = itr->second;
	}
	iterm(seg[right]){
		seg[idx][itr->first] += itr->second;
	}
}

void query(int idx, int l, int r, int x, int y, mii& val){
	if(lazy[idx].size()){
		iterm(lazy[idx]){
			seg[idx][itr->first] += itr->second*(r-l+1);
		}
		if(l!=r){
			iterm(lazy[idx]){
				lazy[left][itr->first] += lazy[idx][itr->first];
			}
			iterm(lazy[idx]){
				lazy[right][itr->first] += lazy[idx][itr->first];
			}
		}
		lazy[idx].clear();
	}
	if(l > y || r < x || l > r){
		return;
	}
	//cout << "query : " << idx << " " << l << " " << r << " " << x << " " << y <<  endl;
	if(l >= x && r <= y){
		iterm(seg[idx]){
			val[itr->first] += itr->second;
		}
		return;
	}

	mii val1, val2;
	query(left, l, mid, x, y, val1);
	query(right, mid+1, r, x, y, val2);

	iterm(val1){
		val[itr->first] = itr->second;
	}
	iterm(val2){
		val[itr->first] += itr->second;
	}
}

int phi(mii& val) //euler totient
{ 
    int result = 1;
    iterm(val){
    	result = (result*power(itr->first, itr->second))%MO;
    }
    //cout << "result : "<< result << "\n";

    printmap(val);

    iterm(val){
		result = (result-((result*MODinverse(itr->first))%MO)+MO)%MO;
	}
	//cout << endl;
    return result; 
} 





signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	preprocess();

	cin >> n >> q;

	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}

	// //cout << "hello\n";

	build();


	while(q--){
		string s;
		cin >> s;
		if(s == "MULTIPLY"){
			int l, r, x;
			cin >> l >> r >> x;
			mii mp;
			pfs(x, mp);
			update(0, 0, n-1, l-1, r-1, mp);

		}
		else{
			int l, r;
			cin >> l >> r;
			mii val;
			query(0, 0, n-1, l-1, r-1, val);
			// //cout <<"phi ";
			cout << phi(val) << endl;
		}


	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	//cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




