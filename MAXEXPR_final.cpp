#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define SIG 100.0

void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

signed main(){
	read_file();
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		pair<int,int> kk[n];
		int cc[n];
		for (int i = 0; i < n; ++i)
		{
			float x; cin >> x;
			kk[i].first = x*SIG;
			kk[i].second = i;
		}
		for (int i = 0; i < n; ++i)
		{
			float x; cin >> x;
			cc[i] = x*SIG;
		}

		int su = 0;
		for (int i = 0; i < n; ++i)
		{
			su+=kk[i].first*cc[i];
		}

		sort(kk, kk+n);
		reverse(kk, kk+n);

		int suf[n];
		suf[n-1] = kk[n-1].first;
		for (int i = n-2; i >= 0; --i)
		{
			suf[i]=kk[i].first+suf[i+1];
			//cout << suf[i] << "->" << kk[i].first << " ";
		}
		//cout <<"\n\n\n\n\n\n\n";

		int i = 0;
		int val = 1;
		int aans[n];
		int ans = 0;
		int sum = 0;
		while(i < n-1){
			int inc = (n-i)*suf[i]*val*val;
			int v = inc/suf[i+1];
			int cur = ans + (n-i)*val;
			int ahead = ans + (val-1)*(val-1) + (n-i-1)*(val-1+v)*(val-1+v);
			cout << i << " " << inc << " " << v << " " << cur << " " << ahead << endl;
			if(inc > su || ahead > cur){
				aans[i] = val-1;
				ans += aans[i]*aans[i];
				i++;
			}
			else{
				val++;
				su-=inc;
			}
		}

		aans[i] = su/kk[i].first;
		ans+=aans[i]*aans[i];

		cout << ans << " ";
		for (int i = 0; i < n; ++i)
		{
			cout << aans[i]-cc[i] << " ";
		}
		cout << endl;


	}

}