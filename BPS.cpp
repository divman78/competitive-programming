#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define FASTIO ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);


int spf[N];
int mm[N];

void seive(){
	for (int i = 2; i < N; ++i)
	{
		spf[i] = i;
	}
	for (int i = 2; i < N; ++i)
	{
		if(spf[i] == i){
			for (int j = i*2; j < N; j+=i)
			{
				if(spf[j] == j)
					spf[j] = i; 
			}
		}
	}

	// for(int i = 2; i < 20; i++){
	// 	cout << spf[i] << " ";
	// }
	//cout << endl;
}

set<int> findprimes(int x){
	set<int> v;
	// cout << endl;
	// cout << x << "->";
	while(x!=1){
		v.insert(spf[x]);
		x/=spf[x];
	}
	// for(set<int>::iterator i = v.begin(); i!=v.end(); i++){
		// cout << *i << " ";
	// }
	// cout << endl;
	return v;
}

void calc(set<int> v, int ind, int prod){
	if(ind == -1){
		mm[prod]++;
		return;
	}
	int x = *v.begin();
	v.erase(x);
	calc(v, ind-1, prod*x);
	calc(v, ind-1, prod);
}



signed main(){
	FASTIO
	//freopen("input.txt","r",stdin);
	seive();
	int t = 1;
	cin >> t;
	while(t--){
		// cout << "ehllo\n";
		memset(mm, 0, sizeof(mm));
		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}
		// cout << "eheleo";

		for (int i = 0; i < n; ++i)
		{
			set<int> v = findprimes(arr[i]);
			calc(v, v.size()-1, 1);
		}

		int ans = 0;		
		for(int i = 1; i < N; i++){
			if(mm[i]){
				set<int> v = findprimes(i);
				ans = max(ans, mm[i]*(int)(v.size()));
			}
			// cout << i->first << " " << i -> second << " "<< v.size() << endl;
		}

		cout << ans << endl;
	}
}