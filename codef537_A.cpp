#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 50010
#define int unsigned long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define NO cout << "NO"; return 0;

int n, k , a, b;

int recurse(int s, int e, int na, vector<int> & arr){
	int l = (e-s+1);
	int ret = l*b*na;
	if(na == 0) ret = a;
	if(l < 2 or na == 0){
		return ret;
	}
	int mid = s+(l/2)-1;
	int na1 = upper_bound(arr.begin(), arr.end(), mid) - lower_bound(arr.begin(), arr.end(), s);
	int na2 = na-na1;
	return min(ret, recurse(s, mid, na1, arr)+recurse(mid+1, e, na2, arr));

}


signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif

	int t=1;
	//cin >> t; 
	while(t--){
		cin >> n >> k >> a >> b;
		vector<int> arr(k);
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}

		sort(arr.begin(), arr.end());

		cout << recurse(1, 1 << n, k, arr);





        
	}

	return 0;
}




