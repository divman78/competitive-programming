#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define MX 1001000
#define N 100100
#define mid ((l+r) >> 1)
#define left (2*ind+1)
#define right (2*ind+2)

void preprocess(){
	
}

int spf[MX];

void seive(){

	spf[1] = 1;
	for (int i = 2; i < MX; ++i)
	{
		if(!spf[i]){
			for(int j = i; j < MX; j+=i){
				if(!spf[j]){
					spf[j] = i;
				}
			}
		}
	}
}

int n, m;
int seg[4*N];
int arr[N];
set<int> ss;

void build(int l, int r, int ind){
	//cout<<l<<" "<<r<<endl;
	if(l>r) return;
	if(l == r){
		seg[ind] = spf[arr[l]];
		return;
	}

	build(l, mid, left);
	build(mid+1, r, right);
	seg[ind] = max(seg[left], seg[right]);
}

void update(int l, int r, int ind, int upd){
	//cout << upd << " " << l << " " << r << endl;
	if(l>r || l>upd || r<upd){
		return;
	}
	if(l == r){
		arr[l]/=spf[arr[l]];
		seg[ind]=spf[arr[l]];
		return;
	}
	update(l, mid, left, upd);
	update(mid+1, r, right, upd);
	seg[ind] = max(seg[left], seg[right]);
}

void update(int l, int r){
	set<int> :: iterator itr = ss.lower_bound(l);
	vector<int> del;
	while(itr!=ss.end() && *itr<=r){
		update(0, n-1, 0, *itr);
		if(arr[*itr]==1){
			del.push_back(*itr);
		}
		itr++;
	}

	for (int i = 0; i < del.size(); ++i)
	{
		ss.erase(del[i]);
	}
}

int query(int l, int r, int ind, int ll, int rr){
	if(l>r || l>rr || r<ll){
		return 0;
	}
	if(l>=ll&&r<=rr){
		return seg[ind];
	}
	return max(query(l, mid, left, ll, rr), query(mid+1, r, right, ll, rr));
}







signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();

	seive();
	int t=1;
	//cout << " dsf ";
	cin >> t; 
	while(t--){

		cin>>n>>m;

		for (int i = 0; i < n; ++i)
		{
			cin>>arr[i];
			if(arr[i]!=1){
				ss.insert(i);
			}
		}
		build(0,n-1,0);

		while(m--){
			int ty, l,r;
			cin>>ty>>l>>r;
			if(ty){
				cout<<query(0,n-1,0,l-1,r-1)<<" ";
			}
			else{
				update(l-1,r-1);
			}
		}

		cout<<"\n";




		


        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















