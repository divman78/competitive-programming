//thanks mit
#include<bits/stdc++.h>
using namespace std;
#define N 5050
#define mod 1000000007

long long n, k;
long long arr[N];
long long real_ans;

long long ans;

long long inv(long long a, long long b){  //a*b < LONGLONG_MAX
 return 1<a ? b - inv(b%a,a)*b/a : 1;
}

// long long add(long long a, long long b){
// 	//return ((a%mod) + (b%mod))%mod;
// 	return a + b;
// }

// long long sub(long long a, long long b){
// 	//return ((a%mod) - (b%mod) + mod) % mod;
// 	return a - b;
// }

// long long mul(long long a, long long b){
// 	//return ((a%mod) * (b%mod))%mod;
// 	return a * b;
// }

// long long divi(long long a, long long b){
// 	// long long gcd = __gcd(a,b);
// 	// a /= gcd, b /= gcd;
// 	// return ((a%mod) * (inv(b, mod)%mod))%mod;
// 	return a / b;
// }

long long add(long long a, long long b){
	return ((a%mod) + (b%mod))%mod;
	//return a + b;
}

long long sub(long long a, long long b){
	return ((a%mod) - (b%mod) + mod) % mod;
	//return a - b;
}

long long mul(long long a, long long b){
	return ((a%mod) * (b%mod))%mod;
	//return a * b;
}

long long divi(long long a, long long b){
	long long gcd = __gcd(a,b);
	a /= gcd, b /= gcd;
	return ((a%mod) * (inv(b, mod)%mod))%mod;
	//return a / b;
}

long long pow(long long a, long long b){
	long long res = 1;
	while(b > 0){
		if(b & 1){
			res = mul(res, a);
		}
		b = b >> 1;
		a = mul(a, a);
	}
	return res;
}

// void calc(long long cnt, long long prod, long long index){
// 	if(index >= n){
// 		return;
// 	}
// 	if(cnt == 0){
// 		calc(cnt + 1, prod, index + 1);
// 		calc(cnt, prod, index + 1);
// 		return;
// 	}
// 	if(cnt == k - 1){
// 		real_ans = mul(real_ans, prod);
// 		calc(cnt, prod, index + 1);
// 		return;
// 	}

// 	calc(cnt, prod, index  + 1);
// 	calc(cnt + 1, mul(prod, arr[index]), index + 1);
// }

void calc2(){	
	long long f[n + 1];
	f[0] = 1;
	for(long long i = 1; i <= n; i++){
		f[i] = mul(i,f[i-1]);
		//cout << f[i]<<" ";
	}
	//cout << endl;

	long long temp = divi(f[n-1], mul(f[n-k], f[k-1]));
	//cout<<"temp = "<< temp << endl;

	long long counting[n];
	//cout <<"counting \n";
	for(long long i = 0; i < n; i++){
		if(i < k - 1){
			counting[i] = 0;
		}
		else{
			counting[i] = divi(f[i], mul(f[i - k + 1], f[k - 1]));
		}
		//cout << counting[i] << " ";
	}
	//cout << endl;
	//cout << "temp \n";

	for(long long i = 0; i < n; i++){
		long long temp1 = sub(temp, add(counting[i], counting[n - i - 1]));
		// ////cout << add(counting[i], counting[n - i - 1]) << " -> ";
		////cout << temp1  << " -> "<<arr[i]<<"\t";
		
		if(temp1 != 0) ans = mul(ans, pow(arr[i],temp1));
		//cout<< temp1 <<" " << pow(arr[i],temp1) <<" "<<ans << endl;
	}
	//cout << endl;
	//////cout << ans << endl;
}




int main(){
	//freopen("input.txt","r",stdin);
	cout << divi(11,16);
	long long t;
	cin >> t;
	//srand(time(NULL));
	while(t--){
		cin >> n >> k;
		//n = rand()%(20 - 3 + 1) + 3; k = rand()%(n - 3 + 1) + 3;
		//n = 7, k = 6;
		//real_ans = 1;
		ans = 1;
		for(long long i = 0; i  < n; i++){

			cin >> arr[i];
			//arr[i] =  i + 1;
		}
		sort(arr, arr + n);
		//calc(0, 1, 0);
		//////cout << real_ans << endl;
		calc2();
		// if(ans != real_ans){

		// 	//cout <<"something went wrong!!!!!!!!\n";
		// 	//cout << n <<" "<< k << endl;
		// 	//cout << "your ans = "<< ans << "\n";
		// 	//cout << "real ans = "<< real_ans << "\n";
		// 	exit(0);
		// }
		cout << ans << endl;
	}
}