#include <bits/stdc++.h>

using namespace std;

int main() {
  // ios::sync_with_stdio(false);
  // cin.tie(0);
  freopen("input.txt","r",stdin);
  int tt;
  cin >> tt;
  while (tt--) {
    int n;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
      cin >> a[i];
    }
    const int LOG = 20;
    vector<vector<int>> pr(n, vector<int>(LOG, 0));
    vector<int> mx(n);
    mx[0] = a[0];
    vector<int> imp(n, 0);
    imp[0] = 1;
    for (int i = 1; i < n; i++) {
      int x;
      cin >> x;
      x--;
      if (imp[x]) {
        pr[i][0] = x;
      } else {
        pr[i][0] = pr[x][0];
      }
      if (a[i] > mx[x]) {
        imp[i] = 1;
      }
      //cout << pr[i][0] + 1<< "  -  ";
      mx[i] = max(mx[x], a[i]);
    }
    for (int j = 1; j < LOG; j++) {
      for (int i = 0; i < n; i++) {
        pr[i][j] = pr[pr[i][j - 1]][j - 1];
        //cout << pr[i][j] << " ";
      }
      //cout << "\n";
    }
    int ttt;
    cin >> ttt;
    int ans = 0;
    while (ttt--) {
      int x, y;
      cin >> x >> y;
      x ^= ans;
      y ^= ans;
      cout << "querpo "<< x << " " << y << endl;
      x--;
      ans = 0;
      if (y < a[0]) {
        ans++;
        y = a[0];
      }
      if (x != 0 && imp[x] && y < a[x]) {
        ans++;
      }
      for (int j = LOG - 1; j >= 0; j--) {
        if (y < a[pr[x][j]]) {
          ans += (1 << j);
          x = pr[x][j];
        }
      }

      cout << ans << '\n';
    }
  }
  return 0;
}
