#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;
typedef unsigned long long ull;
#define MOD 1000000007
#define pii pair<int,int>
#define pll pair<ll,ll>
#define PB push_back
#define POPB  pop_back
#define TR(v,it) for(auto it=v.begin();it!=v.end();it++)
#define FT(i,n) for(int i=0;i<n;i++)
#define FRT(i,n) for(int i=n-1;i>=0;i--)
#define FTG(i,a,b) for(int i=a;i<=b;i++)
#define FTGR(i,a,b) for(int i=a;i>=b;i--)
#define MP make_pair
#define F first
#define S second
 
void shoot(){
    #ifndef ONLINE_JUDGE
    freopen ("input.txt","r",stdin);
    freopen ("output.txt","w",stdout);
    #else
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    #endif
}
 

void initDSU(){
  for (int i = 0; i < N; ++i)
  {
    parent[i] = i;
  }
}

int root (int i)
{
    while(parent[i] != i)    
    {
        parent[i] = parent[parent[i]] ; 
        i = parent[i]; 
    }
    return i;
}

 
void uni(int a, int b) {
  parent[root(a)] = parent[root(b)];
}

bool conn(int a, int b){
  return (root(a) == root(b));
}



ll power(ll n,ll pwr){
  ll ans=1LL;
  n%=MOD;
  while(pwr){
      if(pwr&1)
      ans=(ans*n)%MOD;
      n=(n*n)%MOD;
      pwr>>=1;
  }
  return ans;
}

bool prime[4000000];
void BuildSieve(){
    ll i,p;
    memset(prime, true, sizeof(prime));
    for (p=2;p<=2000; p++){
        if (prime[p]){
            for(i=p*2;i<=4000000;i+=p){
              prime[i]=false;
            }
        }
    }
}
 
ll bigMOD(ll num,ll n){
  if(n==0) return 1;
  ll x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}
 
ll MODinverse(ll num){
  return bigMOD(num,MOD-2)%MOD;
}
 
ll gcd(ll a, ll b){
    if (a == 0)
        return b;
    return gcd(b%a, a);
}
 
bool isPowerOfTwo (ll x){
  return x && (!(x&(x-1)));
}



int spf[N];
void BuildSieve(){
    int i,p;
    memset(spf, 0, sizeof(spf));
    for(p=2;p<N; p++){
        if (spf[p] == 0){
            for(i=p;i<N;i+=p){
              if(spf[i]==0)spf[i]=p;
            }
        }
    }
}


map<int,int> push_primes(int x){
  map<int, int> pfactors;
  while(x != 1){
    pfactors[spf[x]] = 1;
    x/=spf[x];
  }
  return pfactors;
}



// no no no no no no no no no no no no no yes yes yes yes yes yes yes yes yes yes yes yes yes 
//                                        ^^^

int binary_search(int lo,int hi){
  while (lo < hi){
   int mid = lo + (hi-lo)/2;
      if (p(mid) == true)
         hi = mid;
      else
         lo = mid+1;
  }
          
   //if (p(lo) == false)
      //complain                // p(x) is false for all x in S!
      
   return lo ;        // lo is the least x for which p(x) is true
   //return lo-1     //lo is the highest x for which p(x) is false
}

void isKthBitSet(int n, int k) 
{ 
    if ((n >> (k - 1)) & 1) 
        return true; 
    else
        return false; 
} 


void makedistinct(vector<int>& vec){
  sort( vec.begin(), vec.end() );
  vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
}


//start of fenwick tree
const int MAXN = 1000 * 1000 + 10;

int fen[MAXN];

void add(int x, int val)   //adds val from 0, x index
{
    for (int i = x + 1; i < MAXN; i += i & (-i)) fen[i] += val;
}

int get(int x) //get less than x
{
    int ret = 0;
    for (int i = x; i > 0; i -= i & (-i)) ret += fen[i];
    return ret;
}

int sum(int x, int y)
{
    return get(y) - get(x);
}

//end of fenwick tree

int binary_search(int lo,int hi){
  while (lo < hi){
   int mid = lo + (hi-lo)/2;
      if (poss(mid) == true){
        hi = mid;
      }
      else{
        lo = mid+1;
      }
  }  
   return lo ;       
}

vector<vector<int> > multiply(vector<vector<int> >& mat1, vector<vector<int> >& mat2){
  vector<vector<int> > res(m);
  for (int i = 0; i < m; ++i)
  {
    for(int j = 0; j < m; j++){
      int p = 0;
      for(int k = 0; k < m; k++){
        p = (p+((mat1[i][k]*mat2[k][j])%MOD))%MOD;
      }
      res[i].push_back(p);
    }
  }
  return res;
}


long long add(long long a,long long b){
    return (a+b)%mod;
}

long long sub(long long a,long long b){
    return ((a%mod)-(b%mod)+mod)%mod;
}

long long mul(long long a,long long b){
    return ((a%mod)*(b%mod))%mod;
}

bool isprime(int x){
  if(x == 2){
    return true;
  }
  if(x%2==0){
    return false;
  }
  for(int i = 3; i <= sqrt(x); i+=2){
    if(x%i==0) return false;
  }
  return true;
}


int ternary_search(int l, int r)
{
  while(r-l>=3)
  {
    int mid1 = l + (r-l)/3;
    int mid2 = r - (r-l)/3;
    if(f(mid1) < f(mid2))
      r = mid2;
    else
       l = mid1;
  }
  if(l==r)
    return f(l);
  else if (l+1==r)
    return min(f(l), f(r));
  else
    return min(f(l), min(f(l+1), f(l+2)));
}


bool isKthBitSet(int n, int k) 
{ 
    if ((n >> (k - 1)) & 1) 
        return true; 
    else
        return false; 
} 

void printbinary(int n){
  for (int i = 10; i >= 1; --i)
  {
    cout << (int) isKthBitSet(n, i);
  }
  cout << endl;
}


int vectoint(vector<int>& vec){
  int n = 0;
  for (int i = 0; i < vec.size(); ++i)
  {
    n += vec[i] * pow(10,i);
  }
  return n;
}

vector<int> intovec(int n){
  cout << "n=" << n << endl;
  vector<int> vec;
  while(n){
    vec.push_back(n%10);
    n/=10;
  }
  printarr(vec, vec.size());
  return vec;
}


int countnums(int lx, int rx){ // numbers in [lx, rx]
  return upper_bound(arr.begin(), arr.end(), rx)-lower_bound(arr.begin(),arr.end(), lx);
}
