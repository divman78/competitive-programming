#include<bits/stdc++.h>
using namespace std;
#define N 1000010

long long n;
bool prime[N];
long long arr1[N];
long long arr[N];
void seive(){
	for (long long i = 0; i < N; ++i)
	{
		prime[i] = true;
	}
	prime[0] = prime[1] = false;
	for (long long i = 2; i < N; ++i)
	{
		if(prime[i]){
			for (long long j = 2*i; j <= N; j+=i)
			{
				prime[j] = false;
			}
		}
	}

	std::vector<int> v;
	for (long long i = 0; i < N; ++i)
	{
		if(prime[i]){
			//cout << i << " ";
			v.push_back(i);
		}
	}
	set<int> ss;

	for (long long i = 0; i < v.size(); ++i)
	{
		for (long long j = i; j < v.size(); ++j)
		{
			if((v[i]%2 && v[j]%2)||(v[i]%2 == 0 && v[j]%2 == 0))
				//cout << v[i]+v[j] <<  " ",
				ss.insert(v[i]+v[j]);

		}
	}
	// for (set<int> :: iterator i = ss.begin(); i != ss.end(); ++i)
	// {
	// 	cout << *i << " ";
	// }
	for (long long i = 1; i < 100; ++i)
	{
		for (long long j = i; j < 100; ++j)
		{
			long long x = i xor j;
			if(x == 2 || x == 0) 
				cout << i << "-"<<  j << "\t";
		}
	}
}

long long solve(){
	long long cnt= 0;
	for (long long i = 0; i < n-1; ++i)
	{
		for (long long j = i+1; j < n; ++j)
		{
			long long x = arr[i] xor arr[j];
			if(x%2 == 0 && x!=2 && x!=0 ){
				cnt++;
			}
		}
	}

	return cnt;
}


int main(){
	//seive();
	//freopen("output.txt","r",stdin);
	long long t;
	cin >> t;
	long long orig;
	while(t--){
		cin >> n;
		for (long long i = 0; i < N; ++i)
		{
			arr1[i] = 0;
		}
		long long odds = 0, evens = 0;
		for (long long i = 0; i < n; ++i)
		{
			cin >> arr[i];
			if(arr[i]&1) odds++;
			else         evens++;
			arr1[arr[i]]++;
		}
		if(n <= 1000) 
		{
			orig = solve(); cout << orig << endl;
		 	continue;
		}

		long long cnt = 0;
		for (long long i = 1; i+2 < N; i+=4)
		{ 
		    cnt += arr1[i+2]*arr1[i];
		}
		for (long long i = 4; i+2 < N; i+=4)
		{
			cnt += arr1[i+2]*arr1[i];
		}
		for (long long i = 1; i < N; i++)
		{
			cnt += max(0LL, (long long)(arr1[i]*(arr1[i]-1LL))/2LL);
		}

		long long ans = (((odds-1LL)*odds)/2LL) + (((evens-1LL)*evens)/2LL) - cnt;
		cout << ans << endl;
		cout << endl;
	}
}