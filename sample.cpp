#include<bits/stdc++.h>
using namespace std;
#define debug(num) cout<<"debug "<<num<<"\n"
#define N 100000
#define MAX 40000000000000000
#define trap(i) for(int i = 0;; i++) 
#define show(a, n) for(int i =0 ; i < n; i++) cout<<a[i]<<" "; cout<<"\n"

long long arr[N];
long long arr1[N];
long long ans[N];
long long myans[N];
long long n;
long long cnt;

bool isKthBitSet(long long n, long long k)
{
    if (n & (1 << (k-1)))
        return true;
    else
        return false;
}

bool isValid(long long x){
	cnt = 0;
	for(long long i = 0; i < n; i++){
		if(isKthBitSet(x,i+1)) arr1[i] = arr[i];
		else arr1[i] = -arr[i];
	}
	bool valid = true;
	for(long long i = 0; i < n-1; i++){
		long long sum = arr1[i];
		for(long long j = i + 1; j < n; j++){
			sum = sum + arr1[j];
			if(sum < 1){
				valid = false;
				break;
		    }
		}
		if(!valid){
			break;
		}
	}
	if(valid){
		for(long long i = 0; i < n; i++){
			cnt = cnt + arr1[i];
		}
	}
	else cnt = MAX;
	return valid;
}

void solved(){
	long long loop = pow(2,n) - 1;
	long long cnt1 = MAX;
	for(long long i = 1; i <= loop; i++){
		if(isValid(i)){
			//debug(i);
			if(cnt1 > cnt){
				cnt1 = cnt;
				for(long long i = 0; i < n; i++){
					ans[i] = arr1[i];
				    //cout<<ans[i]<<" ";
				} 
				//cout<<endl;
			}
		}
	}
}

vector<int> FindMaxSum(vector<int>& arrx, int ni)
{
  vector<int> temp1,temp2,temp3;
  int incl = arrx[0];
  int excl = 0;
  int excl_new;
  temp1.push_back(0);
 
  for (int i = 1; i < ni; i++)
  {
     /* current max excluding i */
    if(incl > excl)
    {
      temp3 = temp1;
    }
    else
    {
      temp3 = temp2;
    }
    excl_new = max(incl,excl);
 
     /* current max including i */
     incl = excl + arrx[i];
     temp1 = temp2;
     temp1.push_back(i);
     excl = excl_new;
     temp2 = temp3;
  }
 
   /* return max of incl and excl */
  if(incl > excl)
    {
      return temp1;
    }
    else
    {
      return temp2;
    } 
}

void func(vector<int>& push_vec, vector<bool>& flag, int x){

	vector<int> vec;

	for(int i = 0; i < push_vec.size(); i++){
		vec.push_back(arr[push_vec[i]]);
	}
	vector<int> vect1 = FindMaxSum(vec,vec.size());
	/*int j = 0;
	for(int i = 0; i< vec.size(); i++){
		if(vec[i]==vect1[j]){
			j++;
			flag[push_vec[i]] = true;
		}
	}*/
	//cout<<"dfsdgfss"<<endl;
	for(int i = 0; i < vect1.size(); i++){
		    //cout<<vect1[i]<<" "<<x<<endl;
			flag[2*vect1[i]+x] = true;
	}
}

void solve(){
	/*code goes here*/
	if(n==1){
		myans[0] = 0;
		return;
	}
	if(n==2){
		myans[0]=(arr[0]>arr[1])?arr[0]:-arr[0];
		myans[1]=(arr[0]>arr[1])?-arr[1]:arr[1];
		if(arr[0]==arr[1]){
			myans[0]=arr[0];
			myans[1]=arr[1];
		}
		return;
	}
	vector<int> mark;
	if(arr[0]<arr[1]){
		mark.push_back(0);
	}
	for(int i = 1; i < n-1; i++){
		if(arr[i]<arr[i+1] && arr[i]<arr[i-1]){
			mark.push_back(i);
		}
	}
	if(arr[n-1]<arr[n-2]){
		mark.push_back(n-1);
	}
	//for(int j = 0; j < mark.size(); j++) cout<<mark[j]<<" ";
	//cout<<"\n";
	vector<bool> flag(n);
	for (int i = 0; i < n; i++) flag[i] = false;
	int i = 0;
	while(i<mark.size()){
		int p = mark[i];
		//cout<<"p = "<<p<<"\n";
		vector<int> push_vec;
		push_vec.push_back(mark[i++]);
		while(i<mark.size()){
			if(mark[i]-mark[i-1]==2){
				if(arr[mark[i]-1]-arr[mark[i]] <= arr[mark[i-1]]){
					push_vec.push_back(mark[i++]);
				}
				else{
					break;
				}
			}
			else{
				break;
			}
		}
		/*cout<<"dfs  "<<i<<endl;
		for(int j = 0; j < push_vec.size(); j++){
			cout<<arr[push_vec[j]]<<" ";
		}
		cout<<"\n";*/
		func(push_vec, flag, p);
	}
	for(int i = 0; i < n; i++){
		if(flag[i]){
			myans[i] = -arr[i];
		}
		else{
			myans[i] = arr[i];
		}
	}
}

int main(){
	//cout<<INT_MAX<<"\n";
	//freopen("input.txt","r",stdin);
	//freopen("output.txt","w",stdout);
	srand(time(NULL));
	long long t;
	cin >> t;
	while(1){
		//int arrxx[]={6, 10, 3, 1, 9 ,7 ,6 ,5 ,9, 7, 8, 7, 9, 10, 1, 10, 1, 9, 3, 5};
		//cin >> n;
		n = rand()%20 + 2;
		//n = 20;
		for(long long i = 0; i < n; i++){
			//cin >> arr[i];
			arr[i] = rand()%10 + 1;
			//arr[i]=arrxx[i];
		}
		//solved();
		solve();
		for(long long i = 0; i < n; i++){
			if(ans[i] != myans[i]){
				cout<<"original ans = ";
				for(long long i = 0; i < n; i++) cout<<ans[i]<<" "; 
		        cout<<"\n";
		        cout<<"my ans =       ";
		        for(long long i = 0; i < n; i++) cout<<myans[i]<<" "; 
		        cout<<"\n";
		        if(t>20){
		        	exit(0);
		        }
		        break;
			}
		}
		cout<<"\n";
	}
	//trap(x);
	return 0;
}