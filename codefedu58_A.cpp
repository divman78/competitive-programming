#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 201010
//#define N 10
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define int long long
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);


bool fit(int x, int y, int h, int w){
	return ((x <= h and y <= w) or (y <= h and x <= w));
}

int spf[N];
void BuildSieve(){
    int i,p;
    memset(spf, 0, sizeof(spf));
    for(p=2;p<N; p++){
        if (spf[p] == 0){
            for(i=p;i<N;i+=p){
              if(spf[i]==0)spf[i]=p;
            }
        }
    }

    // for (int i = 0; i <= 20; ++i)
    // {
    // 	cout << spf[i] << " ";
    // }
}


map<int,int> push_primes(int x){
	map<int, int> pfactors;
	while(x != 1){
		pfactors[spf[x]] = 1;
		x/=spf[x];
	}
	return pfactors;
}

vector<int> adj[N];
map<int, int> mm[N];
int arr[N];

int ans = 0;

void dfs(int v, int pr = -1){
	//cout << "startv = " << v << endl;
	for(int i = 0; i < adj[v].size(); i++){
		int u = adj[v][i];
		if(u != pr){
			//cout << "dfs("<< u <<", " << v << ")" << endl; 
			dfs(u, v);
			for(map<int, int>:: iterator itr = mm[v].begin(); itr != mm[v].end(); itr++){
				//cout<< u << " " << v << " "  << itr->first << " " << mm[u][itr->first] << " " << itr->second << endl;
				ans = max(ans, itr->second + mm[u][itr->first]);
				mm[v][itr->first] = max(mm[u][itr->first]+1, itr->second);
			}
		}
	}

	for(map<int, int>:: iterator itr = mm[v].begin(); itr != mm[v].end(); itr++){
		ans = max(ans, itr->second);
	}
	//cout << "endv : " << v << endl;
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	BuildSieve();
	int n;
	cin >> n;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
		mm[i] = push_primes(arr[i]);
//		printmap(mm[i]);
	}

	// for (int i = 0; i < n-1; ++i)
	// {
	// 	for(int j = i+1; j < n; j++){
	// 		cout<<"hey : " << i+1 << " " << j+1 << " " << __gcd(arr[i], arr[j]) << endl;
	// 	}
	// }
	for (int i = 0; i < n-1; ++i)
	{
		int x, y;
		cin >> x >> y;
		adj[x-1].push_back(y-1);
		adj[y-1].push_back(x-1);
	}

	dfs(0);


	cout << ans << endl;


	
	return 0;
}



