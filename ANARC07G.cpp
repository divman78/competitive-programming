#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define N 500100

string removeSpaces(string str)  
{ 
    str.erase(remove(str.begin(), str.end(), ' '), str.end()); 
    str.erase(remove(str.begin(), str.end(), '\r'), str.end()); 
    return str; 
} 

vector<string> split(string s){
	string delimiter = " ";
	vector<string> ret;
	size_t pos = 0;
	string token;
	while ((pos = s.find(delimiter)) != string::npos) {
	    token = s.substr(0, pos);
	    token = removeSpaces(token);
	    if(token!="")ret.push_back(token);
	    s.erase(0, pos + delimiter.length());
	}
	s = removeSpaces(s);
	if(s!="")ret.push_back(s);

	return ret;
} 

struct ticket{
	int single, family, cost;

	friend ticket operator + (const ticket &t1, const ticket &t2){
		ticket t3;
		t3.single = t1.single+t2.single;
		t3.family = t1.family+t2.family;
		t3.cost = t1.cost+t2.cost;
		return t3;
	}
};

struct data{
	ticket wip, wop, alone;

	ticket minimum (){
		wip 
	}
};



signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif

	string s;
	getline(cin, s);
	int t = 0;
	while(1){
		cout << s << endl;
		vector<string> v = split(s);
		if(!v.size()){
			getline(cin,s);
			continue;
		}
		//cout << v.size()<<endl;
		if(v.size()==2){
			if(v[0].length()==1 && v[0][0] == '0'){
				break;
			}
		    if(v[1].length()==1 && v[1][0] == '0'){
				break;
			}
		}

		int n, m;

		stringstream ss;
		ss << v[0];
		ss >> n;
		ss.clear();
		ss << v[1];
		ss >> m;


		map<string,int> mm;
		getline(cin, s);
		int num = 0;
		vector<vector<int> > parents;

		while(1){
			v = split(s);
			if(!v.size()){
				getline(cin,s);
				continue;
			}
			if(v[0][0]>='0' && v[0][0]<='9'){
				break;
			}
			if(!mm[v[0]]){
				mm[v[0]] = ++num;
				parents.push_back(vector<int>);
			}
			int pr = mm[v[0]]-1;

			for(int i = 1; i < v.size(); i++){
				if(!mm[v[i]]){
					mm[v[i]] = ++num;
					parents.push_back(vector<int>);
				}
				parents[mm[v[i]]-1].push_back(pr);
			}
			getline(cin, s);
		}

		vector<vector<int> > adj(num);

		for (int i = 0; i < num; ++i)
		{
			adj[parents[i][0]].push_back(i);
		}

		bool visited[num];
		memset(visited, 0, sizeof(visited));

		ticket ans = {0,0,0};

		for (int i = 0; i < num; ++i)
		{
			if(!visited[i]){
				ans = ans + dfs(i, adj);
			}
		}

		cout <<++t <<". " << ans.single << " " << ans.family << " " << ans.cost << "\n";

	}
	return 0;
}




















