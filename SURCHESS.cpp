#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false), cin.tie(0)
#define PA(a,sz) for(int i = 0; i <= sz; i++) cout << a[i] << " "; cout << endl
#define MX 1000000000

int fun(int i,int j, char x){
	return ((x == '0' && (((i%2!=0) && !(j%2!=0)) || (!(i%2!=0) && (j%2!=0)) )) ||
		(x == '1' && (((i%2!=0) && (j%2!=0)) || (!(i%2!=0) && !(j%2!=0)) )));
}

signed main(){
	FAST_IO;
	//freopen("input.txt", "r", stdin);
	int n, m;
	scanf("%d%d", &n, &m);
	char arr[n+1][m+1];
	memset(arr, 0, sizeof(arr));
	for (int i = 1; i <= n; ++i)
	{
		char s[m];
		scanf("%s",s);
		for (int j = 1; j <= m; ++j){
			arr[i][j] = s[j-1];
		}
	}
	
	int cntx[n+1][m+1];
	memset(cntx, 0, sizeof(cntx));
	int cnty[m+1][n+1];
	memset(cnty, 0, sizeof(cnty));
	int totalx = 0, totaly = 0;
	for (int i = 1; i <= n; ++i)
	{
		totalx = 0;
		for (int j = 1; j <= m; ++j)
		{
			totalx += fun(i, j, arr[i][j]);
			cntx[i][j] = totalx;
		}
	}

	for (int i = 1; i <= m; ++i)
	{
		totaly = 0;
		for (int j = 1; j <= n; ++j)
		{
			totaly += fun(j, i, arr[j][i]);
			cnty[i][j] = totaly; 
		}
	}

	int sz = min(m, n);	
	int val[n+1][m+1][sz+1];
	memset(val, 0, sizeof(val));

	int mx[sz+1];
	memset(mx, MX, sizeof(mx));
	for(int i = 0; i <= sz; i++){
		mx[i] = MX;
	}
	for (int i = 1; i <= sz; ++i)
	{
		for (int j = 1; j <= n-i+1; ++j)
		{
			for (int k = 1; k <= m-i+1; ++k)
			{
				int temp = (cntx[j+i-1][k+i-1]-cntx[j+i-1][k-1]) + 
				           (cnty[k+i-1][j+i-1] - cnty[k+i-1][j-1]) 
				            - fun(j+i-1, k+i-1, arr[j+i-1][k+i-1]);
				val[j][k][i] = val[j][k][i-1] + temp;
				mx[i] = min(val[j][k][i], min(mx[i] ,i*i-val[j][k][i]));
				
			}
		}
	}

	int ans[m*n+1];
	memset(ans, INT_MIN, sizeof(ans));
	ans[0] = 0;
	for (int i = 1; i <= sz; ++i)
	{
		ans[mx[i]] = max(ans[mx[i]], i);
	}

	if(ans[0] == INT_MIN) ans[0] = 1;
	for (int i = 1; i <= m*n; ++i)
	{
		ans[i] = max(ans[i], ans[i-1]);
	}

	int q;
	scanf("%d",&q);
	while(q--){
		int x;
		scanf("%d",&x);
		if(x > m*n) cout << sz << endl;
		else        cout << ans[x] << endl;
	}
}