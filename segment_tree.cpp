#include<bits/stdc++.h>
using namespace std;

#define N 200050
//#define N 200

int range[4*N];
int arr[N];
int n, nfill;

void build(int l, int r, int node){
	if(l == r){
		range[node] = arr[l];
		return;
	}
	int mid = (l+r)/2;
	build(l, mid, 2*node);
	build(mid+1, r, 2*node+1);
	range[node] = min(range[2*node], range[2*node+1]); 
}

int find(int node, int l, int r, int ll, int rr){
	if(ll >= l && rr <= r){
		return range[node];
	}
	if(ll > r || rr < l){
		return INT_MAX;
	}
	int mid = (ll+rr)/2;
	return min(find(2*node, l, r, ll, mid), find(2*node+1, l, r, mid+1, rr));
}

void b(){
	build(1,nfill,1);
}

int f(int l, int r){
	return find(1, l, r, 1, nfill);
}

void pre(){
	int height = ceil(log2((double)n));
	nfill = pow(2, height);
}


int main()
{
	freopen("output.txt","r",stdin);
	int q;
	cin >> n >> q;
	pre();
	for (int i = n+1; i <= nfill; ++i)
	{
		arr[i] = INT_MAX;
	}
	cin >> arr[1];
	bool flag = false;
	if(arr[1] == q) flag = true;
	for (int i = 2; i <= n; ++i)
	{
		cin >> arr[i];
		if(arr[i] == 0){
			arr[i] = arr[i-1];
		}
		if(arr[i] == q){
			flag = true;
		}
	}
	for (int i = n-1; i > 0; --i)
	{
		if(arr[i] == 0){
			arr[i] = arr[i+1];
		}
	}
	if(arr[1] == 0){
		flag = true;
		for (int i = 1; i <= n; ++i)
		{
			arr[i] = q;
		}
	}
	if(flag != true){
		cout << "NO\n";
		return 0;
	}
	unordered_map<int, int> lm, rm;
	for (int i = 1; i <= n; ++i)
	{
		if(!lm[arr[i]]){
			lm[arr[i]] = i;
		}
		rm[arr[i]] = i;
	}
	b();
	for(unordered_map<int,int> :: iterator i = lm.begin(), j = rm.begin(); i!=lm.end(); i++, j++){
		int temp = f(i->second, j->second);
		if(i->first > temp){
			cout << "NO\n";
			exit(0);
		}
	}
	cout << "YES\n";
	for (int i = 1; i <= n; ++i)
	{
		cout << arr[i] << " ";
	}
	return 0;
}