#include <bits/stdc++.h>
 
using namespace std;
 
int dp[205][205], dp1[205], n, x, y, a, K;
vector <int> v[205];
 
void solve (int nd, int pd){
 
     for (int i = 0; i < (int)v[nd].size (); ++i){
          int newn = v[nd][i];
          if (newn == pd)
              continue;
 
          solve (newn, nd);
 
          fill (dp1, dp1+n+1, 0);
 
          for (int j = 0; j <= n; ++j)
               for (int k = 0; k <= n; ++k)
                    if (j+k+1 > K)
                        dp1[min(k+1, j)] = max (dp1[min (k+1, j)], dp[nd][j] + dp[newn][k]);
 
          for (int j = 0; j <= n; ++j)
               dp[nd][j] = max (dp[nd][j], dp1[j]);
 
       /*   printf ("%d %d\n", nd, newn);
          for (int j = 0; j <= n; ++j)
               printf ("%d ", dp[nd][j]);
          printf("---------------\n");*/
     }
}
 
int main(){
 
    scanf ("%d%d", &n, &K);
    for (int i = 1; i <= n; ++i)
         scanf ("%d", &dp[i][0]);
 
    for (int i = 1; i < n; ++i){
         scanf ("%d%d", &x, &y);
         v[x].push_back (y);
         v[y].push_back (x);
    }
 
    solve (1, -1);
 
    int sol = -1;
    for (int i = 0; i <= n; ++i)
         sol = max (sol, dp[1][i]);
 
    printf ("%d\n", sol);
 
    return 0;
}