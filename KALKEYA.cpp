#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 1; i <= n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 300005

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int t;
	cin >> t;
	while(t--){
		int n, k;
		cin >> n >> k;
		cout << n << " " << k << endl;
		int arr[n+1];
		map<int,int> mm;
		for(int i = 1; i <= n; i++) arr[i] = i;
		do{
			bool flag = false;
			for(int i = 1;  i <= n; i++){
				if(abs(arr[i]-i)<k){
					flag = true;
					break;
				}
			}
			if(!flag){
				//printarr(arr, n);
				int num = 0;
				for(int i = n; i >= 1; i--){
					num = num + pow(10,(n-i))*arr[i];
				}
				mm[num] = 1;
			}

		}while(next_permutation(arr+1, arr+n+1));
		for(map<int, int>::iterator itr =  mm.begin(); itr!=mm.end(); itr++){
			cout << itr->first << "\n";
			break;
		}
		
		cout << "ENDOFTEST "<< t << "\n\n\n";
	}





















	



	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}