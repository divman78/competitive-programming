
// // A simple C++ program to show working of getline 
// #include <bits/stdc++.h> 
// using namespace std; 
// int main() 
// { 
// 	freopen("input.txt","r",stdin);
//     string str; 
//     int t = 4; 
//     while (t--) 
//     { 
//         // Read a line from standard input in str 
//         getline(cin, str); 
//         cout << str << " : newline" << endl; 
//     } 
//     return 0; 
// } 

// C++ program to split 
// string into substrings 
// which are separated by 
// separater using boost::split 
  
// this header file contains boost::split function 
#include <bits/stdc++.h> 
#include <boost/algorithm/string.hpp> 
using namespace std; 
  
int main() 
{ 
	freopen("input.txt","r",stdin);
    string input("geeks\tfor\tgeeks"); 
    vector<string> result; 
    boost::split(result, input, boost::is_any_of("\t")); 
  
    for (int i = 0; i < result.size(); i++) 
        cout << result[i] << endl; 
    return 0; 
} 