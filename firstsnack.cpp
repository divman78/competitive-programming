#include<bits/stdc++.h>
using namespace std;
#define endl "\n"
#define N 100005


int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		string strings[n];
		for(int i = 0; i < n; i++)
			cin >> strings[i];
		map<string,int> mm;
		int ans = 0;
		for(int i = 0; i < n; i++){
			if(mm[strings[i]]){
				ans += mm[strings[i]]/2;
			}
			else{
				int df = 1;
				if(strings[i][0] == 'j' || strings[i][0] == 'k'){
					df = 0;
				}
				int temp = 2;
				for(int j = 1; j < strings[i].size(); j++){
					int xx = ((df == 1 && (strings[i][j] == 'd' || strings[i][j] == 'f')) ||(df == 0 && (strings[i][j] == 'j' || strings[i][j] == 'k'))) ? 4 : 2;
					temp += xx;
					if(strings[i][j] == 'j' || strings[i][j] == 'k'){
						df = 0;		
					}
					else{
						df = 1;
					}
				}
				mm[strings[i]] = temp;
				ans += temp;
			}
			//cout << mm[strings[i]] << " " << ans << endl;
		}
		cout << ans << endl;
	}
}