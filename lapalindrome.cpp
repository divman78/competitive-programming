#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		string s;
		cin >> s;
		int f1[26], f2[26] ;
		for (int i = 0; i < 26; ++i)
		{
			f1[i] = 0;
			f2[i] = 0;
		}
		int n = s.length();
		int x = n/2;
		if(n&1){
			x = n/2 + 1;
		}
		for (int i = 0; i < n/2; ++i)
		{
			f1[s[i]-'a']++;
		}
		for (int i = x; i < n; ++i)
		{
			f2[s[i]-'a']++;
		}
		int flag = 0;
		for (int i = 0; i < 26; ++i)
		{
			if(f1[i] != f2[i]){
				flag = 1;
				break;
			}		
		}
		if(flag){
			cout << "NO\n";
		}
		else{
			cout << "YES\n";
		}

	}
}