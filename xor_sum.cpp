#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MOD 1000000007
#define N 100100
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define NO cout << "NO\n"; return 0;
#define ll long long
#define bits 64
#define mid ((l+r) >> 1)
#define left (2*id+1)
#define right (2*id+2)

struct counter{
	int num_ones, num_zeros;
};


int fact[N];
int arr[N];
int n;
counter seg[4*N][bits];
int pw[N];

ll bigMOD(ll num,ll n){
  if(n==0) return 1;
  ll x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}

ll MODinverse(ll num){
  return bigMOD(num,MOD-2)%MOD;
}

int ncr(int num, int r){
	if(r > num) return 0;
	return ((fact[num]*(MODinverse(fact[num-r]))%MOD)*MODinverse(fact[r]))%MOD;
}

int isKthBitSet(int num, int k) 
{ 
    return ((num >> (k - 1)) & 1) ;
} 

void build(int l=0, int r=n-1, int id=0){
	if(l < r) return;
	if(l == r){
		for (int i = 0; i < bits; ++i) seg[id][i].num_ones = isKthBitSet(arr[l], i), seg[id][i].num_zeros = 1 - isKthBitSet(arr[l], i); 
	}
    build(l, mid, left);
    build(mid+1, r, right);
    for (int i = 0; i < bits; ++i) seg[id][i].num_ones = seg[left][i].num_ones + seg[right][i].num_ones, seg[id][i].num_zeros = seg[left][i].num_zeros + seg[right][i].num_zeros; 
}


counter[] query(int l, int r, int x, int y, int id){
	counter ret[bits];
	if(r < x || l > y | l > r) {
		for (int i = 0; i < bits; ++i)
		{
			ret[i].num_ones = 0;
			ret[i].num_zeros = 0;
		}
		return ret;
	}
	if(l >= x && r <= y){
		return seg[id];
	}
	ret = query(l, mid, x, y, left);
	counter temp[bits] = query(l, mid+1, x, y, right);
	for (int i = 0; i < bits; ++i) ret[i].num_ones += temp[i].num_ones, ret[i].num_zeros += temp[i].num_zeros;
	return ret; 
}


int calc_ans(int l, int r){
	counter val[bits] = query(0, n-1, l, r, 0);

	int carry = 0;
	int ans = 0;
	for (int i = 0; i < bits; ++i)
	{
		int o = val[i].num_ones;
		int z = val[i].num_zeros;
		int temp = ((o*((z*(z-1))/2)) + ncr(o,3))%MOD;
		ans = ans+(temp*pw[i]);  
	}

	return ans;


}




signed main(){
	fact[0] = 1;
	for(int i = 1; i < N; i++){
		fact[i] = (i*fact[i-1])%MOD;
	}

	pw[0] = 1;
	for(int i = 1; i < N; i++){
		pw[i] = (2*pw[i-1])%MOD;
	}

	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif


	cin >> n;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}
	int q, dum;
	cin >> q >> dum;

	build();

	while(q--){
		int l, r;
		cin >> l >> r;
		l--, r--;
		int ans = calc_ans(l, r);
		cout << ans << endl;
	}



	




























	return 0;
}




