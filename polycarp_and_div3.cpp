#include<bits/stdc++.h>
using namespace std;

int digsum(int dig){
	int sum = 0;
	while(dig){
		sum += dig%10;
		dig/=10;
	}
	if(sum/10){
		return digsum(sum);
	}
	return sum;
}

bool valid(int n){
	return n == 0 || n == 3 || n == 6 || n == 9;
}

int main(){
	//freopen("input.txt","r",stdin);
	int t = 1;
	//cin >> t;
	while(t--){
		string s;
		cin >> s;
		// cout << "s  = " << s << endl;
		int n = s.length();
		// cout << "n = " << n << endl;
		int arr[n];
		for (int i = 0; i < n; ++i)
			arr[i] = (s[i]-48);
		int pivot = 0;
		int ans = 0;
		for(int i = 0; i  < n; i++){
			int sum = 0;
			// cout << i << " -> ";
			for(int j = i; j >= pivot; j--){
				sum += arr[j];
				// cout << sum << " ";
				if(valid(digsum(sum))){
					// cout << pivot+1 << " ";
					pivot = i+1;
					ans++;
				}
			}
			// cout << endl;
		}
		//cout << endl << "ans = ";
		cout << ans  << endl;


	}





}