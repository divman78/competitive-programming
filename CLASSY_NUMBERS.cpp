#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 300005
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define DIG 18

int buf[DIG];
vector<int>  v;
//int counter = 0;
void preprocess(int n, int cnt, int num){
	//counter++;
	// cout << n <<" " << cnt << endl;
	if(n == 0){
		v.push_back(num);
		return;
	}
	preprocess(n-1, cnt, num*10);
	if(cnt < 3){
		for(int i = 1; i <= 9; i++){
			preprocess(n-1, cnt+1, num*10 + i);
		}
	}
}
//2298231
//2905651
signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int t=1;
	cin >> t; 
	preprocess(DIG, 0, 0);
	sort(v.begin(), v.end());
	v.push_back((int)1e18);
	//printarr(v, v.size());
	//cout << counter << endl;
	while(t--){
		int n1, n2;
		cin >> n1 >> n2;
		int y = upper_bound(v.begin(), v.end(), n2)-v.begin();
		int x = lower_bound(v.begin(), v.end(), n1)-v.begin();
		cout << y-x << endl;

        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}