#include<bits/stdc++.h>
using namespace std;

#define int unsigned long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MOD 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

int bigMOD(int num,int n){
  if(n==0) return 1;
  int x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	cin >> t; 
	while(t--){

		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}

		if(n == 1){
			cout << arr[0] << endl;
			continue;
		}

		int sum[n] ={0};
		sum[0] = arr[0];
		sum[1] = (sum[0]+(arr[1]*arr[0])%MOD+arr[0]+arr[1])%MOD;

		int ans[n] = {0};
		ans[0] = arr[0];
		ans[1] = ((arr[1]*arr[0])%MOD+arr[0]+arr[1])%MOD;

		int sum2 = (arr[0]*arr[1])%MOD;

		for (int i = 2; i < n; ++i)
		{
			ans[i] = (((ans[i-1] + (arr[i]*bigMOD(2,i-1))%MOD)%MOD) +  ((((sum[i-2] + (sum2*arr[i])%MOD)%MOD) + ((((arr[i-1]*bigMOD(2,i-2))%MOD)%MOD)*arr[i])%MOD)%MOD)%MOD)%MOD;
			sum[i] = (sum[i-1]+ans[i])%MOD;
			sum2 = (((sum2*arr[i])%MOD) + (arr[i-1]*((bigMOD(2,i-2)*arr[i])%MOD))%MOD)%MOD; 
		}

		cout << ans[n-1] << endl; 


        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















