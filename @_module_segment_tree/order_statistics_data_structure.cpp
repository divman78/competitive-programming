//question number of triplets such a[i] < a[j] < a[k] (i < j < k)

#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp> // Common file
#include <ext/pb_ds/tree_policy.hpp>     // Including tree_order_statistics_node_updat

using namespace __gnu_pbds;
using namespace std;

typedef long long ll;
typedef tree<ll, null_type, less<ll>, rb_tree_tag, tree_order_statistics_node_update> ordered_set;

const ll MAXN = 1e6 + 5;
ll N;
ll A[MAXN];
int main() {
    #ifndef ONLINE_JUDGE
    freopen("/home/divakar/programs/input.txt","r",stdin);
    #endif
    ios::sync_with_stdio(0);
    cin.tie(0);
    cin >> N;
    for (ll i = 0; i < N; i++) {
        cin >> A[i];
    }
    ordered_set lset;
    ordered_set rset;
    for (ll i = 1; i < N; i++) {
        rset.insert(A[i]);
    }
    ll ans = 0;
    for (ll i = 0; i < N; i++) {
        ll leftCnt = lset.size() - lset.order_of_key(A[i] + 1);
        ll rightCnt = rset.order_of_key(A[i]);
        ans += leftCnt * rightCnt;
        lset.insert(A[i]);
        rset.erase(A[i + 1]);
    }
    cout << ans << endl;
}