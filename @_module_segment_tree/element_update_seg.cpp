#include<bits/stdc++.h>
using namespace std;
#define N (int)(1<<18)+10
#define left (2*idx+1)
#define right (2*idx+2)
#define mid ((l+r)>>1)
#define int long long

int seg[4*N];
int arr[N];
int n, m;

void build(int idx, int l, int r, int& level){
	if(l > r){
		return;
	}
	if(l == r){
		seg[idx] = arr[l];
		level = 1;
		return;
	}

	build(left, l, mid, level);
	build(right, mid+1, r, level);
	if(level == 1)seg[idx] = seg[left]|seg[right];
	else seg[idx] = seg[left]^seg[right];
	level*=-1;
}

void update(int idx, int l, int r, int udx, int val, int& level){
	if(l>r || l>udx || r<udx){
		return;
	}
	if(l == r){
		seg[idx] = val;
		arr[l] = val;
		level = 1;
		return;
	}
	update(left, l, mid, udx, val, level);
	update(right, mid+1, r, udx, val, level);
	if(level == 1)seg[idx] = seg[left]|seg[right];
	else seg[idx] = seg[left]^seg[right];
	level*=-1;
}


signed main(){
	freopen("/home/divakar/programs/input.txt","r",stdin);
	cin >> n >> m;
	int level=1;
	n = 1<<n;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}

	build(0,0,n-1,level);

	while(m--){
		int p, val;
		cin >> p >> val;
		update(0, 0, n-1, p-1, val,level);
		cout << seg[0] << "\n";
	}

}