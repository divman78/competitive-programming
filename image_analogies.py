from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import sys
import math
from nearpy import Engine
from nearpy.hashes import RandomBinaryProjections

A1 = cv2.imread('./images/blurA1.jpg')
A2 = cv2.imread('./images/blurA2.jpg')
B1 = cv2.imread('./images/blurB1.jpg')

# A1 = cv2.cvtColor(A1, cv2.COLOR_BGR2YUV)
# A2 = cv2.cvtColor(A1, cv2.COLOR_BGR2YUV)
# B1 = cv2.cvtColor(A1, cv2.COLOR_BGR2YUV)

BIG = 5
SMALL = 3
NUM_FEATURES = 3
KAPPA = .5
NNF = 

NNF = BIG * BIG * 3
nnf = SMALL * SMALL * NUM_FEATURES



def image_pyr(A1, A2, B1, s):
	(h, w, _) = A1.shape;
	A1_pyramid = [A1]
	A2_pyramid = [A2]
	B1_pyramid = [B1]
	s_pyramid = [s]

	while(h >= 50 and w >= 50):
	    A1 = cv2.pyrDown(A1)
	    A2 = cv2.pyrDown(A2)
	    B1 = cv2.pyrDown(B1)
	    s = cv2.pyrDown(s)
	    A1_pyramid.append(A1)
	    A2_pyramid.append(A2)
	    B1_pyramid.append(B1)
	    s_pyramid.append(s)
	    (h, w, _) = A1.shape
	return (A1, A2, B1, s)

def extend_Image(X, bordersize):
	row, col= X.shape[:2]
	bottom= X[row-2:row, 0:col]
	mean= cv2.mean(bottom)[0]

	border=cv2.copyMakeBorder(X, top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[mean,mean,mean] )
	return border



def concat_feature(X1_pyramid, X2_pyramid, l, i, j, L):
	F = [0]*(NNF*2 + nnf*2)




def best_coherence_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, l, L, i, j):
	(A_h, Aw, _) = A1_pyramid[l].shape
	border_big = math.floor(BIG/2)

	F_q = concat_feature(B1_pyramid, B2_pyramid, l, i ,j ,L)

	min_dist = inf

	r_star_i = -1
	r_star_j = -1

	done = False

	for ii in (i-border_big,i+border_big+1):
		for jj in (j-border_big,j+border_big+1):
			if ii == i and jj == j:
				done = True
				break
			s_i = s_pyramid[l][ii][jj][0]
			s_j = s_pyramid[l][ii][jj][1]

			F_sr_i = s_i + (i-ii)
			F_sr_j = s_j + (j-jj)

			if (F_sr_i >= A_h or F_sr_i < 0 or F_sr_j >= Aw or F_sr_j < 0):
				continue

			F_sr = concat_feature(A1_pyramid, A2_pyramid, l, F_sr_i, F_sr_j, L)

			dist = sum(np.power(F_sr-F_q),2)

			if(dist < min_dist):
				min_dist = dist
				r_star_i = ii
				r_star_j = jj
				best_coh_i = F_sr_i
				best_coh_j = F_sr_j 

		if(done):
			break
	if(r_star_i == -1 or r_star_j == -1):
		best_coh_i = -1
		best_coh_j = -1


	return (best_coh_i, best_coh_j)


def best_approximate_match(A1_features, A1_pyramid, B1_pyramid, B1_features, l, i, j):
	# # Dimension of our vector space
	# dimension = len(A1_features[l][0][0])
	# print("dim00")
	# print(dimension)

	# # Create a random binary hash with 10 bits
	# rbp = RandomBinaryProjections('rbp', 10)

	# # Create engine with pipeline configuration
	# engine = Engine(dimension, lshashes=[rbp])

	# # Index 1000000 random vectors (set their data to a unique string)
	# print(l)
	# print(len(A1_pyramid))
	# size_A = A1_pyramid[l].shape

	# for ii in range(size_A[0]):
	# 	for jj in range(size_A[1]):
	# 		v = np.asarray(A1_features[l][ii][jj])
	# 		#if(ii == 0 and jj == 0):
	# 		#	print(A1_features[l][ii][jj])
	# 		index = ii*size_A[0]+jj
	# 		engine.store_vector(v, index)

	# query = np.asarray(B1_features[l][i][j])
	# result = engine.neighbours(query)
	# print("intdd")
	# print(result[0])
	# #(_, ind, __) = result[0]
	# ind = 0;



	#best_app_i = ind/size_A[0]
	#best_app_j = ind - size_A[0]*best_app_i



    





	# for index in range(len(A_features(l))):
	#     v = numpy.random.randn(dimension)
	#     engine.store_vector(v, 'data_%d' % index)

	# Create random query vector
	#query = numpy.random.randn(dimension)

	# Get nearest neighbours
	#N = engine.neighbours(query)
	best_app_i = -1
	best_app_j = -1

	return (best_app_i, best_app_j)




def get_indices(i, j, N, h, w):
	border = math.floor(N/2)
	i_top = i-border
	pad_top = 1
	if(i_top < 1):
		start_i = 1
		pad_top = 2 - i_top
	else:
		start_i = i_top
	
	i_bot = i+border
	pad_bot = N
	if i_bot > h:
		pad_bot = N-(i_bot-h)
		end_i = h
	else:
		end_i = i_bot

	j_left = j-border
	pad_left = 1
	if j_left < 1:
		start_j = 1
		pad_left = 2 - j_left
	else:
		start_j = j_left

	j_right = j + border
	pad_right = N
	if j_right > w:
		end_j = w
		pad_right = N - (j_right-w)
	else:
		end_j = j_right


	flag =False

	return (start_i, end_i, start_j, end_j, pad_top, pad_bot, pad_left, pad_right)







	




def concat_feature(X_pyramid, X_prime_pyramid, l, i, j, L):
	X_fine = X_pyramid[l]
	X_prime_fine = X_prime_pyramid[l]
	(x_fine_height, x_fine_width, _) = X_fine.shape

	if(l+l < L):
		X_coarse = X_pyramid[L+1]
		X_prime_coarse = X_prime_pyramid[L+1]
		(x_coarse_height, x_coarse_width, _) = X_coarse.shape

	X_fine_nhood = np.zeros(BIG)
	X_prime_fine_nhood = np.zeros(SMALL)
	X_coarse_nhood = np.zeros(SMALL)

	(x_fine_start_i, x_fine_end_i, x_fine_start_j, x_fine_end_j, pad_top, pad_bot, pad_left, pad_right) = get_indices(i, j, BIG, x_fine_height, x_fine_width)

	X_fine_nhood[pad_top:pad_bot+1, pad_left: pad_right+1] = X_fine[x_fine_start_i:x_fine_end_i+1, x_fine_start_j: x_fine_end_j+1]
	X_fine_nhood = X_fine_nhood * G_BIG

	X_prime_fine_nhood[pad_top:pad_bot+1,pad_left:pad_right+1] = X_prime_fine[x_fine_start_i:x_fine_end_i+1,x_fine_start_j:x_fine_end_j+1]
	X_prime_fine_nhood = np.multiply(X_prime_fine_nhood,G_BIG)

	if(l+1 <= L):
		(x_coarse_start_i, x_coarse_end_i, x_coarse_start_j, x_coarse_end_j, pad_top, pad_bot, pad_left, pad_right, flag) = get_indices(floor(i/2), floor(j/2), N_SMALL, x_coarse_height, x_coarse_width)

	if(flag == false):
		X_coarse_nhood[pad_top: pad_bot+1,pad_left: pad_right+1] = X_prime_coarse[x_coarse_start_i:x_coarse_end_i+1,x_coarse_start_j:x_coarse_end_j+1]
		X_coarse_nhood = np.multiply(X_coarse_nhood, G_SMALL)

		X_prime_coarse_nhood[pad_top:pad_bot+1,pad_left:pad_right+1] = X_prime_coarse[x_coarse_start_i: x_coarse_end_i+1,x_coarse_start_j:x_coarse_end_j+1]
		X_prime_coarse_nhood = np.multiply(X_prime_coarse_nhood, G_SMALL)


	F = [0]*(NNF*2 + nnf*2)

	F[0:NNF-1] = X_fine_nhood.flatten
	temp = X_prime_fine_nhood.transpose()
	temp[0:NNF-1] = temp.flatten()
	temp[12:] = 0
	F[NNF:2*NNF-1] = temp

	if(l+1 <= L):
		F[2*NNF: 2*NNF + nnf] = X_coarse_nhood.flatten()
		F[2*NNF+nnf+1:] = X_prime_coarse_nhood.flatten()

	return F






def best_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, A_features, B_features, l, L, i, j):
	print("hello")
	print(len(A_features))

	(best_app_i, best_app_j) = best_approximate_match(A_features, A1_pyramid, B1_pyramid, B_features, l, i, j)
	(h, w, _) = B1_pyramid[l].shape;
	if(i < 4 or j < 4 or i > h-4 or j > w-4):
		best_i = best_app_i
		best_j = best_app_j
		return (best_i, best_j)

	(best_coh_i, best_coh_j) = best_coherence_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, l, L, i, j)

	if((best_coh_i == -1) or (best_coh_j == -1)):
		best_i = best_app_i
		best_j = best_app_j
		return (best_i, best_j)


	F_p_app = concat_feature(A1_pyramid, A2_pyramid, l, best_app_i, best_app_j, L)
	F_p_coh = concat_feature(A1_pyramid, A2_pyramid, l, best_coh_i, best_coh_j, L)

	F_q = concat_feature(B1_pyramid, B2_pyramid, l, i, j, L)

	d_app = sum(np.power((F_p_app[:] - F_q[:]),2));
	d_coh = sum(np.power((F_p_coh[:] - F_q[:]),2));
	d_app = 10000000000000000000000000000000

	if(d_coh <= d_app * (1 + (2^(l-L))*KAPPA)):
		best_i = best_coh_i
		best_j = best_coh_j
	else:
		best_i = best_app_i
		best_j = best_app_j








def create_image_analogy(A1, A2, B1):
	(hb, wb, _) = B1.shape
	s = np.zeros((hb, wb, 2), dtype = A1.dtype)
	
	(h, w, _) = A1.shape
	A1_pyramid = [A1]
	A2_pyramid = [A2]
	B1_pyramid = [B1]
	B2 = np.zeros(B1.shape, dtype = A1.dtype)
	B2_pyramid = [B2]
	s_pyramid = [s]

	while(h >= 50 and w >= 50):
	    A1 = cv2.pyrDown(A1)
	    A2 = cv2.pyrDown(A2)
	    B1 = cv2.pyrDown(B1)
	    (hb, wb, _) = B1.shape
	    B2 = cv2.pyrDown(B2)
	    s = np.zeros((hb, wb, 2), dtype = B1.dtype)
	    A1_pyramid.append(A1)
	    A2_pyramid.append(A2)
	    B1_pyramid.append(B1)
	    B2_pyramid.append(B2)
	    s_pyramid.append(s)

	    (h, w, _) = A1.shape

	L = len(A1_pyramid)

	print("Ss")
	print(len(s_pyramid))

	A1_pyramid_extend = []
	B1_pyramid_extend = []


	for i in range(0, L):
		A1_pyramid_extend.append(extend_Image(A1_pyramid[i], BIG));
		B1_pyramid_extend.append(extend_Image(B1_pyramid[i], BIG));

	A_features = [None]*L
	B_features = [None]*L

	for i in range(0, L):
		A_l = A1_pyramid_extend[i]
		B_l = B1_pyramid_extend[i]

		(ha, wa, _) = A1_pyramid[i].shape
		(hb, wb, _) = B1_pyramid[i].shape
		print((ha, wa))

		AI_features = [[None]*wa]*ha
		BI_features = [[None]*wa]*ha

		for j in range(0, ha):
			for k in range(0, wa):
				AI_features[j][k] = A_l[j: j + BIG + 1, k: k + BIG + 1].flatten()
				if(i == 0 and j == 0 and k == 0):
					print("AI_features")
					print(AI_features[j][k])
				# if(c):
				# 	print("hello1")
				# 	print(AI_features[j][k])
		A_features[i] = AI_features
		#if(i == 0):
		#	print(A_features)

		for j in range(0, hb):
			for k in range(0, wb):
				BI_features[j][k] = B_l[j: j + BIG + 1, k: k + BIG + 1].flatten()
				# if(True):
				# 	print("AI_features")
				#		print(len(BI_features[j][k]))
				#if(i == 0 and j == 0 and k == 0):

				# 	print("hello2")
				# 	print(BI_features[j][k])
				# 	print("hello3")
		B_features[i] = BI_features


		# print("Hello1")
		# print(len(A_features))
		# print(len(B_features))


	for l in range(L-1, -1, -1):
		B2_l = B2_pyramid[l]
		(height, width, __) = B2_l.shape
		for i in range(0, height):
			for j in range(0, width):
				(best_i, best_j) = best_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, A_features, B_features, l, L, i, j)

				

				B2_pyramid[l][i][j][1] = A2_pyramid[l][best_i][best_j][0]

				B2_pyramid[l][i][j][1] = B2_pyramid[l][i][j][1]
				B2_pyramid[l][i][j][2] = B2_pyramid[l][i][j][2]

				s_pyramid[l][i][j][0] = best_i
				s_pyramid[l][i][j][1] = best_j


	B2 = B2_pyramid[0]













create_image_analogy(A1, A2, B1)

	


# img = extend_Image(A1, 100)
# cv2.imshow('img', img)
# cv2.waitKey(0)








