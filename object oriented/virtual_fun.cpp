#include<bits/stdc++.h>
using namespace std;

class Base{
private:
	int a;
public:
	virtual void fun(){
		cout << "Base fun" << " " << endl;
	}
	void fun1(){
		cout << "Base fun1" << " " << endl;
	}
};

class Child : public Base{
private:
	int a;
public:
	void fun(){
		cout << "Child fun" << " " << endl;
	}
	virtual void fun1(){
		cout << "Child fun1" << " " << endl;
	}
};

int main(){

	Base *ptr = new Child;
	ptr->fun();
	ptr->fun1();

}