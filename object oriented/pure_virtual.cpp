#include<bits/stdc++.h>
using namespace std;

//Descructor should be declared virtual in base class

//variables should be protected in abstract class

class Shape{ //Abstract base class
public:
	virtual void draw()=0;
	void hello(){

	}
	virtual ~Shape(){

	}
};

class Circle : public Shape{
private:
	int a;
public:
	void draw(){
		cout << "Child fun" << " " << endl;
	}
};

int main(){

	Shape *ptr = new Circle;
	ptr->draw();

}