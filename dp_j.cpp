#include<bits/stdc++.h>
using namespace std;
double dp[350][350][350];
int n, cnt[4];
double calc(int cnt1, int cnt2, int cnt3){
	if((cnt1<0||cnt2<0||cnt3<0) || cnt1+cnt2+cnt3==0) return 0;
	if(dp[cnt1][cnt2][cnt3]+1>1e-20) return dp[cnt1][cnt2][cnt3];
	double su = cnt1+cnt2+cnt3+0.0;
	dp[cnt1][cnt2][cnt3] = n/su 
	                       + calc(cnt1-1,cnt2,cnt3)*(cnt1/su) 
	                       + calc(cnt1+1,cnt2-1,cnt3)*(cnt2/su)
	                       + calc(cnt1,cnt2+1,cnt3-1)*(cnt3/su);
	return dp[cnt1][cnt2][cnt3];
}
int main(){
	cin >> n;
	for (int i = 0; i <= n; ++i) for(int j = 0; j <= n; j++) for(int k = 0; k <= n; k++) dp[i][j][k]=-1;
	for (int i = 0; i < n; ++i){
		int x; cin >> x;
		cnt[x]++;
	}
	cout << setprecision(10) << fixed << calc(cnt[1], cnt[2], cnt[3]) << endl;	
}
