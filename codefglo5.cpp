#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}

struct coords{
	int x, y, z, i;
};

bool fun(const coords& c1, const coords& c2){
	return (c1.z>c2.z);
}

bool fun1(const coords& c1, const coords& c2){
	return (c1.y>c2.y);
}

bool fun2(const coords& c1, const coords& c2){
	return (c1.x>c2.x);
}

signed main(){
	init();

	//TEST_CASES
	{
		int n;
		cin >> n;
		coords cr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> cr[i].x >> cr[i].y >> cr[i].z;
			cr[i].i = i;
		}

		sort(cr, cr+n, fun);

		for (int i = 0; i < n;)
		{                                            	
			int j = i;
			while(j < n-1 && cr[j+1].z==cr[j].z){
				j++;
			}
			if((j-i+1)&1){
				if(j+1<n){
					cr[j].z=cr[j+1].z;
				}
				j--;
			}
			sort(cr+i, cr+j+1, fun1);

			for(int k = i; k <= j;)
			{
				int l = k;
				while(l<=j-1 && cr[l+1].y==cr[l].y){
					l++;
				}
     			if((l-k+1)&1){
	    			if(l+1<n){
		    			cr[l].y=cr[l+1].y;
			    	}
				    l--;
			    }
				sort(cr+k, cr+l+1, fun2);
				k = l+1;

			}
			//cout << i << " " << j << endl;
			i = j+1;
		}

		// for (int i = 0; i < n; ++i)
		// {
		// 	cout << cr[i].x << " " << cr[i].y << " " << cr[i].z << endl;
		// }
		for (int i = 0; i < n; ++i)
		{
			cout << cr[i].i+1 ;
			if(i&1)cout << endl;
			else cout << " ";
		}
	}
	return 0;
}