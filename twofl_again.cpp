#include<bits/stdc++.h>
using namespace std;
#define N 2200
#define VAL (int)(1e6) + 100

struct info{
    int value;
    int count;
    unordered_map<int,int> neighbours;
};

int n, m;
int arr[N][N];
int merge_arr[N][N];
int visited[N][N];

bool valid_index(int x, int y){
    return (x < n && x >= 0 && y < m && y >= 0);
}

int hsh(int x, int y){
    return (x) * (n+1) + y; 
}

void input_arr(){
    for(int i = 0; i < n ; i++){
        for(int j = 0; j  < m ; j++){
            cin >> arr[i][j];
        }
    }
}

void output_arr(){
    for(int i = 0; i < n ; i++){
        for(int j = 0; j  < m ; j++){
            cout << arr[i][j] << " ";
        }
        cout<<endl;
    }
}

void merge(int i,int j, info& info_, int ind, unordered_map<int, info>& index2info, unordered_map<int,vector<int> >& val2index){
    //cout<<"merge"<<endl;
    //cout<<i <<" "<<j<<endl;
    if(!valid_index(i, j)){
        return;
    }
    if(visited[i][j]){
        if(arr[i][j] != info_.value){
        }
        return;
    }
    if(arr[i][j] != info_.value){
        return;
    }
    visited[i][j] = true;
    merge_arr[i][j] = ind;
    
    info_.count++;
    merge(i + 1, j, info_, ind, index2info, val2index);
    merge(i - 1, j, info_, ind, index2info, val2index);
    merge(i, j + 1, info_, ind, index2info, val2index);
    merge(i, j - 1, info_, ind, index2info, val2index);
}

void merge2(int i,int j, info& info_, int ind, unordered_map<int, info>& index2info, unordered_map<int,vector<int> >& val2index){
    if(!valid_index(i, j)){
        return;
    }
    if(visited[i][j]){
        if(arr[i][j] != info_.value){
            info_.neighbours[merge_arr[i][j]] = 1;    
        }
        return;
    }
    if(arr[i][j] != info_.value){
        info_.neighbours[merge_arr[i][j]] = 1;
        return;
    }
    visited[i][j] = true;
    
    merge2(i + 1, j, info_, ind, index2info, val2index);
    merge2(i - 1, j, info_, ind, index2info, val2index);
    merge2(i, j + 1, info_, ind, index2info, val2index);
    merge2(i, j - 1, info_, ind, index2info, val2index);
}


void merge_util(unordered_map<int, info>& index2info, unordered_map<int,vector<int> >& val2index){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            if(!visited[i][j]){
                //cout <<hsh(i,j)<<"\n";
                index2info[hsh(i,j)].value = arr[i][j];
                index2info[hsh(i,j)].count = 0;
                merge(i, j, index2info[hsh(i,j)], hsh(i,j), index2info,val2index);
                val2index[arr[i][j]].push_back(hsh(i,j));
            }
        }
    }
    for(int i = 0; i < n ; i++){
        for(int j = 0 ; j < m; j++){
            visited[i][j] = false;
        }
    }
    for(int i = 0; i < n; i++){
        for(int j = 0; j < m; j++){
            if(!visited[i][j]){
                merge2(i, j, index2info[hsh(i,j)], hsh(i,j), index2info,val2index);
            }
        }
    }
    
}


void dfs(unordered_map<int, info>& index2info, unordered_map<int,vector<int> >& val2index, unordered_map<int, int>& mark, int val1, int val2,int index, int& temp){
    for(unordered_map<int,int>::iterator i = index2info[index].neighbours.begin(); i != index2info[index].neighbours.end(); i++){
        if((index2info[i->first].value == val1 || index2info[i->first].value == val2) && mark[i->first] != 1 ){
            mark[i -> first] = 1;
            //cout << "hello"<<endl;
            temp += index2info[i->first].count;
            dfs(index2info, val2index,mark, val1, val2, i -> first, temp);
        }
    }
}

void driver(){
    unordered_map<int, info> index2info;
    unordered_map<int,vector<int> > val2index;
    for(int i = 0; i < n ; i++){
        for(int j = 0 ; j < m; j++){
            visited[i][j] = false;
        }
    }
    merge_util(index2info, val2index);
    //cout<<endl;
    // for(int i = 0; i < n ; i++){
    //     for(int j = 0 ; j < m; j++){
    //         cout<<merge_arr[i][j]<<" ";
    //     }
    //     cout<<endl;
    // }
    
    int ans = 0;
    int temp, temp2 , temp3;
    for(unordered_map<int, vector<int> >::iterator i = val2index.begin(); i != val2index.end(); i++){
        temp = temp2 = temp3 = 0;
        for(int j = 0; j  < i -> second.size(); j++){
            temp = index2info[i->second[j]].count;
            //cout<<i->second[j]<<" "<<temp<<endl;
            for(unordered_map<int, int>::iterator k = index2info[i->second[j]].neighbours.begin(); k != index2info[i->second[j]].neighbours.end(); k++){
                if(i ->  first < index2info[k -> first].value)
                {
                    //cout << i -> first <<" " << i -> second[j] <<" "<< k -> first <<endl;
                    temp2 = index2info[k -> first].count;
                    temp3 = 0;
                    unordered_map<int,int> mark;
                    mark[i -> second[j]] = 1;
                    mark[k ->first] = 1;
                    dfs(index2info, val2index, mark, i->first,index2info[k -> first].value,k -> first, temp3);
                    //cout <<i -> second[j] <<" "<< k -> first <<" "<< temp3 <<endl;
                    ans = max(temp + temp2 + temp3, ans);
                }
            }
            ans = max(ans, temp);
        }
    }
    cout << ans << endl;
}

int main(){
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    //freopen("input.txt","r",stdin);
    //freopen("output.txt","w",stdout);
    int t;
    t = 1;
    //cin >> t;
    while(t--){
        
        cin >> n >> m;
        input_arr();
        //output_arr();
        driver();
    }
}