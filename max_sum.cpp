#include<bits/stdc++.h>
using namespace std;
vector<int> FindMaxSum(int arr[], int n)
{
  vector<int> temp1,temp2,temp3;
  int incl = arr[0];
  int excl = 0;
  int excl_new;
  temp1.push_back(0);
 
  for (int i = 1; i < n; i++)
  {
     /* current max excluding i */
    if(incl > excl)
    {
      temp3 = temp1;
    }
    else
    {
      temp3 = temp2;
    }
    excl_new = max(incl,excl);
 
     /* current max including i */
     incl = excl + arr[i];
     temp1 = temp2;
     temp1.push_back(i);
     excl = excl_new;
     temp2 = temp3;
  }
 
   /* return max of incl and excl */
  if(incl > excl)
    {
      return temp1;
    }
    else
    {
      return temp2;
    } 
}
 
/* Driver program to test above function */
int main()
{
  int arr[] = {5,7,7};
  int n = sizeof(arr) / sizeof(arr[0]);
  vector<int> v =  FindMaxSum(arr, n);
  for (int i = 0; i < v.size(); ++i)
  {
    cout<<v[i]<<" ";
  }
}