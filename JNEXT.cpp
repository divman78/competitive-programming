#include<bits/stdc++.h>
using namespace std;

int main(){
	freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}
		int flag = 0;
		for (int i = n-2; i >= 0; --i)
		{
			if(arr[i] < arr[i+1]){
				swap(arr[i],arr[i+1]);
				sort(arr+i+1,arr+n);
				flag = 1;
				break;
			}
		}
		if(flag){
			for (int i = 0; i < n; ++i)
			{
				cout << arr[i];
			}
			cout << endl;
		}
		else{
			cout << -1;
			cout << endl;
		}
	}
}