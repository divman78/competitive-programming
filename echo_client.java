import java.io.*;
import java.net.*;

public class echo_client{
	public static void main(String args[]){
		try{
			Socket s = new Socket("localhost",8085);

			DataInputStream din = new DataInputStream(s.getInputStream());
			DataOutputStream dout = new DataOutputStream(s.getOutputStream());

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			String str = "";
			while(!str.equals("exit")){
				str = din.readUTF();
				System.out.println("server: " + str);
				str = br.readLine();
				dout.writeUTF(str);
				dout.flush();
			}
			dout.close();
			s.close();
		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
}