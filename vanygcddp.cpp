#include<bits/stdc++.h>
using namespace std;
#define MAX 100 // arr[i] between 1 to 100
#define MOD 1000000007
int main(){
	int n;
	cin >> n;
	int arr[n+1];

	int dp[n+1][MAX+1];
	memset(dp, 0, sizeof(dp));

	for (int i = 1; i <= n ; ++i)
	{
		cin >> arr[i];
		dp[i][arr[i]] = 1;
	}

    int res = 0;
	for (int i = 1; i <= n ; ++i)
	{
		for (int j = i - 1; j >= 1; --j)
		{
			if(arr[i] > arr[j]){
				for (int k = 1; k <= MAX; ++k)
				{
					int g = __gcd(arr[i],k);
					dp[i][g] = (dp[i][g] + dp[j][k]) % MOD;
				}
			}
		}

		res = (res + dp[i][1]) % MOD;

	}

	cout << res << endl;

	return 0;
}