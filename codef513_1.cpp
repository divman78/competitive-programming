#include <bits/stdc++.h>
using namespace std;
#define int long long
#define mp(xx, yy) make_pair(xx, yy)
#define pp pair<int, int>

bool cmp(const pp &l,const pp &r) {
    return l.first < r.first;
}


signed main(){
	ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_JUDGE
//	freopen("input.txt","r",stdin);
//	freopen("output1.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	#endif

	int t=1;
	cin >> t;
	while(t--){
	int n,m;
	cin>>n>>m;
	int arr1[n+1], arr2[m+1];
	for (int i = 1; i <= n; ++i)
	{
		cin >> arr1[i];
	}
	for (int i = 1; i <= m; ++i)
	{
		cin >> arr2[i];
	}

	int k;
	cin >> k;

	int pre1[n], pre2[m];
	pre1[0] = 0,  pre2[0] = 0;
	
	//cout << "pre1 - > \n";
	for (int i = 1; i <= n; ++i)
	{
		pre1[i] = arr1[i] + pre1[i-1];
		//cout << pre1[i] << " ";
	}
	//cout << endl;
	//cout << "pre2 - > \n";
	for (int i = 1; i <= m; ++i)
	{
		pre2[i] = arr2[i] + pre2[i-1];
		//cout << pre2[i] << " ";
	}
	//cout << endl;
	//cout << "re -> \n";

	map<int, int> mat2;
	for (int i = 1; i <= m; ++i)
	{
		//cout << i << "\n";
		for (int j = 1; j <= m-i+1; ++j)
		{
			//cout << j  << " " << j + i - 1 << endl;
			mat2[pre2[j+i-1]-pre2[j-1]] = i;
		}
	}
	//cout << endl;
	//cout << "mat2 - > \n";
	vector<pair<int,int> > vec;

	for (map<int, int> :: iterator i = mat2.begin(); i != mat2.end(); ++i)
	{
		//cout << i->first << " " << i->second<< endl;
		vec.push_back(mp(i->first, i->second));
	}
	//cout << endl;

	sort(vec.begin(),vec.end());

	//cout << "vec -----------\n";
	for (int i = 1; i < vec.size(); ++i)
	{
		//cout << vec[i].first <<" -> " << vec[i].second << " ";
		vec[i].second = max(vec[i-1].second, vec[i].second);
	}
	//cout <<endl;
	//cout << "vec -----------\n";
	//cout <<endl;
	int ans = 0;

	for (int i = 1; i <= n; ++i)
	{
		int temp = k/arr1[i];
		//cout << temp << " -> " ;
		{
			int p = lower_bound(vec.begin(), vec.end(), make_pair(temp, 0), cmp)-vec.begin();
			if(p == vec.size()) p = vec.size()-1;
			else if(vec[p].first == temp);
			else if(p  == 0) continue;
			else p--;
			ans = max(ans, vec[p].second); 
			//cout << p << " - > ";
		}

		//cout << ans  << "\n";
	}
	//cout << endl;
	//cout << "final-> \n";
	for (int i = 1; i < n; ++i)
	{
		for (int j = i+1; j <= n; ++j)
		{
			int temp = k/(pre1[j]-pre1[i-1]);
			//cout << temp << " -> ";
			{
				int p = lower_bound(vec.begin(), vec.end(), make_pair(temp, 0), cmp)-vec.begin();
				if(p == vec.size()) p = vec.size()-1;
				else if(vec[p].first == temp);
				else if(p  == 0) continue;
				else p--;
				ans = max(ans, vec[p].second*(j-i+1)); 
			}
			//cout << ans  << " ";
		}

		//cout << endl;
	}
	//cout << "ans -->  \n";
	cout << ans;
	cout << endl;
    }
}