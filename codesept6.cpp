#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","w",stdout);
	int t;
	cin >> t;
	while(t--){
		long long n, m, x, y;
		cin >> n >> m >> x >> y;
		n--, m--;
		if((n == 1LL && m == 1LL)){
			cout << "Chefirnemo" << endl;
			continue;
		}
		n -= (x*(n/x));
		m -= (y*(m/y)); 
		if((n == 0LL && m == 0LL) || (n-1LL == 0LL && m-1LL == 0LL)){
			cout << "Chefirnemo" << endl;
		}
		else{
			cout << "Pofik" << endl;
		}
	}
}