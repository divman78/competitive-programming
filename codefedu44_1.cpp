#include<bits/stdc++.h>
using namespace std;


 main(){
	long long n;
	cin >> n;
	long long k, l;
	cin >> k >> l;
	long long arr[n*k+1];
	for (long long i = 1; i <= n*k; ++i)
	{
		cin >> arr[i];
	}

	sort(arr+1, arr+n*k+1);
	long long mini = arr[1];
	long long co = l+arr[1];
	multiset<long long> ss;
	long long ans = 0;
	for (long long i = 1; i <= n*k; ++i)
	{
		if((i-1)%k == 0){
			if(arr[i] > co){
				if(ss.empty()){
					cout << 0 << endl; exit(0);
				}
				ans += abs(*ss.begin());
				ss.erase(*ss.begin());
			}
			else{
				ans += arr[i];
			}
		}

		else if(arr[i] <= co){
			ss.insert(-arr[i]);
		}
	}
	cout << ans << endl;

}