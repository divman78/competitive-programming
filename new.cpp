#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 50010
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;


void preprocess(){
	
}

int n, a, b;
mii m[N];
mii mm;
void rec(int n, int marks){
	//cout << n << " " << marks << endl;
	if(n == 0){
		mm[marks] = 1;
		return;
	}
	if(m[n][marks]){
		return;
	}
	m[n][marks] = 1;
	rec(n-1, marks+a);
	rec(n-1, marks-b);
	rec(n-1, marks);

}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	cin >> t; 
	while(t--){
		cin >> n >> a >> b;
		map<int,int> mm;
		while(n){
			int mx = n*a;
			int mn = -n*b;

			for(int i = mx; i >= mn; i-=(a+b)){
				if(mm[i]){
					break;
				}
				mm[i] = 1;
			}
			n--;
		}
		mm[0] = 1;
		cout << mm.size() << endl;
		
		








        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}