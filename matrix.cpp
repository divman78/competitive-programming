#include <stdio.h>
#include <stack>
#include <map>
#include <string.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <math.h>
#include <vector>
#include <set>
#include <queue>
#include <climits>
#include <unordered_map>
#include <iterator> 
#include <bitset>
#include <complex>
#include <random>
#include <chrono>
using namespace std;
#define ll long long
#define ull unsigned long long
#define ui unsigned int
#define mp make_pair
#define inf32 INT_MAX
#define inf64 LLONG_MAX
#define PI acos(-1)
#define cos45 cos(PI/4)
#define ld long double
#define inf 1000000
#define pii pair<int, int>
#define pll pair<ll, ll>
#define pli pair<ll, int>
#define pil pair<int, ll>
#pragma GCC optimize ("O3")
//#define x first
//#define y second
const int mod = (1e9) + 7;
const double eps = 1e-10;
const int siz = 1e5 + 5, siz2 = 21, lg = 21, block = 317, block2 = 1000, mxv = 1e6;
ll n;
int m, fact[105];
int mul_mod(ll a, ll b, ll mod) {
	return (a*b) % mod;
}
ll add_mod(ll a, ll b, ll mod) {
	return (a + b) % mod;
}
struct matrix {
	vector<vector<int>> mat;
	int r, c;
	matrix(int r, int c, bool idnt) {
		this->r = r;
		this->c = c;
		mat = vector<vector<int>>(r);
		for (int i = 0; i < r; i++) {
			mat[i] = vector<int>(c);
		}
		if (idnt) {
			for (int i = 0; i < r; i++) {
				mat[i][i] = 1;
			}
		}
	}
	matrix* mul(matrix *m2, int mod) {
		int c2 = m2->c;
		matrix *out = new matrix(r, c2, 0);
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c2; j++) {
				int tot = 0;
				for (int k = 0; k < c; k++) {
					tot = add_mod(tot, mul_mod(mat[i][k], m2->mat[k][j], mod), mod);
				}
				out->mat[i][j] = tot;
			}
		}
		return out;
	}
	void cpy(matrix *m2) {
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				mat[i][j] = m2->mat[i][j];
			}
		}
	}
	matrix* exp(ll p, int mod) {
		matrix *tmp, *cur = new matrix(r, r, 0), *out = new matrix(r, r, 1);
		cur->cpy(this);
		while (p > 0) {
			if (p & 1) {
				tmp = out->mul(cur, mod);
				delete out;
				out = tmp;
			}
			tmp = cur->mul(cur, mod);
			delete cur;
			cur = tmp;
			p >>= 1;
		}
		delete cur;
		return out;
	}
};
































































































































































































































































































































































































































































































































































































































































































































































































































































































































