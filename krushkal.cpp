#include<bits/stdc++.h>
using namespace std;
#define pb(x) push_back(x)
#define N (int)2e5 + 20
#define mp(x,y) make_pair(x, y)
#define piii pair<int, pair<int,int> > 

int n, m;
piii adj[N];
int v[N];

int parent(int x){
	int par = x;
	while(v[par]!=par){
		v[par] = v[v[par]];
		par = v[par];
	}
	return par;
}

void merge(int x, int y){
	int parx = parent(x);
	int pary = parent(y);
	v[parx] = pary;
}



long long krushkals_demo(){
	long long mspW = 0;
	for(int i = 1; i <= n; i++){
		v[i] = i;
	}
	int mx = m;
	sort(adj,adj + mx);
	for(int i = 0; i < mx; i++){
		int x = adj[i].second.first, y = adj[i].second.second;
		if(parent(x)!=parent(y)){
			mspW += (long long) adj[i].first;
			merge(x, y);
		}
	}
	return mspW;
}
int main(){
	cin >> n >> m;
	int j = 0;
	for(int i = 0; i < m; i ++){
		int x, y, weight;
		cin >> x >> y >> weight;
		piii p1= mp(weight, mp(x,y));
		adj[j++] = p1;

	}
	long long ans = krushkals_demo();
	cout << ans << endl;
}