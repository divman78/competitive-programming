#include<bits/stdc++.h>
using namespace std;
#define print(Arr, N) for (int i = 1; i <= N; ++i) cout << Arr[i] << " "; cout << endl


int main(){
	int t;
	//cin >>  t;
	t = 1;
	while(t--){
		int n;
		cin >> n;
		int arr[n+1];
		for (int i = 1; i <= n; ++i)
		{
			arr[i] = i;
		}
		for (int i = 1; i < n/2; ++i)
		{
			swap(arr[i],arr[i+1]);
		}
		for (int i = n/2+1; i < n; ++i)
		{
			swap(arr[i],arr[i+1]);
		}
		print(arr,n);
		cout << n << " ";
		for (int i = 1; i <= n-1; ++i)
		{
			cout << i << " ";
		}
		cout << endl;
	}
}