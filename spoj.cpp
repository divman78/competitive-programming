#include<bits/stdc++.h>
using namespace std;
#define mp(x,y) make_pair(x, y)

int findParent(int x, vector<pair<int, int> >& dsu){
	while(x != dsu[x].first){
		dsu[x] = dsu[dsu[x].first];
		x = dsu[x].first;
	}
	return x;
}


void doUnion( int y, int x, vector<pair<int, int> >& dsu){
	int parx = findParent(x, dsu);
	int pary = findParent(y, dsu);
	if(parx != pary){
		dsu[parx].first = pary;
		dsu[pary].second += dsu[parx].second;
	}
}


int main(){
	//freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n; 
		vector<pair<int, int> >  pn(n - 1);
		for (int i = 0; i < n - 1; ++i)
		{
			int p, q;
			cin >> p >> q;
			pn[i]= mp(p - 1, q - 1);
		}
		int q;
		cin >> q;
		vector<pair<int, int> > pq(q);
		int ban[n-1];
		for (int i = 0; i < n - 1; ++i) ban[n] = false;

		for(int i = 0; i < q; i++){
			char ch; 
			cin >> ch;
			if(ch == 'R'){
				int x;
				cin >> x;
				pq[i] = mp(1, x - 1);
				ban[x - 1] = true;

			}
			else {
				pq[i] = mp(0, -1);
			}
		}

		vector<pair<int, int> > dsu(n);
		for(int i = 0; i < n; i++){
			dsu[i].first  = i;
			dsu[i].second = 1;
		}

		for(int i = 0; i < n - 1; i++){
			if(!ban[i]){
				//cout<<"hello " << i << endl;
				doUnion(pn[i].first, pn[i].second, dsu);
			}
		}

		bool visited[n];
		for (int i = 0; i < n; ++i)
		{
			visited[i] = false;
		}

		int ans = 0;

		for (int i = 0; i < n - 1; ++i)
		{
			int pari = findParent(i, dsu);
			if(!visited[pari]){
				visited[pari] = true;
				ans += (dsu[pari].second*(dsu[pari].second - 1))/2;
			}
		}

	    vector<int>  ansArr;
		for(int i = q - 1; i >= 0; i--){
			if(pq[i].first){
				int p = dsu[findParent(dsu[pn[pq[i].second].first].first, dsu)].second;
				int q = dsu[findParent(dsu[pn[pq[i].second].second].first, dsu)].second;
				ans -= ((p* (p - 1))/2 + (q* (q - 1))/2);
				doUnion(pn[pq[i].second].first, pn[pq[i].second].second, dsu);
				ans += (dsu[findParent(dsu[pn[pq[i].second].first].first, dsu)].second * (dsu[findParent(dsu[pn[pq[i].second].first].first, dsu)].second - 1))/2; 
			}
			else{
				ansArr.push_back((n*(n-1))/2 - ans);
			}
		}


		for(int i = ansArr.size() - 1 ;i >= 0; i--){
			cout<< ansArr[i]<<endl;
		}

        cout << endl;
		//cout << "END OF TEST CASE"<<endl;

	}



	return 0;
}