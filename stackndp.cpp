#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define mp(x, y) make_pair(x, y)
#define ff first 
#define ss second 
#define print(arr, n) for (int i = 0; i < n; ++i) // << arr[i] << " "; // << endl
int n, q;
int arr[N];
int dp[N];
struct node
{
	int ind;
	int num;
};

node max(node& s1, node& s2)
{
	if(s1.num == s2.num){
		if(s1.ind < s2.ind){
			return s1;
		}
		else{
			return s2;
		}
	}
	else if(s1.num > s2.num){
		return s1;
	}
	else{
		return s2;
	}
}

node tree[2*N];


void build(){
	for (int i = 0; i < n;++i)
	{
		tree[n+i].num = arr[i];
		tree[n+i].ind = i;
	}
	for (int i = n-1; i > 0 ; --i)
	{
		tree[i] = max(tree[i<<1], tree[i<<1|1]);
	}
}

int query(int x, int y){ // [x, y) zero based indexing
	node res;
	res.num = INT_MIN;
	for (int l = x+n, r = y+n; l < r; l >>= 1 , r >>= 1)
	{
        // << l << " " << r  << endl;		
		if(l&1) res = max(res, tree[l++]);
		if(r&1) res = max(res, tree[--r]);

		//// << l << " " << r  << " "<< res.ind<< " " << tree[r+1].ind <<endl;
	}
	return res.ind;
}

void process(){
	stack<pair<int, int>> sta;
	int par[n];
	par[n-1] = n-1;
	sta.push(mp(arr[n-1], n-1));
	for (int i = n-2; i >= 0; --i)
	{
		while(sta.top().ff <= arr[i]){
			sta.pop();
			if(sta.empty()){
				break;
			}
		}
		if(sta.empty()){
			par[i] = i;
		}
		else {
			par[i]=sta.top().ss;
		}
		sta.push(mp(arr[i], i));
	}

	for (int i = n-1; i >= 0; --i)
	{
		if(par[i] == i){
			dp[i] = 0;
		}
		else{
			dp[i] = dp[par[i]] + 1;
		}
	}
	build();
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	//freopen("input.txt","r",stdin);
	cin >> n >> q;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}
	process();
	int ans = 0;

	while(q--){

		int x, y;
		cin >> x >> y;
		x = (x+ans)%n;
		y = (y+ans)%n;
		if(x > y){
			swap(x, y);
		}
		// <<  x <<" "<< y << endl;
		int l = x;
		int r =  query(x, y+1);	
		// << r << endl;
		ans = (dp[l]-dp[r]) + 1;
		cout << ans << endl;
	}
}




#include <bits/stdc++.h>
// #pragma GCC optimize ("Ofast")
// #pragma GCC target ("sse4")
// #pragma GCC optimize ("unroll-loops")
#define ll          long long
#define ld          long double
#define pb          push_back
#define pii         pair<int,int>
#define vi          vector<int>
#define all(a)      (a).begin(),(a).end()
#define F           first
#define S           second
#define sz(x)       (int)x.size()
#define hell        1000000007
#define endl        '\n'
using namespace std;
#define MAXN		100005
int A[MAXN],nge[20][MAXN];
int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int n,q,i,j,ans=0,l,r,a,b;
	cin>>n>>q;
	for(i=0;i<n;i++)
		cin>>A[i];
	A[n]=INT_MAX;
	stack<int> st;
	for(i=0;i<=n;i++){
		while(!st.empty() and A[st.top()]<A[i]){
			nge[0][st.top()]=i;
			st.pop();
		}
		st.push(i);
	}
	nge[0][n]=n;
	for(i=1;i<20;i++)
		for(j=0;j<=n;j++)
			nge[i][j]=nge[i-1][nge[i-1][j]];
	while(q--){
		cin>>a>>b;
		l=(a+ans)%n;
		r=(b+ans)%n;
		if(l>r)
			swap(l,r);
		ans=1;
		for(i=19;i>=0;i--)
			if(nge[i][l]<=r){
				ans+=(1<<i);
				l=nge[i][l];
			}
		cout<<ans<<endl;
	}
	return 0;
}