#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 260

int adj[N][N];
int ans[N*(N-1)/2];

int dnc(int l, int r){
	if(r-l==1)
		return l;
	int conn1 = dnc(l, (l+r)/2);
	int conn2 = dnc((l+r)/2, r);
	if(adj[conn1][conn2]){
		ans[adj[conn1][conn2]] = 1;
		return conn1;
	}
	ans[adj[conn2][conn1]] = 1;
	return conn2;
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int t;
	cin >> t;
	//t=1;
	while(t--){
		int n;
		cin >> n;
		int num = (n*(n-1))/2;
		memset(adj, 0, sizeof(adj));
		memset(ans, 0, sizeof(ans));
		for(int i = 1; i  <= num; i++){
			int x, y;
			cin >> x >> y;
			adj[x-1][y-1] = i;
		}
		if((n&(n-1))){
			cout << -1 << endl;
			continue;
		}
		dnc(0, n);
		cout << (n - 2) * (n - 1) / 2 << '\n';
		for(int i = 1; i <= num; i++){
			if(!ans[i]){
				cout << i << " ";
			}
		}
		cout << endl;

		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}