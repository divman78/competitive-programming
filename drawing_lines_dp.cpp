#include<bits/stdc++.h>
using namespace std;
#define N 2001
#define INF int(2e9)


signed main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int t;
	cin >> t;
	while(t--){
		int n, k;
		cin >> n >> k;
		vector<vector<int> >v(N);
		for (int i = 0; i < n; ++i)
		{
			int y, sx, ex;
			cin >> y >> sx >> ex;
			ex = (int)(2)*ex;
			v[y].push_back(ex);
		}

		for (int i = 0; i < N; ++i)
		{
			sort(v[i].begin(), v[i].end());
		}


		int fill[N];
		int yfill[N];
		int (*fptr) = fill;
		int (*yptr) = yfill;

		int filln[N];
		int yfilln[N];
		int (*fptrn) = filln;
		int (*yptrn) = yfilln;

		memset(fill, INF, sizeof(fill));
		fill[0] = 0;
		yfill[0] = 0;
		int cur = 0;

		for (int i = 0; i < N; ++i){
			for (int j = 0; j < v[i].size(); ++j){
				for(int k = 0; k <= cur; k++){
					int temp =  fptr[k] + v[i][j] + (i-yptr[k]);
					if(temp  <= fptr[k+j+1]){
						fptrn[k+j+1] = temp;
						yptrn[k+j+1] = i;
					}
					else{
						fptrn[k+j+1] = fptr[k+j+1];
						yptrn[k+j+1] = yptr[k+j+1];
					}
				}
			}

			fill[0] = 0;
			yfill[0] = 0;
			swap(fptr, fptrn);
			swap(yptrn, yptr);
			cur += v[i].size();
		}

		int ans = 0;

		for (int i = N-1; i >= 0; --i)
		{
			if(fill[i]<=k) {ans = i; break;}
		}

		cout << ans << endl;
	}
}