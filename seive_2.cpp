#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define sz 10
int cnt[N];
vector<int> memo[sz];
void seive(){
	for (int i = 2; i < N; ++i)
	{
		if(!cnt[i])
		for(int j = i; j < N; j+=i){
			cnt[j]++;
		}
	}

	for (int i = 2; i < N; ++i)
	{
		if(cnt[i] < sz) memo[cnt[i]].push_back(i);
	}
}

int main(){

	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif

	seive();
	int t;
	cin >> t; 
	while(t--){
		int a, b, k;
		cin >> a >> b >> k;
		cout << upper_bound(memo[k].begin(), memo[k].end(), b)-
		            lower_bound(memo[k].begin(), memo[k].end(), a) << endl;
	}

}