def getaffinewarp(img, tlist, tlistm):
    affineTransform = getaffineTransform(tlist, tlistm)
    img = 255 * np.ones(img_in.shape, dtype = img_in.dtype)
    for i in len(affineTransform): 
        tri1 = tlist[i]
        tri2 = tlistm[i]
        r1 = cv2.boundingRect(tri1)
        r2 = cv2.boundingRect(tri2)
        tri1Cropped = []
        tri2Cropped = []
        img1Cropped = img1[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
        img2Cropped = cv2.warpAffine( img1Cropped, warpMat, (r2[2], r2[3]), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101 )
        mask = np.zeros((r2[3], r2[2], 3), dtype = np.float32)
        cv2.fillConvexPoly(mask, np.int32(tri2Cropped), (1.0, 1.0, 1.0), 16, 0);
        img2Cropped = img2Cropped * mask
        img[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] = img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] * ( (1.0, 1.0, 1.0) - mask )
        img[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] = img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] + img2Cropped
    return img2