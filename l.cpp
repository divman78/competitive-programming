#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}

signed main(){
	init();

	TEST_CASES
	{
		int n;
		cin >> n;

		int x[n], h[n];
		for (int i = 0; i < n; ++i)
		{ 
			cin >> x[i] >> h[i];
		}
		sort(x, x+n);
		sort(h, h+n);

		int ind[n];

		int l = 0, r = n-1;
		int ll = 0, rr = n-1;

		bool flag = true;

		while(l<r){

			if(flag){
				ind[l++] = ll++;
				ind[r--] = ll++;
				flag = false;
			}
			else{
				ind[r--] = rr--;
				ind[l++] = rr--;
				
				flag = true;
			}
		}
		if(l==r) ind[l] = rr;

		for (int i = 0; i < n; ++i)
		{
			cout << ind[i] << " ";
		}
		cout << endl;


		int ans = 0;

		for (int i = 1; i < n; ++i)
		{
			ans += (h[ind[i]]+h[ind[i-1]])*(x[i]-x[i-1]);
		}
		cout << ans << endl;
		
	}


	




	return 0;
}