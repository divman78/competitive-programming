import math
a, b = raw_input().split()

a = int(a)
b = int(b)

lga = math.log(a,2);
lgb = math.log(b,2);

if(lga*b > lgb*a):
	print(">")
elif(lga*b < lgb*a):
	print("<")
else:
	print("=")
