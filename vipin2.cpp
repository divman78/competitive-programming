/*
	Algorithm : BFS
*/
#include<bits/stdc++.h>
#define int long long
#define mx 10000
using namespace std;
map<int,int> mp;
int n,d;
int fun(int n){
	int sum = 0;
	while(n){
		sum += n % 10;
		n /= 10;
	}
	return sum;
}
bool check(){
	for(int i=1;i<=9;i++){
		if(mp[i] == 0){
			return true;
		}
	}
	return false;
}
void solve(int n){
	queue<pair<int,int> > Q;
	Q.push(make_pair(n,1));
	while(!Q.empty()){
		if(!check()){
			break;
		}
		pair<int,int> p = Q.front();
		Q.pop();
		if(p.second >= 20){
			break;
		}
		if(mp[p.first] > p.second || mp[p.first] == 0)
		mp[p.first] = p.second;
		if(p.first > 9){
			Q.push(make_pair(fun(p.first),p.second+1));
		}
		Q.push(make_pair(p.first+d,p.second+1));
	}
}
signed main(){
	int t;
	cin>>t;
	while(t--){
		mp.clear();
		cin>>n>>d;
		solve(n);
		for(int i=1;i<=9;i++){
			if(mp[i] != 0){
				cout<<i<<" "<<mp[i]-1<<endl;
				break;
			}
		}
	}
}