#include<bits/stdc++.h>
using namespace std;


#define LEN1 4
#define LEN2 4
//#define LEN3 ()

vector<uint8_t> m_Value({0x0a,0x8f, 0x01,0xa1, 0xb1, 0x0c, 0xa1, 0xf0});
vector<uint8_t> Msg1Vec;
vector<uint8_t> Msg2Vec;
vector<uint8_t> Msg3Vec;


vector<uint8_t> Msg1(){
	Msg1Vec.resize(LEN1/2,0);
	uint8_t x = (m_Value[0] & 0xf0);
	//printf("******************%x\n",x==0 );
	if( x == 0 ){
		for(int i = 0; i < LEN1/2; i++){
			Msg1Vec[i] += (m_Value[i]&0x0f)<<4;
			Msg1Vec[i] += (m_Value[i+1]&0xf0)>>4;
		}
	}
	else{
		for(int i = 0; i < LEN1/2; i++){
			Msg1Vec[i] = m_Value[i];
		}
	}

}

vector<uint8_t> Msg3(){
	uint8_t x = (m_Value[0] & 0xf0);
	int LEN3 = 2*m_Value.size()-(LEN1+LEN2)-1;
	if( x == 0 ){
		if(LEN3&1){
			Msg3Vec.resize(ceil(LEN3/2.0),0);
			cout << Msg3Vec.size() << endl;
			for(int i = LEN1/2, j = 0; j < Msg3Vec.size(); i++){
				Msg3Vec[j++] += (m_Value[i]&0x0f);
				Msg3Vec[j]   += (m_Value[i+1]&0xf0);
			}
		}
		else{
			Msg3Vec.resize((LEN3/2),0);
			for(int i = LEN1/2, j = 0; j < LEN3/2; i++){
				Msg3Vec[j]   += (m_Value[i]&0x0f)<<4;
				Msg3Vec[j++] += (m_Value[i+1]&0xf0)>>4;
			}
		}
	}
	else{
		for(int i = LEN1/2; i < LEN1/2+LEN3/2; i++){
			Msg3Vec[i] = m_Value[i];
		}
	}

}

vector<uint8_t> Msg2(){
	Msg2Vec.resize(LEN2/2);
	for(int i = m_Value.size()-LEN2/2, j = 0; i < m_Value.size(); i++){
		Msg2Vec[j++] = m_Value[i];
	}
}


int main(){   	
   	for(int i = 0; i < m_Value.size(); i++) {
   		printf("%x\n", m_Value[i]);
   	}
   	cout << "\n\n\n";
   	Msg1();
   	for (int i = 0; i < Msg1Vec.size(); ++i)
   	{
   		printf("%x",Msg1Vec[i]);	
   	}
   	cout << "\n\n\n";
   	Msg2();
   	for (int i = 0; i < Msg2Vec.size(); ++i)
   	{
   		printf("%x",Msg2Vec[i]);	
   	}
   	cout << "\n\n\n";
   	Msg3();
   	for (int i = 0; i < Msg3Vec.size(); ++i)
   	{
   		printf("%x",Msg3Vec[i]);	
   	}
   	cout << endl;
}