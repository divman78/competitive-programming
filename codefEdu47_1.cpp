#include<bits/stdc++.h>
using namespace std;



int main(){
	string s;
	cin >> s;
	string str = "";
	int n = s.length();
	vector<pair<char, int>  >vec;
    int i = 0;
	while(i < n){
		char ch = s[i];
		int cnt = 0;
		while(i < n){
			if(ch != s[i]) break;
			i++, cnt++;
		}
		vec.push_back(make_pair(ch, cnt));
	}

	stack<pair<char, int> > sta;

	for (int i = 0; i < vec.size(); ++i){ 
		//cout << vec[i].first <<" " << vec[i].second << endl;
		if(sta.empty()){
			if(vec[i].first == '0'){
				while(vec[i].second --) str += '0';
			}
			else{
				sta.push(vec[i]);
			}
		}
		else{
			if(sta.top().first == vec[i].first){
				int temp = sta.top().second;
				sta.pop();
				sta.push(make_pair(vec[i].first, vec[i].second + temp));
			}
			else if(sta.top().first - 1 == vec[i].first){
				//cout << "hello \n";
				while(vec[i].second --) str += vec[i].first;
			}
			else if(sta.top().first == '1' && vec[i].first == '2'){
				//cout << "tello\n";
				int temp = sta.top().second;
				//cout <<temp <<  endl;
				while(temp --) str += '1';
				sta.pop();
				sta.push(vec[i]);
			}
			else if(sta.top().first == '2' && vec[i].first == '0'){
				int temp = sta.top().second;
				while(temp --) str += '2';
				sta.pop();
				while(vec[i].second --) str += '0';
			}

		}
		//cout << str << "\n";
		
	}

	if(!sta.empty()){
		int temp = sta.top().second;
		while(temp --) str += sta.top().first;
	}
	cout << str << endl;
}