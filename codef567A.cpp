#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n, m) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 0; j < m; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

int n, m;

struct triplet{
	char a, b, c;
	int sza, szb, szc;
};

bool valid(triplet x){
	return x.a!=' ' && x.b!= ' ' && x.c!=' ' && x.sza != 0 && x.szb != 0 && x.szc != 0 && x.szb <= min(x.sza, x.szc);
}

bool equals(triplet x, triplet y){
	return x.a == y.a && x.b == y.b && x.c == y.c && x.szb == y.szb;
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cin >> t; 
	while(t--){
		
		cin >> n >> m;
		string s[n+2];
		for (int i = 1; i <= n; ++i)
		{
			cin >> s[i];
		}
		char emp[m];
		for(int i = 0; i < m; i++) emp[i] = ' ';
		s[n+1] = emp;
	    s[0] = emp;

		int ch1[n+2][m];
		memset(ch1, 0, sizeof(ch1));
		for (int j = 0; j < m; ++j)
		{
			int cnt = 1;
			for (int i = 1; i <= n; ++i)
			{
				if(s[i][j]!=s[i+1][j]){
					ch1[i][j] = cnt;
					cnt = 1;
				}
				else {
					cnt++;
				}
			}
		}

		int ch2[n+2][m];
		memset(ch2, 0, sizeof(ch2));
		for (int j = 0; j < m; ++j)
		{
			int cnt = 1;
			for (int i = n; i >= 1; --i)
			{
				if(s[i][j]!=s[i-1][j]){
					ch2[i][j] = cnt;
					cnt = 1;
				}
				else {
					cnt++;
				}
			}
		}

		//print2D(ch1, n, m);
		//print2D(ch2, n, m);

		triplet save[n+2][m];

		for (int i = 1; i <= n; ++i)
		{
			for(int j = 0; j < m; j++){
				save[i][j].a = ' ';
				save[i][j].b = ' ';
				save[i][j].c = ' ';
				save[i][j].sza = 0;
				save[i][j].szb = 0;
				save[i][j].szc = 0;
			}
		}



		for (int i = 1; i <= n; ++i)
		{
			for (int j = 0; j < m; ++j)
			{
				if(ch1[i][j]){
					save[i][j].a = s[i-ch1[i][j]][j];
					save[i][j].b = s[i][j];
					save[i][j].c = s[i+1][j];
					save[i][j].sza = ch1[i-ch1[i][j]][j];
					save[i][j].szb = ch1[i][j];
					save[i][j].szc = ch2[i+1][j];
				}

				//cout << save[i][j].a << save[i][j].b << save[i][j].c << " ";
				//cout << save[i][j].sza << save[i][j].szb << save[i][j].szc << " ";
			}
			//cout << endl;
		}

		int ans = 0;

		for (int i = 1; i <= n; ++i)
		{
			int cnt = 0;
			for (int j = 0; j < m; ++j)
			{
				if(ch1[i][j] && valid(save[i][j]) && equals(save[i][j], save[i][j-1])){
					cnt++;
				}
				else if(valid(save[i][j])){
					ans += (cnt*(cnt+1))/2;
					cnt = 1;
				}
				else{
					ans += (cnt*(cnt+1))/2;
					cnt = 0;
				}
			}
			if(cnt){
				ans += (cnt*(cnt+1))/2;
			}
		}

		cout << ans << endl;














		


		

        
		//cout << "ENDOFTEST "<< t << "\n";
	}








	return 0;
}




















