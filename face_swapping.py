from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import sys

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)
 
	# loop over the 68 facial landmarks and convert them
	# to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)
 
	# return the list of (x, y)-coordinates
	return coords


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('/home/divakar/Downloads/shape_predictor_68_face_landmarks.dat')


def initImage(img_name):
    image = cv2.imread('/home/divakar/Desktop/' + img_name)
    image = imutils.resize(image, width=500)
    return image



# Check if a point is inside a rectangle
def rect_contains(rect, point) :
    if point[0] < rect[0] :
        return False
    elif point[1] < rect[1] :
        return False
    elif point[0] > rect[2] :
        return False
    elif point[1] > rect[3] :
        return False
    return True
 
# Draw a point
def draw_point(img, p, color ) :
    cv2.circle( img, p, 2, color, cv2.cv.CV_FILLED, cv2.CV_AA, 0 )
 
 
def sorti(li):
    sum = 0
    for i in range(0,6):
        sum += li[i]
    return sum
# Draw delaunay trianles
def get_delaunay(size, shape, img) :
    
    #cv2.imshow('img',img)
    #cv2.waitKey(100)
    r = (0, 0, size[1], size[0])
    subdivObj = cv2.Subdiv2D(r)
    shape = shape.tolist()
    subdivObj.insert(shape)
    triangleList = subdivObj.getTriangleList()
    triangleList = sorted(triangleList, key = sorti) 
    
    
    tlist = []
    F = 1
 
    for t in triangleList :
         
        pt1 = (t[0], t[1])
        pt2 = (t[2], t[3])
        pt3 = (t[4], t[5])
        F = F+1

        tlist.append([pt1,pt2,pt3])
    #triangleList = []
    #cv2.imshow('img',img)
    #cv2.waitKey(100)
    
    return tlist

def getdelaunayUtil(image, shape):
    size = image.shape  
    tlist = get_delaunay(size, shape, image)
    return tlist





def detectFace(image):
    shapes = []
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector(gray, 1)
    for (i, rect) in enumerate(rects):
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        shapes.append(shape)
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        #cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        #cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        convexHullPts = cv2.convexHull(shape, clockwise = True)

        shape = shape.tolist()

        #shape.append([w+x, h+y])
        #shape.append([x, h+y])
        #shape.append([w+x, y])
        #shape.append([x, y])
        
        shapes.append(shape)
        #print(shape)
    #cv2.imshow('img', image)
    #cv2.waitKey(0)
    return shapes[0]





def morphedImagePoints(img1tlist, img2tlist, alpha):
    mpoints = []
    for i in range(len(img1tlist)):
        triangle = []
        for j in range(0, 3):
            x1 = img1tlist[i][j][0]*(1-alpha) + img2tlist[i][j][0]*(alpha)
            y1 = img1tlist[i][j][1]*(1-alpha) + img2tlist[i][j][1]*(alpha)
            triangle.append([x1, y1])
        mpoints.append(triangle)
    return mpoints

def getaffineTransform(imgtlist, tlistm):
    affineTransform = []
    for i in range(len(imgtlist)):
        dst = cv2.getAffineTransform(imgtlist[i],tlistm[i])
        affineTransform.append(dst)
    return affineTransform

#\begin{equation*} M(x_m,y_m) = (1 - \alpha) I(x_i, y_i) + \alpha J(x_j, y_j) \end{equation*}




def getaffinewarp(img, tlist, tlistm):
    img2 = 255 * np.ones(img.shape, dtype = img.dtype)
    shape = img.shape
    for i in range(len(tlist)): 
        tri1 = tlist[i]
        tri2 = tlistm[i]
        if i == 1:
            cv2.line(img,tri1[0], tri1[1], (255, 0 ,0 ))
            cv2.line(img, tri1[1], tri1[2], (255, 0 ,0 ))
            cv2.line(img, tri1[2], tri1[0], (255, 0 ,0 ))
        
        warpMat = cv2.getAffineTransform(np.float32(tri1),np.float32(tri2))                             
        img2Temp = cv2.warpAffine( img, warpMat, (shape[1], shape[0]), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101 )
        mask = np.zeros((shape[0],shape[1], 3), dtype = np.float32)
        cv2.fillConvexPoly(mask, np.int32(tri1), (1.0, 1.0, 1.0), 16, 0)
        img2 = img2 * ((1.0,1.0, 1.0) - mask)
        img2Temp = img2Temp*mask
        img2 = img2 + img2Temp
    #cv2.imshow('img', img)
    #cv2.waitKey(0)
    return img2




#imagename1 = "Hillary_Clinton_official_Secretary_of_State_portrait_crop.jpg"
#imagename2 = "Donald_Trump_official_portrait.jpg"

imagename1 = "2018-07-12-173127.jpg"
imagename2 = "don1.jpg"

image1 = initImage(imagename1)
image2 = initImage(imagename2)

points1 = detectFace(image1)
points2 = detectFace(image2)

tlist1 = getdelaunayUtil(image1, points1)
tlist2 = getdelaunayUtil(image2, points2)

dict1 = {}
dict2 = {}

for i in range(len(points1)):
    dict1[points1[i]] = i
for i in range(len(points2)):
    dict2[points2[i]] = i

tind1 = []
tind2 = []

for i in range(len(tlist1)):
    tind1.append([dict1[tlist1[i][0]], dict1[tlist1[i][1]], dict1[tlist1[i][2]]])

for i in range(len(tlist2)):
    tind1.append([dict2[tlist1[i][0]], dict2[tlist2[i][1]], dict1[tlist2[i][2]]])






alpha = 0.5


tlistm = morphedImagePoints(tlist1, tlist2, alpha)

imageMat1 = cv2.imread('/home/divakar/Desktop/'+imagename1)
imageMat2 = cv2.imread('/home/divakar/Desktop/'+imagename2)

imgMat1 = 255 * np.ones(image1.shape, dtype = image1.dtype)
imgMat2 = 255 * np.ones(image2.shape, dtype = image2.dtype)

imgg1 = getaffinewarp(image1, tlist1, tlistm)
imgg2 = getaffinewarp(image2, tlist2, tlistm)
#size1 = imgg1.shape
#size2 = imgg2.shape
#print(size1)
#print(size2)
#print(img2)


#img = cv2.addWeighted(imgg1, alpha, imgg2, 1-alpha, 0)

cv2.imshow('img', imgg1)
cv2.waitKey(0)

#print(tlist1)



def getaffinewarp11(img, tlist, tlistm):
    #affineTransform = getaffineTransform(tlist, tlistm)
    img2 = 255 * np.ones(img.shape, dtype = img.dtype)
    for i in range(len(tlist)): 
        tri1 = tlist[i]
        tri2 = tlistm[i]
        r1 = cv2.boundingRect(np.int32(tri1))
        r2 = cv2.boundingRect(np.int32(tri2))
        print(r1, r2)
        tri1Cropped = []
        tri2Cropped = []
        for j in range(0,3):
            tri1Cropped.append([tri1[j][0]-r1[0], tri1[j][1]-r1[1]])
            tri2Cropped.append([tri2[j][0]-r2[0], tri2[j][1]-r2[1]])
        warpMat = cv2.getAffineTransform(np.float32(tri1),np.float32(tri2))
        img1Cropped = img[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
        szz = img1Cropped.shape
        
        print((szz,r2[3], r2[2]))
        img2Cropped = cv2.warpAffine( img1Cropped, warpMat, (r2[2], r2[3]), None, flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_REFLECT_101)
        mask = np.zeros((r2[3], r2[2], 3), dtype = np.float32)
        cv2.fillConvexPoly(mask, np.int32(tri2Cropped), (1.0, 1.0, 1.0), 16, 0)
        img2Cropped = img2Cropped * mask
        #print(img2.shape)
        #print(mask.shape)
        #img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] = img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] * ( (1.0, 1.0, 1.0) - mask )
        #img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] = img2[r2[1]:r2[1]+r2[3], r2[0]:r2[0]+r2[2]] + img2Cropped
        size = img2.shape
        
        #for p in range(max(0,r2[1]), min(size[0],r2[1]+r2[3])):
        #    for q in range(max(0,r2[0]), min(size[1],r2[0]+r2[2])):
        #        #print((p,q))
        #        img2[p][q] = img2Cropped[p-r2[1]][q-r2[0]]
    return img2