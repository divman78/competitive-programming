
#include<bits/stdc++.h>
using namespace std;
int main() 
{ 
    std::vector<int> v{ 1,2,3,4 }; 
  
    // Print vector 
    std::cout << "Vector contains :"; 
    for (unsigned int i = 0; i < v.size(); i++) 
        std::cout << " " << v[i]; 
    std::cout << "\n"; 
  
    std::vector<int>::iterator low1, low2; 
  
    // std :: lower_bound 
    low1 = std::lower_bound(v.begin(), v.end(), 3); 
    low2 = std::upper_bound(v.begin(), v.end(), 3); 
  
    std::cout << "\nlower_bound for element 20 at position : " << (low1 - v.begin()); 
    std::cout << "\nlower_bound for element 55 at position : " << (low2 - v.begin()); 
  
    return 0; 
}