#include<bits/stdc++.h>
using namespace std;
#define N 100005
#define MX 201
#define MO 998244353

int dp[N][MX][2];
int pref[MX][2];
int arr[N];

int main(){
	//freopen("input.txt","r",stdin);
	int n;
	cin >> n;
	for (int i = 1; i <= n; ++i){
		cin >> arr[i];
	}
	if(arr[1]==-1){
		for(int i = 1; i <= MX; i++){
			dp[1][i][0] = 1;
			pref[i][0] = pref[i-1][0] + dp[1][i][0];
		}
	}
	else{
		dp[1][arr[1]][0] = 1;
		for(int i = arr[1]; i <= MX; i++){
			pref[i][0] = 1;
		}
	}

	for(int i = 2; i <= n; i++){
		for(int j = 1; j <= MX; j++){
			if(arr[i] != -1 && arr[i] != j) continue;
			dp[i][j][0] = (pref[j-1][0] + pref[j-1][1])%MO;
			dp[i][j][1] = (((pref[200][1] - pref[j-1][1] + MO)%MO) +
			                 ((pref[j][0] - pref[j-1][0] + MO)%MO))%MO;
		}
		for(int j = 1; j <= MX; j++){
			pref[j][0] = (pref[j-1][0] + dp[i][j][0])%MO;
			pref[j][1] = (pref[j-1][1] + dp[i][j][1])%MO;
		}
	}
	cout << pref[MX-1][1] << endl;


}