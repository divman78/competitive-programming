#include<bits/stdc++.h>
using namespace std;

int main(){
	int t;
	cin >> t;
	while(t--){
		int n, k, q;
		cin >> n >> k >> q;
		int arr[n+2];
		arr[0] = 0;
		for (int i = 1; i <= n; ++i)
		{
			cin >> arr[i];
		}
		arr[n+1] = 0;
		string s;
		cin >> s;
		int prefmax[n+2] = {0}, int pref[n+2] = {0};
		for (int i = 1; i <= n; ++i)
		{
			if(arr[i-1] == 1 && arr[i] == 1){
				pref[i] = pref[i]+1;
				prefmax[i] = max(pref[i], prefmax[i-1]);
			}
		}

		int sufmax[n+2] = {0}, int suf[n+2] = {0};
		for (int i = n; i >= 1; ++i)
		{
			if(arr[i+1] == 1 && arr[i] == 1){
				suf[i] = suf[i]+1;
				sufmax[i] = max(suf[i], sufmax[i+1]);
			}
		}
		int cnt = 0;
		for (int i = 0; i < s.length(); ++i)
		{
			if(s[i] == '!'){
				cnt = (cnt+1)%n;
			}

			else{
				int ans = max(prefmax[n-cnt], sufmax[n-cnt+1])
				if(suf[n-cnt+1] && pref[n-cnt]){
					ans = max(suf[n-cnt+1], pref[n-cnt]);
				}
				cout << ans << endl;
			}
		}

	}
}