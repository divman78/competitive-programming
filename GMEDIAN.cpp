#include<bits/stdc++.h>
using namespace std;
#define N 1005
#define MO 1000000007
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define int long long
int n;
int cnt[2*N];
int C[N][N];
int gt[2*N];
int ls[2*N];
int arr[N];
int pow2[N];

void preprocess(){
	pow2[0] = 1;
	for(int i = 1; i  < N; i++){
		pow2[i] = (pow2[i-1]*2)%MO;
	}
	C[0][0] = 1;
	for(int i = 1; i < N; i++){
		C[i][0] = 1;
		for(int j = 1 ; j <= i; j++){
			C[i][j] = C[i-1][j] + C[i-1][j-1];
			C[i][j]%=MO; 
		}
	} 
}

int organs = 0;

void fun(vector<int> v, int ind){
	if(ind==-1){
		sort(v.begin(),v.end());
		int sz = v.size();
		if(sz&1){
			//printarr(v, v.size());
			organs++;
			organs%=MO;
		}
		else if(sz > 0){
			if(v[sz/2] == v[sz/2-1]){
				printarr(v, v.size());
				organs++;
				organs%=MO;
			}
		}
		return;
	}
	fun(v, ind-1);
	v.push_back(arr[ind]);
	fun(v, ind-1);

}

signed main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	preprocess();
	int t;
	cin >> t;
	while(t--){
		cin >> n;
		memset(cnt, 0 , sizeof(cnt));
		memset(gt, 0 , sizeof(gt));
		memset(ls, 0 , sizeof(ls));
		for(int i = 0; i < n; i++){
			int x;
			cin >> x;
			cnt[x]++;
			arr[i] = x;
		}
		sort(arr, arr+n);
		////////////////////////////
	    organs = 0;
		std::vector<int> v;
		fun(v, n-1);
		cout << "organs = " << organs-pow2[n-1] << endl;
		////////////////////////////
		// printarr(cnt, 2*N);

		for(int i = 1; i < 2*N; i++){
			ls[i] = cnt[i-1] + ls[i-1]; 
		}
		for(int i = 2*N-2; i >= 0; i--){
			gt[i] = cnt[i+1] + gt[i+1]; 
		}
		// printarr(gt, 11);
	    // printarr(ls, 11);


		int ans = 0;
		// cout << "\nSdfs\n";
		for(int i = 0; i < n; i++){
			for(int j = 0; j <= min(i,n-i-1); j++){
				ans =(ans + (C[i][j]*C[n-i-1][j])%MO)%MO;
				// cout << i << " " << j << " " << (C[i][j]*C[n-i-1][j])%MO << endl;
			}
		}
		// cout << ans  << endl;

		for (int i = 0; i < 2*N; ++i){
			for(int j = 2; j <= cnt[i]; j++){
				int temp = 0;
				if(j&1){
					for(int k = 0; k < min(ls[i], gt[i]); k++){
						temp = (temp + (C[ls[i]][k]*C[gt[i]][k+1])%MO)%MO;
						temp = (temp + (C[ls[i]][k+1]*C[gt[i]][k])%MO)%MO;
					}
					if(ls[i]>gt[i]){
						temp = (temp + (C[ls[i]][gt[i]+1]*C[gt[i]][gt[i]])%MO)%MO;
					}
					else if(ls[i]<gt[i]){
						temp = (temp + (C[ls[i]][ls[i]]*C[gt[i]][ls[i]+1])%MO)%MO;
					}
				}
				else{
					for(int k = 0; k <= min(ls[i], gt[i]); k++){
						temp = (temp + (C[ls[i]][k]*C[gt[i]][k])%MO)%MO;
						//if(i == 2){
						// cout <<"temp = " << temp << endl;
						//}
					}
				}
				if(temp){
					cout << i << " " << cnt[i] <<" " << j << " " << temp <<" "<<C[cnt[i]][j]*temp<< endl;
				}

				ans = (ans + (C[cnt[i]][j]*temp)%MO)%MO;
			}
		}
		cout << "ans = ";

		cout << ans-pow2[n-1] << endl;
		//cout << pow2[n-1] << endl;

	}
}