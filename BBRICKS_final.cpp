//combinatrics - stars and bars
#include<bits/stdc++.h>
using namespace std;
#define endl "\n"
#define int long long
#define print(x) cout << "#x = " << x << endl
#define K 1001
#define N (1e9) + 1
#define MOD 1000000007
int fact[K];
int factinv[K];
int Coef[K][K];

int pow(int x, int r, int mo){
	long long res = 1;
	while(r){
		if(r&1){
			res = (res*x)%mo;
			r--;
		}
		else{
			res = (res*res)%mo;
			r /= 2;
		}
	}
	return (int)res;
}

int inv(int x){
	return pow(x, MOD-2);
}

int nCr(int n, int k){
	return (((fact[n]*factinv[n-k])%MOD)*factinv[k])%MOD;
}

signed main(){
	#ifdef ONLINE_JUDGE
	ios_base::sync_with_stdio(false); cin.tie(0);
	#endif

	#ifndef ONLINE_JUDGE
	freopen("input.txt","r", stdin);
	#endif
	
	fact[0] = 1;
	for(int i = 1;  i <= K; i++){
		fact[i] = (fact[i-1] * i)%MOD;
	}
	for (int i = 0; i <= K; ++i)
	{
		factinv[i] = inv(fact[i]);
	}
	Coef[1][0] = 2;
	for(int i = 2; i <= K; i++){
		for(int j = 0; j < i; j++){
			Coef[i][j] = (Coef[i-1][j] + Coef[i-1][j-1])%MOD;
		}
	}
	int t;
	cin >> t;
	while(t--){
		int n, k;
		cin >> n >> k;
		int ans = 0;
		for(int i = 0; i < k; i++){
			ans = (ans + ((Coef[k][i] * nCr(n-i, k))%MOD))%MOD;
		}
		cout << ans << endl;
	}
	
}

//input 
/*
1
3 2
*/