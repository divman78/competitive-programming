#include<bits/stdc++.h>
using namespace std;
#define N 500100

int n,m;
int arr[N];

int fin(int l, int r, int x){
	if(x >= l && x <= r) return x;
	else if(x > r) return INT_MAX;
    else return l;
}

bool p(int x){
	int mn = 0;
	for (int i = 0; i < n; ++i){
		int p, left = (arr[i]+x)%m,right = arr[i];
		if(right>left) p = min(fin(right, m-1, mn), fin(0,left,mn));
		else p = fin(right, left, mn);
		if(p==INT_MAX) return false;
		mn = p;
	}
	return true;
}

int binary_search(int lo,int hi){
    while (lo < hi){
        int mid = lo + (hi-lo)/2;
        if (p(mid) == true) hi = mid;
        else lo = mid+1;
    }
    return lo ;        
}

signed main(){
	cin >> n >> m;		
	for (int i = 0; i < n; ++i) cin >> arr[i];
	cout << binary_search(0,m-1) << endl;
}