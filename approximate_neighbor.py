#Example Usage
#import numpy

#from nearpy import Engine
#from nearpy.hashes import RandomBinaryProjections

# Dimension of our vector space
# dimension = 500

# # Create a random binary hash with 10 bits
# rbp = RandomBinaryProjections('rbp', 10)


# print(rbp)

# # Create engine with pipeline configuration
# engine = Engine(dimension, lshashes=[rbp])

# # Index 1000000 random vectors (set their data to a unique string)
# for index in range(100000):
#     v = numpy.random.randn(dimension)
#     if(index == 1):
#     	print(v)

#     engine.store_vector(v, 'data_%d' % index)

# # Create random query vector
# query = numpy.random.randn(dimension)

# # Get nearest neighbours
# N = engine.neighbours(query)[0]


# print(N)


# import pysparnn.cluster_index as ci

# import numpy as np
# from scipy.sparse import csr_matrix

# features = np.random.binomial(1, 0.01, size=(1000, 20000))
# features = csr_matrix(features)

# # build the search index!
# data_to_return = range(1000)
# cp = ci.MultiClusterIndex(features, data_to_return)

# cp.search(features[:5], k=1, return_distance=False)
# >> [[0], [1], [2], [3], [4]]

# from annoy import AnnoyIndex
# import random

# f = 100000
# t = AnnoyIndex(f)  # Length of item vector that will be indexed
# for i in xrange(10):
#     #v = [random.gauss(0, 1) for z in xrange(f)]
#     v = [z for z in xrange(i,f+i)]
#     t.add_item(i, v)

# t.build(10) # 10 trees
# t.save('test.ann')

# # ...

# query = [z for z in xrange(-1,f-1)]


# u = AnnoyIndex(f)
# u.load('test.ann') # super fast, will just mmap the file
# print(u.get_nns_by_vector(query, 2, search_k=-1, include_distances=False)) # will find the 1000 nearest neighbors


# [ 46,  74,  44,  61,  92,  55,  72, 105,  60,  82, 117,  67,  90,
#        126,  74,  15,  28,  12,  22,  36,  12,  25,  44,  11,  27,  54,
#         11,  46,  76,  27,  16,  18,  18,  11,  12,   8,   6,  12,   1,
#          6,  18,   0,  13,  28,   1,  38,  38,  38,  38,  38,  38,  38,
#         38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,
#         38,  38,  38,  38,  38,  38,  38,  38,  38,  38]


# [ 46,  74,  44,  61,  92,  55,  72, 105,  60,  82, 117,  67,  90,
#        126,  74,  15,  28,  12,  22,  36,  12,  25,  44,  11,  27,  54,
#         11,  46,  76,  27,  16,  18,  18,  11,  12,   8,   6,  12,   1,
#          6,  18,   0,  13,  28,   1,  38,  38,  38,  38,  38,  38,  38,
#         38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,  38,
#         38,  38,  38,  38,  38,  38,  38,  38,  38,  38]


import numpy as np
import colorsys

l = np.array([[(1,2,3),(4,5,6)],[(7,8,9),(10,11,12)], [(13,14,15),(16,17,18)]])

rgb = (1, 1, 1)
yiq = colorsys.rgb_to_yiq(rgb[0]/255.0, rgb[1]/255.0, rgb[2]/255.0)
rgb = colorsys.yiq_to_rgb(yiq[0], yiq[1], yiq[2])
print(rgb[0]*255, rgb[1]*255, rgb[2]*255)

# print(l[0:3, 0:3].flatten())


