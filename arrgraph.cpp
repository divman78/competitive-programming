#include<bits/stdc++.h>
using namespace std;
#define endl "\n"

int primes[] = {29, 31, 37, 41, 43, 47};

void dfs(int x, int p,int* visited, vector<vector<int> >& v){
	if(visited[x]) return;
	visited[x] = 1;
	for(int i = 0; i < v[x].size();i++){
		int y = v[x][i];
		if(p!=y)dfs(y, x, visited, v);
	}
}

int main(){
	//cout << __gcd(47,47);
	ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}
		vector<vector<int> >v(n);

		for (int i = 0; i < n-1; ++i)
		{
			for (int j = i+1; j < n; ++j)
			{
				if(__gcd(arr[i],arr[j])==1)v[i].push_back(j), v[j].push_back(i);
			}
		}

		int visited[n];
		memset(visited, 0, sizeof(visited));
		//dfs(0, 0, visited, v);

		stack<pair<int,int> > sta;
		sta.push(make_pair(0,0));

		while(!sta.empty()){
			pair<int,int> p = sta.top();
			sta.pop();
			if(visited[p.first]) continue;
			visited[p.first]=1;
			for (int i = 0; i < v[p.first].size(); ++i)
			{
				if(v[p.first][i] != p.second) sta.push(make_pair(v[p.first][i], p.first));
			}

		}
		int notconn = 0;
		int not47 = 0;
		for (int i = 0; i < n; ++i)
		{
			if(!visited[i]){
				notconn = 1;
				break;
			}
			not47 = arr[i]!=47;
		}
		if(notconn){ arr[0] = 47; if(!not47) arr[0] = 43;} 
		cout<<notconn<<endl;
		for (int i = 0; i < n; ++i)
		{
			cout << arr[i] << " ";
		}
		cout << endl;




	}
}