#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 300005

#define print(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";


signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int t;
	cin >> t;
	//t=1;
	while(t--){
		int n;
		cin >> n;
		int arr[n+2][n+2];
		memset(arr, 1, sizeof(arr));
		for(int i = 0; i < n; i++){
			string s;
			cin >> s;
			for(int j = 0; j < n; j++){
				arr[i+1][j+1] = s[j] =='.' ? 1 : 0;
			}
		}
		//print(arr, n);

		int srow[n+2][n+2];
		memset(srow, 1, sizeof(srow));
		for(int i = 1; i <= n; i++){
			for(int j = n; j >= 1; j--){
				srow[i][j] = min(arr[i][j], srow[i][j+1]);
			}
		}
		//print(srow, n);

		int scol[n+2][n+2];
		memset(scol, 1, sizeof(scol));
		for(int j = 1; j <= n; j++){
			for(int i = n; i >= 1; i--){
				scol[i][j] = min(arr[i][j], scol[i+1][j]);
			}
		}
		//print(scol, n);
		int ans = 0;
		for(int i = 1; i <= n; i++){
			for(int j = 1; j <= n; j++){
				if(scol[i][j] == 1 && srow[i][j] == 1){
					ans ++;
				}
			}
		}
		cout << ans;
        cout << endl;
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}