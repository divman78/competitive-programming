#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 2000001
//#define N 100
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)

int seg[N], lazy[N];


void set_lazy(int ind, int l, int r){
	if(lazy[ind]){
		seg[ind] = lazy[ind];
		if(l != r){
			seg[ind*2+1] = lazy[ind];
			seg[ind*2+2] = lazy[ind];
		}
		lazy[ind] = 0;
	}
}


void update(int l, int r, int gl, int gr, int ind, int change){
	//cout << l << " " << r << endl;
	set_lazy(ind, l, r);
	if(l > r || l > gr || r < gl) {
		return;
	}
	if(l >= gl && r <= gr){
		seg[ind] = change;
		//cout << seg[ind] << " ";
		if(l!=r){
			lazy[ind*2+1] = change;
			lazy[ind*2+2] = change;
		}
		return;

	}
	int mid = (l+r)/2;
	update(l, mid, gl, gr, ind*2+1, change);
	update(mid+1, r, gl, gr, ind*2+2, change);
	seg[ind] = max(seg[ind*2+1], seg[ind*2+2]);
}


int query(int l, int r, int gl, int gr, int ind){
	//cout << l << " " << r << endl;
	set_lazy(ind, l, r);
	if(l > r || l > gr || r < gl) return 0;
	if(l >= gl && r <= gr){
		if(l == r && l == 11){
			//cout << "wowo\n";
		}

		return seg[ind];
	}
	//cout << "kyot\n";
	int mid = (l+r)>>1;
	return max(query(l, mid, gl, gr, ind*2+1), query(mid+1, r, gl, gr, ind*2+2));
}


signed main(){
	//FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int n;
	cin >> n;
	for (int i = 0; i < n; ++i)
	{
		//cout << i << endl;
		int  l, h, p, c, x;
		cin >> l >> h >> p >> c >> x;
		int mx = query(0, 500050, x-1, x+l-1, 0);
		//cout << mx << endl;
		if(c){
			update(0, 500050, x, x+l-1, 0, mx+1);
			update(0, 500050, x+p-1, x+p-1, 0, mx+h+1);
		}
		else{
			int h1 = query(0, 500050, l+p-1, l+p-1, 0);
			if(mx-h1 >= h){
				update(0, 500050, x, x+l-1, 0, mx+1);
			}
			else{
				update(0, 500050, x, x+l-1, 0, h1+h+1);
			}
		}
	}

	cout << seg[0] << endl;









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




