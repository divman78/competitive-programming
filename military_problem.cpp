#include<bits/stdc++.h>
using namespace std;

vector<int> adj[200000];
int ans[2000000];
vector<int> vect;
int childs[2000000];

void dfs(int curr, int par){
	for(int i = 0; i < adj[curr].size(); i++){
		if(adj[curr][i] != par){
			flag = true;
			vect.push_back(adj[curr][i]);
			ans[adj[curr][i]] = vect.size() - 1;
			dfs(adj[curr][i], curr);
			childs[curr] += childs[adj[curr][i]] + 1;
		}
	}
}




int main(){
	int n, q;
	cin >> n >> q;
	int arr[n];
	for (int i = 1; i < n; ++i){
		int temp;
		cin >> temp;
		adj[i].push_back(temp - 1);
		adj[temp - 1]. push_back(i);
	}

	for(int i = 0; i < n; i++){
		sort(adj[i].begin(), adj[i].end());
	}

    vect.push_back(0);
	dfs(0, 0);

    for (int i = 0; i < q; ++i)
    {
    	int u, v;
    	cin >> u >> v;
    	u--; 
    	if(childs[u] < v - 1){
    		cout << -1 << " ";
    	}
    	else{
    		cout << vect[v + ans[u] - 1] + 1 << " ";
    	}
    }
}