#include<bits/stdc++.h>
using namespace std;
#define eps 1e-9

int main(){
//	freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		double a, k;
		cin >> a >> k;
		double arr[3];
		for (int i = 0; i < 3; ++i)
		{
			cin >> arr[i];
		}

		sort(arr, arr+3);
		cout << fixed << setprecision(6);
		double p1 = arr[0] + k;
		double p2 = arr[2] - k;
		if(p1 >= p2){
			cout<<a*a<<endl;
			continue;
		}
		else{
			double p3 = p2 - p1;
			if(p3 >= a){
				cout<<0.0<<endl;
			}
			else{
				cout<<(a-p3)*a<<endl;
			}
		}
		

	}
}