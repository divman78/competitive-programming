#include<bits/stdc++.h>
using namespace std;
#define MAXN 4000000
#define int long long


int fen[MAXN];

void add(int x, int val)   
{
    for (int i = x + 1; i < MAXN; i += i & (-i)) fen[i] += val;
}

int get(int x) // get sum less than x
{
    int ans = 0;
    for (int i = x; i > 0; i -= i & (-i)) ans += fen[i];
    return ans;
}

int sum(int x, int y)
{
    return get(y) - get(x);
}

void makedistinct(vector<int>& vec)
{
  sort( vec.begin(), vec.end() );
  vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
}



signed main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int n;
	cin >> n;

	vector<int>  arr(n);
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}
	std::vector<int> v(arr);
	makedistinct(v);

	for (int i = 0; i < n; ++i)
	{
		arr[i] = lower_bound(v.begin(), v.end(), arr[i]) - v.begin();
	}

	vector<int> arr2(n);
	for (int i = n-1; i >= 0; --i)
	{
		add(arr[i], 1);
		arr2[i] = get(arr[i]);
	}



	memset(fen, 0, sizeof(fen));

	for (int i = 0; i < n; ++i)
	{
		add(arr[i], arr2[i]);
	}

	int ans = 0;

	for (int i = 0; i < n; ++i)
	{
		add(arr[i], -arr2[i]);
		ans += get(arr[i]);
	}

	cout << ans << endl;







	
}