#include<bits/stdc++.h>
using namespace std;
#define l first 
#define r second
#define ind second
#define mp(x,y) make_pair(x,y) 

bool cmp(const pair<int,int>& p1, const pair<int, int>& p2 ){
	return p1.first < p2.first;
}

int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		int q;
		cin >> n >> q;
		pair<int, int> inter[n];
		for (int i = 0; i < n; ++i)
		{
			cin >> inter[i].l >> inter[i].r;
		}

		sort(inter, inter+n);
		vector<pair<int,int> > buff(n);
		for (int i = 0; i < n; ++i)
		{
			buff[i].l = inter[i].l;
			buff[i].ind = i;
		}	
		while(q--){
			int x;
			cin >> x;
			pair<int, int> pp;
			if(x <= buff[0].first){
				cout << abs(x-buff[0].first) << endl;
			}
			else if(x >= inter[n-1].first){
				if(x>=inter[n-1].second)
					cout << -1 <<endl;
				else
					cout << 0 << endl;
			}
			else{
				pp = *lower_bound(buff.begin(), buff.end(), mp(x, 0), cmp);
				if(pp.first >= x){
					pp.first = inter[--pp.second].first;
				}
				if(x >= pp.first && x < inter[pp.second].r){
					cout << 0 << endl;
				}
				else {
					//cout << inter[pp.second].first << " ";
					cout << abs(x-inter[pp.second+1].first) << endl;
				} 
			}
		}
	}
}