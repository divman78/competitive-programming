#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 50010
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)

void pr(stack<char> sta){
	if(sta.empty()){
		cout << "- - -\n";
		return;
	}
	while(!sta.empty()){
		cout << sta.top() << " ";
		sta.pop();
	}
	cout << endl;
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	freopen("output.txt","w",stdout);
	#endif

	int t=1;
	//cin >> t;
	while(t--){
	int n;
	cin >> n;
	string s;
	cin >> s;
	int pre[n];
	int suf[n];
	//cout << s  << endl;


	pre[0] = ((s[0] == ')') ? -1:1);
	for (int i = 1; i < n; ++i)
	{
		if(pre[i-1] == -1){
			pre[i] = -1;
			continue;
		}
		pre[i] = pre[i-1] + ((s[i] == ')') ? -1:1);
	}

	suf[n-1] = ((s[n-1] == '(') ? -1:1);
	for (int i = n-2; i >= 0; --i)
	{
		if(suf[i+1] == -1){
			suf[i] = -1;
			continue;
		}
		suf[i] = suf[i+1] + ((s[i] == '(') ? -1:1);
	}

	//printarr(pre,n);
	//printarr(suf,n);


	int ans = 0;

	for(int i = 0; i < n; i++){
		if(i == 0 && suf[i+1]!=-1){
			if(suf[i+1] + ((s[i] == ')') ? -1:1) == 0) ans++;
			continue;
		}

		if(i == n-1&& pre[i-1]!=-1){
			if(pre[i-1] + ((s[i] == '(') ? -1:1) == 0) ans++;
			continue;
		}

		if(pre[i-1] != -1 and suf[i+1] != -1){
			if(s[i] == '('){
				if(pre[i-1]-1 == suf[i+1]) ans++;
			}
		    else if(s[i] == ')'){
				if(pre[i-1] == suf[i+1]-1) ans++;
			}
		}
	}


	cout << ans << " ";

	}






}