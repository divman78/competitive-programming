from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import sys
import math
from nearpy import Engine
from nearpy.hashes import RandomBinaryProjections
import colorsys
from annoy import AnnoyIndex
import random


# A1 = cv2.imread('./images/toy-newshore-src.jpg')
# A2 = cv2.imread('./images/toy-newshore-blur.jpg')
# B1 = cv2.imread('./images/newflower-src.jpg')

# A1 = cv2.imread('./images/blurA1.jpg')
# A2 = cv2.imread('./images/blurA2.jpg')
# B1 = cv2.imread('./images/blurB1.jpg')

# A1 = cv2.imread('./images/blurA1.jpg')
# A2 = cv2.imread('./images/blurA2.jpg')
# B1 = cv2.imread('./images/newflower-src.jpg')

# A1 = cv2.imread('./images/rose-src.jpg')
# A2 = cv2.imread('./images/rose-emboss.jpg')
# B1 = cv2.imread('./images/dandilion-src.jpg')

A1 = cv2.imread('./images/rhone-src.jpg')
A2 = cv2.imread('./images/rhone.jpg')
B1 = cv2.imread('./images/newflower-src.jpg')

# A1 = cv2.imread('./images/toy-newshore-src.jpg')
# A2 = cv2.imread('./images/toy-newshore-blur.jpg')
# B1 = cv2.imread('./images/rhone-src.jpg')

# A1 = colorsys.rgb_to_yiq(A1)
# A2 = colorsys.rgb_to_yiq(A2)
# B1 = colorsys.rgb_to_yiq(B1)



# for i in range(A1.shape[0]):
# 	for j in range(A1.shape[1]):
# 		A1[i][j] = colorsys.rgb_to_yiq(A1[i][j][0]/255.0, A1[i][j][1]/255.0, A1[i][j][2]/255.0)

# for i in range(A2.shape[0]):
# 	for j in range(A2.shape[1]):
# 		A2[i][j] = colorsys.rgb_to_yiq(A2[i][j][0]/255.0, A2[i][j][1]/255.0, A2[i][j][2]/255.0)

# for i in range(B1.shape[0]):
# 	for j in range(B1.shape[1]):
# 		B1[i][j] = colorsys.rgb_to_yiq(B1[i][j][0]/255.0, B1[i][j][1]/255.0, B1[i][j][2]/255.0)



BIG = 5
SMALL = 3
NUM_FEATURES = 3
NNF = BIG * BIG * 3
nnf = SMALL * SMALL * NUM_FEATURES
KAPPA = 0.1


def fspecial_gauss(size, sigma=1):
    """Function to mimic the 'fspecial' gaussian MATLAB function
    """
    x, y = np.mgrid[-size//2 + 1:size//2 + 1, -size//2 + 1:size//2 + 1]
    g = np.exp(-((x**2 + y**2)/(2.0*sigma**2)))
    return g/g.sum()

G_BIG = fspecial_gauss(5)
G_SMALL = fspecial_gauss(3)

def extend_Image(X, bordersize):
	row, col= X.shape[:2]
	bottom= X[row-2:row, 0:col]
	mean= cv2.mean(bottom)[0]
	border=cv2.copyMakeBorder(X, top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[mean,mean,mean])
	return border


def toyiq(X_features):
	# print(X_features)
	if(isinstance(X_features[0], list) == False):
		return X_features
	for i in range(0,len(X_features)):
		for j in range(0, len(X_features[0])):
			X_features[i][j] = colorsys.rgb_to_yiq(X_features[i][j][0]/255.0, X_features[i][j][1]/255.0, X_features[i][j][2]/255.0)
	return X_features




def concat_feature(X1_pyramid, X2_pyramid, l, i, j, L):
	F = np.zeros(((nnf*2)+(NNF*2)), np.float)
	X1_fine = X1_pyramid[l]
	X2_fine = X2_pyramid[l]
	X1_fine_size = X1_fine.shape
	X2_fine_size = X2_fine.shape

	#print(X1_fine[max(0,i-BIG): min(X1_fine_size[0], i+BIG), max(0,j-BIG): min(X1_fine_size[1], j+BIG)].flatten())
	#print(max(0,i-BIG/2), min(X1_fine_size[0], i+BIG/2), max(0,j-BIG/2), min(X1_fine_size[1], j+BIG/2))
	X1_fine_features = X1_fine[max(0,i-BIG/2): min(X1_fine_size[0], i+BIG/2), max(0,j-BIG/2): min(X1_fine_size[1], j+BIG/2)]
	X1_fine_features = toyiq(X1_fine_features)
	if(len(X1_fine_features) == NNF):
		X1_fine_features = X1_fine_features*G_BIG
	X1_fine_features = X1_fine_features.flatten()
	F[0:min(len(X1_fine_features), NNF)] = X1_fine_features

	X2_fine_features = X2_fine[max(0,i-BIG/2): min(X2_fine_size[0], i+BIG/2), max(0,j-BIG/2): min(X2_fine_size[1], j+BIG/2)]
	X2_fine_features = toyiq(X2_fine_features)
	if(len(X2_fine_features) == NNF):
		X2_fine_features = X2_fine_features*G_BIG
	X2_fine_features = X2_fine_features.flatten()
	F[NNF: min(NNF + len(X2_fine_features), 2*NNF)] = X2_fine_features

	if(l+1 >= L):
		return F 


	X1_coarse = X1_pyramid[l+1]
	X2_coarse = X2_pyramid[l+1]
	X1_coarse_size = X1_coarse.shape
	X2_coarse_size = X2_coarse.shape




	X1_coarse_features = X1_coarse[max(0,i/2-SMALL/2): min(X1_coarse_size[0], i/2+SMALL/2), max(0,j/2-SMALL/2): min(X1_coarse_size[1], j/2+SMALL/2)]
	X1_coarse_features = toyiq(X1_coarse_features)
	if(len(X1_coarse_features) == nnf):
		X1_coarse_features = X1_coarse_features*G_SMALL
	X1_coarse_features = X1_coarse_features.flatten()
	F[2*NNF :min(2*NNF+len(X1_coarse_features),2*NNF+ nnf)] = X1_coarse_features

	X2_coarse_features = X2_coarse[max(0,i/2-SMALL/2): min(X2_coarse_size[0], i/2+SMALL/2), max(0,j/2-SMALL/2): min(X2_coarse_size[1], j/2+SMALL/2)]
	X1_coarse_features = toyiq(X1_coarse_features)
	if(len(X2_coarse_features) == nnf):
		X2_coarse_features = X2_coarse_features*G_SMALL
	X2_coarse_features = X2_coarse_features.flatten()
	F[2*NNF+ nnf: min(2*NNF+ nnf + len(X2_coarse_features),2*NNF + 2*nnf)] = X2_coarse_features

	return F


def best_coherence_match(A_features, B_features, A1_pyramid, B1_pyramid, A2_pyramid, B2_pyramid, s_pyramid, l, L , i, j):
	(A_h, Aw, _) = A1_pyramid[l].shape
	border_big = int(math.floor(BIG/2))

	F_q = concat_feature(B1_pyramid, B2_pyramid, l, i ,j ,L)

	min_dist = float("inf")

	r_star_i = -1
	r_star_j = -1

	done = False
	(h, w, _) = B1_pyramid[l].shape
	#print(h, w)
	#print(len(s_pyramid[l][1]))
	for ii in range(max(0,i-border_big),min(h, i + border_big)):
		for jj in range(max(0,j-border_big),min(w ,j + border_big)):
			if ii == i and jj == j:
				done = True
				break
			#print(l, ii, jj , 0)
			s_i = s_pyramid[l][ii][jj][0]
			s_j = s_pyramid[l][ii][jj][1]

			#print((s_i, s_j))

			F_sr_i = s_i + (i-ii)
			F_sr_j = s_j + (j-jj)

			if (F_sr_i >= A_h or F_sr_i < 0 or F_sr_j >= Aw or F_sr_j < 0):
				continue

			F_sr = concat_feature(A1_pyramid, A2_pyramid, l, F_sr_i, F_sr_j, L)

			F_sr = np.array(F_sr)
			F_q= np.array(F_q)

			#print((F_sr), len(F_q));

			dist = sum(np.power(F_sr-F_q, 2))

			if(dist < min_dist):
				min_dist = dist
				r_star_i = ii
				r_star_j = jj
				best_coh_i = F_sr_i
				best_coh_j = F_sr_j 

		if(done):
			break
	if(r_star_i == -1 or r_star_j == -1):
		best_coh_i = -1
		best_coh_j = -1


	return (best_coh_i, best_coh_j)


def best_approximate_match(A_features, B_features, A1_pyramid, B1_pyramid,  A2_pyramid, B2_pyramid, l, L , i, j):
	#print("hello")
	Al_features = A_features[l]
	# #print(Al_features)
	w = len(Al_features[0])
	f = len(Al_features[0][0])
	# t = AnnoyIndex(f)  # Length of item vector that will be indexed
	# for ii in range(h):
	# 	for jj in range(len(Al_features[0])):
	# 		v = Al_features[ii][jj]
	# 		t.add_item(ii*len(Al_features)+jj, v)

	# t.build(10) # 10 trees
	# t.save('test.ann')

	if(j == 40):
		print((l, i, j))

	query = B_features[l][i][j]

	u = AnnoyIndex(f)
	u.load('test.ann' + str(l)) # super fast, will just mmap the file
	#print(u.get_nns_by_vector(query, 1, search_k=-1, include_distances=False)) # will find the 1000 nearest neighbors
	ind = u.get_nns_by_vector(query, 1, search_k=-1, include_distances=False)[0]

	best_app_i = ind / w
	best_app_j = ind - best_app_i*w

	if(best_app_i == 40):
		print("sdf")
		print(ind)

	return (best_app_i, best_app_j)
	#return(1,1)

	# dimension = len(Al_features[0][0])

	# # Create a random binary hash with 10 bits
	# rbp = RandomBinaryProjections('rbp', 10)


	# #print(rbp)

	# # Create engine with pipeline configuration
	# engine = Engine(dimension, lshashes=[rbp])

	# (h, w, _) = A1_pyramid[l].shape

	# # Index 1000000 random vectors (set their data to a unique string)
	# for ii in range(h):
	# 	for jj in range(w):
	# 		v = Al_features[ii][jj] 
	# 		engine.store_vector(v, ii*h+jj)
	    

	# # Create random query vector
	# query = B_features[l][i][j]

	# # Get nearest neighbours
	# N = engine.neighbours(query)
	# if(len(N)==0):
	# 	return (-1, -1)
	# (_,ind,_) = N[0]
	# ind = int(ind)

	# best_app_i = ind / h
	# best_app_j = ind - best_app_i*h

	# return (best_app_i, best_app_j)



def best_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, A_features, B_features, l, L, i, j):
	#(best_app_i, best_app_j) = (1,1)

	(best_app_i, best_app_j) = best_approximate_match(A_features, B_features, A1_pyramid, B1_pyramid,  A2_pyramid, B2_pyramid, l, L , i, j)
	#if(True):
	#	return (best_app_i, best_app_j)
	(best_coh_i, best_coh_j) = best_coherence_match(A_features, B_features, A1_pyramid, B1_pyramid, A2_pyramid, B2_pyramid, s_pyramid, l, L , i, j)
	best_i = -1
	best_j = -1
	if(best_coh_i == -1 or best_coh_j == -1):
		
		(best_i, best_j) = (best_app_i, best_app_j)
		if(best_i == 30):
				print("app1")
	elif(best_app_i == -1 or best_app_j == -1):
		
		(best_i, best_j) = (best_app_i, best_app_j)
	 	(best_i, best_j) = (best_coh_i, best_coh_j)
	 	if(best_i == 30):
				print("coh1")
		#return (i, j)
	else:

		F_p_app = concat_feature(A1_pyramid, A2_pyramid, l, best_app_i, best_app_j, L)
		F_p_coh = concat_feature(A1_pyramid, A2_pyramid, l, best_coh_i, best_coh_j, L)

		F_q = concat_feature(B1_pyramid, B2_pyramid, l, i, j, L)
		#print(F_q)

		d_app = sum(np.power((F_p_app - F_q),2));
		d_coh = sum(np.power((F_p_coh- F_q),2));


		if(d_coh <= d_app * (1 + (2^(l-L))*KAPPA)):
			
			best_i = best_coh_i
			best_j = best_coh_j
			if(best_i == 30):
				print("coh" + str(d_coh - d_app * (1 + (2^(l-L))*KAPPA)))
		else:
			
			best_i = best_app_i
			best_j = best_app_j
			if(best_i == 30):
				print("app" + str(d_coh - (d_app * (1 + (2^(l-L))*KAPPA))))


	#if(best_i == 30):
	#	print(best_i, best_j)



	return (best_i, best_j)
	#return (best_coh_i, best_coh_j)








def create_image_analogies(A1, A2, B1):
	A1_pyramid = [A1]
	A2_pyramid = [A2]
	B1_pyramid = [B1]
	(hb, wb, _) = B1.shape
	s = np.zeros((hb, wb, 2), np.uint8)
	B2 = np.zeros((hb, wb, 3), np.uint8)
	B2_pyramid = [B2]
	s_pyramid = [s]
	(ha, wa, _) = A1.shape
	while(ha >= 50 and wa >= 50):
		A1 = cv2.pyrDown(A1)
		A2 = cv2.pyrDown(A2)
		B1 = cv2.pyrDown(B1)
		B2 = cv2.pyrDown(B2)

		(hb, wb, _) = B1.shape
		(ha, wa, _) = A1.shape

		s = np.zeros((hb, wb, 2), np.uint8)

		A1_pyramid.append(A1)
		A2_pyramid.append(A2)
		B1_pyramid.append(B1)
		B2_pyramid.append(B2)
		s_pyramid.append(s)



	L = len(A1_pyramid)

	print(L)

	# for l in range(L):
	# 	cv2.imwrite("./images/"+ "A1"+ str(l) +".jpg", A1_pyramid[l])
	# 	cv2.imwrite("./images/"+ "B1"+ str(l) +".jpg", B1_pyramid[l])
	# 	cv2.imwrite("./images/"+ "B1"+ str(l) +".jpg", B1_pyramid[l])
	# 	cv2.imwrite("./images/"+ "B2"+ str(l) +".jpg", B2_pyramid[l])

	A1_extended_pyramid = []
	B1_extended_pyramid = []

	for l in range(L):
		A1_extended_pyramid.append(extend_Image(A1_pyramid[l], BIG/2))
		B1_extended_pyramid.append(extend_Image(B1_pyramid[l], BIG/2))

	# for l in range(L):
	# 	cv2.imwrite("./images/"+ "A1"+ str(l) +".jpg", A1_extended_pyramid[l])
	# 	cv2.imwrite("./images/"+ "B1"+ str(l) +".jpg", B1_extended_pyramid[l])

	A_features = [None]*L
	B_features = [None]*L



	for l in range(L):
		(ha, wa, _) = A1_pyramid[l].shape
		Al_features = [[None]*wa]*ha
		for i in range(ha):
			for j in range(wa):
				Al_features[i][j] = A1_extended_pyramid[l][i : i + BIG, j : j + BIG].flatten()/255.0
				# if(l == 0 and i == 0 and j == 0):
				#  	print(Al_features)
		A_features[l] = Al_features
		#if(l==1):
			#print(A_features)

	for l in range(L):
		(hb, wb, _) = B1_pyramid[l].shape
		Bl_features = [[None]*wb]*hb
		for i in range(hb):
			for j in range(wb):
				Bl_features[i][j] = B1_extended_pyramid[l][i : i + BIG, j : j + BIG].flatten()/255.0
				#print(Bl_features)
				# if(l == 0 and i == 0 and j == 0):
				# 	print(Bl_features)
		B_features[l] = Bl_features

	#print("len = " + str(len(B1_pyramid[2][0])))


	for l in range(L-1, -1, -1):

		Al_features = A_features[l]
		# #print(Al_features)
		h = len(Al_features)
		f = len(Al_features[0][0])
		t = AnnoyIndex(f)  # Length of item vector that will be indexed
		for ii in range(h):
			for jj in range(len(Al_features[0])):
				v = Al_features[ii][jj]
				t.add_item(ii*len(Al_features)+jj, v)

		t.build(10) # 10 trees
		t.save('test.ann' + str(l))


		B2_l = B2_pyramid[l]
		(height, width, __) = B2_l.shape
		for i in range(0, height):
			for j in range(0, width):
				#print((i, j))
				(best_i, best_j) = best_match(A1_pyramid, A2_pyramid, B1_pyramid, B2_pyramid, s_pyramid, A_features, B_features, l, L, i, j)
				# best_i = i
				# best_j = j
				if(best_i == 30 or best_j == 30):
					print((best_i, best_j, A1_pyramid[l].shape))
				(YA, _, _) = colorsys.rgb_to_yiq(A2_pyramid[l][best_i][best_j][0]/255.0, A2_pyramid[l][best_i][best_j][1]/255.0, A2_pyramid[l][best_i][best_j][2]/255.0)
				(_,IB, BQ) = colorsys.rgb_to_yiq(B1_pyramid[l][i][j][0]/255.0, B1_pyramid[l][i][j][1]/255.0, B1_pyramid[l][i][j][2]/255.0)



				(r,g,b) = colorsys.yiq_to_rgb(YA, IB, BQ)
				r = r*255
				g = g*255
				b = b*255

				B2_pyramid[l][i][j][0] = r

				#B2_pyramid[l][i][j][0] = B1_pyramid[l][i][j][0]
				B2_pyramid[l][i][j][1] = g
				B2_pyramid[l][i][j][2] = b


				s_pyramid[l][i][j][0] = best_i
				s_pyramid[l][i][j][1] = best_j

	return B2_pyramid[0]
















B2 = create_image_analogies(A1, A2, B1)

#B2 = B1 

# for i in range(B2.shape[0]):
# 	for j in range(B2.shape[1]):
# 		B2[i][j] = colorsys.yiq_to_rgb(B2[i][j][0], B2[i][j][1], B2[i][j][2])
# 		B2[i][j] = B2[i][j]*255

cv2.imwrite("./images/"+ "B2.jpg", B2)
 


