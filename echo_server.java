import java.io.*;
import java.net.*;

public class echo_server{
	public static void main(String args[]){
		try{
			ServerSocket server = new ServerSocket(8085);
			Socket client = server.accept();

			DataInputStream din = new DataInputStream(client.getInputStream());
			DataOutputStream dout = new DataOutputStream(client.getOutputStream());

			dout.writeUTF("Connected, Type 'exit' to stop");
			dout.flush();

			String str = "";
			while(!str.equals("exit")){
				str = din.readUTF();
				System.out.println("Client: " + str);
				dout.writeUTF(str);
				dout.flush();
			}
			din.close();
			client.close();
			server.close();

		}
		catch(IOException e){
			System.out.println(e.getMessage());
		}
	}
}

