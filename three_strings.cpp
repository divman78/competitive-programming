#include<bits/stdc++.h>
using namespace std;
#define int long long
#define P1 31
#define MOD1 1000000007

struct Hashs 
{
	vector<int> hashs;
	vector<int> pows;
	int P;
	int MOD;

	Hashs() {}

	Hashs(string &s, int P, int MOD) : P(P), MOD(MOD) 
	{
		int n = s.length();
		pows.resize(n+1, 0);
		hashs.resize(n+1, 0);
		pows[0] = 1;
		for(int i=n-1;i>=0;i--) 
		{
			hashs[i]=(1LL * hashs[i+1] * P + s[i] - 'a' + 1) % MOD;
			pows[n-i]=(1LL * pows[n-i-1] * P) % MOD;
		}
		pows[n] = (1LL * pows[n-1] * P)%MOD;
	}
	int get_hash(int l, int r) 
	{
		int ans=hashs[l] + MOD - (1LL*hashs[r+1]*pows[r-l+1])%MOD;
		ans%=MOD;
		return ans;
	}
};

signed main(){
	freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		string a, b, c;
		cin >> a >> b >> c;
		int al = a.length();
		int bl = b.length();
		int cl = c.length();

		Hashs hasha = Hashs(a, P1, MOD1);
		Hashs hashb = Hashs(b, P1, MOD1);
		Hashs hashc = Hashs(c, P1, MOD1);

		//containment of c in a
		int ind1 = -1;
		for(int i = 0; i < min(al, cl); i++){
			int hc = hashc.get_hash(0,i);
			int ha = hasha.get_hash(al-i-1,al-1);
			//cout << hc  << " " << ha << endl;
			//cout << c.substr(0, i+1) << " " << ra.substr(0,i+1) << endl; 
			if(hc == ha){
				ind1 = i;
			}
		} 

		if(ind1+1 == cl){
			cout << al + bl << endl;
			continue;
		}

		//containment of c in b
		int ind2 = -1;
		for(int i = 0; i < min(bl, cl - ind1 - 1); i++){
			int hc = hashc.get_hash(cl-i-1, cl-1);
			int hb = hashb.get_hash(0, i);
			if(hc == hb){
				ind2 = i;
			}
		}
		
		int req = ind1 + ind2 + 2;
		int zl = cl - req;
		cout << al + zl + bl << endl;


	}
}

/*
Test Case

6
bc
c
bc
abc
cde
bccd
abc
cde
cec
aaa
aaa
aaa
baa
aab
aaa
bcc
bccd
bccd


answer

3
6
7
6
6
7

*/