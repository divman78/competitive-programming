#include<bits/stdc++.h>
using namespace std;

//start ordered set
#include <ext/pb_ds/assoc_container.hpp> 
#include <ext/pb_ds/tree_policy.hpp> 
using namespace __gnu_pbds; 

#define ordered_set tree<int, null_type,less<int>, rb_tree_tag,tree_order_statistics_node_update> 
//end ordered set

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define MX LLONG_MAX
#define mp(x,y) make_pair(x,y)

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif

	int n, m, q;
	cin >> n >> m >> q;

	int arr[m+1]={0};

	for (int i = 0; i < n; ++i){
		int x; cin >> x;
		arr[x-1]++; 
	}

	arr[m] = MX;



	vector<pair<int,int> > vec1;
	for (int i = 0; i <= m; ++i)
	{
		vec1.push_back(mp(arr[i], i));
	}

	vector<pair<int,int> > quer;
	for (int i = 0; i < q; ++i)
	{
		int x; cin >> x;
		quer.push_back(mp(x, i));
	}

	sort(vec1.begin(), vec1.end());
	sort(quer.begin(), quer.end());

	for (int i = 0; i < m; ++i)
	{
		cout << vec1[i].first << " " << vec1[i].second << endl;
	}

	int ans[q];
	ordered_set oset;

	oset.insert(vec1[0].second);
	int interm = 0;
	int pos = 0;
	int curr = 0;

	cout << "hello \n";

	for (int i = 0; i < q; ++i)
	{
		int reach = quer[i].first;
		reach-=n;
		cout << "start \n";
		while(reach > (vec1[pos+1].first-vec1[pos].first)*(pos+1)+curr-interm){
			cout << i << " " << vec1[pos+1].first << " " << vec1[pos].first << " " << pos << " " << curr << " " << interm << endl;
			curr += ((vec1[pos+1].first-vec1[pos].first)*(pos+1)-interm);
			oset.insert(vec1[pos++].second);
			interm=0;
		}
		//cout <<  vec1[pos].first << " " << vec1[pos+1].first << " " << pos << " " << curr << " " << interm << " " << reach << endl;
		interm = (vec1[pos+1].first-vec1[pos].first)*(pos+1)+curr-interm-reach;
		ans[quer[i].second] = *oset.find_by_order(((interm+pos-1)%(pos+1)));
	}



	for (int i = 0; i < q; ++i)
	{
		cout << ans[i]+1 << endl;
	}

	return 0;
}