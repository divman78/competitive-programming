#include<bits/stdc++.h>
using namespace std;
#define N 200000
#define MOD 1000000007
#define int long long
int f[N];

long long modpow(long long a, long long b) {
    long long x = 1, y = a;
	while(b > 0) {
		if(b%2 == 1) {
			x=(x*y);
			if(x>MOD) x%=MOD;
		}
		y = (y*y);
		if(y>MOD) y%=MOD;
		b /= 2;
	}
	return x;
}


int inv(int x){
	return modpow(x, MOD-2);
}

int C(int n,int r){
	if(n < r) return 1;
	return (((f[n] * inv(f[n-r])) % MOD) * inv(f[r])) % MOD;
}

signed main(){
	//freopen("input.txt","r",stdin);
	f[0]=1;
	for (int i = 1; i < N; ++i)
	{
		f[i] = (i*f[i-1])%MOD;
		//cout << f[i] << " " ;
	}
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int arr[n];
		for (int i = 0; i < n; ++i) cin >> arr[i];

		sort(arr, arr+n);
	    int cnt[n];
	    cnt[0] = 1;
	    int prev = arr[0];
	    int cc = 1;
		for (int i = 1; i < n; ++i)
		{
			if(arr[i]!=prev){
				cnt[i] = cc;
				cc = 1;
				prev = arr[i];
			}
			else{
				cc++;
				cnt[i] = cc;
			}
		}
		for (int i = 0; i < n; ++i)
		{
			cout << cnt[i]  << " ";
		}
		int ans = 1;

		for (int i = n-1; i > 0 ; i-=2)
		{
			if(arr[i] == arr[i-1]) 
				ans = (ans * C(cnt[i], 2))%MOD;
			else                 //cout << C(cnt[i], 1) << " " << cnt[i] << " ",  
				ans = (ans * C(cnt[i], 1))%MOD;
		}

		cout << ans << endl;

	}
	return 0;
}