#include <bits/stdc++.h>
#define int long long 
using namespace std;
template<typename T>bool chmax(T&a,T b){if(a<b){a=b;return true;}return false;}
template<typename T>bool chmin(T&a,T b){if(a>b){a=b;return true;}return false;}
 

#define DUMP(x)  cerr<<#x<<" = "<<(x)<<endl;
 
struct edge { int to, cost; };
 
const int INF = 1e18;
const int MOD = 1e9+7;
 
signed main()
{
   #ifndef ONLINE_JUDGE
   freopen("/home/divakar/programs/input.txt","r",stdin);
   freopen("/home/divakar/programs/output.txt","w",stdout);
   #endif
   int t;
   cin >> t;
   while(t--){
   int n, W; cin >> n >> W;
   vector<int> w(n), v(n);
   for (int i = 0; i < n; i++) {
      cin >> w[i] >> v[i];
   }
 
   int vsum = accumulate(v.begin(), v.end(), 0LL);
 
   vector<int> dp(vsum+1, INF);
   dp[0] = 0;
   for (int i = 0; i < n; i++) {
      for (int j = vsum; j >= 0; j--) {
         if (j + v[i] <= vsum) {
            chmin(dp[j + v[i]], dp[j] + w[i]);
         }
      }
   }
 
   for (int i = vsum; i >= 0; i--) {
      if (dp[i] <= W) {
         cout << i << endl;
         //return 0;
         break;
      }
   }
   }
 
   return 0;
}