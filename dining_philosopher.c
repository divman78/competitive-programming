#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<semaphore.h>
#include<pthread.h>

#define N 5
#define LEFT (phili + 1)%N
#define RIGHT (phili + N -1)%N
#define H 0
#define E 1
#define T 2

sem_t mutex;
sem_t S[N];
int state[N];
int phil[] = {0,1,2,3,4};

void test(int phili);
void take_fork(int phili);
void put_fork(int phili);
void* philosopher(int num);

int main(){
	int i;
	pthread_t thread_id[N];
	sem_init(&mutex, 0, 1);
	for (int i = 0; i < N; ++i)
	{
		sem_init(&S[i], 0 ,0);
	}
	for (int i = 0; i < N; ++i)
	 {
	 	pthread_create(&thread_id[i], NULL, philosopher, &phil[i]);
	 } 
	 for (int i = 0; i < N; ++i)
	 {
	 	pthread_join(&thread_id[i],NULL);
	 }
}

test(int phili){
	if(state[phili] == H && state[LEFT] != E && state[RIGHT]){
		state[phili] = E;
		sleep(2);
		printf("philosopher %d taking %d and %d forks \n",phil[phili], LEFT+1, RIGHT+1);
		printf("philosopher %d is eating now\n", phil[phili]);
		sem_post(&S[phili]);
	}
}