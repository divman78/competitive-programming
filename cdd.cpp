#include<bits/stdc++.h>
using namespace std;
#define endl "\n"
#define int long long


signed main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE 
	freopen("input.txt","r",stdin);
	#endif
	int t = 1;
	//cin >> t;
	while(t--){
		int n;
		cin >> n;
		int arr[n+2];
		for (int i = 1; i <= n; ++i)
		{
			cin >> arr[i];
		}
		int p = arr[1];
		int q;
		int cnt = 1;
		int ans = 0;
		for (int i = 2; i <= n; ++i)
		{
			if(arr[i] == arr[i-1] + 1){
				q  = arr[i];
				cnt++;
				if(i == n){
					if(p != 1){
						cnt--;
					}
					if(q != 10){
						cnt--;
					}
					ans += cnt;
				}
			}
			else{
				if(cnt >= 2)
				{
					if(p != 1){
						cnt--;
					}
					if(q != 1000){
						cnt--;
					}
					ans += cnt;
				}
				cnt = 1;
				p = arr[i];
			}
		}
		cout << ans << endl;



	}
}