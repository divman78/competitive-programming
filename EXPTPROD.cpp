#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define MOD 1000000007
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;

void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

int bigMOD(int num,int n){
  if(n==0) return 1;
  int x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}

int MODinverse(int num){
  return bigMOD(num,MOD-2)%MOD;
}

vector<int> multiply(vector<int>& arr1, vector<int>& arr2){
	int sz = arr1.size();
	vector<int> ret(sz,0);
	for (int i = 0; i < sz; ++i)
	{
		for (int j = 0; j < sz; ++j)
		{
			int t = (i*j)%sz;
			ret[t] = (ret[t]+arr1[i]*arr2[j])%MOD;
		}
	}
	return ret;
}

vector<int> ModPow(vector<int>& freq, int k){
	if(k==1){
		return freq;
	}
	vector<int> ret = ModPow(freq,k/2);
	ret = multiply(ret, ret);
	if(k%2==1) ret = multiply(ret, freq);
	return ret;
}


signed main(){
	read_file();
	int t;
	cin >> t;
	while(t--){
		int n,k;
		cin >> n >> k;
		vector<int> freq(n,0);
		for (int i = 0; i < n; ++i)
		{
			int x;
			cin >> x;
			freq[x%n]++;
		}
		vector<int> vans = ModPow(freq, k);
		int ans = 0;
		for (int i = 0; i < n; ++i)
		{
			ans = (ans+i*vans[i])%MOD;
		}
		ans = (ans*MODinverse(bigMOD(n,k)))%MOD;
		cout << ans << endl;
	}
	return 0;
}