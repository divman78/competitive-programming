#include<bits/stdc++.h> 
using namespace std;
#define N 400400
#define ll long long

pair<ll, ll> lr[N];
ll m, n;

bool predicate(ll x){

  ll current = lr[0].first;
  for (int i = 1; i < n; ++i){
    if(current + x > lr[i].second){
      return false;
    }
    current = max(current + x, lr[i].first);
  }

  ll left = current;

  for(int i = n-2; i>=0; i--){
    current = min(current - x, lr[i].second);
  }

  return m - left + current >= x;
}

int main(){
  cin >> m >> n;
  for(int i = 0; i < n; i++){
  cin >> lr[i].first;
  cin >> lr[i].second;
  }

  ll ans = 0;

  ll lo = 1, hi = m;
  while (lo <= hi) {
  ll mid = lo + hi >> 1;
    if (predicate(mid)) {
      ans = mid;
      lo = mid + 1;
    } 
    else {
      hi = mid - 1;
    }
  }

  cout << ans << endl;
}