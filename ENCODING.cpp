#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MOD 1000000007
#define N 100100
#define MX 2000000000

void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

int bigMOD(int num,int n){
  if(n==0) return 1;
  int x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}

int fx[N][10];
int power10[2*N];

void preprocess(){

	power10[0] = 1;
	for (int i = 1; i < 2*N; ++i)
	{
		power10[i] = power10[i-1]*10;
		if(power10[i]>=MOD) power10[i]-=MOD;
	}

	for (int i = 0; i < 10; ++i){
		fx[0][i] = i;
	}

	for (int i = 1; i < N; ++i){
		int temp = (power10[i*2]-power10[i*2-2]+MOD)%MOD;
		temp=(temp+power10[2*i-1]*9)%MOD;
		for (int j = 0; j < 10; ++j){
			fx[i][j] = (fx[i-1][9]+j*temp)%MOD;
			if(j>0) fx[i][j] = (fx[i][j] + fx[i][j-1])%MOD;
		}
	}	
}

pair<int,int> pre(string& s, int i){
	int num = s[i]-48;
	int ret = 0,cnt = 0;
	if(num>0) {ret = fx[i][num-1]; cnt = (num*power10[i])%MOD;}
	if(i){
		pair<int,int> prev = pre(s, i-1);
		//for num
		ret = (ret+((prev.second*power10[i])%MOD)*num)%MOD;
		ret = ret+prev.first;
		cnt = (cnt+prev.second)%MOD;
	}
	return mp(ret, cnt);
}

int naive(string& s){
	int ans = 0;
	int n = s.length();
	for (int i = 1; i < s.length(); ++i)
	{
		if(s[i]!=s[i-1]) ans = (ans+power10[i-1]*(s[i-1]-48))%MOD;
	}
	ans = (ans+power10[n-1]*(s[n-1]-48))%MOD;
	for (int i = n-1; i >= 0; --i)
	{
		if(s[i]=='0') s[i]=9;
		else{
			s[i]--;
			if(i==0&&s[i]=='0')
		}
	}
	return ans;
}


signed main(){
	read_file();
	clock_t clk = clock();
	
	preprocess();
	int t=1;
	cin >> t; 
	while(t--){
		int n1, n2;
		cin >> n1 >> n2;
		string s1, s2;
		cin >> s1 >> s2;

		for (int i = n1-1; i >= 0; --i)
		{
			if(s1[i]-48==0) s1[i]=9;
			else{
				s1[i]--;
				break;
			}
		}

		int p1 = pre(s1, n1-1).first;
		int p2 = pre(s2, n2-1).first;


		int ans = (p2-p1+MOD)%MOD;

		//int ans = (naive(s2)-naive(s1)+MOD)%MOD;

		cout << ans << endl;



        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















