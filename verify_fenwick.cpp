#include<bits/stdc++.h>
using namespace std;
#define MAXN 1000

int fen[MAXN];

void add(int x, int val)   //adds val from 0 to x index
{
    for (int i = x + 1; i < MAXN; i += i & (-i)) fen[i] += val;
}

int get(int x)
{
    int ans = 0;
    for (int i = x; i > 0; i -= i & (-i)) ans += fen[i];
    return ans;
}

int sum(int x, int y)
{
    return get(y) - get(x);
}


signed main(){
	for (int i = 1; i <= 10; ++i)
	{
		add(i, 1);
		for (int j = 0; j <= 10; ++j)
		{
			cout << get(j) << endl;
		}
		cout << endl;
	}
}