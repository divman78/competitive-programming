#include<bits/stdc++.h>
using namespace std;
#define int long long

void makedistinct(vector<int>& vec){
  sort( vec.begin(), vec.end() );
  vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
}

const int MAXN = 1000 * 22;
const int MO = 5e6;

int fen[55][MAXN];

void add(int ki, int x, int val)
{
    for (int i = x + 1; i < MAXN; i += i & (-i)) fen[ki][i] = (fen[ki][i]+val)%MO;
}

int get(int ki, int x)
{
    int ret = 0;
    for (int i = x; i > 0; i -= i & (-i)) ret = (ret+fen[ki][i])%MO;
    return ret;
}

int dp[55][MAXN];

signed main(){

	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif

	int n, k;
	cin >> n >> k;

	int arr[n];
	vector<int> vec;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
		vec.push_back(arr[i]);
	}
	makedistinct(vec);


	for (int i = 0; i < n; ++i)
	{
		arr[i] = 1+(lower_bound(vec.begin(), vec.end(), arr[i]) - vec.begin());
	}



	for (int i = 0; i < n; ++i)
	{
		for (int j = 1; j <= k; ++j)
		{
			int curr;
			if(j == 1){
				curr = (1-dp[j][arr[i]]+MO)%MO;
			}
			else{
				curr = (get(j-1, arr[i]) - dp[j][arr[i]] + MO)%MO;
			}
			add(j, arr[i], curr);
			dp[j][arr[i]] = (dp[j][arr[i]] + curr)%MO;
		}
	}

	cout << get(k, n+1) << endl;


}