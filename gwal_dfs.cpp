#include<bits/stdc++.h>
using namespace std;
#define MOD 1000000007
#define int unsigned long long


void dfs(int u, int factor, int prsize, vector<vector<int> >& adj, vector<int>& ans){
	for(int i = 0; i < adj[u].size(); i++){
		int v = adj[u][i];
		ans[v] = (ans[u] + (i*factor)%MOD)%MOD;
		dfs(v, (factor*prsize)%MOD, adj[v].size(), adj, ans);
	}
}

signed main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
    int t;
    cin >> t;
    while(t--){
    	int n;
    	cin >> n;
    	vector<vector<int> > adj(n);
    	for(int i = 0; i < n; i++){
    		int cnt;
    		cin >> cnt;
    		while(cnt--){
    			int ch;
    			cin >> ch;
    			ch--;
    			adj[i].push_back(ch);
    		}
    	}
    	vector<int> ans(n);
    	ans[0] = 1;
    	dfs(0, 1, adj[0].size(), adj, ans);

    	for (int i = 0; i < n; ++i)
    	{
    		cout << ans[i];
    		if(i != n-1){
    			cout << " ";
    		}
    		else{
    			cout << "\n";
    		}
    	}
    }	
}