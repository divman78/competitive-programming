#include<bits/stdc++.h>
using namespace std;
#define N 500500

int n;

struct Range{ int prefixSum , suffixSum ,Sum ,maxSum ; };

Range range[ 2*N ];

Range merge(Range a , Range b)
{
	if(a.Sum == INT_MIN){
		return b;
	}
	if(b.Sum == INT_MIN){
		return a;
	}
    Range res ;

    res.Sum = a.Sum + b.Sum;

    res.maxSum = max( max( a.maxSum , b.maxSum ) , (a.suffixSum + b.prefixSum ) ) ;

    res.prefixSum = max( a.prefixSum , a.Sum + b.prefixSum );

    res.suffixSum = max( b.suffixSum , b.Sum + a.suffixSum );

    return res ;
}

void build(){
	for (int i = n-1; i > 0 ; --i)
	{
		range[i] = merge(range[i << 1], range[i << 1 | 1]);
	}
}

int query(int l, int r){
	Range res1, res2;
	res1.prefixSum = res1.suffixSum = res1.Sum = res1.maxSum = INT_MIN;
	res2.prefixSum = res2.suffixSum = res2.Sum = res2.maxSum = INT_MIN;
	for (l = l+n, r = r+n; l < r; l >>= 1, r >>= 1)
	{
		if(l&1) res1 = merge(res1, range[l++]);
		if(r&1) res2 = merge(range[--r], res2);
	}
	return merge(res1, res2).maxSum;
}



int main(){
	//freopen("input.txt","r",stdin);
	cin >> n;
	int arr[n];
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
		range[i+n].prefixSum = range[i+n].suffixSum = range[i+n].Sum = range[i+n].maxSum = arr[i];
	}
	build();
	int m;
	cin >> m; 
	while(m--){
		int x, y;
		cin >> x >> y;
		int ans = query(x-1, y);
		cout << ans << endl;
	}
	return 0;
}