#include<bits/stdc++.h>
using namespace std;
#define endl "\n"
#define int long long

bool check(std::vector<int> v){
	for(int i=0;i<v.size();i++){
		if(v[i] != 1){
			return false;
		}
	}
	return true;
}

bool check2(std::vector<int> &v){
	for(int i=0;i<v.size();i++){
		if(v[i]%2 == 1){
			return false;
		}
	}
	return true;
}

void change(std::vector<int> &v){
	for(int i=0;i<v.size();i++){
		if(v[i]%2 == 1){
			v[i]++;
		}
	}
}

void change2(std::vector<int> &v){
	for(int i=0;i<v.size();i++){
		if(v[i]%2 == 0){
			v[i] /= 2;
		}
	}
}

void print(std::vector<int> v){
	for (int i = 0; i < v.size(); ++i)
	{
		cout << v[i] << " ";
	}
	cout << endl;
}

signed main(){
//	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE 
//	freopen("input.txt","r",stdin);
	#endif
	int t = 1;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int nn = n;
		if(n == 1){
			cout<<1<<" "<<0<<endl;
			continue;
		}
		map<int,int> mm;
		for (int i = 2; i <= (nn); ++i)
		{
			while(n%i == 0){
				n/=i;
				mm[i]++;
			}
		}
		//cout<<"Hello"<<endl;
		int cnt = 0;
		int mx = -1;
		int ans = 1;
		vector<int> p;
		for(map<int,int> :: iterator itr = mm.begin(); itr!= mm.end(); itr++){
			//cout << itr->first << " " << itr->second << endl;
			mx = max(mx,itr->second);
			ans = ans * itr->first;
			p.push_back(itr->second);
		}
		//cout << n << " ";

		n = mx;

		int count  = 0;
		int flag = 0;

		int j = 1;

		if(mx == 1){
			cout<<ans<<" "<<0<<endl;
			continue;
		}
		while(true){
			int  p1 = pow(2,j);
			if( p1 == n){
				count = j;
				break;
			}
			else if(p1 > n){
				count = j;
				flag = 1;
				count++;
				break;
			}
			j++;
		}

		if(flag == 0){
			//cout<<"Hello"<<endl;
			for(int i=0;i<p.size();i++){
				if(p[i] != mx){
					count++;
					break;
				}
			}
		}

		cout << ans << " "<< count << endl;



	}
}