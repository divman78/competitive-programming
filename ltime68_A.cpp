#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MOD 1000000007
#define N 50010
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
//#define ONLINE_JUDGE

int dp[900][900];
int vt[900];
vector<int> adj[900];



signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();


	int t;
	cin >> t;
	while(t--){
		int n, m, k;
		cin >> n >> m >> k;

		memset(dp, 0, sizeof(dp));
		for (int i = 0; i < n; ++i) adj[i].clear();


		for (int i = 0; i < m; ++i)
		{
			int u, v;
			cin >> u >> v;
			adj[u-1].push_back(v-1);
			adj[v-1].push_back(u-1);
		}

		int qq;
		cin >> qq;
		memset(vt, -1, sizeof(vt));

		int flag = 0;

		for (int i = 0; i < qq; ++i)
		{
			int vv, tt;
			cin >> vv >> tt;
			if(vt[tt] != -1){
				flag = 1;
			}
			vt[tt] = vv-1; 
		}

		if(flag){
			cout << 0 << endl;
			continue;
		}



		for (int i = 0; i < n; ++i)
		{
			adj[i].push_back(i);
		}

		memset(dp, 0, sizeof(dp));
		dp[0][0] = 1;

		for (int i = 1; i <= k; ++i)
		{
			if(vt[i] != -1){
				for (int j = 0; j < adj[vt[i]].size(); ++j)
				{
					dp[vt[i]][i] = (dp[vt[i]][i] + dp[adj[vt[i]][j]][i-1])%MOD;
				}
			}
			else{
				for (int j = 0; j < n; ++j)
				{
					for(int l = 0; l < adj[j].size(); l++)
					{
						dp[j][i] = (dp[j][i] + dp[adj[j][l]][i-1])%MOD;
					}
				}
			}
		}

		int ans = 0;
		for (int i = 0; i < n; ++i)
		{
			ans = (ans+dp[i][k])%MOD;
		}

		cout << ans;
		cout << endl;
	}

	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif




	return 0;
}














//teja349
// #include <bits/stdc++.h>
// #include <vector>
// #include <set>
// #include <map>
// #include <string>
// #include <cstdio>
// #include <cstdlib>
// #include <climits>
// #include <utility>
// #include <algorithm>
// #include <cmath>
// #include <queue>
// #include <stack>
// #include <iomanip>
// #include <ext/pb_ds/assoc_container.hpp>
// #include <ext/pb_ds/tree_policy.hpp> 
// //setbase - cout << setbase (16); cout << 100 << endl; Prints 64
// //setfill -   cout << setfill ('x') << setw (5); cout << 77 << endl; prints xxx77
// //setprecision - cout << setprecision (14) << f << endl; Prints x.xxxx
// //cout.precision(x)  cout<<fixed<<val;  // prints x digits after decimal in val

// using namespace std;
// using namespace __gnu_pbds;

// #define f(i,a,b) for(i=a;i<b;i++)
// #define rep(i,n) f(i,0,n)
// #define fd(i,a,b) for(i=a;i>=b;i--)
// #define pb push_back
// #define mp make_pair
// #define vi vector< int >
// #define vl vector< ll >
// #define ss second
// #define ff first
// #define ll long long
// #define pii pair< int,int >
// #define pll pair< ll,ll >
// #define sz(a) a.size()
// #define inf (1000*1000*1000+5)
// #define all(a) a.begin(),a.end()
// #define tri pair<int,pii>
// #define vii vector<pii>
// #define vll vector<pll>
// #define viii vector<tri>
// #define mod (1000*1000*1000+7)
// #define pqueue priority_queue< int >
// #define pdqueue priority_queue< int,vi ,greater< int > >
// #define flush fflush(stdout) 
// #define primeDEN 727999983
// mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

// // find_by_order()  // order_of_key
// typedef tree<
// int,
// null_type,
// less<int>,
// rb_tree_tag,
// tree_order_statistics_node_update>
// ordered_set;

// vector<vi> adj(12345);
// vector<vii> vec(12345);
// int dp[12345],dp1[12345];
// int ans[12345];
// int member[12345];
// int main(){
//   freopen("input.txt","r",stdin);
//     std::ios::sync_with_stdio(false); cin.tie(NULL);
//   	int t;
//   	cin>>t;
//   	while(t--){
//   		int n,m,k;
//   		cin>>n>>m>>k;
//   		int i;
//   		int u,v;
//   		rep(i,n){
//   			adj[i].clear();
//   		}
//   		rep(i,k+2){
//   			member[i]=-1;
//   		}
//   		rep(i,m){
//   			cin>>u>>v;
//   			u--;
//   			v--;
//   			adj[u].pb(v);
//   			adj[v].pb(u);
//   		}
//   		int q;
//   		cin>>q;
//   		int tim;
//   		int j;
//       int flag=0;
//   		rep(i,q){
//   			cin>>u>>tim;
//   			u--;
//   			if(member[tim]==-1){
//           member[tim]=u;
//         }
//         else if(member[tim]!=u){
//           flag=1;
//         }
//   		}
//       if(flag){
//         cout<<0<<endl;
//         continue;
//       }
//   		rep(i,n){
//   			dp[i]=0;
//   		}
//   		dp[0]=1;
//   		rep(tim,k+1){
//         if(member[tim]!=-1){
//           rep(i,n){
//             if(i!=member[tim])
//               dp[i]=0;
//           }
//         }
//         if(tim==k)
//           break;
//   			rep(i,n){
//   				dp1[i]=dp[i];
//   				rep(j,adj[i].size()){
//   					dp1[i]+=dp[adj[i][j]];
//   					if(dp1[i]>=mod)
//               dp1[i]-=mod;
//   				}
//   			}
//   			rep(i,n){
//   				dp[i]=dp1[i];
//   			}
//   		}
//       int sumi=0;
//   		rep(i,n){
//         sumi+=dp[i];
//         if(sumi>=mod)
//           sumi-=mod;
//       }
//       cout<<sumi<<endl;
//   	}
//     return 0;   
// }