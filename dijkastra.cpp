#include<bits/stdc++.h>
using namespace std;
#define mp(x, y) make_pair(x, y)
#define int long long
#define MAX 100000000000000000000000


int n, e, k;

void dij(vector<int>* adj, vector<pair<int, pair<int,int> > > edges){
	int d[n];
	int ed[e+1];
	for (int i = 0; i <= e; ++i)
	{
		ed[i] = n;
	}
	for (int i = 0; i < n; ++i)
	{
		d[i] = MAX;
	}
	priority_queue<pair<int,int>> pq;

	pq.push(mp(0, 0));

	set<int> ans;
	bool visited[n];
	for (int i = 0; i < n; ++i)
	{
		visited[i] = false;
	}
	visited[0] = true;

	while(ans.size() < k && !pq.empty()){
		pair<int, int> p = pq.top();
		int distv = -p.first, v = p.second;
		// cout << "fanta \n";
		// cout << v << " " << distv << endl;
		if(ed[v]!=n)ans.insert(ed[v]);
		pq.pop();
		for(int i = 0; i < adj[v].size(); i++){
			pair<int, pair<int, int> > ev = edges[adj[v][i]];
			int vv = ev.second.first;
			if(vv == v) vv = ev.second.second; 
			//cout<< "vv=" << vv << " " << adj[v][i] << " ";
			if(visited[vv] == false){
				if(ev.first + distv < d[vv]){
					//cout << " ehllo " << vv << " ";
					ed[vv] = adj[v][i];
					d[vv] = ev.first + distv;
					pq.push(mp(-d[vv], vv));
					visited[v] = true;
				}
			}
		}
		//cout << endl;
	}

	cout << ans.size() << endl;
	for (set<int>::iterator i = ans.begin(); i != ans.end(); i++)
	{
		cout << (*i) + 1 << " "; 
	}
	cout << endl;



}


signed main(){
	//freopen("input.txt","r",stdin);
	cin >> n >> e >> k;
	vector<pair<int, pair<int,int> > > edges;
	vector<int> adj[n];

	for (int i = 0; i < e; ++i)
	{
		int x, y, w;
		cin >> x >> y >> w;
		adj[x-1].push_back(i);
		adj[y-1].push_back(i);
		edges.push_back(mp(w, mp(x-1, y-1)));
	}

	dij(adj, edges);



}