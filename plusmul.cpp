//In the Name of God
//Ya Ali

#include<bits/stdc++.h>

#define err(A) cout<<#A<<" = "<<(A)<<endl

using namespace std;

const int M=1e9+7;
const int maxn=100100;

int n;

int a[maxn];

int t[maxn];

int dp[maxn];
int pp[maxn];

int main()
{
  ios::sync_with_stdio(0);cin.tie(0);
  #ifndef ONLINE_JUDGE
  freopen("/home/divakar/programs/input.txt","r",stdin);
  freopen("/home/divakar/programs/output.txt","w",stdout);
  #endif

  t[0]=1;
  for(int i=1;i<maxn;i++)
    t[i]=(t[i-1]+t[i-1])%M;
  
  int T;
  cin>>T;
  while(T--)
    {
      cin>>n;
      
      for(int i=1;i<=n;i++)
  cin>>a[i];

      int tot=a[1];

      dp[1]=a[1];
      pp[1]=a[1];
      
      for(int i=2;i<=n;i++)
  {
    pp[i]=(1ll*pp[i-1]*a[i]%M+1ll*a[i]*t[i-2]%M)%M;
    dp[i]=(tot+pp[i])%M;
    tot=(tot+dp[i])%M;
   // cout << dp[i] << " ";
  }
      cout<<dp[n]<<endl;
    }
  return 0;
}
