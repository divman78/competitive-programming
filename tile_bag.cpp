#include<bits/stdc++.h>
#include<vector>
#include<set>
#include<string>
#include<map>
#include<queue>
#include<stack>

#define int unsigned long long
#define endl "\n"
#define ll long long 

const int MOD=(int)1e9+7;


using namespace std;



const int mod = 1e9 + 7;
int inv(int a) {
	int res = 1;
	for (int n = mod - 2; n > 0; n >>= 1) {
		if (n & 1) res = (res * a) % mod;
		a = (a * a) % mod;
	}
	return res;
}



ll bigMOD(ll num,ll n){
  if(n==0) return 1;
  ll x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}

 
ll MODinverse(ll num){
  return bigMOD(num,MOD-2)%MOD;
}
 


// Global Variable Ends

signed main(){
	ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
//	freopen("output.txt","w",stdout);
	#endif
	

	int t;
	cin>>t;
	while(t--){
		int x,y,s;
		cin>>x>>y>>s;
		int p,q;
		cin>>p>>q;
		if(s % x == 0){
			int count = 1;
			
			while(x < s){
				s = s/2;
				count = count*2;
			}
			int num = count*(q);
			int dem = p;
			int g = __gcd(num,dem);
			num = num/g;
			dem = dem/g;
			int m = inv(dem);
			cout<<(num*m)%MOD<<endl;
		}
		else{
			int count = 1;
			while(y < s){
				s = s/2;
				count = count*2;
			}
			int num = count*(q);
			int dem = q-p;
			int g = __gcd(num,dem);
			num = num/g;
			dem = dem/g;
			int m = inv(dem);
			cout<<(num*m)%MOD<<endl;
		}
			
	}
		
	
	return 0;
}