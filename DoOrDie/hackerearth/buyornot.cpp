#include<bits/stdc++.h>
using namespace std;

int n, k;

int quer[1001][1001];

void rec(vector<vector<pair<int,int> > >& vec, vec<bool>& vis, int w){
	if(w>100) return;
	if(vec[w].size()==0){
		rec(vec,vis,w+1);
	}
	for (int i = 0; i < vec[w].size(); ++i)
	{
		int u = vec[w][i].first;
		int v = vec[w][i].second;
		int uv = vis[u]; vv = vis[v];
		if(!uv || !vv){
			quer[u][v] = 1; quer[v][u] = 1; vis[u]  = 1; vis[v] = 1;
		}
		rec(vec, vis, w+1);
		vis[u]=uv; vis[v]=vv;
	}
}

int main(){
	int t;
	cin >> t;
	while(t--){

		memset(quer,0,sizeof(quer));
		
		cin >> n >> k;
		vector<vector<pair<int,int> > > vec(101);

		for (int i = 0; i < k; ++i)
		{
			int x, y, c;
			cin >> x >> y >> c;
			vec[c].push_back(make_pair(x,y));
		}

		vector<bool> vis(n,false);

		rec(vec, vis, 1);

		int m;
		cin >> m;
		int cnt = 0;
		for (int i = 0; i < m; ++i)
		{
			int x, y;
			cin >> x >> y;
			if(quer[x][y]){
				cnt++;
			}
		}
		cout << cnt << "/" << (m-cnt) << endl;
	}
}