#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 1001
#define int long long

int k, n;
int arr[N];
int mem[N][N];

int calc(int nn, int kk){
	if(nn == 0){
		return kk;
	}
	if(mem[nn][kk]!=0){
		return mem[nn][kk];
	}
	mem[nn][kk] = max(calc(nn-1, kk), calc(nn-1, kk^arr[nn-1]));
	return mem[nn][kk];
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	clock_t clk = clock();

	int t;
	cin >> t;
	//t=1;
	while(t--){
		cin >> n;
		cin >> k;
		for(int i = 0; i < n; i++){
			cin >> arr[i];
		}
		memset(mem, 0, sizeof(mem));
		int ans = 0;
		ans = calc(n, k);
		cout << ans;
		cout << endl;
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}