#include<bits/stdc++.h>
#include<vector>
#include<map>
#define TLE while(1);
#define mx 10005
using namespace std;
vector<int> tree[mx];
vector<int> white,black;
int visited[mx];
void dfs(int node,int p_color){
	visited[node] = 1;
	if(p_color){
		black.push_back(node);
	}
	else{
		white.push_back(node);
	}
	for(int i=0;i<tree[node].size();i++){
		if(!visited[tree[node][i]]){
			dfs(tree[node][i],!p_color);
		}
	}
}
signed main(){
	freopen("input.txt","r",stdin);
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		white.clear();
		black.clear();
		for(int i=1;i<=n;i++){
			tree[i].resize(0);
			visited[i] = 0;
		}
		for(int i=0;i<n-1;i++){
			int x,y;
			cin>>x>>y;
			tree[x].push_back(y);
			tree[y].push_back(x);	
		}
		dfs(1,0);
		map<int,int> wb,bb;
		if(black.size() == white.size()){
			cout<<1<<endl;
		}
		else{
			cout<<2<<endl;
			int a = white.size();
			int b = black.size();
			if(a > b){
				for(int i=0;i<white.size();i++){
					if(tree[white[i]].size() == 1){
						black.push_back(white[i]);
						wb[white[i]] = 1;
						a--;
						b++;
					}
					if(a == b){
						break;
					}
				}
			}
			else{
				for(int i=0;i<black.size();i++){
					if(tree[black[i]].size() == 1){
						white.push_back(black[i]);
						bb[black[i]] = 1;
						a++;
						b--;
					}
					if(a == b){
						break;
					}
				}
			}
		}
		for(int i=0;i<white.size();i++){
			if(wb[white[i]] == 0){
				cout<<white[i]<<" ";
			}
		}
		cout<<endl;
		for(int i=0;i<black.size();i++){
			if(bb[white[i]] == 0){
				cout<<black[i]<<" ";
			}
		}
		cout<<endl;
	}
}
