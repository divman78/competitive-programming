#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

char fun(int p){
	if(p%4==1){
		return 'A';
	}
	if(p%4==3){
		return 'B';
	}
	if(p%4==2){
		return 'C';
	}
	if(p%4==0){
		return 'D';
	}
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	//freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cin >> t; 
	while(t--){

		int n, m, k;
		cin >> n >> m >> k;

		vector<int> arr(m);
		for (int i = 0; i < m; ++i)
		{
			cin >> arr[i];
		}

		int st = 0;
		int removed = 0;
		int ans = 0;
		while(removed != m){
			int page = ((arr[st]-removed)/k) + (int)(((arr[st]-removed)%k)?1:0);
			int cnt = lower_bound(arr.begin(), arr.end(), page*k+1+removed)-arr.begin()-removed;
			removed += cnt;
			st=removed;
			ans++;
		}

		cout << ans << endl;




        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















