 

#include <cassert>
#include<iostream>
#include<algorithm>
#include<cstdio>
#include<complex>
#include<vector>
#include<set>
#include<map>
#include<cmath>
#include<queue>
#include<string>
#include<cstdlib>
#include<memory.h>
#include<ctime>
#include<bitset>
#include<fstream>
#include<queue>
#include<stack>
#include<unordered_map>
#include<unordered_set>
#define LocalHost

using namespace std;

typedef long double ld;

typedef long long ll;
typedef pair<int,int>	pii;
typedef pair<ld,ld>	pdd;
typedef vector<int> vi;
typedef vector<ld> vd;
typedef pair<ll,ll> pl;

#define FOR(i,a,b)		for(int i=(a);i<(b);i++)	
#define REP(i,n)		FOR(i,0,n)
#define SORT(v)			sort((v).begin(),(v).end())
#define UN(v)			SORT(v),(v).erase(unique((v).begin(),(v).end()),(v).end())
#define CL(a,b)			memset(a,b,sizeof a)
#define pb				push_back	

int n;
int a[111111];

int s[1<<20];

void setMax(int pos,int val){
	pos += (1<<19);
	while(pos){
		s[pos]=max(s[pos],val);
		pos>>=1;
	}
}
int getMax(int l,int r){
	l+=(1<<19);
	r+=(1<<19);
	int res = 0;
	while(l<=r){
		res=max(res,s[l]);
		res=max(res,s[r]);
		l=(l+1)>>1;
		r=(r-1)>>1;
	}
	return res;
}
vi t[111111];

int solve(int l,int r,int lim){
	if(lim<0) return 100;
	if(l>r)return 0;
	if(l+1>=r) return 1;
	int mx = getMax(l,r);
	int pos = *lower_bound(t[mx].begin(),t[mx].end(),l);

	int res = 100;
	if(pos-l>r-pos){
		res=min(res,solve(pos+1,r,min(lim-1,res)));
		res=min(res,solve(l,pos-1,min(lim-1,res)));
	}else{
		res=min(res,solve(l,pos-1,min(lim-1,res)));
		res=min(res,solve(pos+1,r,min(lim-1,res)));
	}

	return 1 + res;
}

int main(){	
#ifdef LocalHost
	//freopen("input.txt","r",stdin);
	//freopen("output.txt","w",stdout);
#endif

	int tc;
	cin>>tc;
	REP(TC,tc){
		cin>>n;
		REP(i,n)t[i].clear();
		REP(i,n){
			int z = (i+(1<<19));
			while(z){
				s[z]=0;
				z>>=1;
			}
		}
		vi v;
		REP(i,n)scanf("%d",a+i),v.pb(a[i]);
		UN(v);
		REP(i,n)a[i]=lower_bound(v.begin(),v.end(),a[i])-v.begin();
		REP(i,n)setMax(i,a[i]),t[a[i]].pb(i);
		cout<<solve(0,n-1,100)<<"\n";
	}

#ifdef LocalHost
	printf("TIME: %.3lf\n",ld(clock())/CLOCKS_PER_SEC);
#endif
	return 0;
}