#include<bits/stdc++.h>
using namespace std;

#define mp(x, y) make_pair(x, y)
#define int long long

void dfs(int u, int pr, vector<vector<pair<int,int> > >& adj, vector<int>& fuel0, vector<int>& w){
	int fuelc = 0;
	for(int i = 0; i < adj[u].size(); i++){
		int v = adj[u][i].second;
		if(v == pr)
			continue;
		dfs(v, u, adj, fuel0, w);
		if(fuelc < fuel0[v] - adj[u][i].first){
			fuelc = fuel0[v] - adj[u][i].first;
			if(u == 1){
				////cout << v+1 << " "<<  adj[u][i].first << endl;
			}
		}
	}
	////cout << u+1  <<  " -> " << fuelc << endl;
	fuel0[u] = w[u] + fuelc;
}

int ans = 0;

void dfs2(int u, int pr, int max1, int max2, int u1, int u2, vector<vector<pair<int,int> > >& adj, vector<int>& fuel0, vector<int>& w){
	int fuelu = fuel0[u];
	int temp;
	if(u==pr){
		ans = max(ans, fuel0[u]);
		max1 = 0, max2 = 0;
		u1 = -1, u2 = -1;
		for (int i = 0; i < adj[u].size(); ++i)
		{
			int v = adj[u][i].second;
			if(v == pr) 
				continue;
			if(fuel0[v] - adj[u][i].first > max1){
				u1 = v;
				max1 = fuel0[v] - adj[u][i].first;
			}
		}
		for (int i = 0; i < adj[u].size(); ++i)
		{
			int v = adj[u][i].second;
			if(v == pr || v == u1) 
				continue;
			if(fuel0[v] - adj[u][i].first > max2){
				u2 = v;
				max2 = fuel0[v] - adj[u][i].first;
			}
		}
		for (int i = 0; i < adj[u].size(); ++i)
		{
			int v = adj[u][i].second;
			if(v == pr) 
				continue;
			dfs2(v, u, max1, max2, u1, u2, adj, fuel0, w);
		
		}
		return;
	}
	for(int i = 0; i < adj[u].size(); i++){
		if(adj[u][i].second == pr){
			temp = -adj[u][i].first + w[pr] + fuel0[u];
			break;
		}
	}
	if(u1 == u){
		temp += max2;
	}
	else if(u2 == u){
		temp += max1;
	}
	else{
		temp += max1;
	}
	ans = max(ans, max(fuel0[u], temp));

	max1 = 0, max2 = 0;
	u1 = -1, u2 = -1;
	for (int i = 0; i < adj[u].size(); ++i)
	{
		int v = adj[u][i].second;
		if(v == pr) 
			continue;
		if(fuel0[v] - adj[u][i].first > max1){
			u1 = v;
			max1 = fuel0[v] - adj[u][i].first;
		}
	}
	for (int i = 0; i < adj[u].size(); ++i)
	{
		int v = adj[u][i].second;
		if(v == pr || v == u1) 
			continue;
		if(fuel0[v] - adj[u][i].first > max2){
			u2 = v;
			max2 = fuel0[v] - adj[u][i].first;
		}
	}
	for (int i = 0; i < adj[u].size(); ++i)
	{
		int v = adj[u][i].second;
		if(v == pr) 
			continue;
		dfs2(v, u, max1, max2, u1, u2, adj, fuel0, w);
	}
}


signed main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int n;
	cin >> n;
	vector<vector<pair<int,int> > >adj(n);
	vector<int> w(n);
	for (int i = 0; i < n; ++i)
	{
		cin >> w[i];
	}
	for(int i = 0; i < n-1; i++){
		int u, v, wt;
		cin >>  u >> v >> wt;
		u--, v--;
		adj[u].push_back(mp(wt, v));
		adj[v].push_back(mp(wt, u));
	}
	vector<int> fuel0(n,0);

	dfs(0,0, adj, fuel0, w);
	// for(int i = 0; i < n; i++){
	// 	//cout<< i+1 << " -> " << fuel0[i] << "\n";
	// }
	dfs2(0, 0, 0, 0, -1, -1, adj, fuel0, w);
	cout << ans << endl;
}
