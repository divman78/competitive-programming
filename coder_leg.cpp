#include <bits/stdc++.h>
using namespace std;
#define ll long long
#define f(i, x, n) for(int i = x; i < (int)(n); ++i)

int const N = 100000;
bool dp[N + 1];
int x[100], p[N];

bool solve(){
	int n, k;
	scanf("%d%d", &n, &k);
	f(i, 0, n)scanf("%d", p + i);
	f(i, 0, k)scanf("%d", x + i);
	f(i, 1, n + 1)dp[i] = false;
	int mx = p[0], id = 0;
	f(i, 1, n)if (p[i] > mx)mx = p[i], id = i;
	f(i, n - id, n + 1)f(j, 0, k)if (x[j] <= i && !dp[i - x[j]]) { dp[i] = true; break; }
	return dp[n];
}

int main(){
freopen("input.txt","r",stdin);
	int t;
	scanf("%d", &t);
	while (t--)printf("%s\n", solve() ? "Chef" : "Garry");;
}