#include<bits/stdc++.h>
using namespace std;
#define int unsigned long long


#define ll unsigned long long 

#define MOD 1000000007

ll bigMOD(ll num,ll n){
  if(n==0) return 1;
  ll x=bigMOD(num,n/2);
  x=x*x%MOD;
  if(n%2==1) x=x*num%MOD;
  return x;
}

 
ll MODinverse(ll num){
  return bigMOD(num,MOD-2)%MOD;
}


signed main(){
	freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		int x, y;
		cin >> x >> y;
		int s, u, v;
		cin >> s >> u >> v;
		int cnt = 1;
		do{
			if(s == x){
				int num = cnt * v;
				int den = u;
				int gcd = __gcd(num, den);
				num/=gcd;
				den/=gcd;
				int ans = (num*MODinverse(den))%MOD;
				cout << ans << endl;
				break;
			}
			else if(s == y){
				u =  v-u;
				int g = __gcd(v, u);
				u/=g, v/=g;
				cout << u << " " << v << " " << cnt << endl;
				int num = cnt * v;
				int den = u;
				int gcd = __gcd(num, den);
				num/=gcd;
				den/=gcd;
				int ans = (num*MODinverse(den))%MOD;
				cout << ans << endl;
				break;
			}
			s/=2;
			cnt *= 2;
		}while(1);
	}
}