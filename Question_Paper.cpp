#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","r",stdin);	
	int t;
	cin >> t; 
	while(t--){
		int n, a, b;
		cin >> n >> a >> b;
		int ans = 0;
		int visited[a+b] ={0};
		while(n>=0){
			if(visited[(n*b)%(a+b)]){
				n--;
				continue;
			}
			visited[(n*b)%(a+b)] = 1;
			ans += n+1;
			n--;
		}

		cout << ans << endl;
	}
}