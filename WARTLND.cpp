#include<bits/stdc++.h>
using namespace std;
#define N 100010
#define int long long
#define iterm(mm) for(map<int,int>::iterator x=mm.begin();x!=mm.end();x++)
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0); 

int n;
int parent[N];
int sz[N];
int cnt[N];
vector<pair<int,int> > ed[N];
int visited[N];



//dsu starts..............................
void initDSU(vector<int>& vec){
  for (int j = 0; j < vec.size(); ++j)
  {
  	int i = vec[j];
    parent[i] = i;
    sz[i] = 1;
    visited[i] = 0;
  }
}

int root (int i)
{
    while(parent[i] != i)    
    {
        parent[i] = parent[parent[i]]; 
        i = parent[i]; 
    }
    return i;
}

 
void uni(int a, int b) {
  int ra = root(a);
  int rb = root(b);
  if(ra == rb) return;
  if(sz[rb] < sz[ra]){
  	swap(ra, rb);
  }
  sz[rb]+=sz[ra];  
  parent[ra] = rb;
}

bool conn(int a, int b){
  return (root(a) == root(b));
}

int dsusize(int a){
	return sz[root(a)];
}
//dsu ends................................

void file_read(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	freopen("output1.txt","w",stdout);
	#endif
}



signed main(){
	FAST_IO;
	file_read();
	int t;
	cin >> t;
	while(t--){
		fill(ed, ed+N, vector<pair<int,int> > ());
		cin >> n;
		vector<int> vec;
		for (int i = 0; i < n-1; ++i)
		{
			int x, y, z;
			cin >> x >> y >> z;
			ed[z].push_back(make_pair(x-1,y-1));
			vec.push_back(i);
		}
		vec.push_back(n-1);

		for (int i = N-1; i >= 1; --i)
		{
			initDSU(vec);
			vec.clear();
			for(int j = i; j < N; j+=i){
				for(auto e: ed[j]){
					if(!conn(e.first,e.second))
						cnt[i] += dsusize(e.first)*dsusize(e.second);
					uni(e.first, e.second);
					vec.push_back(e.first); vec.push_back(e.second);
				}
				if(j>i)cnt[i]-=cnt[j];
			}
		}

		int ans = 0;
		for (int i = 1; i < N; ++i)
		{
			ans+=i*cnt[i];
			cnt[i]=0;
		}

		cout << ans << "\n";
	}
}