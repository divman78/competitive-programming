#include <stdio.h>

int main() {
	int t, n, m, k, i, j;
	scanf("%d",&t);
	while(t--){
	    scanf("%d%d%d", &n, &m, &k);
	    int arr[n][m];
	    for(i = 0; i < n; i++){
	        for(j = 0; j < m; j++){
	           scanf("%d",&arr[i][j]);
	        }
	    }
	    k %= m;
        for(i = 0; i < n; i++){
	        for(j = k; j != (k-1+m)%m; j = (j+1)%m){
	           printf("%d ",arr[i][j]);
	        }
	        printf("%d ", arr[i][(k-1+m)%m]);
	    }
	    printf("\n");
	}
	return 0;
}