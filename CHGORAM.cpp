#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define MX 10101010101010

void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

signed main(){
	read_file();

	int t;
	cin >> t;
	while(t--){
		int n, m, k, q;
		cin >> n >> m >> k >> q;

		vector<int> vec[n+1];

		for (int i = 0; i < k; ++i)
		{
			int r, c;
			cin >> r >> c;
			vec[r].push_back(c);
		}

		for (int i = 1; i <= n; ++i)
		{
			sort(vec[i].begin(), vec[i].end());
			reverse(vec[i].begin(), vec[i].end());
		}

		vector<int> cols(q);
		for (int i = 0; i < q; ++i)
		{
			cin >> cols[i];
		}

		sort(cols.begin(), cols.end());

		int lcost, rcost, l, r;

		if(vec[1].size()){
			lcost = vec[1][0]-1;
			rcost = lcost+(vec[1][vec[1][0]-vec[i].size()-1]);
			l = vec[1][0];
			r = vec[1][vec[i].size()-1];
		}
		else{
			lcost = rcost = 0;
			l = r = 1;
		}

		for (int i = 2; i <= n; ++i)
		{
			int rnxt = vec[i][0];
			int lnxt = vec[i][vec[i].size()-1];

			int rli = upper_bound(cols.begin(), cols.end(), r) - cols.begin();
			int rri = 1+(lower_bound(cols.begin(), cols.end(), r) - cols.begin());

			int lli = upper_bound(cols.begin(), cols.end(), l) - cols.begin();
			int lri = 1+(lower_bound(cols.begin(), cols.end(), l) - cols.begin());



			r = rnxt;
			l = lnxt;
		}
	}


	




	return 0;
}