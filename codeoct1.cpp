#include<bits/stdc++.h>
using namespace std;
#define ll unsigned long long
#define int unsigned long long
#define REC 100000

ll n, m;

ll digsum(ll& dig, ll& cnt){
	while(dig/10){
		cnt++;
		ll sum = 0;
		while(dig){
			sum += dig%10;
			dig/=10;
		}
		dig = sum;
	}
}

int rec = REC;
int nans = 10, cntans = INT_MAX;
void calc(int nn, int cnt){
	if(rec == 0) return;
	rec--;
	//cout <<"f ";
	int x = nn;
	int cnttemp = cnt;
	while(x/10){
		cnttemp++;
		ll sum = 0;
		while(x){
			sum += x%10;
			x/=10;
		}
		x = sum;
		calc(x, cnttemp);
		calc(x+m, cnttemp+1);
	}
	if(x < nans){
		nans = x;
		cntans = cnttemp;
	}
	calc(n+m, cnt+1);
}

void call(){
	nans = 10, cntans = INT_MAX;
	rec = REC;
	calc(n, 0);
}

main(){
	ios_base::sync_with_stdio(false), cin.tie(0);
	freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		cin >> n >> m;
		//call();
		cout <<"t = "<<  t << endl;
		cout << "n = " << n << " ,m = " << m << endl;
		ll cnt1 = INT_MAX;
		ll num1 = 10;
		ll cnt1m;
		ll tempcnt1, tempsum;
		for (ll i = 0; i <= 10000; ++i)
		{
			tempcnt1 = i;
			tempsum = n + i*m;
			digsum(tempsum, tempcnt1);
			if(tempsum < num1){
				num1 = tempsum;
				cnt1 = tempcnt1;
				cnt1m = i;
			}
		}
		ll cnt2 = 0;
		ll num2 = 10;
		ll tempcnt2 = 0;

		for (int i = 0; i <= 10000; ++i)
		{
			while(n/10){
				tempcnt2++;
				ll sum = 0;
				while(n){
					sum += n%10;
					n/=10;
				}
				n = sum;
			}
			if(n < num2){
				num2 = n;
				cnt2 = tempcnt2;
			}
			n += m;
			tempcnt2++;
		}

		 cout << "num1 = " << num1 << " ,cnt1 = " << cnt1 << " ,cnt1m = " << cnt1m << endl;
		 cout << "num2 = " << num2 << " ,cnt2 = " << cnt2 << endl;
		 //cout << "nans = " << nans << " ,cntans = " << cntans << endl;
		 //if(num1!= nans || cntans < min(cnt1, cnt2)) cout << " sorry " << endl, //exit(0); 
		 cout << endl;
		//cout << num1 << " " << min(cnt1, cnt2) << endl;
		//cout << num1 << " " << cnt1 << endl;
	}
}