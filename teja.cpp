//teja349
#include <bits/stdc++.h>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <utility>
#include <algorithm>
#include <cmath>
#include <queue>
#include <stack>
#include <iomanip>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp> 
//setbase - cout << setbase (16); cout << 100 << endl; Prints 64
//setfill -   cout << setfill ('x') << setw (5); cout << 77 << endl; prints xxx77
//setprecision - cout << setprecision (14) << f << endl; Prints x.xxxx
//cout.precision(x)  cout<<fixed<<val;  // prints x digits after decimal in val

using namespace std;
using namespace __gnu_pbds;

#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >
#define vl vector< ll >
#define ss second
#define ff first
#define ll long long
#define pii pair< int,int >
#define pll pair< ll,ll >
#define sz(a) a.size()
#define inf (1000*1000*1000+5)
#define all(a) a.begin(),a.end()
#define tri pair<int,pii>
#define vii vector<pii>
#define vll vector<pll>
#define viii vector<tri>
#define mod (1000*1000*1000+7)
#define pqueue priority_queue< int >
#define pdqueue priority_queue< int,vi ,greater< int > >
#define flush fflush(stdout) 
#define primeDEN 727999983


// find_by_order()  // order_of_key
typedef tree<
int,
null_type,
less<int>,
rb_tree_tag,
tree_order_statistics_node_update>
ordered_set;

int c[100005],d[100005];
vi v,v1;
int main(){
  freopen("input.txt","r",stdin);
    std::ios::sync_with_stdio(false); cin.tie(NULL);
  	int t;
  	cin>>t;
  	while(t--){
  		int a,b,i,j,ans=0,val;
  		v.clear();
  		v1.clear();
  		cin>>a>>b;
  		rep(i,a*b){
  			cin>>c[i];
  		}
  		rep(i,a*b){
  			cin>>d[i];
  		}
  		rep(i,a*b){
  			if(c[i]>d[i]){
  				v.pb(c[i]);
  			}
  			else{
  				v1.pb(d[i]);
  			}
  		}
  		sort(all(v));
  		sort(all(v1));
  		i=(int)v.size()-1;
  		j=(int)v1.size()-1;
  		//cout<<i<<" "<<j<<endl;
  		while(i>=0){
  			while(v1[j]>v[i]&&j>=0){
  				j--;
  			}
  			if(j+1>=b-1){
  				j-=(b-1);
  				i--;
  				ans++;
  			}
  			else{
  				val=j+1;
  				if(i+val>=b-1){
  					ans++;
  					i-=(b-1-val);
  					ans+=(i/b);
  					break;
  				}
  				else{
  					break;
  				}
  			}

  		}
  		cout<<ans<<endl;
  	}
    return 0;   
}