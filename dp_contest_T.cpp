#include<bits/stdc++.h>
using namespace std;
#define printarr(arr, n) for(int i = 0; i < n; i++) {cout << arr[i] << " ";} cout << endl; 
#define MO 1000000007
int main(){
	//freopen("input.txt","r",stdin);
	int n;
	string s;
	cin>>n>>s;
	int dp[n+2][n+2];
	for (int i = 0; i < n+2; ++i)
	{
		for (int j = 0; j < n+2; ++j)
		{
			dp[i][j] = 0;
		}
	}
	dp[1][1] = 1;
	for (int len= 2; len <= n; len++)
	{
		for (int last = 1; last <= len; ++last)
		{
			if(s[len-2]=='>'){
				dp[len][last] = (dp[len-1][len-1]-dp[len-1][last-1]+MO)%MO;
			}else{
				dp[len][last] = dp[len-1][last-1];
			}
			//cout << len << " " << last << " " << dp[len][last] << endl;
		}
		for (int i = 2; i <= len; ++i)
		{
			dp[len][i] = (dp[len][i]+dp[len][i-1])%MO;
		}
	}


	cout << dp[n][n] << endl;

}





