import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { FormsModule } from '@angular/forms';
import { Validators, NgForm } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { FileUploader } from 'ng2-file-upload';
import { baseURL } from '../../shared/baseurl';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Item } from 'ng2-simple-dropdown';
import { IMultiSelectOption ,IMultiSelectSettings} from 'angular-2-dropdown-multiselect';

import { ProcessHTTPMsgService } from '../../services/process-httpmsg.service';
import { ToastrListingService } from '../toastr.service';

import { Router, ActivatedRoute, Params } from "@angular/router";


import { ListingDetailService } from '../../services/listing-detail.service';

interface Obj{
  id : any;
  name: string;
}


@Component({
  selector: 'app-bordered',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  encapsulation: ViewEncapsulation.None
})



export class ProductComponent implements OnInit {

  colorModel: number[] ;
  show = false;
  
  colorOptions: IMultiSelectOption[] = [{'id':89,'name':'dfd'} ];
  colorSettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    containerClasses: 'col-md-9',
    dynamicTitleMaxItems: 3,
    displayAllSelectedText: true
  };


  product: {}; //{id:any, name:any, mrp:any, discount:any,description:any,brand:any,picture:any,sizeChart:any};
  picUrl: string[];
  filesToUpload: Array<File> = [];
  urls = [];
  @ViewChild('myInput')
  myInputVariable: any;
  dropdownSettings: {};
  fixedCategory: Array<string> = [];
  fixedCategoryComission: Array<string> = [];
  category: '';
  initData: any;
  fixedColors:Array<string>= [];
  //fixedColors:Array<Obj>= [];
  comission: any;
  isCategory: boolean;
  isPayable: boolean;
  Payment: any;
  _id: any;
  oldUrls= [];
  deleteUrls = [];

  regularForm: FormGroup;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private listingDetailService: ListingDetailService,
    private http: HttpClient,
    private processHTTPMsgService: ProcessHTTPMsgService,
    private toastservice: ToastrListingService) {

  }




  // "{
  //   ""status"" : ""success"", 
  //  ""message"" : ""new product added successfully"",
  //  ""product"" : {
  //                  ""Name"": """",
  //                  ""Description"": """",
  //                   ""brand"": """",
  //                    ""_id"": """",
  //                    ""price"":
  //                      {
  //                            ""mrp"": ,
  //                            ""discounted"":
  //                     },
  //                     ""picture"": [], ""sizeChart"":""""
  //            }
  //  }
  //   "
  // "Name": this.regularForm.get('name').value,
  //           "Description": this.product['description'],
  //           "brand": this.product['brand'],
  //           "SizeChart":this.product['sizeChart'],
  //           "price.mrp":this.regularForm.get('mrp').value,
  //           "price.discounted": this.regularForm.get('discount').value,
  //           "isReturnable": this.product['returnable'],
  //           "Condition":  this.product['Condition'],
  //           "category": this.product['category'],
  //           "dryCleaned": this.product['dryCleaned'],
  //           "material":this.product['material'],
  //           "colors":this.product['colors'],
  //           "Pictures":this.product['pictures']
  ngOnInit() {
    this.show = false;

    this.colorModel = [];
    this.colorOptions = [
    ]

    this.regularForm = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'mrp': new FormControl(null, [Validators.required, Validators.pattern("[0-9]*")]),
      'discount': new FormControl(null, [Validators.required, Validators.pattern("[0-9]*")]),
      'category': new FormControl(null, [Validators.required]),
      'colors':new FormControl(null),
    }, { updateOn: 'blur' });
  

    this._id = this.route.snapshot.paramMap.get('id');
    if(this._id == null){
      this.router.navigate(['/listing/add-product']);
    }
    this.product = {
      id: '',
      name: '',
      description: '',
      mrp: '',
      discount: '',
      brand: '',
      picture: [],
      sizeChart: '',
      dryCleaned: false,
      notDryCleaned: true,
      material: [],
      colors: [],
      //colors:['Pink','Black','White'],
      Condition: '',
      returnable: false,
      notReturnable:true,
      category: ''
    }


    this.listingDetailService.initializeForm()
    .map(res => { console.log(res);return res; })
    .subscribe(res => {
      return new Promise(resolve => {
        // Simulate server latency with 2 second delay
        setTimeout(() => resolve(res), 2000);
        
      })
        .then(res => {
          console.log(res);
          this.initData = res;
          console.log(this.initData['data']['colors']);

          this.fixedColors = this.initData['data']['colors'];
          //this.product['colors'] = this.initData['data']['colors'];
          console.log(typeof this.colorOptions);

          var Options:Obj[];

          for(var i = 0; i < this.initData['data']['colors'].length; i++){
            this.colorOptions.push({"id":i+1, "name":this.initData['data']['colors'][i]});
          }
          //this.colorOptions = Options;
          console.log(this.colorOptions);

          for (var i = 0; i < this.initData['data']['category'].length; i++) {
            this.fixedCategory.push(this.initData['data']['category'][i]['name']);
            this.fixedCategoryComission.push(this.initData['data']['category'][i]['comission']);
          }

          console.log(this.fixedCategory);
          console.log(this.fixedCategoryComission);
          console.log(this.fixedColors);

        }, error => {
          console.log("error occured in /admin/showFeatures due to" + error);
          this.toastservice.typeError();
        }).then(()=>{
          this.http.request("post",baseURL+'/blogger/product/view',{body:{"productId":this._id}})
          .map(res => {return res['product']})

          .catch(error => {
            return this.processHTTPMsgService.handleError(error);
          })
          .subscribe(product => {
            console.log(product);
            var str: string;
            this.urls = product['Pictures'];
            this.oldUrls = product['Pictures'];
            this.urls = product['Pictures'].map(pic => 'http://192.168.31.54:8000/'+pic);
            this.oldUrls = (this.urls);
            for(var i = 0; i  < this.urls.length; i++){
              this.oldUrls.push(this.urls[i]);
            }
            // for(var i = 0; i < this.urls.length; i++){
            //   str = this.urls[i];
            //   console.log(typeof str);
            //   console.log(str);
            //   this.urls[i] = str.replace('localhost:4200','http://192.168.31.54:8000/');
            //   console.log(this.urls[i]);
            //   this.oldUrls[i] = this.urls[i];
            // }
            //console.log(picture);
            ////this.regularForm.controls['category'].setValue(product['category'], {onlySelf: true});
            //console.log(this.regularForm.get('category').value);
            //this.regularForm.controls['colors'].setValue( product['colors']);
            // for(var i = 0; i < product['colors'].length; i++){
            //   for(var j = 0; j  <  this.fixedColors.length; j++){
            //     if(product['colors'][i]==this.fixedColors[j]){
            //       this.product['colors'].push({'item_id':j+1,'item_text':this.fixedColors[j]});
            //     }
            //   }
            // }
            this.product['category'];
            for(var i = 0; i  <  product['colors'].length ; i++){
              for(var j = 0;  j < this.colorOptions.length; j++){
                if(this.colorOptions[j]['name'] == product['colors'][i]){
                  this.colorModel.push(j+1);
                }
              }
            }
            this.product['colors'] =  product['colors'];
            console.log(this.regularForm.get('colors').value);
            console.log(this.product['colors']);
            this.regularForm.get('name').setValue(product['Name']);
            this.product['description'] = product['Description'];
            this.product['brand'] = product['brand'];
            this.product['id'] = product['_id'];
            this.regularForm.get('mrp').setValue(product['price']['mrp']);
            this.regularForm.get('discount').setValue(product['price']['discounted']);
            this.product['picture']  = product['Pictures'];
            this.product['sizeChart']  = product['SizeChart'];
            this.product['dryCleaned'] = product['dryCleaned'];
            this.product['returnable'] = product['isReturnable'];
            //this.regularForm.get('category').setValue(product['category']);
            
            this.product['Condition'] = product['Condition'];
            this.product['material'] = product['material'];
            this.show = true;
          });

        })
          
    });
    

    
    console.log(this.route.snapshot.paramMap.get('id'));
    // this.product = {
    //   id: this.route.snapshot.paramMap.get('id'),
    //   name: this.route.snapshot.paramMap.get('name'),
    //   description: this.route.snapshot.paramMap.get('description'),
    //   mrp: this.route.snapshot.paramMap.get('mrp'),
    //   discount: this.route.snapshot.paramMap.get('discount'),
    //   brand: this.route.snapshot.paramMap.get('brand'),
    //   picture: this.route.snapshot.paramMap.get('picture'), 
    //   sizeChart: this.route.snapshot.paramMap.get('sizeChart')
    // }
    this.isCategory = false;
    this.isPayable = false;


    console.log(this.fixedCategory);
    console.log(this.fixedCategoryComission);
    //this.fixedCategory= ['shirt','Tshirt'];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  /*onSave(){
      this.listingDetailService.addMyListing(this.product)
     .subscribe(res => alert(res.message));

  }*/


  onSave() {
    console.log(this.regularForm.get('name').value);
    const formData: any = new FormData();
    var DATA:{};
    const formData1: any = new FormData();
    const files = this.filesToUpload;
    console.log(files);

    for (let i = 0; i < files.length; i++) {
      formData1.append("Pictures", files[i], files[i]['name']);
    }
    console.log(this.product['category']);
    console.log(formData1['Pictures']);

    var comission;

    this.http.post(baseURL + '/',{body: ''})

    this.http.post(baseURL + '/blogger/product/uploadImage',formData1)
    .catch(error => {
      return this.processHTTPMsgService.handleError(error);
    })
    .subscribe(
      res => {
        console.log(res);
        this.product['pictures'] = res['data']; 
        return new Promise(resolve => {
          setTimeout(() => resolve(), 2000);
        })
        .then(()=>{
          console.log("ddfdf");
          console.log(this.product['colors']);
          console.log('form data variable :   ' + this.product.toString());
          console.log("ddfdf");
          for (var pair of formData.entries()) {
            console.log("hello world");
            console.log(pair[0] + ', ' + pair[1]);
          }

          DATA ={
            "productId":this._id,
            "Name": this.regularForm.get('name').value,
            "Description": this.product['description'],
            "brand": this.product['brand'],
            "SizeChart":this.product['sizeChart'],
            "price.mrp":this.regularForm.get('mrp').value,
            "price.discounted": this.regularForm.get('discount').value,
            "isReturnable": this.product['returnable'],
            "Condition":  this.product['Condition'],
            "category": this.product['category'],
            "dryCleaned": this.product['dryCleaned'],
            "material":this.product['material'],
            "colors":this.product['colors'],
            "Pictures":this.product['pictures'],
            "comission": this.comission,
          }

          console.log(DATA);
          this.http.request("put",baseURL + '/blogger/product/updateProduct', {body:DATA})
          .map(res => {
            return res;
          }).catch(error => {
            return this.processHTTPMsgService.handleError(error);
          }).subscribe(
            res => {
              this.toastservice.typeSuccess1('product successfully updated');
              console.log(res);
            },
            error => {
              this.toastservice.typeError();
              console.log(error);
            }
          )
          })
          },
          error => {
            this.toastservice.typeError();
            console.log(error);
          }
    );
  }

  onSelect(event) {
    console.log("dfsddfdsfdfs");

  }
  set(event) {
    console.log("hi");
    console.log(event);
    console.log(event['item'].data);
    // this.product['category'] = ; 
  }
  fileChangeEvent(fileInput: any) {

    if (fileInput.target.files.length + this.urls.length > 5) {
      this.myInputVariable.nativeElement.value = "";
      alert("Select Pictures should be less than 5");
    }
    var inputValue = (<HTMLInputElement>document.getElementById('picture')).value;
    console.log("helooooooooooooooo");
    console.log(inputValue);
    var inputVal = <Array<File>>fileInput.target.files;

    for( var i = 0; i < inputVal.length; i++){
      this.filesToUpload.push(inputVal[i]);
    }
    //this.product.photo = fileInput.target.files[0]['name'];
    let files = fileInput.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
    console.log(this.urls);
    console.log(this.oldUrls);

  }

  onDeleteImage(url){
    console.log(url);
    console.log(this.oldUrls);
    var deleteIndex = this.urls.indexOf(url);
    var deleteOldIndex = this.oldUrls.indexOf(url);
    console.log(this.oldUrls['deleteOldIndex']);
    if(deleteOldIndex != -1) {
      this.deleteUrls.push(this.oldUrls[deleteOldIndex]);
      this.http.request('post',baseURL+'/blogger/product/deleteProductImage',{body:{'productId': this._id, 'url':this.oldUrls[deleteOldIndex]}})
      .subscribe(res => {
        console.log(res);
      },error => {
        this.toastservice.typeError();
        console.log(error);
      });
      this.oldUrls.splice(deleteOldIndex,1);
    }
    console.log(deleteIndex);
    console.log(typeof this.filesToUpload);
    console.log(this.filesToUpload);
    this.filesToUpload.splice(deleteIndex, 1);

    //console.log(this.filesToUpload[deleteIndex]);
    //console.log(delete this.filesToUpload[deleteIndex]);
    this.urls.splice(deleteIndex, 1);
  }

  onItemSelect(event) {
    console.log(this.product['colors']);
    //var deleteIndex = this.product['colors'].indexOf('Black');
    //this.product['colors'].push('Green');
    //this.product['colors'].splice(deleteIndex,1);
   // console.log(this.regularForm.get('colors').value);

  }
  onSelectAll(event) {

  }

  onOptionsSelected(event) {
    console.log(event);
    console.log("dfsfdfeeeeeeeeeeeeeeeee");
    //this.product['category'] = event;
    this.product['category'] = event['srcElement']['value'];
    console.log(this.product['category']);
    this.comission = '';
    this.isCategory = false;
    console.log(this.regularForm.get('category').value);
    // this.product['category'] = this.regularForm.get('category').value;

    this.product['category'] = event['srcElement']['value'];
    console.log(this.product['category']);
    for (var i = 0; i < this.fixedCategory.length; i++) {
      if (this.fixedCategory[i] == this.product['category']) {
        this.comission = this.fixedCategoryComission[i];
        break;
      }
    }
    if (this.comission != '') {
      this.isCategory = true;
    }
    this.onCalculate(1);
  }

  onCalculate(event) {
    this.isPayable = false;
    console.log("onchangemrpdiscount");
    if (this.regularForm.valid) {
      console.log(this.regularForm.get('mrp').value - this.regularForm.get('discount').value - this.comission);
      this.Payment = this.regularForm.get('mrp').value - this.regularForm.get('discount').value - this.comission;
      this.isPayable = true;
    }
  }


  onAddFile(event) {
    console.log("hdfsfsdf");
    console.log(event);
    // this.product.picture.push();
  }

  onDelete() {

  }


  onChange() {
    this.product['colors'] =[];
    for(var  i = 0; i < this.colorModel.length; i++){
      this.product['colors'].push(this.colorOptions[this.colorModel[i]]['name']);
    }
    console.log(this.product['colors']);
    //console.log(this.optionsModel);
  }


}
