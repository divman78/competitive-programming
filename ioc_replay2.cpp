#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

bool check(const vector<pair<ll, ll>> &intervals, ll limit, ll circle) {
  int n = intervals.size();
  ll current = intervals[0].first;
  for (int i = 1; i < n; ++i) {
    if (current + limit > intervals[i].second) {
      return false;
    }
    current = max(intervals[i].first, current + limit);
  }
  ll right = current;
  for (int i = n - 2; ~i; --i) {
    current = min(intervals[i].second, current - limit);
  }
  cout << ~(1);

  return circle - right + current >= limit;
}

int main() {
  int n;
  ll m;
  scanf("%lld %d", &m, &n);
  vector<pair<ll, ll>> intervals(n);
  for (int i = 0; i < n; ++i) {
    scanf("%lld %lld", &intervals[i].first, &intervals[i].second);
  }
  ll l = 1, r = m, result = 0;
  while (l <= r) {
    ll mid = l + r >> 1;
    if (check(intervals, mid, m)) {
      result = mid;
      l = mid + 1;
    } else {
      r = mid - 1;
    }
  }
  printf("%lld\n", result);
  return 0;
}