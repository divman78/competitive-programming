#include<bits/stdc++.h>
using namespace std;
#define MX 5001
#define N 100100
#define mid ((l+r)>>1)

bitset<MX> sega[4*N];
bitset<MX> segb[4*N];
int n;
int arr[N];
int brr[N];

int cnt;

void build(int id , int l, int r){
	//cout << id  << l << r << endl;
	if(l > r) return;
	if(l == r){
		sega[id].set(arr[l]);
		segb[id].set(brr[l]);
		return;
	}
	build(id*2+1, l, mid);
	build(id*2+2, mid+1, r);
	sega[id] = sega[2*id+1] | sega[2*id+2];
	segb[id] = segb[2*id+1] | segb[2*id+2];
}



bitset<MX> querya(int id, int l, int r, int x, int y){
	//cout << x <<  " " << y << " " << id << endl;
	if(l > r || l > y || r < x) return 0;
	if(l >= x and r <= y){
	    return sega[id];		
	}
	return querya(id*2+1, l, mid, x, y) | querya(id*2+2, mid+1, r, x, y);
}

bitset<MX> queryb(int id, int l, int r, int x, int y){
	//cout << x <<  " " << y << " " << id << endl;
	if(l > r || l > y || r < x) return 0;
	if(l >= x and r <= y){
	    return segb[id];		
	}
	return queryb(id*2+1, l, mid, x, y) | queryb(id*2+2, mid+1, r, x, y);
}


signed main(){
	freopen("input.txt","r",stdin);
	cin >> n;
	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}

	for (int i = 0; i < n; ++i)
	{
		cin >> brr[i];
	}

	build(0,0,n-1);

	// for (int i = 0; i < 20; ++i)
	// {
	// 	cout << sega[i] << endl;
	// }
	int q;
	cin >> q;
	while(q--){
		int a, b, c, d;
		cin >> a >> b >> c >> d;
		bitset<MX> ans = querya(0, 0, n-1, a-1, b-1) | queryb(0, 0, n-1, c-1, d-1);
		cout << ans.count() << endl;
	}

}