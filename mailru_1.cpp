#include<bits/stdc++.h>
using namespace std;
#define MO 1000000007
#define int long long
#define endl "\n"
#define  NO cout << "NO" << endl; return 0


signed main(){
	//ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	
	int t=1;
	//cin>>t;
	while(t--)
	{
		int n,k;
		cin>>n>>k;
		int arr[n];
		for(int i=0;i<n;i++){
			cin>>arr[i];
		}
		int count = 0;
		for(int i=0;i<n;i++){
			int k = 0;
			for(int j=i;j<n;j++){
				k = k ^ arr[j];
				if(k != 0){
					count++;
				}
			}
		}
		cout<<count<<endl;

	}
	
	return 0;
}