// CPP program to demonstrate the 
// set::lower_bound() function 
#include <bits/stdc++.h> 
using namespace std; 
int main() 
{ 
  
    set<int> s; 
    cout << *s.lower_bound(10) <<endl;
    // Function to insert elements 
    // in the set container 
    s.insert(10); 
    s.insert(40); 
    s.insert(20); 
    s.insert(50); 
    s.insert(60); 
    s.insert(70); 

    
  
    cout << "The set elements are: "; 
    for (auto it = s.begin(); it != s.end(); it++) 
        cout << *it << " "; 
  
    // when 2 is present 
    auto it = s.lower_bound(10); 
    cout << "\nThe lower bound of key 10 is "; 
    cout << (*it) << endl; 
  
    // when 3 is not present 
    // points to next greater after 3 
    it = s.lower_bound(30); 
    cout << "The lower bound of key 30 is "; 
    cout << (*it) << endl; 
  
    // when 8 exceeds the max element in set 
    it = s.lower_bound(80); 
    cout << "The lower bound of key 80 is "; 
    cout << (*it) << endl; 
  
    return 0; 
} 