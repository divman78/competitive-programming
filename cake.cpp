#include<iostream>
using namespace std;

int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int zeroes = n/2;
		if(n & 1) zeroes = n - (n+1)/2;
		cout << 1 <<" "<< 1;
		for(int i = 1; i <= zeroes; i++){
			cout << 0;
		}
		cout << endl;
	}
}