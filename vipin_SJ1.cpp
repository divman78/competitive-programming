#include<bits/stdc++.h>
#include<vector>
#define int long long
#define mx 100005
using namespace std;
vector<int> tree[mx];
int visited[mx];
int arr[mx];
int brr[mx];
map<int,int> mp;
void reset(int n){
	mp.clear();
	for(int i=1;i<=n;i++){
		tree[i].resize(0);
		visited[i] = 0;
	}
}
void dfs(int n,int g){
	visited[n] = 1;
	int k = __gcd(g,arr[n]);
	for(int i=0;i<tree[n].size();i++){
		if(!visited[tree[n][i]]){
			dfs(tree[n][i],k);
		}
	}
	if(tree[n].size() == 1 && n != 1){
		// Leaf Node
		int p = __gcd(k,brr[n]);
		mp[n] = brr[n] - p;
	}
}
signed main(){
	ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
	freopen("input.txt","r",stdin);
	int t;
	cin>>t;
	while(t--){
		int n;
		cin>>n;
		reset(n);
		for(int i=1;i<n;i++){
			int x,y;
			cin>>x>>y;
			tree[x].push_back(y);
			tree[y].push_back(x);
		}
		for(int i=1;i<=n;i++){
			cin>>arr[i];
		}
		for(int i=1;i<=n;i++){
			cin>>brr[i];
		}
		dfs(1,arr[1]);
		for(map<int,int>::iterator it = mp.begin(); it != mp.end(); ++it){
			cout<<it->second<<" ";
		}
		cout<<endl;
	}
}