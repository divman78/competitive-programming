//package math_codet;

import java.io.*;
import java.util.*;
 /******************************************
*    AUTHOR:         AMAN KUMAR SINGH        *
*    INSTITUITION:   KALYANI GOVERNMENT ENGINEERING COLLEGE  *
******************************************/
class lets_do {
    InputReader in;
    PrintWriter out;
    Helper_class h;
    final long mod=1000000007;
    final int lgN = 22;
    public static void main(String[] args) throws java.lang.Exception{
        new lets_do().run();
    }
    void run() throws Exception{
    	in=new InputReader(System.in);
        out = new PrintWriter(System.out);
        h = new Helper_class();
        int t = h.ni();
        while(t-->0)
            solve();
        out.flush();    
        out.close();
    }
    ArrayList<Integer>[] tree;
    long[] arr, max;
    int[] cnt;
    int[][] anc;
    void dfs(int v, int p){
        anc[v][0] = p;
        for(Integer u: tree[v]){
            if(u != p){
                if(arr[u] > max[v]){
                    cnt[u] = cnt[v] + 1;
                    max[u] = arr[u];
                }
                else{
                    cnt[u] = cnt[v];
                    max[u] = max[v];
                }
                dfs(u, v);
            }
        }
    }

    void solve(){
        int n = h.ni();
        arr = new long[n];
        max = new long[n];
        cnt = new int[n];
        tree = new ArrayList[n];
        anc = new int[n][lgN];
        int i = 0, j = 0;
        for(i = 0; i < n; i++){
            arr[i] = h.nl();
            tree[i] = new ArrayList<Integer>();
            for(j = 0; j < lgN; j++)
            	anc[i][j] = -1;
        }
        for(i = 2; i <= n; i++){
            int x = h.ni() - 1;
            tree[x].add(i - 1);
            tree[i - 1].add(x);
        }
        max[0] = arr[0];
        cnt[0] = 1;
        dfs(0, 0);
        for(i = 0; i < n; i++) {
            for(j = 1; j < lgN; j++) {
            	anc[i][j] = anc[anc[i][j - 1]][j - 1];
            }
        }
        long ans = 0;
        int q = h.ni();
        while(q-- > 0){
            long a = h.nl();
            long b = h.nl();
            a ^= ans;
            b ^= ans;
            int v = (int)a - 1;
            long w = b;
            if(max[v] <= b){
                ans = 0;
                h.pn(ans);
                continue;
            }
            else if(max[0] > w){
                ans = cnt[v];
                h.pn(ans);
                continue;
            }
            else{
                int curr = v;
                for(i = lgN - 1; i >= 0; i--){
                    if(anc[curr][i] != -1 && max[anc[curr][i]] > w)
                        curr = anc[curr][i];
                }
                curr = anc[curr][0];
                ans = cnt[v] - cnt[curr];
                h.pn(ans);
            }
        }
    }
    final Comparator<Entity> com=new Comparator<Entity>(){
        public int compare(Entity x, Entity y){
            int xx=Integer.compare(x.a, y.a);
            if(xx==0){
                int xxx=Integer.compare(x.b,y.b);
                return xxx;
            }
            else
                return xx;
        }
    };
    class Entity{
        int a;
        int b;
        Entity(int p, int q){
            a=p;
            b=q;
        }
    }
    class Helper_class{
        long gcd(long a, long b){return (b==0)?a:gcd(b,a%b);}
        int gcd(int a, int b){return (b==0)?a:gcd(b,a%b);}
        long gcd1(long a, long b){return (b==0)?a:gcd(b,a%b);}
        int bitcount(long n){return (n==0)?0:(1+bitcount(n&(n-1)));}
        void p(Object o){out.print(o);}
        void pn(Object o){out.println(o);}
        void pni(Object o){out.println(o);out.flush();}
        String n(){return in.next();}
        String nln(){return in.nextLine();}
        int ni(){return Integer.parseInt(in.next());}
        long nl(){return Long.parseLong(in.next());}
        double nd(){return Double.parseDouble(in.next());}

        long mul(long a,long b){
            if(a>=mod)a%=mod;
            if(b>=mod)b%=mod;
            a*=b;
            if(a>=mod)a%=mod;
            return a;
        }
        long modPow(long a, long p){
            long o = 1;
            while(p>0){
                if((p&1)==1)o = mul(o,a);
                a = mul(a,a);
                p>>=1;
            }
            return o;
        }
        long add(long a, long b){
            if(a>=mod)a%=mod;
            if(b>=mod)b%=mod;
            if(b<0)b+=mod;
            a+=b;
            if(a>=mod)a-=mod;
            return a;
        }
    }

    class InputReader{
        private InputStream stream;
        private byte[] buf = new byte[1024];
        private int curChar;
        private int numChars;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int read() {
            if (numChars == -1)
                throw new UnknownError();
            if (curChar >= numChars) {
                curChar = 0;
                try {
                    numChars = stream.read(buf);
                } catch (IOException e) {
                    throw new UnknownError();
                }
                if (numChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int peek() {
            if (numChars == -1)
                return -1;
            if (curChar >= numChars) {
                curChar = 0;
                try {
                    numChars = stream.read(buf);
                } catch (IOException e) {
                    return -1;
                }
                if (numChars <= 0)
                    return -1;
            }
            return buf[curChar];
        }
 
        public void skip(int x) {
            while (x-- > 0)
                read();
        }
 
        public int nextInt() {
            return Integer.parseInt(next());
        }
 
        public long nextLong() {
            return Long.parseLong(next());
        }
 
        public String nextString() {
            return next();
        }
 
        public String next() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            StringBuffer res = new StringBuffer();
            do {
                res.appendCodePoint(c);
                c = read();
            } while (!isSpaceChar(c));
 
            return res.toString();
        }
 
        public String nextLine() {
            StringBuffer buf = new StringBuffer();
            int c = read();
            while (c != '\n' && c != -1) {
                if (c != '\r')
                    buf.appendCodePoint(c);
                c = read();
            }
            return buf.toString();
        }
 
        public double nextDouble() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = read();
            }
            double res = 0;
            while (!isSpaceChar(c) && c != '.') {
                if (c == 'e' || c == 'E')
                    return res * Math.pow(10, nextInt());
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = read();
            }
            if (c == '.') {
                c = read();
                double m = 1;
                while (!isSpaceChar(c)) {
                    if (c == 'e' || c == 'E')
                        return res * Math.pow(10, nextInt());
                    if (c < '0' || c > '9')
                        throw new InputMismatchException();
                    m /= 10;
                    res += (c - '0') * m;
                    c = read();
                }
            }
            return res * sgn;
        }
 
        public boolean hasNext() {
            int value;
            while (isSpaceChar(value = peek()) && value != -1)
                read();
            return value != -1;
        }
 
        private boolean isSpaceChar(int c) {
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
    }
}