// #pragma GCC optimize("O3")
#include <bits/stdc++.h>

using namespace std;

using ll = long long;
using vll = vector<ll>;
using vii = vector<int>;
using pii = pair<int, int>;
template <typename T = vii> 
using vec = vector<T>;

const ll INF = numeric_limits<ll>::max(),
         MOD = 1e9 + 7;

const int INF_i = numeric_limits<int>::max(),
          MAX = 2e6;

vll primes;
bool is_composite[MAX + 1];

void sieve() {
    fill_n(is_composite, MAX + 1, false);
    for (int i = 2; i <= MAX; i++) {
        if (!is_composite[i]) {
            primes.push_back(i);
        }
        for (auto p : primes) {
            ll j = i * p;
            if (j > MAX) break;
            is_composite[j] = true;
            if (i % p == 0) break;
        }
    }
}

ll f(ll n) {
    ll res = 1;
    for (ll p : primes) {
        if (p * p > n) break;
        int cnt = 0;
        while (n % p == 0) n /= p, cnt++;
        res *= cnt + 1;
    }
    if (n > 1) {
        res *= 2;
    }
    return res;
}

int main() {
    //cin.tie(NULL);
    //ios_base::sync_with_stdio(false);
    // freopen("in.in", "r", stdin);

    sieve();

    int TT;
    cin >> TT;
    for (int T = 1; T <= TT; T++) {
        int n;
        cin >> n;
        vll v(n);
        for (int i = 0; i < v.size(); i++) {
            cin >> v[i];
        }
        sort(v.begin(), v.end());
        ll res = v[0] * v.back();
        for (int i = 0; i < n; i++) {
            if (res != v[i] * v[n - 1 - i]) {
                res = -1;
                break;
            }
        }
        if (res != -1) {
            if (n + 2 != f(res)) 
                res = -1;
        }
        cout << res << '\n';
    }   
}