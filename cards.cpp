#include<bits/stdc++.h>
using namespace std;
int main(){
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n; 
		int arr[n];
		for(int i = 0; i < n; i++){
			cin >> arr[i];
		}
		int x = 0;
		for(int i = 1;  i < n; i++){
			if(arr[i] < arr[i-1]){
				x = i;
			}
		}

		bool flag = true;
		//cout << x << endl;

		for(int i = x+1;  i != x; i = (i+1)%n){
			if(arr[i] < arr[(i-1+n)%n]){
				flag = false;
			}
		}
		if(flag){
			cout << "YES";
		}
		else{
			cout << "NO";
		}
		cout << "\n";
	}
}