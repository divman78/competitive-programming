// set::lower_bound/upper_bound
#include <iostream>
#include <set>
using namespace std;
int main ()
{
  std::set<int> myset;
  std::set<int>::iterator itlow,itup;

  int arr[] = {10,5,7,4,6,3,2,1,9,8};

  for (int i=0; i<10; i++) myset.insert(arr[i]); // 10 20 30 40 50 60 70 80 90

  //myset.erase(1);
  itlow=myset.lower_bound (2);                //       ^
  itup=myset.upper_bound (9);                 //                   ^
cout << *itlow <<         " " << *itup << endl; 
  myset.erase(itlow,itup);            // 10 20 70 80 90

  std::cout << "myset contains:";
  for (std::set<int>::iterator it=myset.begin(); it!=myset.end(); ++it)
    std::cout << ' ' << *it;
  std::cout << '\n';

  return 0;
}