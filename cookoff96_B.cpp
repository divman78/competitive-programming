#include<bits/stdc++.h>
using namespace std;
#define MAXX 10000
#define N 100012
#define mii map<int,int>
#define ll long long

int spf[MAXX];

void seive(){
	for (int i = 1; i < MAXX; ++i){
		spf[i] = i;
	}
	for (int i = 2; i < MAXX; i+=2){
		spf[i] = 2;
	}
	for (int i = 3; i < MAXX; i+=2){
		if(spf[i] == i){
			for (int j = i*i; j < MAXX; j+=i){
				if(spf[j] == j)
					spf[j] = i;
			}
		}
	}
}

void getPrimeFactors(int x, int i, mii& mm1, mii& mm2){
	cout << x << "->>>>\n";
	while(x != 1){
		cout << spf[x]<<" ";
		if(!mm2[spf[x]]) 
			mm2[spf[x]] = i;
		if(mm2[spf[x]] != i)  
			mm1[spf[x]]++;
		mm1[spf[x]]++;
		x/=spf[x];
	}
	cout << endl;
}

int main(){
	seive();
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		int arr[n + 1];
		for (int i = 1; i <= n; ++i)
			cin >> arr[i];
		mii mm1;
        mii mm2;
        cout << endl;
        for (int i = 1; i <= n; ++i){
        	getPrimeFactors(arr[i],i,mm1,mm2);
        }
        ll ans = 1;
        cout << endl;
        for (mii::iterator i = mm1.begin(); i!=mm1.end(); i++){
        	cout << i->first << " -> "<<i->second<<endl;
        	int x = (i->second)/(n-1);
        	if(x) ans = ans * (ll)x * (ll)i->first;
        }
        cout<<ans<<endl;
	}

}