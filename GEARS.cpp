#include<bits/stdc++.h>
using namespace std;

struct query{
	int type; int a; int b; int c;
};

int isBlocked[100050];



int parent(int x, pair<int,int>* dsu){
	while(x != dsu[x].first){
		x = dsu[x].first;
	}
	return x;
}



int finddistance(int x, pair<int,int>* dsu, int* distance){
	int distance1 = 1;
	while(x != dsu[x].first){
		distance1 *= distance[x];
		x = dsu[x].first;
	}
	return distance1;
}



void merge(int x, int y, pair<int,int>* dsu, int* distance){
	int parx = parent(x, dsu);
	int pary = parent(y, dsu);
	if(isBlocked[parx] || isBlocked[pary]){
		isBlocked[parx] = 1;
		isBlocked[pary] = 1;
		return;
	}

	if(parx == pary){
		if(finddistance(x, dsu, distance) == finddistance(y, dsu, distance)){
			isBlocked[pary] = 1;
		}
		return;
	}
	if(dsu[parx].second > dsu[pary].second){
		swap(parx, pary);
	}
	distance[parx] = -1 * finddistance(y, dsu, distance) * finddistance(x, dsu, distance);
	dsu[parx].first = pary;
	dsu[pary].second += dsu[parx].second;
}



int main(){ 
	int n, q;
	cin >> n >> q;
	int arr[n];
	for (int i = 0; i < n; ++i){
		cin >> arr[i];
	}
	query quer[q];
	for(int i = 0; i < q; i++){
		cin >> quer[i].type;
		if (quer[i].type == 3){
			cin >> quer[i].a;
			cin >> quer[i].b;
			cin >> quer[i].c;
		}
		else{
			cin >> quer[i].a;
			cin >> quer[i].b;
		}
	}

	pair<int, int> dsu[n];
	for(int i = 0; i < n; i++) dsu[i].first = i, dsu[i].second = 1;

	int distance[n];
    for(int i = 0; i < n; i++) distance[i] = 1;

    for (int i = 0; i < n; ++i) isBlocked[i] = 0;

    for(int i = 0; i < q; i++){
    	if(quer[i].type == 1){
    		arr[quer[i].a - 1] = quer[i].b;
    	}
    	else if(quer[i].type == 2){
    		merge(quer[i].a - 1, quer[i].b - 1, dsu, distance);	
    	}
    	else{
    		int para = parent(quer[i].a - 1, dsu);
    		int parb = parent(quer[i].b - 1, dsu);
    		if(para != parb || isBlocked[para] || isBlocked[parb]){
    			cout << "0\n";
    		}
    		else{
    			int dista = finddistance(quer[i].a - 1, dsu, distance);
    			int distb = finddistance(quer[i].b - 1, dsu, distance);
    			if(dista != distb){
    				cout << "-";
    			}
    			long long num = (long long)arr[quer[i].a - 1] * (long long)quer[i].c;
    			long long den = (long long)arr[quer[i].b - 1];
    			long long gcd = __gcd(num, den);
    			num/=gcd, den/=gcd;
    			cout << num <<"/"<<den<<endl;
    		}
    	}
    }

}