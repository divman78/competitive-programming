#include<bits/stdc++.h>
using namespace std;

int main(){
	long long n, x, y;
	cin >> n >> x >> y;
	string str;
	cin >> str;
	bool isOne = str[0]-48;
	long long cnt0 = !(str[0]-48);
	for(long long i = 1; i < n; i++){
		if(isOne && !(str[i]-48)){
			isOne = false;
			cnt0 ++;
		}
		else if(!isOne && (str[i]-48)){
			isOne = true;
		}
	}
	long long ans = 1000000000000000000;
	for(long long i = 0; i < cnt0; i++){
		ans = min(ans,x*(i) + y*(cnt0 - i));
	}
	if(cnt0 == 0) cout << 0;
	else cout << ans << endl;
}