#include<bits/stdc++.h>
using namespace std;
#define N 5050
#define mod 1000000007

int n, k;
int arr[N];
int ans;


void calc(int cnt, int prod, int index){
	if(index >= n){
		return;
	}
	if(cnt == 0){
		calc(cnt + 1, prod, index + 1);
		calc(cnt, prod, index + 1);
		return;
	}
	if(cnt == k - 1){
		ans = (ans * prod)%mod;
		calc(cnt, prod, index + 1);
		return;
	}

	calc(cnt, prod, index  + 1);
	calc(cnt + 1, (prod * arr[index])%mod, index + 1);
}


int main(){
	int t;
	cin >> t;
	while(t--){
		cin >> n >> k;
		ans = 1;
		for(int i = 0; i  < n; i++){
			cin >> arr[i];
		}
		sort(arr, arr + n);
		calc(0, 1, 0);
		cout << ans << endl;

	}
}