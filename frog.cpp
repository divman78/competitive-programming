#include<bits/stdc++.h> 
using namespace std;

int main() {

	int n, c;
	cin >> n >> c;

	int arr[n];

	for (int i = 0; i < n; ++i)
	{
		cin >> arr[i];
	}

	int ans = (arr[0]*arr[0]+arr[n-1]*arr[n-1]-2*arr[0]*arr[n-1])+c;

	cout << ans << endl;

	for (int i = 1; i < n; ++i)
	{
		ans = min(ans, arr[0]*arr[0]+arr[n-1]*arr[n-1]+2*(arr[i]*(arr[i]-arr[0])-arr[i]*arr[n-1])+2*c);
		cout << i+1 << " " << arr[0]*arr[0]+arr[n-1]*arr[n-1]+2*(arr[i]*(arr[i]-arr[0])-arr[i]*arr[n-1])+2*c << endl;
	}

	cout << ans << endl;
} 