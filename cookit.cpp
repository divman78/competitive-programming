#include <bits/stdc++.h>
// #pragma GCC optimize ("Ofast")
// #pragma GCC target ("sse4")
// #pragma GCC optimize ("unroll-loops")
#define ll          long long
#define ld          long double
#define pb          push_back
#define pii         pair<int,int>
#define vi          vector<int>
#define all(a)      (a).begin(),(a).end()
#define F           first
#define S           second
#define sz(x)       (int)x.size()
#define hell        1000000007
#define endl        '\n'
using namespace std;
#define MAXN		100005
int A[MAXN],nge[20][MAXN];
int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	cout.tie(0);
	int n,q,i,j,ans=0,l,r,a,b;
	cin>>n>>q;
	for(i=0;i<n;i++)
		cin>>A[i];
	A[n]=INT_MAX;
	stack<int> st;
	for(i=0;i<=n;i++){
		while(!st.empty() and A[st.top()]<A[i]){
			nge[0][st.top()]=i;
			st.pop();
		}
		st.push(i);
	}
	nge[0][n]=n;
	for(i=1;i<20;i++)
		for(j=0;j<=n;j++)
			nge[i][j]=nge[i-1][nge[i-1][j]];
	while(q--){
		cin>>a>>b;
		l=(a+ans)%n;
		r=(b+ans)%n;
		if(l>r)
			swap(l,r);
		ans=1;
		for(i=19;i>=0;i--)
			if(nge[i][l]<=r){
				ans+=(1<<i);
				l=nge[i][l];
			}
		cout<<ans<<endl;
	}
	return 0;
}