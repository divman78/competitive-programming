//WA
//very near the correct answer
#include<bits/stdc++.h>
using namespace std;
#define N 1000000
#define LOG 21

// #define prnt(dd) cout << "debug :: " << dd << endl; 
 #define printArr(arr) for (int i = 0; i < n; ++i) cout << arr[i] << " "; cout << endl;

int n;
int a[N];
int pr[N][LOG];
int cnt[N];
int mx[N];
// void dfs(int visit, vector<vector<int> >& adj){

// 	mx[visit] = max(mx[pr[visit][0]], a[visit]);
// 	cnt[visit] = cnt[pr[visit][0]] + (mx[pr[visit][0]] < a[visit]);
// 	for (int i = 0; i < adj[visit].size(); ++i)
// 	{
// 		int node = adj[visit][i];
// 		if(node != pr[visit][0]){
// 			pr[node][0] = visit;
// 			dfs(node, adj);
// 		}
// 	}
// }

// void dfs2(vector<vector<int> >& adj){
// 	sta.push(0);
// 	while(!sta.empty()){
// 		int visit = sta.top();
// 		sta.pop();
// 		mx[visit] = max(mx[pr[visit][0]], a[visit]);
// 		cnt[visit] = cnt[pr[visit][0]] + (mx[pr[visit][0]] < a[visit]);
// 		for (int i = 0; i < adj[visit].size(); ++i)
// 	    {
// 			int node = adj[visit][i];
// 			if(node != pr[visit][0]){
// 				pr[node][0] = visit;
// 				sta.push(node);
// 			}
// 		}
// 	}
// }

main(){
	ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL);
	//freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	while(t--){
		
		cin >> n;
		stack<int> sta;
        vector<vector<int> > adj(n);
		for (int i = 0; i < n; ++i)
		{
			cin >> a[i];
		}
		for (int i = 1; i < n; ++i)
		{
			int x;
			cin >> x;
			x--;
			adj[i].push_back(x);
			adj[x].push_back(i);
		}
		pr[0][0] = 0;
		cnt[0] = 1;
		mx[0] = a[0];
		//preprocess cnt and mx arrays as suggested in editorial
		//dfs(0, adj);
		//dfs2(adj);
		///////////////////////////////////////////////
		sta.push(0);
		while(!sta.empty()){
			int visit = sta.top();
			sta.pop();
			mx[visit] = max(mx[pr[visit][0]], a[visit]);
			cnt[visit] = cnt[pr[visit][0]] + (mx[pr[visit][0]] < a[visit]);
			for (int i = 0; i < adj[visit].size(); ++i)
		    {
				int node = adj[visit][i];
				if(node != pr[visit][0]){
					pr[node][0] = visit;
					sta.push(node);
				}
			}
		}
		/////////////////////////////////////////////////

		//binary lifting 2^i = 2^(i-1) + 2^(i-1) 
		for (int i = 0; i < n; ++i)
			for (int j = 1; j < LOG; ++j)
				pr[i][j] = pr[pr[i][j-1]][j-1];

		// printArr(cnt);
		// printArr(mx);
		int q, ans;
		cin >> q;
		ans = 0;
		while(q--){
			int v, w;
			cin >> v >> w;
			v ^= ans;
			w ^= ans;
			//cout << v  << " "<< w << endl;
			v--;
			ans = cnt[v];
			//if all nodes in path have less than or equal value to w, then zero updates
			if(mx[v] <= w){
				ans = 0;
				cout << 0 << endl;
				continue;
			}
			// if root is not the first update
			if(w >= mx[0]){
				// find the v for which mx[v] > w and all ancestors u of v have mx[u] <= w
				for (int i = LOG - 1; i >= 0; --i)
				{
					if(pr[v][i] != -1 && w < mx[pr[v][i]]){
						v = pr[v][i];
					}
				}
				// decrease cnt of ancestors of v
				//cout << "debug :: " << cnt[pr[v][0]] << endl;
				ans -= cnt[pr[v][0]];
			}
			cout << ans << endl;
		}

	}
}