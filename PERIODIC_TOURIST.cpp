/**
 *    author:  tourist
 *    created: 19.10.2018 18:47:39       
**/
#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(0);
  int tt;
  cin >> tt;
  while (tt--) {
    int n;
    cin >> n;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
      cin >> a[i];
    }
    set<int> starts;
    int mx = 0;
    for (int i = 0; i < n; i++) {
      if (a[i] != -1) {
        mx = max(mx, a[i]);
        starts.insert(i - a[i] + 1);
      }
    }
    vector<int> s(starts.begin(), starts.end());
    int d = 0;
    for (int i = 1; i < (int) s.size(); i++) {
      d = __gcd(d, s[i] - s[0]);
    }
    if (d == 0) {
      cout << "inf" << '\n';
      continue;
    }
    if (mx > d) {
      cout << "impossible" << '\n';
      continue;
    }
    cout << d << '\n';
  }
  return 0;
}