#include<bits/stdc++.h>
using namespace std;
#define int long long
#define P1 31
#define MOD1 1000000007



struct Hashs 
{
	vector<int> hashs;
	vector<int> pows;
	int P;
	int MOD;

	Hashs() {}

	Hashs(string &s, int P, int MOD) : P(P), MOD(MOD) 
	{
		int n = s.length();
		pows.resize(n+1, 0);
		hashs.resize(n+1, 0);
		pows[0] = 1;
		for(int i=n-1;i>=0;i--) 
		{
			hashs[i]=(1LL * hashs[i+1] * P + s[i] - 'a' + 1) % MOD;
			pows[n-i]=(1LL * pows[n-i-1] * P) % MOD;
		}
		pows[n] = (1LL * pows[n-1] * P)%MOD;
	}
	int get_hash(int l, int r) 
	{
		int ans=hashs[l] + MOD - (1LL*hashs[r+1]*pows[r-l+1])%MOD;
		ans%=MOD;
		return ans;
	}
};

signed main(){
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif

	string s = "kpkp";
	
	Hashs h1 = Hashs(s, P1, MOD1);

	cout << h1.get_hash(0,1) << " "<< h1.get_hash(2,3);


}

/*test case:

0001
avavavavavavavab

*/