#include<bits/stdc++.h>
using namespace std;
#define N (int)1e6

int big[N];
int sz[N];
int dep[N];
int cnt[N];

vector<int> adj[N];
int n;

void dfs1(int v, int par, int depth){
	dep[v] = depth;
	for(int i = 0; i < adj[v].size(); i++){
		if(adj[v][i] != par){
			dfs1(adj[v][i], v, depth + 1);
		}
	}
}

void dfs2(int v, int par){
	sz[v]++;
	for(int i = 0; i < adj[v].size(); i++){
		if(adj[v][i] != par){
			dfs2(adj[v][i], v);
			sz[v] += sz[adj[v][i]];
		}
	}
}

int res[N];
int best, maxi;

void add(int v, int par, int x){
	cnt[dep[v]] += x;
	if(cnt[best] < cnt[dep[v]] || (cnt[best] == cnt[dep[v]] && best > dep[v])){
		best = dep[v];
	}
	for (int i = 0; i < adj[v].size(); ++i){
		if(adj[v][i] != par && !big[adj[v][i]]){
			add(adj[v][i], v, x);
		}
	}

}

void dfs3(int v, int par, int keep){
	int mx = -1, biggest = -1;
	for(int i = 0; i < adj[v].size(); ++i){
		if(adj[v][i] != par && sz[adj[v][i]] > mx){
			mx = sz[i];
			biggest = adj[v][i];
		}
	}

	for (int i = 0; i < adj[v].size(); ++i){
		if(adj[v][i] != par && adj[v][i] != biggest){
			dfs3(adj[v][i], v, 0);
		}
	}

	best = 0;

    if(biggest != -1){
    	dfs3(biggest, v, 1), big[biggest] = 1;
    }

    add(v, par, 1);

    res[v] = best - dep[v];
    //cout << v << " -> " <<best << endl;
    for (int i = 0; i < n; ++i)
    {
    	//cout << cnt[i] << " ";
    }
    //cout << endl;

    if(biggest != -1){
    	big[biggest] = 0;
    }

    if(keep == 0){
    	add(v, par, -1);
    }
}


int main(){
	cin >> n;
	for (int i = 1; i <= n - 1; ++i){
		int x, y;
		cin >> x >> y;
		adj[x-1].push_back(y-1);
		adj[y-1].push_back(x-1);
	}

	dfs1(0, 0, 0);
	dfs2(0, 0);
	dfs3(0, 0, 1);

	for (int i = 0; i < n; ++i){
		cout << res[i] <<  endl;
	}

}