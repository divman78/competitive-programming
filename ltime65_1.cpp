#include<bits/stdc++.h>
using namespace std;
#define N 51
#define MO 1000000007
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl
#define int long long
int n, s;
int ans;
int arr[N];

int calc(){
	// cout << "ab aya \n";
	//printarr(arr, n);
	for(int i = 0; i < n-1; i++){
		for(int j = i+1; j < n; j++){
			ans = (ans + __gcd(arr[i], arr[j]))%MO;
			// cout << __gcd(arr[i], arr[j])  <<  "->" << ans << " ";
		}
		// cout << endl;
	}
}

void rec(int ind, int left){
	// cout << ind  << " i " << n << " bha i  ";
	if(ind >= n){
		//cout << "cosa \n";
		if(left) return;
		calc();
		return;
	}

	if(ind == n-1){
		// cout << " hddsffd____________________\n" ;
		arr[ind] = left;
		rec(ind+1, 0);
		arr[ind] = -1;
		return;
	}
	for(int i = 1; i <= left-n+ind+1; i++){
		arr[ind] = i;
		rec(ind+1, left-i);
		arr[ind] = -1;
	}
}

signed main(){
	//ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("output1.txt","r",stdin);
	freopen("input.txt","w",stdout);
	#endif
	int t;
	cin >> t;
	while(t--){
		cin >> n >> s;
		int left = 0;
		for(int i = 0; i < n; i++){
			cin >> arr[i];
			if(arr[i] != -1) 
				left += arr[i];
		}
		left = s - left;
		sort(arr, arr+n);
		reverse(arr, arr+n);
		int mark = -1;
		for(int i = n-1; i >= 0; i--){
			if(arr[i]!=-1){
				mark = i;
				break;
			}
		}
		ans = 0;
		// cout  << "mark = " << mark << endl;
		rec(mark+1, left);
		printf("%lld\n", ans);
        // cout << "ENDOFTEST\n";
	}
}