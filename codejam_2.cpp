#include<bits/stdc++.h>
using namespace std;
#define pb(x) push_back(x)
#define mp(x,y) make_pair(x,y)
#define casei(t) cout<<"Case# "<<t<<": "
#define A 65

void solve(int t){
	int n;
	cin>>n;
	int arr[n];
	for(int i = 0; i < n; i++){
		cin>>arr[i];
	}
	sort(arr,arr+n);
	int suf[n];
	suf[0]=arr[0];
	for(int i = 1; i < n; i++){
		suf[i] = suf[i-1]+arr[i];
		cout<<suf[i]<<" ";
	}
	cout<<"\n";
	int ans = 0;
	for(int i = 0; i < n-1; i++){
		for(int j = n; j>=i+1; j--){
			int sub;
			if(i == 0){
				sub = 0;
			}
			else{
				sub = suf[i-1];
			}
			if(arr[j]*6<suf[j-1]-sub){
				ans = -1;
				break;
			}
			ans = n-i;
		}
		if(ans > 0){
			break;
		}
		else ans = 0;
	}
	casei(t);
	cout<<ans<<"\n";
}

int main(){
	int t;
	cin >> t;
	for(int i = 1; i <= t; i++)
	{
		solve(i);
	}
}