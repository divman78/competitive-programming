#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define show(arr, n) for(int i = 0; i < n; i++) cout<<arr[i]<<" "; cout<<"\n";

int main(){

	int n;
	cin>>n;
	string s;
	cin>>s;
	int arr[n];
	for(int i = 0; i < n; i++){
		arr[i] = s[i] -48;
		if(arr[i]==0){
			arr[i] = -1;
		}
		else{
			arr[i] = 1;
		}
	}
	string ans = "YES\n";
	int last = arr[0];
	for(int i = 1; i < n; i++){
		if(last != arr[i]*(-1)){
			ans = "NO\n";
			break;
		}
	}
	cout<<ans;
}