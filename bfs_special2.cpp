#include <bits/stdc++.h>
using namespace std;

//best bfs code i ever seen 
//By yujinwunz
//red coders are bravest warriors in competitive programming

bool seen[2000][2000];

int dx[4] = {-1, 0, 1, 0};
int dy[4] = {0, 1, 0, -1};
int cl[4] = {1, 0, 0, 0};
int cr[4] = {0, 0, 1, 0};

struct Node {
	int x, y, lcost, rcost;
};
bool operator <(const Node &me, const Node &other) {
	return me.lcost+me.rcost > other.lcost+other.rcost;
}

string grid[2000];

int n, m;
int sy, sx;
int nl, nr;

int main() {
	ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
	cin>>n>>m;
	cin>>sy>>sx;
	cin>>nl>>nr;
	sy--;sx--;
	for (int i = 0; i < n; i++) {
		cin>>grid[i];
	}

	priority_queue<Node> work;
	work.push({sx, sy, 0, 0});
	int ans = 0;
	while (work.size()) {
		auto [x, y, lcost, rcost] = work.top();
		work.pop();
		if (seen[x][y]) continue;
		seen[x][y] = true;
		
		if (lcost <= nl && rcost <= nr) {
			//cerr<<y<<" "<<x<<endl;
			ans++;
		} else continue;

		for (int i = 0; i < 4; i++) {
			int nx = x + dx[i];
			int ny = y + dy[i];
			if (nx < 0 || nx >= m) continue;
			if (ny < 0 || ny >= n) continue;
			if (grid[ny][nx] == '*') continue;
			work.push({nx, ny, lcost+cl[i], rcost+cr[i]});
		}
	}

	cout<<ans<<endl;
}