#include<bits/stdc++.h>
using namespace std;
#define N 5000100
#define MO 1000000007
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0); cout.tie(0)

int isprime[N];
int ans[N];
void preprocess(){
	for (int i = 0; i < N; ++i)
	{
		isprime[i] = 1;
	}

	for (int i = 2; i < N; ++i)
	{
		if(isprime[i])
		for(int j = i+i; j < N; j+=i)
		{
			isprime[j] = 0;
		}
	}

	for (int i = 2; i < N; ++i)
	{
		isprime[i] += isprime[i-1];
	}

	ans[1] = 1;
	for (int i = 2; i < N; ++i){
		ans[i] = ((long long) ans[i-1] * isprime[i])%MO; 
	}

}

signed main(){
	FAST_IO;

    #ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();

	int t;
	scanf("%d", &t);
	while(t--){
	int n;
	scanf("%d", &n);
	printf("%d\n", ans[n]);
	}

	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
}