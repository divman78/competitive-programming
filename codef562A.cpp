#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

int n , m;
int a[N],b[N];
bool flag[N];
bool flag1[N];
bool flag2[N];

int call(int x, bool* flag){
	if(x>n){
		return 0;
	}
	int nxt = -1;
	for (int i = 0; i < m; ++i)
	{
		if(a[i] == x || b[i] == x) flag[i] = true;
		else nxt = i;
	}

	return nxt;
}

bool fun(int x){
	memset(flag, false, sizeof(flag));
	int nxt = call(x, flag);
	if(nxt==-1){
		return true;
	}
	memset(flag1, false, sizeof(flag1));
	call(a[nxt], flag1);

	int ret = 1;

	for (int i = 0; i < m; ++i)
	{
		if(!flag1[i] && !flag[i]){
			ret = 0;
		}
	}
	if(ret==0){
		memset(flag2, false, sizeof(flag2));
		call(b[nxt], flag2);

		ret = 1;

		for (int i = 0; i < m; ++i)
		{
			if(!flag2[i] && !flag[i]){
				ret = 0;
			}
		}
	}


	return (ret)? true:false;





}



signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	
	cin >> n >> m;
	for (int i = 0; i < m; ++i)
	{
		int x, y;
		cin >> x >> y;
		a[i] = min(x,y);
		b[i] = max(x,y);
	}	
	if(m == 2){
		cout << "YES\n";
	}
	else if(fun(a[0]) || fun(b[0])){
		cout << "YES\n";
	}
	else{
		cout << "NO\n";
	}
}




















