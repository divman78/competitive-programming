#include <bits/stdc++.h>

using namespace std;

const long long MOD = 1e9 + 7;

struct State {
    long long cnt0, cntX;
    int xr;
};

int main() {
    ios_base::sync_with_stdio(false); cin.tie(0);
    int n, x;
    cin >> n >> x;
    vector<int> a(n);
    for (int i = 0; i < n; i++) {
        cin >> a[i];
    }
    vector<vector<int>> g(n);
    for (int i = 0; i < n-1; i++) {
        int u, v;
        cin >> u >> v;
        u--; v--;
        g[u].push_back(v);
        g[v].push_back(u);
    }

    long long ans = 0;
    function<State(int, int)> dfs;
    dfs = [&] (int v, int p) -> State {
        State st = {1, 0, a[v]};
        for (int u: g[v]) {
            if (u == p) {
                continue;
            }
            auto nxt = dfs(u, v);
            int xr = nxt.xr ^ st.xr;
            auto cnt0 = (nxt.cnt0 * st.cnt0 + nxt.cntX * st.cntX) % MOD;
            auto cntX = (nxt.cnt0 * st.cntX + nxt.cntX * st.cnt0) % MOD;
            st = {cnt0, cntX, xr};
        }
        long long curCnt0 = 0, curCntX = 0;
        if (st.xr == x) {
            curCntX = st.cnt0;
        }
        if (st.xr == 0) {
            curCnt0 = st.cntX;
        }
        if (v == 0) {
            ans = (curCntX + curCnt0) % MOD;
        }
        st.cntX = (st.cntX + curCntX) % MOD;
        st.cnt0 = (st.cnt0 + curCnt0) % MOD;
        return st;
    };
    dfs(0, -1);
    cout << ans << endl;
}