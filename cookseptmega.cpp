//correct
#include<bits/stdc++.h>
using namespace std;
#define N 2000100


int n;
int t;
int arr[N];

int main(){
	//freopen("input.txt","r",stdin);
	cin >> t;
	while(t--){
		cin >> n;
		int sum = 0;
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
			sum += arr[i];
		}
		int left = -1, right = -1;

		for (int i = 0; i < n; ++i)
		{
			if(arr[i] == 1){
				right = i;
			}
		}
		for (int i = n-1; i >= 0; --i)
		{
			if(arr[i] == 1){
				left = i;
			}
		}
		int x;
		if(left == -1){
			x = n - (right+1);
		}
		else if(right == -1){
			x = left;
		}
		else if(left == -1 && right == -1){
			x = sum/2;
		}
		else {
			x = min(n - (right+1), left);
		}

		 cout << sum - x << endl;
		

	}
}