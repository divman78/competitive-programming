#include<bits/stdc++.h>
using namespace std;

signed main(){
	//ios_base::sync_with_stdio(false); cin.tie(0); 
	//freopen("input.txt","r",stdin);
	int t;
	cin >> t;
	//t=1;
	while(t--){
		int n, k;
		cin >> n >> k;
		int arr[n];
		for(int i = 0; i < n; i++){
			cin >> arr[i];
		}
		sort(arr, arr+n);

		vector<int> arr2;
		int ans = 0;
		for(int i = 0; i < n-1; i++){
			if(arr[i] > k)
				arr2.push_back(arr[i]-k);
			else
				ans += arr[i];
		}
		for(int i = arr2.size()-1;  i > 0; i--){
			int x = arr2[i]/i;
			int p = arr2[i]-x*i;
			for(int j = 0; j < i; j++){
				int temp = min(arr2[j],x);
				arr2[j] -= temp;
				arr2[i] -= temp;
			}
			for(int j = i-1; j >= 0; j--){
				if(p>0 && arr2[j]>0){
					arr2[j]-- , arr2[i]--;
					p--;
				}
			}
		}
		int sum = 0;
		for(int i = 0; i < arr2.size(); i++){
			//cout << arr2[i] << " ";
			sum += arr2[i];
			ans += k;
		}
		//cout << endl;
		ans += arr[n-1] - sum;
		cout << ans << endl;
	}
	return 0;
}