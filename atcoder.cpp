#include <stdio.h>
typedef long long int lli;
typedef long double ld;

void print(int *a, int n){
    int i;
    for(i=0;i<n;i++)printf("%d ",a[i]);printf("\n");
}

int main(void) {
	// your code goes here
	int t,n,m,k,i,j,z1,z2,a[55], winner;
	scanf("%d",&t);
	while(t--){winner=0;
	    scanf("%d%d%d",&n,&z1,&z2);
	    for(i=0;i<n;i++){
	        scanf("%d",a+i);
	        if(winner)continue;
	        if(abs(z1) == abs(a[i]) || abs(z2) == abs(a[i])){
	            winner=1;
	        }
	    }
	   // printf("%d %d %d\n",n,z1,z2);
	   // print(a,n);
	   
	    if(winner){
	     printf("1\n");   
	     continue;
	    }
	    if(z1==0 || z2==0){
	        printf("2\n");
	        continue;
	    }
	    for(i=0;i<n;i++){
	        for(j=0;j<n;j++){
	            m=a[i]+a[j];k=a[i]-a[j];
	            if((m==z1 || m==z2) || (k==z1 || k==z2)){
	                break;//p1 can not choose +a[i]
	            }
	        }
	        if(j==n)break;//p1 can choose +a[i] end
	        for(j=0;j<n;j++){
	            m=-a[i]+a[j];k=-a[i]-a[j];
	            if((m==z1 || m==z2) || (k==z1 || k==z2)){
	                break;//p1 can not choose -a[i]
	            }
	        }
	        if(j==n)break;//p1 can choose -a[i] end
	    }
	    if(i==n)
	        printf("2\n");
	    else
    	    printf("0\n");
	}
	return 0;
}

