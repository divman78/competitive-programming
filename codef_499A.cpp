#include<bits/stdc++.h>
using namespace std;

int main(){
	int n , k;
	cin >> n >> k;
	string s;
	cin >> s;
	int arr[26];
	for (int i = 0; i < 26; ++i)
	{
		arr[i] = 0;
	}
	int ans = INT_MAX;
	for (int i = 0; i < s.length(); ++i)
	{
		arr[s[i]-'a']++;
	}

	for (int i = 0; i < 26; ++i)
	{
		int wt = 0, cnt = 0;
		if(arr[i] == 0) continue;
		for (int j = i; j < 26;)
		{
			if(cnt == k) break;
			if(arr[j]){
				cnt++;
				wt += j+1;
				j+=2;
			}
			else{
				j++;
			}
		}
		if(cnt == k) {
			ans = min(ans, wt);
			//cout << ans << endl;
		}
	}
	if(ans == INT_MAX)
		cout << -1 << endl;
	else	
		cout << ans << endl;
}