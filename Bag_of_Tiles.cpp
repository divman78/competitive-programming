#include "bits/stdc++.h"
using namespace std;
#ifdef ONLINE_JUDGE
#define trace(...)
#endif

#define fi 					first
#define se 					second
#define sz(a) 				((int) a.size())
#define all(a) 				a.begin(), a.end()
#define rep(i, a, b) 		for (int i = a; i < b; ++i)
#define repv(i, a, b) 		for (int i = b - 1; i >= a; --i)
#define SET(A, val) 		memset(A, val, sizeof(A))
using LL = long long;
using ll = long long;
using db = long double;
using ii = pair <int, int>;


const LL mod = 1e9 + 7;
LL inv(LL a) {
	LL res = 1;
	for (LL n = mod - 2; n > 0; n >>= 1) {
		if (n & 1) res = (res * a) % mod;
		a = (a * a) % mod;
	}
	return res;
}

int main() {
	ios_base::sync_with_stdio(false); cin.tie(NULL);
	freopen("input.txt","r",stdin);
	int t; cin >> t;
	while (t--) {
		LL x, y, s, u, v; cin >> x >> y >> s >> u >> v;
		if (s % y == 0 && bitset<100>(s / y).count() == 1) {
			swap(x, y);
			u = v - u;
		}

		s /= x;
		s %= mod;
		LL ans = (s * v) % mod;
		ans = (ans * inv(u)) % mod;
		cout << ans << '\n';
	}

	return 0;
}
