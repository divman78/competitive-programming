//TLE due because of not using best practices
#include<bits/stdc++.h>
using namespace std;
#define hashmap unordered_map<int, int>
#define N 100010

inline char nc(){
  static char buf[100000],*p1=buf,*p2=buf;
  if (p1==p2) { p2=(p1=buf)+fread(buf,1,100000,stdin); if (p1==p2) return EOF; }
  return *p1++;
}

void fastscan(int& x){
  char c=nc(),b=1;
  if(c == -1) {x=0; return;}
  for (;!(c>='0' && c<='9');c=nc()) if (c=='-') b=-1;
  for (x=0;c>='0' && c<='9';x=x*10+c-'0',c=nc()); x*=b;
}

int n, m;
hashmap mm[N*4];
int ans[N*4];
int lz[N*4];
int a[N], b[N];

// void fastscan(int& ret){
// 	char r;
// 	bool start=false,neg=false;
// 	while(true){
// 		r=getchar();
// 		if((r-'0'<0 || r-'0'>9) && r!='-' && !start){
// 			continue;
// 		}
// 		if((r-'0'<0 || r-'0'>9) && r!='-' && start){
// 			break;
// 		}
// 		if(start)ret*=10;
// 		start=true;
// 		if(r=='-')neg=true;
// 		else ret+=r-'0';
// 	}
	
// }

// int fastscan(int& x){
// 	cin >> x;
// 	return x;
// }

// void fastscan(int &number) 
// { 
//     //variable to indicate sign of input number 
//     bool negative = false; 
//     register int c; 
  
//     number = 0; 
  
//     // extract current character from buffer 
//     c = getchar(); 
//     if (c=='-') 
//     { 
//         // number is negative 
//         negative = true; 
  
//         // extract the next character from the buffer 
//         c = getchar(); 
//     } 
  
//     // Keep on extracting characters if they are integers 
//     // i.e ASCII Value lies from '0'(48) to '9' (57) 
//     for (; (c>47 && c<58); c=getchar()) 
//         number = number *10 + c - 48; 
  
//     // if scanned input has a negative sign, negate the 
//     // value of the input number 
//     if (negative) 
//         number *= -1; 
// } 


void build(int l, int r, int ind){
	mm[ind].clear();
	for (int i = l; i <= r; ++i)  mm[ind][b[i]]++;
	ans[ind]  = lz[ind] = 0;
	for (int i = l; i <= r; ++i)  if(a[i] == b[i]) ans[ind]++;
	if(l == r) return;
    int mid = (l+r)>>1;
    build(l, mid, ind << 1);
    build(mid+1, r, (ind << 1)|1);
}

void modify(int l, int r, int fl, int fr, int ind, int c){
	if(r < fl || fr < l) return;
	if(l >= fl && r <= fr){
		lz[ind] = c;
		ans[ind] = mm[ind].count(c) ? mm[ind][c] : 0;
		return;
	}
	if(lz[ind]){
		lz[ind<<1] = lz[ind];
		lz[(ind<<1)|1] = lz[ind];
		ans[ind<<1] = mm[ind<<1].count(lz[ind]) ? mm[ind << 1][lz[ind]]:0;
		ans[(ind<<1)|1] = mm[(ind<<1)|1].count(lz[ind]) ? mm[(ind << 1)|1][lz[ind]]:0;
		lz[ind] = 0;
	}
	int mid = (l+r)>>1;
	modify(l, mid, fl, fr, ind << 1, c);
	modify(mid+1, r, fl, fr, (ind << 1)|1, c);
	ans[ind] = ans[ind<<1]+ans[(ind<<1)|1];
}

int main(){
	//freopen("input.txt","r",stdin);
	//ios_base::sync_with_stdio(false); cin.tie(NULL);
	int t;
	scanf("%d",&t);
	while(t--){
	    fastscan(n), fastscan(m);
	    for (int i = 1; i <= n; ++i) fastscan(a[i]);
        for (int i = 1; i <= n; ++i) fastscan(b[i]);
        build(1, n, 1);

        while(m--){
        	int x, y, c;
        	fastscan(x), fastscan(y), fastscan(c);
        	x^=ans[1], y^=ans[1], c^=ans[1];
        	modify(1, n, x, y, 1, c);
        	printf("%d\n", ans[1]);
        }
	}
}


// another user similar code giving correct answer
// #include <bits/stdc++.h>
// using namespace std;

// inline char nc(){
//   static char buf[100000],*p1=buf,*p2=buf;
//   if (p1==p2) { p2=(p1=buf)+fread(buf,1,100000,stdin); if (p1==p2) return EOF; }
//   return *p1++;
// }

// inline int getint(){
//   int x;
//   char c=nc(),b=1;
//   if(c == -1)
// 	return 0;
//   for (;!(c>='0' && c<='9');c=nc()) if (c=='-') b=-1;
//   for (x=0;c>='0' && c<='9';x=x*10+c-'0',c=nc()); x*=b;
//   return x;
// }

// #define N 101010

// map <int, int> mp[N<<2];
// int a[N], b[N];
// int ans[N<<2], to[N<<2];

// void build(int L, int R, int id) {
// 	mp[id].clear();
// 	for (int i = L; i <= R; i ++) mp[id][b[i]] ++;
// 	ans[id] = 0;
// 	to[id] = 0;
// 	for (int i = L; i <= R; i ++) if (a[i] == b[i]) ans[id] ++;
// 	if (L == R) return;
// 	int mid = (L + R) >> 1;
// 	build(L, mid, id << 1);
// 	build(mid + 1, R, (id << 1) | 1);
// }

// void set_to(int id, int c) {
// 	to[id] = c;
// 	ans[id] = mp[id].count(c) ? mp[id][c] : 0;
// }

// void push_down(int id) {
// 	if (to[id]) {
// 		set_to(id << 1, to[id]);
// 		set_to((id << 1) | 1, to[id]);
// 		to[id] = 0;
// 	}
// }

// void push_up(int id) {
// 	ans[id] = ans[id<<1] + ans[(id<<1)|1];
// }

// void modify(int le, int ri, int c, int L, int R, int id) {
// 	if (le > R || L > ri) return;
// 	if (le <= L && R <= ri) {
// 		set_to(id, c);
// 		return;
// 	}
// 	push_down(id);
// 	int mid = (L + R) >> 1;
// 	modify(le, ri, c, L, mid, id << 1);
// 	modify(le, ri, c, mid + 1, R, (id << 1) | 1);
// 	push_up(id);
// }

// int main() {
// 	int T;
// 	scanf("%d", &T);
// 	while (T --) {
// 		int n, m;
// 		n = getint(), m = getint();
// 		for (int i = 1; i <= n; i ++) a[i] = getint();
// 		for (int i = 1; i <= n; i ++) b[i] = getint();
// 		build(1, n, 1);
// 		cerr << "build" << endl;
// 		int rlt = ans[1];
// 		int le, ri, c;
// 		while (m --) {
// 			le = getint(), ri = getint(), c = getint();
// 			le ^= rlt, ri ^= rlt, c ^= rlt;
// 			modify(le, ri, c, 1, n, 1);
// 			rlt = ans[1];
// 			printf("%d\n", rlt);
// 		}
// 		cerr << "end" << endl;
// 	}

// 	return 0;
// }