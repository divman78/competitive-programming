#include<bits/stdc++.h>
using namespace std;
#define MOD 1000000007
#define int long long


//f(n) = f(n-1)+f(n-m)
int n, m;

vector<vector<int> > multiply(vector<vector<int> >& mat1, vector<vector<int> >& mat2){
	vector<vector<int> > res(m);
	for (int i = 0; i < m; ++i)
	{
		for(int j = 0; j < m; j++){
			int p = 0;
			for(int k = 0; k < m; k++){
				p = (p+((mat1[i][k]*mat2[k][j])%MOD))%MOD;
			}
			res[i].push_back(p);
		}
	}
	return res;
}


vector<vector<int> > bigMOD(vector<vector<int> > num, long long n){
  if(n==0){
  	vector<vector<int> > vec(m);
  	for (int i = 0; i < m; ++i)
  	{
  		for (int j = 0; j < m; ++j)
  		{
  			if(i == j) vec[i].push_back(1);
  			else vec[i].push_back(0);
  		}
  	}
  	return vec;
  }
  vector<vector<int> > x=bigMOD(num,n/2);
  x=multiply(x,x);
  if(n%2==1) x=multiply(x,num);
  return x;
}


signed main(){
	//freopen("input.txt","r",stdin);
	cin >> n >> m;
	if(n < m){
		cout << 1 << endl;
		return 0;
	}

	vector<vector<int> > T(m);
	for (int i = 0; i < m; ++i)
	{
		for (int j = 0; j < m; ++j)
		{
			if((i == m-1 && j == 0) || (i == m-1 && j == m-1)){
				T[i].push_back(1);
			}
			else if(i == j-1){
				T[i].push_back(1);
			} 
			else{
				T[i].push_back(0);
			}
		}
	}

	T = bigMOD(T, n-m+1);

	int ans = 0;


	for (int j = 0; j < m; ++j)
	{
		ans = (ans + T[m-1][j])%MOD;
	}

	cout << ans << endl;

}