#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

int n , m;
int arr[N];

bool poss(int x){

	int sz=x;

	int j = 0;

	int ret = 0;
	int sub = -1;

	//cout << endl;

	for (int i = n-1; i >= 0; --i)
	{
		if((j++)%sz == 0) sub++;
		ret += max(0LL, arr[i] - sub);
		//cout << sub << " ";
	}
	//cout << endl;

	return (ret >= m);

}


int binary_search(int lo,int hi){
	while (lo < hi){
		
	 int mid = lo + (hi-lo)/2;
	 //cout << mid << endl;
      if (poss(mid) == true){
      	//cout << "hello\n";
      	hi = mid;
      }
      else{
      	lo = mid+1;
      }
	}  
   return lo ;       
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	//cin >> t; 
	while(t--){

		cin >> n >> m;

		int sum = 0;

		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
			sum += arr[i];
		}
		sort(arr, arr+n);

		if(sum < m){
			cout << -1 << endl;
			return 0;
		}

		int ans = binary_search(1,n);
		cout << ans << endl;




        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




