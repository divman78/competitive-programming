#include<bits/stdc++.h>
using namespace std;
#define MO 1000000007
#define N 1010
#define int long long


int n, k;
int arr[N][3][N]; // 0-00 1-01 2-10

int solve(){

	memset(arr, 0, sizeof(arr));
	arr[1][0][0] = 1, arr[1][1][1] = 1, arr[1][2][1] = 1;
	for (int i = 2; i <= n; ++i)
	{
		for(int j = 0; j <= i; j++)
		{
			arr[i][0][j] = (((((arr[i-1][0][j])%MO) + 
				                arr[i-1][1][j])%MO) + arr[i-1][2][j])%MO;

			if(j>0) arr[i][1][j] = ((( arr[i-1][0][j-1])%MO) + arr[i-1][2][j-1])%MO;

			if(j>0) arr[i][2][j] = (((arr[i-1][0][j-1])%MO) + arr[i-1][1][j-1])%MO;
		}
	}

	return (((arr[n][0][k]+arr[n][1][k])%MO)+arr[n][2][k])%MO;
}

int solve100(){
	int ans;
	
	return solve();
}


signed main(){
	int t;
	cin >> t;
	while(t--){
		cin >> n >> k;
		int realans = solve();
		int ans100 = solve100();
		if(ans != realans){
			cout << t << " ->";
			cout << "realans = " << realans << " ans100 = " << ans100 << endl;
			exit(0);
		}
	}
}