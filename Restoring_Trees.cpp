#include<bits/stdc++.h>
using namespace std;
int main(){
	freopen("input.txt","r",stdin);
	int n;
	cin >> n;
	pair<int,pair<int,int> > tim[n];
	for(int i = 0; i < n; i++) cin >> tim[i].first;
	for(int i = 0; i < n; i++) cin >> tim[i].second.first;
	for(int i = 0; i < n; i++) tim[i].second.second=i;
	sort(tim, tim+n);
    stack<int> sta;
    int ans[n];
    sta.push(0);
    ans[tim[0].second.second]=-1;
    for (int i = 1; i < n; ++i){
    	sta.push(i);
    	while(i+1 == tim[sta.top()].second.first){
    		int node = sta.top(); sta.pop();
    		if(!sta.empty())ans[tim[node].second.second] = tim[sta.top()].second.second;
    		else break;
    	}
    }
    for(int i = 0; i < n; i++) cout << ans[i]+1 << " ";
    cout << endl;
}