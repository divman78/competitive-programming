var express = require('express');
var path = require('path');
var port=process.env.PORT || 5000;
var bodyParser = require('body-parser');
var testFolder = './tests/';
var fs = require('fs');



var app = express();
app.use(bodyParser.json());

function createIndexTemplate(links){
    var htmlTemplate = `
    <!DOCTYPE html>
    <html>
        <head>
            <title>Arshad ka server</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
        </head>
        <body>
			<ul>
				${links}
			</ul>
        </body>
    </html>
    `;
    return htmlTemplate;
}

function goUp(res,name){
	testFolder = path.join(testFolder, name);
	var links = ``;
	fs.readdir(testFolder, (err, files) => {
		files.forEach(file => {
			links+=`<li><a href="http://192.168.43.50:5000/`;
			links+=file;
			links+=`">`;
			links+=file;
			links+=`</a></li>`;
		});
		res.send(createIndexTemplate(links));
	});
}

app.get('/',function(req, res){
	var links = ``;
	testFolder = path.join(__dirname);
	goUp(res,"");
});

app.get('/:fileName', function (req, res) {
	var param = req.params.fileName;
	console.log(param);
	var path1 = path.join(testFolder,param);
	console.log(testFolder);
	fs.lstat(path1,(err,stat) => {
		if(stat && stat.isDirectory()){
			console.log('This si asd');
			goUp(res,param);
		}else{
			res.sendFile(path.join(testFolder,param));
		}
	})
});

app.get('/:dir/:fileName', function (req, res) {
	res.sendFile(path.join(__dirname,req.params.dir, req.params.fileName));
});

app.listen(port, function () {
  console.log(`App listening on port ${port}!`);
});