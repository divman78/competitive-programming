#include<bits/stdc++.h>
using namespace std;

bool fun(const pair<int,int>& p1, const pair<int,int>& p2){
  if(p1.first!=p2.first) return p1.first>p2.first;
  return p1.second>p2.second;
}

int main(){
  //freopen("input.txt","r",stdin);
  int n;
  cin >> n;
  map<pair<int,int>,int> mm;
  vector<pair<int,int> > pts;
  for(int i = 0; i < n; i++){
    int x, y;
    cin >> x >> y;
    mm[make_pair(x,y)]=1;
    pts.push_back(make_pair(x, y));
  }
  sort(pts.begin(), pts.end(), fun);

  int ans = n;
  for(int i = 0; i < n; i++){
    for(int j = i+1; j < n; j++){
      int p = pts[i].first-pts[j].first;
      int q = pts[i].second-pts[j].second;

      int temp_ans=0;
      for(int k = 0; k < n; k++){
      	if(mm.find(make_pair(pts[k].first+p, pts[k].second+q))!=mm.end()) continue;
      	temp_ans++;
      }

      ans = min(ans, temp_ans);
    }
  }
  cout << ans << endl;
  
}