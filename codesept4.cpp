#include<bits/stdc++.h>
using namespace std;
#define N 1010

int arr[N][N];
int n, m, q ;

int main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	freopen("input.txt","r",stdin);
	freopen("output.txt","w",stdout);
	int t = 1;
	cin >> t;
	while(t--){
		string s1, s2;
		cin >> s1 >> s2;
		int n = s2.length(), m = s1.length();
		memset(arr, 0, sizeof(arr));
		arr[0][0] = 0;
		for (int i = 1; i <= m; ++i)
		{
			arr[0][i] = abs((s1[i-1] - 48));
		}
		for (int i = 1; i <= n; ++i)
		{
			arr[i][0] = abs((s2[i-1] - 48));
		}
		
		if(arr[0][1] && arr[1][0]){
			arr[1][1] = 1;
		}
		else{
			arr[1][1] = 0;
		}

		for (int i = 2; i <= m; ++i)
		{
			if(arr[0][i] && arr[1][i-1] == 0)
				arr[1][i] = 1;
			else 
				arr[1][i] = 0;
		}

		for (int i = 2; i <= n; ++i)
		{
			if(arr[i][0] && arr[i-1][1] == 0)
				arr[i][1] = 1;
			else 
				arr[i][1] = 0;
		}

		for (int i = 2; i <= max(n, m); ++i)
		{
			for (int j = i; j <= m; ++j)
			{
				if(arr[i-1][j] || arr[i][j-1])
					arr[i][j] = 0;
				else 
					arr[i][j] = 1;
			}
			for (int k = i; k <= n; ++k)
			{
				if(arr[k-1][i] || arr[k][i-1])
					arr[k][i] = 0;
				else 
					arr[k][i] = 1;
			}
		}
		cout << endl;
		cout << t << endl;
		for (int i = 0; i <= n; ++i)
		{
			for (int j = 0; j <= m; ++j)
			{
				cout << arr[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;
		// cin >> q;
		// while(q--){
		// 	int x, y;
		// 	cin >> x>> y;
		// 	cout <<abs(1 - arr[x][y]);
		// }
		// cout << endl;

	}

}


long long isqrt(unsigned long long x) {
    /* Precondition: make x odd if possible. */
    int sh = __builtin_ctzll(x);
    x >>= (sh&~1);
    
    /* Early escape. */
    if (x&6) return -1;
    
    /* 2-adic Newton */
    int i;
    const int ITERATIONS = 5; // log log x - 1
    unsigned long long z = (3-x)>>1, y=x*z;
    for (i=0; i<ITERATIONS; i++) {
        unsigned long long w = (3 - z*y) >> 1;
        y *= w;
        z *= w;
    }
    assert(x==0 || (y*z == 1 && x*z == y));
    
    /* Get the positive square root.  Also the top bit
     * might be wrong. */
    if (y & (1ull<<62)) y = -y;
    y &= ~(1ull<<63);
    
    /* Is it the square root? */
    if (y >= 1ull<<32) return -1;
    
    /* Yup. */
    return y<<(sh/2);
}