#include<bits/stdc++.h>
using namespace std;

#define int unsigned long long
#define N 100009


void dfs(int u, int par, int gc, vector<vector<int> >& adj, vector<pair<int,int> >& ans,vector<int>& v, vector<int>& m){
	
	int g = __gcd(gc,v[u]);

	for(int i = 0; i < adj[u].size(); i++){
		int vi = adj[u][i];
		if(vi==par) continue;
		dfs(vi, u, g, adj, ans,v,m);
	}

	if(adj[u].size()==1 && u != 0){
		int gcd = __gcd(g,m[u]);
		ans.push_back(make_pair(u,m[u]-gcd));
	}
}


signed main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif

	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		vector<int> v(n);
		vector<int> m(n);
		vector<vector<int> > adj(n);
		for (int i = 0; i < n-1; ++i)
		{
			int x, y;
			cin >> x >> y;
			adj[x-1].push_back(y-1);
			adj[y-1].push_back(x-1);
		}

		for (int i = 0; i < n; ++i)
		{
			cin >> v[i];
		}

		for (int i = 0; i < n; ++i)
		{
			cin >> m[i];
		}

		vector<pair<int,int> > ans;
		dfs(0,0,v[0],adj,ans,v,m);

		sort(ans.begin(), ans.end());

		for (int i = 0; i < ans.size(); ++i)
		{
			cout << ans[i].second << " ";
		}
		cout << endl;
	}
}