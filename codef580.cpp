#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long



void read_file(){
#ifndef ONLINE_JUDGE
	//freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
#endif
}
void init(){
	FAST_IO;
	read_file();
}

signed main(){
	init();

	int n;
	cin >> n;

	int ans[2*n];

	ans[0] = 1;
	ans[n] = 2;

	for (int i = 1, j = 2; i < n; ++i)
	{
		ans[i]=++j;
		ans[i+n]=++j;
		if(ans[(i-1+2*n)%(2*n)]-ans[(i+n-1+2*n)%(2*n)] == -1){
			swap(ans[i], ans[i+n]);
		}
	}

	vector<int> vec;
	for (int i = 0; i < 2*n; ++i)
	{
		vec.push_back(ans[i]);
	}
	for (int i = 0; i < 2*n; ++i)
	{
		vec.push_back(ans[i]);
	}


	map<int,int> mm;
	int sum = 0;
	for (int i = 0; i < n; ++i)
	{
		sum += ans[i];
	}
	mm[sum]=1;

	for (int i = n; i < 3*n; ++i)
	{
		int s = sum - vec[i-n] + vec[i];
		if(abs(s-sum)>1){
			cout << "NO\n";
			return 0;
		}
		mm[s]=1;
		swap(s, sum);
	}

	if(mm.size()>2){
		cout << "NO\n";
		return 0;
	}


	cout << "YES\n";
	for (int i = 0; i < 2*n; ++i)
	{
		cout << ans[i] << " ";
	}
	cout << endl;









	return 0;
}
