import numpy as np
import cv2
import math
import colorsys
import heapq

#constants
INSIDE = 0
KNOWN = 1
BAND = 2

NBRS = 10 #neighbors of a pixel 
epsilon = 1000000

def getnbrs(x,y):
	return [(x,y-1),(x-1,y),(x,y+1),(x+1,y)]


def initialize(mask):
	(h, w) = mask.shape
	T = np.full((h, w), inf, dtype=float)
	F = mask
	narrowband = []
	(inside_y, inside_x) = mask.nonzero()

	for ind in range(len(inside_x)):
		i = inside_x[ind]
		j = inside_y[ind]
		nbrs = getnbrs(x, y)
		for k, l in nbrs:
			if k < 0 or k >= w or l < 0 l >= h:
				continue
			if F[k][l] == BAND:
				continue
			if mask[k][l] == 0:
				F[k][l] = BAND
				T[k][l] = 0.0
				heapq.heappush(narrowband, (0.0, k, l))

	return (F, T, narrowband)


def outside(k,l,h,w):
	if(k < 0 or k >= w or l < 0 or l >= h):
		return False
	return True

def gradient(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Tij = T[i][j]

	prev_j = j-1
	next_j = j+1
	if prev_j < 0 or next_j >= h:
		grad_j=inf
	else:
		flag_prev_j = F[i][prev_j]
		flag_next_j = F[i][next_j]

		if (flag_next_j != INSIDE and flag_prev_j != INSIDE):
			grad_j = (T[i][next_j] - T[i][prev_j]) / 2.0
		elif(flag_prev_j != INSIDE):
			grad_j = Tij - T[i][prev_j]
		elif(flag_next_j != INSIDE):
			grad_j = T[i][next_j] - Tij
		else:
			grad_j = 0.0


	prev_i = i-1
	next_i = i+1
	if prev_i < 0 or next_i >= w:
		grad_i=inf
	else:
		flag_prev_i = F[prev_i][j]
		flag_next_i = F[next_i][j]

		if (flag_next_i != INSIDE and flag_prev_i != INSIDE):
			grad_j = (T[next_i][j] - T[prev_i][j]) / 2.0
		elif(flag_prev_i != INSIDE):
			grad_j = Tij - T[prev_i][j]
		elif(flag_next_j != INSIDE):
			grad_j = T[next_i][j] - Tij
		else:
			grad_j = 0.0

	return (grad_i, grad_j)



def solve(img, mask, F, T, narrowband, int i1, int j1, int j2):
	sol = 1.0 * inf
	(h, w, _) = img.shape
	if(outside(i1,j1,h,w)==False || outside(i2,j2,h,w)==False):
		return sol
	if(f[i1][j1] == KNOWN):
		if(f[i2][j2] == KNOWN):
			r = math.sqrt(2.0*(T[i1][j1]-T[i2][j2])*(T[i1][j1]-T[i2][j2]))
			s = ((T[i1][j1] + T[i2][j2])*r)/2.0
			if(s >= T[i1][j1] && s >= T[i2][j2]):
				sol = s
			else:
				s += r 
				if(s >= T[i1][j1] && s >= T[i2][j2]):
					sol = s
		else:
			sol = 1 + T[i1][j1]
	elif(f[i2][j2] == KNOWN):
		sol = 1+T[i2][j2]

	return sol


def impaint(img, mask, F, T, narrowband, i, j):
	(h, w, _) = img.shape
	Beps = []
	gradij = gradient(img, mask, F, T, narrowband, i, j)
	for ii in range(-epsilon, epsilon+1):
		for jj in range(-epsilon, epsilon+1):
			Beps.append((i+ii,j+jj))

	Ia = (0.0,0.0,0.0)
	s = 0.0
	for k, l in Beps:
		if(outside(k,l,h,w)): #outside
			continue
		r = ((i-k),(j-l))
		rlength = math.sqrt(r[0]**2 + r[1]**2)

		drc = ((r[0]*gradij[0] + r[1]*gradij[1])/rlength)
		dst = 1.0/(rlength*rlength)
		lev = 1.0/(1.0+abs(T[i][j]-T[k][l]))
		weight = drc*dst*lev

		gradI = [(0.0,0.0,0.0), (0.0,0.0,0.0)]
		if(outside(k+1,l) != False and outside(k-1,l)!=False and outside(k,l+1)!=False and outside(k,l-1)!=False):
			gradI = ((img[k+1][l][0]-img[k-1][l][0],img[k+1][l][1]-img[k-1][l][1],img[k+1][l][2]-img[k-1][l][2]),(img[k][l+1][0]-img[k][l-1][0],img[k][l+1][1]-img[k][l-1][1],img[k][l+1][2]-img[k][l-1][2]))
		Ia += weight * (img[k][l] + ((gradI[0][0]*r[0] + gradI[1][0] * r[1]), (gradI[0][1]*r[0] + gradI[1][1] * r[1]), (gradI[0][2]*r[0] + gradI[1][2] * r[1])))
		s += w
	img[i][j] = Ia/s













def ffm(img, mask, F, T, narrowband):
	(h,w,_) = img.shape
	while narrowband:
		(_, i, j) = heapq.heappop(narrowband)
		F[i][j] = KNOWN
		nbrs = getnbrs(i, j)
		for k , l in nbrs:
			if k < 0 or k >= w or l < 0 or l >= h:
				continue
			if F[k][l] != KNOWN:

				if F[k][l] == INSIDE:
					F[k][l] = BAND
					impaint(img, mask, F, T, narrowband, k, l)

				T[k][l] = min(solve(img, mask, F, T, narrowband, k-1,l,k,l-1), solve(img, mask, F, T, narrowband, k+1,l,k,l-1), solve(img, mask, F, T, narrowband, k-1,l,k,l+1), solve(img, mask, F, T, narrowband, k+1,l,k,l+1))

				heapq.heappush(narrowband, (T[k][l], k, l))











def do_image_impainting(img, mask_img):
	mask = np.array(mask_img[:,:,0:0])
	#filling Flag, T and narrowband priority queue according to T
	(F, T, narrowband) = initialize(mask)

	#fast forward march
	ffm(img, mask, F, T, narrowband)

	return img




image_name = './images/lena_in.png'
image_mask_name = './images/lena_mask.png'

image = 

img = do_image_impainting(img, mask_img)

cv2.imwrite()







