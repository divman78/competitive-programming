#include<bits/stdc++.h>
using namespace std;
#define N 100001

int segtree1[N];
int segtree2[N];
int segtree3[N];
int lazytree1[N];
int lazytree2[N];
int lazytree3[N];

int calc(int id){
	return id;
}

void update(int* segtree,int* lazytree,int id, int l, int r, int ll, int rr){
	if(lazytree[id]){
		segtree[id] = calc(id);
		if(l != r){
			lazytree[id*2+1]=1;
			lazytree[id*2+2]=1;
		}
		lazytree[id] = 0;
	}
	if(l >= ll && r <= rr){
		segtree[id] = calc(id);
		if(l != r){
			lazytree[id*2+1]=1;
			lazytree[id*2+2]=1;
		}
		return;
	}
	if(l > r || r < ll || l > rr){
		return;
	}
	int mid = (l+r)/2;
	update(segtree, lazytree, id*2+1, l, mid, ll, rr);
	update(segtree, lazytree, id*2+2, mid+1, r, ll, rr);
	segtree[id] = segtree[id*2+1]+segtree[id*2+2];
}

int getSegment(int* segtree, int* lazytree, int id, int l, int r, int ll, int rr){
	if(lazytree[id]){
		segtree[id] = calc(id);
		if(l != r){
			lazytree[id*2+1]=1;
			lazytree[id*2+2]=1;
		}
		lazytree[id] = 0;
	}

	if(l >= ll && r <= rr){
		return segtree[id];
	}

	if(l > r || r < ll || l > rr){
		return;
	}

	int mid = (l+r)/2;
	return getSegment(segtree, lazytree, id*2+1, l, mid, ll, rr) +
	       getSegment(segtree, lazytree, id*2+2, mid+1, r, ll, rr);
}


int main()
{
	ios_base::sync_with_stdio(false); cin.tie(NULL);
	freopen("input.txt", r, stdin);
	int nx,ny,nz, q;
	cin >> nx >> ny >> nz >> q;
	while(q--){
		int n; 
		cin >> n;
		int x, y;
		if(n == 0){
			cin >> x >> y;
			update(segtree1, lazytree1, 0, 0, n-1, x, y);
		}
		else if(n == 1){
			cin >> x >> y;
			update(segtree2, lazytree2, 0, 0, n-1, x, y);
		}
		else if(n == 2){
			cin >> x >> y;
			update(segtree3, lazytree3, 0, 0, n-1, x, y);
		}
		else{
			int x1, y1, z1, x2, y2, z2;
			cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
			int xx = update(segtree1, lazytree1, 0, 0, n-1, x1, x2);
			int yy = update(segtree1, lazytree1, 0, 0, n-1, y1, y2);
			int zz = update(segtree1, lazytree1, 0, 0, n-1, z1, z2);
			int ans = xx+yy+zz-min(xx, yy)- min(xx, zz) - min(yy, zz) + min(xx, min(xx, yy));
			cout << ans << endl;
		}
	}
	return 0;
}



