#include<bits/stdc++.h>
using namespace std;

long long n;

bool f(long long x){
	long long val = 0;
	long long nx = n;
	while(nx){
		if(nx - x >= 0){
			nx -= x;
			val += x;
		}
		else {
			val += nx;
			nx = 0;
		}
		nx -= nx/10;
		
	}
	return 2 * val >= n;
}

int main(){
	cin >> n;
	long long l = 1, r = n;
	while(l<r){
		long long mid = l + (r-l)/2;
		if(f(mid)){r = mid;    }
		else      {l = mid + 1;}
	}
	cout<< l;
}

//sudo apt-get install -y mongodb-org=3.4 mongodb-org-server=3.4 mongodb-org-shell=3.4 mongodb-org-mongos=3.4 mongodb-org-tools=3.4