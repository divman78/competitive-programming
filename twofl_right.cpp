#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int a[2001][2001];
bool b[2001][2001][5];
int isValid(int row, int col,int n,int m){
 
    return ((row >= 0) && (row < n) &&
           (col >= 0) && (col < m));
}
vector<pair<int,int> > v;
void DFS(int row, int col,int n,int m,int &count,int x,int y){
 
    static int rowNbr[] = {0, 1, 0, -1};
    static int colNbr[] = {1, 0, -1, 0};
 
    b[row][col][4]=true;
    //cout<<"("<<row<<","<<col<<")"<<a[row][col]<<" ";
    v.push_back(make_pair(row,col));
 
    for (int k = 0; k < 4; ++k)
    {
        int r = row + rowNbr[k];
        int c = col + colNbr[k];
        if (isValid(r,c,n,m))
        {
            if(a[r][c] == x || a[r][c] == y){
                if(b[r][c][4]==false){
                    count++;
                    v.push_back(make_pair(r,c));
                    b[row][col][k]=true;
                    b[r][c][(k+2)%4]=true;
                    DFS(r,c,n,m,count,x,y);
                }
                else{
                    b[row][col][k]=true;
                    b[r][c][(k+2)%4]=true;
                }
            }
        }
    }
}
 
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
	int n,m;
	cin>>n>>m;
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
	        cin>>a[i][j];
	    }
	}
	int result=0;
	for(int i=0;i<n;i++){
	    for(int j=0;j<m;j++){
	        for(int k=0;k<2;k++){
	            if(b[i][j][k]==false){
	                if(k==0 && j<m-1 || k==1&& i<n-1){
    	                int count=1;
    	                int s = k==0?a[i][j+1]:a[i+1][j];
    	                DFS(i,j,n,m,count,a[i][j],s);
    	                //cout<<endl;
                        for(int z=0;z<v.size();z++)
                            b[v[z].first][v[z].second][4]=false;
                        v.clear();
    	                result = max(result,count);
	                }
	            }
	        }
	    }
	}
	cout<<result<<endl;
	return 0;
}