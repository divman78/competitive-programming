#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) //cout << arr[i] << " "; //cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) //cout << "#debug " << x << endl;
#define print2D(arr, n) //cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){//cout << arr[i][j] << " ";}//cout << endl;}//cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) //cout << itr->first << "->" << itr->second << " "; //cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO //cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000


int n, k;



signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();


	int t=1;
	////cout << " hello ";
	cin >> t; 
	while(t--){

		cin >> n >> k;

		int arr[n];

		int mx = INT_MIN;
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
			mx = max(arr[i], mx);
		}

		vector<int> vec[mx+1];

		for (int i = 0; i < n; ++i)
		{
			vec[arr[i]].push_back(i+1);
		}

		int ans = 100000000000000000;

		for (int i = 1; i <= mx; ++i)
		{
			if(vec[i].size() >=  k){
				int sumr = 0, suml = 0;
				for (int j = 0; j < k; ++j)
				{
					sumr += vec[i][j];
				}
				int l = 0, r = k-1;
				//cout << i << " -> \n";

				int pre[vec[i].size()+1];

				pre[0] = vec[i][0];

				for (int j = 1; j < vec[i].size(); ++j)
				{
					//cout << vec[i][j] << "->";
					pre[j] = pre[j-1] + vec[i][j];

					//cout << pre[j] << " ";
				}
				//cout << endl;


				for (int j = 0; j < vec[i].size(); ++j)
				{
					

					while(r+1 <= vec[i].size()-1){
						if(abs(vec[i][j] - vec[i][r+1]) < (vec[i][j] -vec[i][l])){
							l++;
							r++;
							//cout << "hello \n";
						}
						else{
							break;
						}
					}

					int cntl = j-l;
					int cntr = r-j;

					if(l-1>=0)suml = pre[j]-pre[l-1];
					else suml = pre[j];
					if(j>=0)sumr = pre[r]-pre[j];
					else sumr = pre[r];

					suml -= vec[i][j];





					//cout  << "j : "<< j << " " << "l : "<< l << " " << "r : "<< r << " " << "sumr : "<< sumr << " " << "suml : "<< suml << " "  <<"cntl : "<< cntl << " " << "cntr : "<< cntr << " ";   
					int p = (abs(sumr-cntr*vec[i][j]) - (((cntr)*(cntr+1))/2)) + (abs(suml-cntl*vec[i][j]) - (((cntl)*(cntl+1))/2)) ;
					//cout << p << endl;
					ans = min(p, ans);


				}
			}
		}

		//cout << "ans = ";
		if(ans ==100000000000000000){
			cout << -1 << endl;
		}


		else cout << ans << endl;


		











        
		////cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	//cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




