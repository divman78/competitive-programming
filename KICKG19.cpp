#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; int tt=0; cin >> t;   while(tt++<t)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)


void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}
void init(){
	FAST_IO;
	read_file();
}

#define N 13
int arr1[N];
int arr2[N];

int ans;
int n, h;
map<int,int> mm1;
map<int,int> mm2;
void calc(int ind, int sum1, int sum2){
	if(ind == -1) {
		mm1[sum1]++;
		mm2[sum2]++;
		return;
	}

	calc(ind-1,sum1, arr2[ind]+sum2);
	calc(ind-1,arr1[ind]+sum1, sum2);
	calc(ind-1,arr1[ind]+sum1, arr2[ind]+sum2);
}

signed main(){
	init();

	TEST_CASES
	{
		ans = 0;
		cin >> n;
		cin >> h;
		for (int i = 0; i < n; ++i)
		{
			cin >> arr1[i];
		}
		for (int i = 0; i < n; ++i)
		{
			cin >> arr2[i];
		}
		calc(n-1,0,0);

		cout << "Case #" << tt <<  ": " << ans << endl;
		
	}


	




	return 0;
}