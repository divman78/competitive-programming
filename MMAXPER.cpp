#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000


int n;
int dp[N][2];
int arr[N][2];

int solve(int i, int x){
	if(dp[i][x]!=0){
		return dp[i][x];
	}
	if(i == 0){
		dp[i][x] = arr[i][x];
		return dp[i][x];
	}

	dp[i][x] = max(solve(i-1,0)+abs(arr[i-1][1]-arr[i][1-x])+arr[i][x],
		           solve(i-1,1)+abs(arr[i-1][0]-arr[i][1-x])+arr[i][x]);
	return dp[i][x];
}

signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	//freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

    cin >> n;
    for (int i = 0; i < n; ++i)
	{
		cin >> arr[i][0] >> arr[i][1];
	}

	cout << max(solve(n-1,0), solve(n-1,1)) << endl;








	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	//cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















