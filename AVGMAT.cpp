#include<bits/stdc++.h>
using namespace std;
#define N 610

int t;
int n, m;
int i, j, k;
int inp[N][N];
int dia1[N][N], dia2[N][N];
int d, ans[N*2];
int upright_row, upleft_row, downright_row, downleft_row;
int upright_col, upleft_col, downright_col, downleft_col;

void fun(int p, int q, int s){
	upright_row = p-1;
	upleft_row = p;
	downright_row = min(n, p+s-1);
	downleft_row = min(n, p+s);

	upright_col = min(m+1,q+s+1);
	upleft_col = max(0, q-s);
	downright_col = min(m+1,q+1+abs(downright_row-p-s+1));
	downleft_col = max(0,q-abs(downleft_row-p-s));
}

main(){
	scanf("%d",&t);
	while(t--){
		memset(dia1, 0, sizeof(dia1));
		memset(dia2, 0, sizeof(dia2));
		scanf("%d%d",&n, &m);
		for (i = 1; i <= n; ++i)
		{
			char str[m];
			scanf("%s", str);
			for(j = 1; j <= m; j++){
				inp[i][j] = str[j-1] - 48;
			}
		}
		for(i = 1; i <= n; i++){
			for(j = 1; j <= m; j++){
				dia1[i][j] = inp[i][j] + dia1[i-1][j+1];
				dia2[i][j] = inp[i][j] + dia2[i-1][j-1];
			}
		}
		d = n+m-2;
		for(k = 1; k <= d; k++){
			ans[k] = 0;
			for(i = 1; i <= n; i++){
				for(j = 1; j <= m; j++){
					if(inp[i][j]){
						fun(i,j,k);
						ans[k] += (dia2[downleft_row][downleft_col]-dia2[upleft_row][upleft_col] 
					        	+ dia1[downright_row][downright_col]-dia1[upright_row][upright_col]);
					} 
				}
			}
		}

		for(i = 1; i <= d; i++){
			cout << ans[i] << " ";
		}
		cout << "\n";
	}
}