#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}


int curr;
bool p(int x){

	int l = min(abs(curr-x),abs(curr+x));
	int r = max(abs(curr-x),abs(curr+x)); 
	//cout << curr << " " << l << " " << r << " " << " " << " " <<  x << " " << endl;
	return (l <= min(abs(curr), abs(x)) && r >= max(abs(curr), abs(x)));
}


int binary_search(int lo,int hi){
  while (lo < hi){
   int mid = lo + (hi-lo)/2;
      if (p(mid) == true)
         hi = mid;
      else
         lo = mid+1;
  }
   return lo ;        
}

vector<int> arr;


int countnums(int lx, int rx){ // numbers in [lx, rx]
	return upper_bound(arr.begin(), arr.end(), rx)-lower_bound(arr.begin(),arr.end(), lx);
}




signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	//cin >> t; 
	while(t--){


		int n;
		cin >> n;

		for (int i = 0; i < n; ++i)
		{
			int x;
			cin >> x;
			arr.push_back(x);
		}

		sort(arr.begin(), arr.end());


		int ans = 0;

	//	printarr(arr, n);



		for (int i = 0; i < n; ++i)
		{
			curr = arr[i];
			int num = binary_search(0, 2*abs(curr));
			cout  << i << " -> " << num << " ";

			int cnt = countnums(abs(num),2*abs(curr));
			//cout << countnums(abs(num),2*abs(curr)) << " " << countnums(-2*abs(curr), -abs(num)) << endl; 
			cnt += countnums(-2*abs(curr), -abs(num));
			if((arr[i] <= 2*abs(curr) && arr[i] >= abs(num)) || (arr[i] <= -abs(num) && arr[i] >= -2*abs(curr))) {
				cnt--;
			}
			ans+=cnt;
		}


		cout << ans/2 << endl;



        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















