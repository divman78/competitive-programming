//segment tree
#include<bits/stdc++.h>
using namespace std;
#define int long long
#define N 100100
#define left 2*id+1
#define right 2*id+2
#define mid ((l+r) >> 1)
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;


int n;
int arr[N];
int seg[4*N][6];
void update(int id, int p, int val, int l, int r, int x, int y){
	if(l!=r && seg[id][p]){
		seg[left][p] += seg[id][p];
		seg[right][p] += seg[id][p];
		seg[id][p] = 0;
	}
	if(l > r || l > y || r < x){
		return;
	}
	if(l >= x && r <= y){
		seg[id][p] += val;
		return;
	}
	update(left, p, val, l, mid, x, y);
	update(right, p, val, mid+1, r, x, y);
}



void update2(int id, int p, int l, int r, int idx){
	if(l!=r && seg[id][p]){
		seg[left][p] += seg[id][p];
		seg[right][p] += seg[id][p];
		seg[id][p] = 0;
	}
	if(l > r || l > idx || r < idx){
		return;
	}
	if(l == r){
		seg[id][p] = 0;
		return;
	}
	update2(left, p, l, mid, idx);
	update2(right, p, mid+1, r, idx);
}



int val;
void query(int id, int p, int l, int r, int x, int y){
	if(l!=r && seg[id][p]){
		seg[left][p] += seg[id][p];
		seg[right][p] += seg[id][p];
		seg[id][p] = 0;
	}
	if(l > r || l > y || r < x){
		return;
	}
	if(l == r){
		val = seg[id][p];
		return;
	}
	query(left, p, l, mid, x, y);
	query(right, p, mid+1, r, x, y);
}


signed main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int t = 1;
	//cin >> t;
	while(t--){
		cin >> n;
		for (int i = 0; i < n; ++i)
		{
			cin >> arr[i];
		}

		int m;
		cin>>m;
		while(m--){
			int type ;
			cin >> type;
			if(type == 1){
				int l, r, p;
				cin >> l >> r >> p;
				l--; r--;
				update(0, p, 1, 0, n-1, l, r);
			}
			else{
				int l, d;
				cin >> l >> d;
				l--;
				update2(0, 2, 0, n-1, l);
				update2(0, 3, 0, n-1, l);
				update2(0, 5, 0, n-1, l);
				arr[l] = d;
			}
		}

		for (int i = 0; i < n; ++i)
		{
			for (int j = 2; j < 6; ++j)
			{
				query(0, j, 0, n-1, i, i);
				while(val>0 && (arr[i] % j == 0)){
					arr[i]/=j;
					val--;
				}
			}
			cout << arr[i] << " ";
		}





	}
	return 0;
}