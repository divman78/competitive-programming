/**
 *    author:  tourist
 *    created: 22.07.2018 19:09:57       
**/
#include <bits/stdc++.h>

using namespace std;

string to_string(string s) {
  return '"' + s + '"';
}

string to_string(const char* s) {
  return to_string((string) s);
}

string to_string(bool b) {
  return (b ? "true" : "false");
}

template <typename A, typename B>
string to_string(pair<A, B> p) {
  return "(" + to_string(p.first) + ", " + to_string(p.second) + ")";
}

template <typename A>
string to_string(A v) {
  bool first = true;
  string res = "{";
  for (const auto &x : v) {
    if (!first) {
      res += ", ";
    }
    first = false;
    res += to_string(x);
  }
  res += "}";
  return res;
}

void debug_out() { cerr << endl; }

template <typename Head, typename... Tail>
void debug_out(Head H, Tail... T) {
  cerr << " " << to_string(H);
  debug_out(T...);
}

#ifdef LOCAL
#define debug(...) cerr << "[" << #__VA_ARGS__ << "]:", debug_out(__VA_ARGS__)
#else
#define debug(...) 42
#endif

template <typename T>
class fenwick {
  public:
  vector<T> fenw;
  int n;

  fenwick(int _n) : n(_n) {
    fenw.resize(n);
  }

  void modify(int x, T v) {
    while (x < n) {
      fenw[x] += v;
      x |= (x + 1);
    }
  }

  T get(int x) {
    T v{};
    while (x >= 0) {
      v += fenw[x];
      x = (x & (x + 1)) - 1;
    }
    return v;
  }
};

const int inf = (int) 1.01e9;

struct node {
  int a = inf;

  inline void operator += (node &other) {
    a = min(a, other.a);
  }
};

int main() {
  ios::sync_with_stdio(false);
  freopen("input.txt","r",stdin);
  freopen("output.txt","w",stdout);
  cin.tie(0);
  int tt;
  cin >> tt;
  while (tt--) {
    int n, k;
    cin >> n >> k;
    map<int,int> mp;
    for (int i = 0; i < n; i++) {
      int foo;
      cin >> foo;
      mp[foo]++;
    }
    vector<int> a, b;
    for (auto &p : mp) {
      a.push_back(p.first);
      b.push_back(p.second);
    }
    debug(a);
    debug(b);
    int m = (int) b.size();
    long long ans = (long long) a[m - 1] * a[m - 1];
    fenwick<node> fenw(n + 1);
    int can_remove = n - k;
    int cc = 0;
    for (int i = m - 1; i >= 0; i--) {
      cc += b[i] - 1;
      fenw.modify(n - min(2, b[i] - 1), {a[i]});
      debug("modify", n - min(2, b[i] - 1), a[i]);
      debug(i, cc, can_remove);
      if (cc <= can_remove) {
        if (i == 0) {
          ans = -1;
        } else {
          if (b[i - 1] >= 4) {
            long long cur = (long long) a[i - 1] * a[i - 1];
            ans = min(ans, cur);
          }
        }
      } else {
        int need_back = cc - can_remove;
        int u = fenw.get(n - need_back).a;
        debug(n - need_back, u);
        if (u != inf) {
          long long cur = (i < 1 ? -1 : (long long) a[i - 1] * u);
          ans = min(ans, cur);
        }
      }
    }
    cout << ans << '\n';
  }
  return 0;
}