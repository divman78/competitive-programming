#include <iostream>
#include <stdio.h>
#include<bits/stdc++.h>
using namespace std;
inline int inp()
{
    int noRead=0;
    char p=getchar_unlocked();
    for(;p<33;)
    {
     p=getchar_unlocked();
    };
    while(p>32)
    {
     noRead = (noRead << 3) + (noRead << 1) + (p - '0');
     p=getchar_unlocked();
    }
    return noRead;
};
int main() {
	freopen("input.txt","r",stdin);
	int t;
	scanf("%d",&t);//cin >>t;
	while(t--){
	    int n,i,j,max=0,sum=0,min;
	    n=inp();//cin>>n;
	    int h[501],k[501], mincut[1001]={0};
	    for(i=0;i<n;i++){
	        h[i]=inp();//cin >>h[i];
	        max=max<2*h[i]?2*h[i]:max;
	    }
	    for(i=0;i<n;i++){
	        k[i]=inp();//cin >>k[i];
	        mincut[k[i]]=1;
	    }
	    
	    for(i=1;i<=max;i++){
	        if(!mincut[i]){
	            min=100000000;
	            for(j=1;j<=i/2;j++){
	                if(mincut[j]+mincut[i-j]<min)
	                    min=mincut[j]+mincut[i-j];
	            
	            }
	            mincut[i]=min;
	        }
	    }
	    for(i=0;i<n;i++){
	        sum+=mincut[2*h[i]];
	    }
	    printf("%d\n",sum);//cout <<sum<<endl;
	}
	return 0;
}