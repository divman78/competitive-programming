#include<bits/stdc++.h>
using namespace std;
//#define N 10101
#define N 101010
#define mset(ds) memset(ds, 0, sizeof(ds))
#define FASTIO ios_base::sync_with_stdio(false); cin.tie(0)
#define MX 1000000000


struct edge{
	int s, e, v;
};

int n, m;
edge ed[N];
int visited[N];
int turn[N];
int tim[N];
vector<int> adj[N];
int currtim;
int cyc;
int cnt;


void dfs(int u){
	if(visited[u]) return;
	visited[u] = 1;
	for(auto v: adj[u]){
		if(visited[v] == 1){
			cyc = 1;
			return;
		}
		else if(!visited[v]) dfs(v);
	}
	visited[u] = 2;
	tim[u] = currtim++;
	// cout << u << endl;
}



bool predicate(int k){
	mset(visited);
	mset(turn);
	mset(tim);

	cyc = currtim = cnt = 0;
	for(int i = 0; i < n; i++) adj[i].clear();
	for(int i = 0; i < m; i++) if(ed[i].v > k) adj[ed[i].s].push_back(ed[i].e);

	for(int i = 0; i < n; i++){
		if(!visited[i]) dfs(i);
		if(cyc) return false;
	}

	for (int i = 0; i < m; ++i)
	{
		if(ed[i].v <= k){
			if(tim[ed[i].s] < tim[ed[i].e]){
				turn[i] = 1, cnt++;
				//cout << "ed " << i << endl;
			}
		}
	}

	return true;
}



int binary_search(int lo,int hi){
	while (lo < hi){
	    int mid = lo + (hi-lo)/2;
	    //cout << "mid " << mid  << endl;
	    if(predicate(mid) == true)
	        hi = mid;
	    else
	        lo = mid+1;
	}
	return lo ;   
}

signed main(){
	FASTIO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	cin >> n >> m;
	for (int i = 0; i < m; ++i)
	{
		cin >> ed[i].s >> ed[i].e >> ed[i].v;
		ed[i].s--; ed[i].e--;
	}

    int ans = binary_search(0,MX);
    predicate(ans);
    cout << ans << " " << cnt << endl;

    for (int i = 0; i < m; ++i)
    {
    	if(turn[i]){
    		cout << i+1 << " ";
    	}
    }
}