#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long

void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

signed main(){
	read_file();

	int t;
	cin >> t;
	while(t--){
		int n, k;
		cin >> n >> k;

		int arr[k];
		for (int i = 0; i < k; ++i)
		{
			cin >> arr[i];
		}

		int dp[2*n+1][2*n+1][2];
		for (int i = 0; i <= 2*n; ++i)
		{
			for (int j = 0; j <= 2*n; ++j)
			{
				for (int k = 0; k < 2; ++k)
				{
					dp[i][j][k]=0;
				}
			}
		}

		int ind = 0;
		if(arr[ind]==1){
			ind++;
		}
		dp[1][1][0]=1;
		for (int i = 2; i <= 2*n; ++i)
		{
			for(int j = 1; j <= i; j++){
				dp[i][j][0] = dp[i-1][j-1][0]+dp[i-1][j-1][1];

				//cout << i << " " << j << " " << 0 << " " << dp[i][j][0] << endl;
			}
			if(ind < k && arr[ind]==i){
				ind++;
			}
			else{
				for(int j = 0; j <= i-1; j++){
					dp[i][j][1] = dp[i-1][j+1][0]+dp[i-1][j+1][1];

					//cout << i << " " << j << " " << 1 << " " << dp[i][j][1] << endl;
				}
			}
		}

		cout << dp[2*n][0][1] << endl;
	}


	




	return 0;
}