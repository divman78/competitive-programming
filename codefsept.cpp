#include<bits/stdc++.h>
using namespace std;

// geeksforgeeks

class Point 
{ 
public: 
    int x, y; 
    Point(int a=0, int b=0):x(a),y(b) {} 
}; 
  
// Utility function to find GCD of two numbers 
// GCD of a and b 
int gcd(int a, int b) 
{ 
    if (b == 0) 
       return a; 
    return gcd(b, a%b); 
} 
  
// Finds the no. of Integral points between 
// two given points. 
int getCount(Point p, Point q) 
{ 
    // If line joining p and q is parallel to 
    // x axis, then count is difference of y 
    // values 
    if (p.x==q.x) 
        return abs(p.y - q.y) - 1; 
  
    // If line joining p and q is parallel to 
    // y axis, then count is difference of x 
    // values 
    if (p.y == q.y) 
        return abs(p.x-q.x) - 1; 
  
    return gcd(abs(p.x-q.x), abs(p.y-q.y))-1; 
} 

//geeksforgeeks


int main(){
	int t;
	cin >> t;
	while(t--){
		int n, m, a, b;
		cin >> n >> m >> a >> b;
		n--, m--;
		cout << endl;
		bool flag1 = false, flag2 = false;
		if(n == 0){
			cout << 1 << endl;
			flag1 = true;
		}
		else{
			cout <<"no " << getCount(Point(a/n, 0), Point(0,b/n)) << endl;
			if(getCount(Point(a/n, 0), Point(0,b/n))){
				cout << 2 << endl;
				flag1 = true;
			}
			else if(n-1 > 0){
				if(getCount(Point(a/(n-1), 0), Point(0,b/(n-1))))
				cout << 3 << endl,
				flag1 = true;
			}
		}

		if(n == 0){
			flag2 = true;
			cout << 4 << endl;
		}
		else{
			if(getCount(Point(a/m, 0), Point(0,b/m))){
				cout << 5 << endl;
				flag2 = true;
			}
			else if(m-1 > 0){
				if(getCount(Point(a/(m-1), 0), Point(0,b/(m-1))))
				cout << 6 << endl,
				flag2 = true;
			}
		}

		if(flag1 && flag2){
			 cout << "Chefirnemo\n";
		}
		else{
			cout << "Pofik\n";
		}


	}
}