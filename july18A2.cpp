//thanks mit
#include<bits/stdc++.h>
using namespace std;
#define N 5050
#define mod 1000000007

long long n, k;
long long arr[N];
long long ans;

// long long inv(long long a, long long b){  //a*b < LONGLONG_MAX
//  return 1<a ? b - inv(b%a,a)*b/a : 1;
// }

long long triangle[N + 1][N + 1];

void makeTriangle() {
    int i, j;

    // initialize the first row
    triangle[0][0] = 1; // C(0, 0) = 1

    for(i = 1; i < N; i++) {
        triangle[i][0] = 1; // C(i, 0) = 1
        for(j = 1; j <= i; j++) {
            triangle[i][j] = (triangle[i - 1][j - 1] + triangle[i - 1][j])%(mod - 1);
        }
    }
}

long long C(int n, int r) {
    return triangle[n][r];
}

long long gcdExtended(long long a, long long b, long long *x, long long *y);
 
// Function to find modulo inverse of b. It returns
// -1 when inverse doesn't
long long inv(long long b, long long m)
{
    long long x, y; // used in extended GCD algorithm
    long long g = gcdExtended(b, m, &x, &y);
 
    // Return -1 if b and m are not co-prime
    if (g != 1)
        return -1;
 
    // m is added to handle negative x
    return (x%m + m) % m;
}
 
// Function to compute a/b under modlo m
long long divi(long long a, long long b)
{
    a = a % mod;
    long long inverse = inv(b, mod);
    if (inverse == -1)
       return -1;
    else
       return (inverse * a) % mod;
}

long long divi1(long long a, long long b, long long m)
{
    a = a % m;
    long long inverse = inv(b, m);
    if (inverse == -1)
       return -1;
    else
       return (inverse * a) % m;
}
 
// C function for extended Euclidean Algorithm (used to
// find modular inverse.
long long gcdExtended(long long a, long long b, long long *x, long long *y)
{
    // Base Case
    if (a == 0)
    {
        *x = 0, *y = 1;
        return b;
    }
 
    long long x1, y1; // To store results of recursive call
    long long gcd = gcdExtended(b%a, a, &x1, &y1);
 
    // Update x and y using results of recursive
    // call
    *x = y1 - (b/a) * x1;
    *y = x1;
 
    return gcd;
}

long long pow(long long a, long long b) {
long long x = 1, y = a;
	while(b > 0) {
		if(b%2 == 1) {
			x=(x*y);
			if(x>mod) x%=mod;
		}
		y = (y*y);
		if(y>mod) y%=mod;
		b /= 2;
	}
	return x;
}

// long long inv(long long a, long long m) {
//     return pow(a,m-2);
// }



long long add(long long a, long long b){
	return ((a%mod) + (b%mod))%mod;
	//return a + b;
}

long long sub(long long a, long long b){
	return ((a%mod) - (b%mod) + mod) % mod;
	//return a - b;
}

long long add1(long long a, long long b, long long m){
	return ((a%m) + (b%m))%m;
	//return a + b;
}

long long sub1(long long a, long long b, long long m){
	return ((a%m) - (b%m) + m) % m;
	//return a - b;
}

long long mul(long long a, long long b){
	return ((a%mod) * (b%mod))%mod;
	//return a * b;
}

long long mul1(long long a, long long b, long long m){
	return ((a%m) * (b%m))%m;
	//return a * b;
}
// long long divi(long long a, long long b){
// 	return ((a%mod) * (inv(b, mod)))%mod;
// 	//return a / b;
// }

// long long pow(long long a, long long b){
// 	long long res = 1;
// 	while(b > 0){
// 		if(b & 1){
// 			res = mul(res, a);
// 		}
// 		b = b >> 1;
// 		a = mul(a, a);
// 	}
// 	return res;
// }


void calc2(){	
	long long f[n + 1];
	// f[0] = 1;
	// for(long long i = 1; i <= n; i++){
	// 	f[i] = mul1(i,f[i-1], mod - 1);
	// 	cout << f[i] << " ";
	// }
	//cout << endl;

	long long temp = C(n - 1, k - 1);//divi1(f[n-1], mul1(f[n-k], f[k-1], mod - 1 ), mod - 1);
	//cout << temp << endl;

	long long counting[n];
	for(long long i = 0; i < n; i++){
		if(i < k - 1){
			counting[i] = 0;
		}
		else{
			counting[i] = C(i, k - 1);//divi1(f[i], mul1(f[i - k + 1], f[k - 1], mod - 1), mod - 1);
		}
		//cout << counting[i] << " ";
	}
	//cout << endl;

	for(long long i = 0; i < n; i++){
		long long temp1 = sub1(temp, add1(counting[i], counting[n - i - 1], mod - 1), mod - 1);
		if(temp1 != 0) ans = mul(ans, pow(arr[i],temp1));
	}
}



int main(){
	//freopen("input.txt","r",stdin);
	makeTriangle();
	long long t;
	cin >> t;
	while(t--){
		cin >> n >> k;
		ans = 1;
		for(long long i = 0; i  < n; i++){

			cin >> arr[i];
		}
		sort(arr, arr + n);
		calc2();
		cout << ans << endl;
	}
}