#include<cstdio>
#include<algorithm>
#include<cmath>

using namespace std;

const int MOD = 1e9 + 7;

int t, N, S, k, pr[123][123], a[1023], sz = 0, p, tmp[123];
long long ans, prec[123], sum = 0;

void gen(int cn, int S, int last) {
    if (cn == k - 1) {
        tmp[k - 1] = S;
        /*printf("TMP:\n");
        for (int i = 0; i < k; i++) {
            printf("%d ", tmp[i]);
        }
        printf("\n");*/  
        long long f = 1, anss = 0;
        int tm = 1;
        for (int i = 1; i < k; i++) {
            f *= (i + 1);
            if (tmp[i] == tmp[i - 1]) {
                tm++;
                f /= tm;
            } else {
                tm = 1;
            }
        }
        f %= MOD;
        //printf("f = %lld\n", f);
        for (int i = 0; i < k; i++) {
            for (int j = i + 1; j < k; j++) {
          //      printf("pr = %d %d %d\n", pr[tmp[i]][tmp[j]], tmp[i], tmp[j]);
                anss += pr[tmp[i]][tmp[j]];
            }
            //printf("prec = %lld %d\n", prec[tmp[i]], tmp[i]);
            anss += prec[tmp[i]];
        }
//        printf("sum = %lld\n", sum);
        anss += sum;
        ans = (ans + anss * f) % MOD;
    } else {
        for (int i = last; i >= 1; i--) {
            if (S - i < k - cn - 1){
                continue;
            } else if (S > (k - cn) * i) {
                break;
            }
                        //printf("!) %d %d %d\n", cn + 1, S - i, i);
            tmp[cn] = i;
            gen(cn + 1, S - i, i);
        }
    }
}

int main() {
    //freopen("n.in", "r", stdin);
    //freopen("n.out", "w", stdout);
    scanf("%d", &t);
    for (int r = 0; r < t; r++) {
        scanf("%d%d", &N, &S);
        ans = 0;
        sum = 0;
        sz = 0;
        k = 0;
        for (int i = 0; i < N; i++) {
            scanf("%d", &p);
            if (p == -1) {
                k++;
            } else {
                S -= p;
                a[sz++] = p;
            }
        }
        //printf("%d %d\n", k, sz);
        for (int i = 0; i < sz; i++) {
            for (int j = i + 1; j < sz; j++) {
                sum += __gcd(a[i], a[j]);
            }
        }
        sum %= MOD;
        for (int i = 1; i <= 50; i++) {
            prec[i] = 0;
            for (int j = 1; j <= 50; j++) {
                pr[i][j] = __gcd(i, j);
            }
            for (int j = 0; j < sz; j++) {
                prec[i] += __gcd(i, a[j]);
            }
            prec[i] %= MOD;
        }
        gen(0, S, S);
        if (S == 0) {
            ans += sum;
        }
        printf("%lld\n", ans);
    }
    return 0;
}