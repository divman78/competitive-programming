#include <bits/stdc++.h>
using namespace std;
struct edge
{
	int x;
	int y;
};
main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		vector<int> graph1[n+1];
		vector<int> graph2[n+1];
		edge grph1[n-1];
		edge grph2[n-1];
		for(int i=0;i<n-1;i++)
		{
			int x,y;
			cin>>x>>y;
			graph1[x].push_back(y);
			graph1[y].push_back(x);
			edge p;
			p.x = x;
			p.y = y;
			grph1[i] = p;
		}
		for(int i=0;i<n-1;i++)
		{
			int x,y;
			cin>>x>>y;
			graph2[x].push_back(y);
			graph2[y].push_back(x);
			edge p;
			p.x = x;
			p.y = y;
			grph2[i] = p;
		}
		int ans[n-1];
		for(int i=0;i<n-1;i++)
		{
			int count = 0;
			edge p = grph1[i];
			// Remove P from graph 1
			for(int k=0;k<graph1[p.x].size();k++)
			{
				if(graph1[p.x][k] == p.y)
				{
					graph1[p.x].erase(graph1[p.x].begin()+k);
					break;
				}
			}
			for(int k=0;k<graph1[p.y].size();k++)
			{
				if(graph1[p.y][k] == p.x)
				{
					graph1[p.y].erase(graph1[p.y].begin()+k);
					break;
				}
			}

			//Add p in graph 2
			int flag = 0;
			for(int k=0;k<graph2[p.x].size();k++)
			{
				if(graph2[p.x][k] == p.y)
				{
					flag = 1;
					break;
				}
			}
			if(flag == 0)
			{
				graph2[p.x].push_back(p.y);
				graph2[p.y].push_back(p.x);
			}
			for(int j=0;j<n-1;j++)
			{
				edge q = grph2[j];
				
				// Add q in graph 1
				int flag = 0;
				for(int k=0;k<graph1[q.x].size();k++)
				{
					if(graph1[q.x][k] == q.y)
					{
						flag = 1;
						break;
					}
				}
				if(flag == 0)
				{
					graph1[q.x].push_back(q.y);
					graph1[q.y].push_back(q.x);
				}


				//Remove q from graph 2

				for(int k=0;k<graph2[q.x].size();k++)
				{
					if(graph2[q.x][k] == q.y)
					{
						graph2[q.x].erase(graph1[q.x].begin()+k);
						break;
					}
				}
				for(int k=0;k<graph2[q.y].size();k++)
				{
					if(graph2[q.y][k] == q.x)
					{
						graph2[q.y].erase(graph2[q.y].begin()+k);
						break;
					}
				}

				if(fun(graph1,n) && fun(graph2,n))
				{
					count++;
				}

				//Remove q from graph 1
				for(int k=0;k<graph1[q.x].size();k++)
				{
					if(graph1[q.x][k] == q.y)
					{
						graph1[q.x].erase(graph1[q.x].begin()+k);
						break;
					}
				}
				for(int k=0;k<graph1[q.y].size();k++)
				{
					if(graph1[q.y][k] == q.x)
					{
						graph1[q.y].erase(graph1[q.y].begin()+k);
						break;
					}
				}


				//Add q in graph 2
				graph2[q.x].push_back(q.y);
				graph2[q.y].push_back(q.x);


			}

			//Add p in Graph 1
			graph1[p.x].push_back(p.y);
			graph1[p.y].push_back(p.x);

			//Remove p in Graph 2


			ans[i] = count;
		}
		for(int i=0;i<n;i++)
		{
			cout<<ans[i]<<" ";
		}
		cout<<endl;
	}
}