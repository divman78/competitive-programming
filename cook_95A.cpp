
#include<bits/stdc++.h>

using namespace std;
 
#define PB push_back
#define MP make_pair
#define ALL(x) (x).begin(),(x).end()
#define SZ(x) (int((x.size())))
#define fi first
#define se second
 
typedef long double 	ld;
typedef long long 		ll;
typedef vector<int> 	VI;
typedef pair<int, int>	PII;
typedef pair<ll, ll> 	PLL;
typedef vector<ll>		VL;
typedef vector<PLL> 	VPL;
typedef vector<PII> 	VPI;
 
#define dbb(x)			cerr << #x << " = " << x << "\t";
#define db(x)        { dbb(x); cerr << endl; }
#define db2(x,y)     { dbb(x); dbb(y); cerr << endl; }
#define db3(x,y,z)   { dbb(x); dbb(y); dbb(z); cerr << endl; }
 
const ll  MOD = 1000000007ll;
 
ll powmod(ll a, ll b) { ll res = 1; a %= MOD; assert(b >= 0); for (; b; b >>= 1) { if (b & 1) res = res*a%MOD; a = a*a%MOD; }return res; }
template <typename t1, typename t2> inline bool upmax(t1 &a, t2 b) { if (a<(t1)b) { a = (t1)b; return true; } else return false; }
template <typename t1, typename t2> inline bool upmin(t1 &a, t2 b) { if (a>(t1)b) { a = (t1)b; return true; } else return false; }
template <typename T> inline T gcd(T a, T b) { return b ? gcd(b, a%b) : a; }
template <typename T> inline T lcm(T a, T b) { return a*(b / gcd(a, b)); }
template <typename T> inline T sqr(T a) { return a*a; }
 
int dx[] = { 1, 0, -1, 0, 1, 1, -1, -1 };
int dy[] = { 0, 1, 0, -1, 1, -1, 1, -1 };
 
const int INF = 1000000404;
const ll  LINF = 4000000000000000404ll;
const ld  PI = acos(-1.0);
const ld  EPS = 1e-9;
 
int SQ = 318;
int timer = 0;
 
#define N 400100
int a[N];
int b[N];
int ind1[N];
int ind2[N];
int p1[N];
 
struct fenwick{
	int n;
	ll f[N];
	fenwick(){};
	fenwick(int _n) :n(_n) {
		n = _n;
		for (int i = 0; i <= n; i++) {
			f[i] = 0;
		}
	}
	void add(int x, int val) {
		while (x <= n) {
			f[x] += val;
			x += (-x & x);
		}
	}
	ll get(int x) {
		ll res = 0;
		while (x > 0) {
			res += f[x];
			x -= (-x & x);
		}
		return res;
	}
};
 
int n;

int solve() {
	db("^_^");
	
	//cin >> n;
	fenwick sum(n);
	fenwick cnt(n);
 
	for (int i = 1; i <= n; i++){
		//cin >> a[i] >> b[i];
		ind1[i] = i;
		ind2[i] = i;
	}
	db("OK");
	sort(ind1 + 1, ind1 + n + 1, [](int x, int y){return (a[x] < a[y]); });
	sort(ind2 + 1, ind2 + n + 1, [](int x, int y){return (b[x] < b[y]); });
 
	for (int i = 1; i <= n; i++) {
		p1[ind2[i]] = i;
		sum.add(i, b[ind2[i]]);
		cnt.add(i, 1);
	}
 
	int ans = 0;
	for (int k = 1; k <= n; k++) {
		int i = ind1[k];
		int l = ans + 1;
		int r = n;
 
		while (l <= r) {
			int m = (l + r) >> 1;
			int len = (int)cnt.get(m);
			if (len == 0 || (sum.get(m) + len - 1) / len <= a[i]) {
				l = m + 1;
				upmax(ans, len);
			}
			else {
				r = m - 1;
				if (len <= ans) break;
			}
		}
 
		sum.add(p1[i], -b[i]);
		cnt.add(p1[i], -1);
	}
	//cout << ans << endl;
	return ans;
 
}
 
int main()
{
	//freopen("INPUT.in", "r", stdin);
	//freopen("OUTPUT.out", "w", stdout);
 
	ios_base::sync_with_stdio(false); cin.tie(0); cout.tie(0);
 
	int t = 1;
	cin >> t;
	db(t);
 
	while (t--) {
 
		solve();
 
	}
 
	getchar();
	getchar();
	return 0;
}

 
int main(){
	long long t;
	cin >> t;
	srand(time(NULL));
	while(t--){

		//cin >> n;
		n = (rand()%10) + 1;
		vector<pair<long long,long long> > arr(n);
		for(long long i = 0; i < n; i++){
			long long x , y;
			//cin >> x >> y;
			x = (rand()%10) + 1;

			arr[i].first = x;
			arr[i].second = y;
			a[i] = x;
			b[i] = y;
		}
 
        int real_ans = solve();
        priority_queue<long long> pq;
        long long sum = 0;
        long long mini = INT_MAX;
        long long ans = 0;
		sort(arr.begin(),arr.end());
		for(long long i = n-1; i >=0;i--){
			//cout <<"Sdfsd";
			long long temp = ans + 1;
			pq.push(arr[i].second);
			mini = min(arr[i].first,mini);
			sum = sum + (long long)arr[i].second;
			while(sum > (long long)mini*(long long)temp ){
				long long x = pq.top();
				pq.pop();
				sum -= x;
				temp--;
			}
			//cout << temp <<" "<< ans <<pq.top()<< " " <<" "<<mini <<" " <<sum<<endl;
			ans = max(temp,ans); 
 
		}
		if(real_ans != ans){
			for(int i = 0; i  < n; i++){
				cout << arr[i].first <<" "<<arr[i].second << endl;

			}
			cout << "real_ans = "<<real_ans <<" || ans = "<<ans;
			exit(0);
		}
		//cout << ans << endl;
 
	}
} 










// 01562-256331
// 7742500524