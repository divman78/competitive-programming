#include<bits/stdc++.h>
using namespace std;

//#define _DEBUG 1

long long n, m, k;
long long arr[21][21];

map<long long, long long> mm[21][21];
long long ans;

void f1(long long x, long long y, long long val, long long cnt){
	val ^= arr[x][y];
	if(cnt == (n + m)/2){
		mm[x][y][val]++;
		return;
	}
	if(x + 1 <= n) f1(x + 1, y, val, cnt + 1);
	if(y + 1 <= m) f1(x, y + 1, val, cnt + 1);
}

void f2(long long x, long long y, long long val, long long cnt){
	if(cnt == (n + m) - ((n + m)/2)){
		if(mm[x][y].count(val ^ k)){
			ans += mm[x][y][val ^ k];
		}
		return;
	}

	if(x - 1 >= 1) f2(x - 1, y, arr[x][y] ^ val, cnt + 1);
	if(y - 1 >= 1) f2(x, y - 1, arr[x][y] ^ val, cnt + 1);

}

int main(){
#ifdef _DEBUG
	freopen("input.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);
#endif
	cin >> n >> m >> k;
	for (int i = 1; i <= n; ++i)
	{
		for (int j = 1; j <= m; ++j)
		{
			cin >> arr[i][j];
		}
	}

	ans = 0;
	f1(1,1,0,1);
	f2(n,m,0,1);
	cout << ans << endl;
}