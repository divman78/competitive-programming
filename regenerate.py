

#reconstruct original image
org = gpA[5]
#coarser to finer 5 --> 0
for i in xrange(5,0,-1):
    #decompress ith image
    ls = cv2.pyrUp(gpA[i])
    #add decompressed image to laplacian of finer level to i, i.e, i-1 
    org = cv2.add(ls, lpA[i-1])

cv2.imwrite('./images2/XX.jpg', org)