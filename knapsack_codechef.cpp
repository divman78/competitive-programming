#include <bits/stdc++.h>
#include <sys/resource.h>
using namespace std;
typedef long long ll;
typedef long double ld;
typedef vector<int> vi;
typedef vector<ll> vl;
typedef vector<vl> vvl;
typedef vector<vi> vvi;
typedef vector<double> vd;
typedef vector<vd> vvd;
typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<pii> vii;
typedef vector<string> vs;
//const int mod = ;

int main() {
  int n, m, P;
  scanf("%d%d%d", &n, &m, &P);
  vi b(m);
  vl bval(m);
  vector<vii> v(m);
  for (int i = 0; i < n; ++i) {
    int a, p, val;
    scanf("%d%d%d", &a, &p, &val);
    --a;
    v[a].emplace_back(p, val);
    bval[a] += val;
  }
  for (int i = 0; i < m; ++i) {
    scanf("%d", &b[i]);
  }
  vl d(P + 1);
  for (int t = 0; t < m; ++t) {
    vl d0 = d;
    for (pii p : v[t]) {
      for (int i = P; i >= p.first; --i) {
        d[i] = max(d[i], d[i - p.first] + p.second);
      }
    }
    for (int i = P; i >= b[t]; --i) {
      d[i] = max(d[i], d0[i - b[t]] + bval[t]);
    }
  }
  ll res = 0;
  for (ll x : d) res = max(res, x);
  printf("%lld\n", res);
  return 0;
}
