#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","r",stdin);
	long long t;
	cin >> t;
	while(t--){
		long long n;
		cin >> n;
		long long h[n];
		long long k[n];
		long long maxi = 0;
		for (long long i = 0; i < n; ++i){
			cin >> h[i];
			maxi = max(maxi, h[i]*2 + 1); 
		}
		long long mini = INT_MAX;
		for (long long i = 0; i < n; ++i){
			cin >> k[i];
			mini = min(mini, k[i]);
		}
		long long dp[maxi];
		for (long long i = 0; i < maxi; ++i){
			dp[i] = INT_MAX;
			if(i < mini) dp[i] = -1;//, cout << dp[i] << " ";
		}
		dp[0] = 0;
		for (long long i = mini; i < maxi; ++i){
			for (long long j = 0; j < n; ++j){
				if(i - k[j] >= 0){
					if(dp[i-k[j]] != -1)
						dp[i] = min(dp[i], dp[i-k[j]] + 1);
				}
			}
			//cout << dp[i] << " ";
		}
		//cout << endl;
		long long sum = 0;
		for (long long i = 0; i < n; ++i){
			//cout << dp[h[i]*2] << " ";
			sum += dp[h[i]*2];
		}
		cout <<sum;
		cout << endl;
	}
}