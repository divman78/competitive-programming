#include<bits/stdc++.h>
using namespace std;


#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define MO 1000000007
#define N 50010
#define int long long
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)




signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif

	string s;
	cin >> s;
	stack<char> sta;
	int ans= 0;
	for (int i = 0; i < s.length(); ++i)
	{
		if(sta.empty()){
			sta.push(s[i]);
		}
		else if(sta.top() == s[i]){
			ans ++;
			sta.pop();
		}

		else{
			sta.push(s[i]);
		}

	}

	string ss = (ans%2) ? "YES":"NO";
	cout << ss << endl;

	return 0;
}




