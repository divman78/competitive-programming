#include<bits/stdc++.h>
using namespace std;

#define print(Arr, N) for (int i = 1; i <= N ; ++i) cout << Arr[i] << " "; cout << endl;

string s1, s2;

int main(){

	ios_base::sync_with_stdio(false); cin.tie(0);
	//freopen("input.txt", "r", stdin);
	//freopen("output1.txt","w",stdout);
	int t;
	cin >> t;
	while(t--){
		cin >> s1 >> s2;
		int m = s1.length(); int n = s2.length();
		int arr1[m+1];
		int arr2[n+1];
		for (int i = 1; i <= m; ++i)
		{
			arr1[i] = s1[i-1]-48;
			
		}

		for (int i = 1; i <= n; ++i)
		{
			arr2[i] = s2[i-1]-48;
		}
		// print(arr1,m);
		// print(arr2, n);

		int calc1[m+1], calc2[n+1];
		if(arr1[1] && arr2[1]){
			calc1[1] = calc2[1] = 1;
		}
		else{
			calc1[1] = calc2[1] = 0;
		}

		for (int i = 2; i <= m; ++i)
		{
			if(arr1[i] && calc1[i-1] == 0)
				calc1[i] = 1;
			else 
				calc1[i] = 0;
		}

		for (int i = 2; i <= n; ++i)
		{
			if(arr2[i] && calc2[i-1] == 0)
				calc2[i] = 1;
			else 
				calc2[i] = 0;
		}

        print(calc1, m);
        print(calc2, n);
		for (int i = 2; i <= m; ++i)
		{
			if(!(i&1)){
				calc1[i] = (!calc1[i]) | calc1[i-1];
			}
			else{
				calc1[i] = calc1[i] | calc1[i-1];
			}
		}

		for (int i = 2; i <= n; ++i)
		{
			if(!(i&1)){
				calc2[i] = (!calc2[i]) | calc2[i-1];
			}
			else{
				calc2[i] = calc2[i] | calc2[i-1];
			}
		}

		print(calc1, m);
		print(calc2, n);

		cout << endl;
		cout << t << endl;
		for (int i = 1; i <= n; ++i)
		{
			for (int j = 1; j <= m; ++j)
			{
				int p, q;
				if(i%2 == 0){
					p = calc2[i];
				}
				else{
					p = !calc2[i];
				}

				if(j%2 == 0){
					q = calc1[j];
				}
				else{
					q = !calc1[j];
				}
				int x = p|q;
				cout << x << " ";
			}
			cout << endl;
		}
		cout << endl;


	}
}
