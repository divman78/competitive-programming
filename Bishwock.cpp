#include<bits/stdc++.h>
using namespace std;

int main(){
	int t = 1;
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	cin >> t;
	#endif
	while(t--){
		string s[2];
		cin >> s[0] >> s[1];
		int n = s[0].length();
		s[0] = 'X' + s[0] + 'X';
		s[1] = 'X' + s[1] + 'X';
		int ans = 0;
		for(int i = 1; i <= n; i++){
			int x=-1, y=-1;
			if(s[0][i] == '0' && s[1][i] == '0'){
				if(s[0][i-1] == '0'){
					x = 0, y = i-1;
				}
				else if(s[1][i-1] == '0'){
					x = 1, y = i-1;
				}
				else if(s[0][i+1] == '0'){
					x = 0, y = i+1;
				}
				else if(s[1][i+1] == '0'){
					x = 1, y = i+1;
				}
			}
			if(x != -1){
				s[0][i] = 'X',  s[1][i] = 'X', s[x][y] = 'X';
				ans ++;
			}
		}
		cout << ans  << endl;


	}

}