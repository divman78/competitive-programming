#include<bits/stdc++.h>
using namespace std;
#define pb(x) push_back(x)
#define mp(x,y) make_pair(x,y)
#define ll long long

int solve(vector<int>& arr, int n, int qi){
	vector<pair<int,int> > arr_sorted(n);
	for(int i = 0; i < n; i++){
		arr_sorted[i].first = arr[i];
		arr_sorted[i].second = i;
	}
	sort(arr_sorted.begin(),arr_sorted.end());
	vector<int> arr1(n);
	vector<int> arr2(n);
	for(int i = 0; i < n; i++){
		arr1[i]=arr_sorted[i].first;
		arr2[i]=arr_sorted[i].second;
	}
	int index = lower_bound(arr1.begin(), arr1.end(), qi) - arr1.begin();
	//cout<<"index= "<<index<<"\n";
	int ind = arr2[index];
	vector<int> push_vec;
	int high = n-1, low = 0, mid;
	while(low <= high){
		mid = (low + high) / 2;
		if(mid == ind)     break;
		else if(mid < ind) low = mid + 1;
		else                 high = mid - 1;
		push_vec.pb(mid);
	}
	int fixed_less = 0;
	int fixed_greater = 0;
	int temp_less = 0;
	int temp_greater = 0;
	for(int i = 0; i < push_vec.size(); i++){
		//cout<<push_vec[i]<<" ";
		if(push_vec[i] < ind){
			if(arr[push_vec[i]]>qi){
				temp_less++;
			}
			else{
				fixed_less++;
			}
		}
		else{
			if(arr[push_vec[i]]<qi){
				temp_greater++;
			}
			else{
				fixed_greater++;
			}
		}
	}
	//cout<<"\n";
	int sw = min(temp_less,temp_greater);
	int less = index - fixed_less - sw;
	int greater = n - index - fixed_greater - sw - 1;
	int ans = sw;
	if(temp_less > temp_greater){
		if(temp_less-temp_greater <= less){
			ans += (temp_less-temp_greater);
		}
		else ans = -1;
	}		
	else if(temp_less < temp_greater){
		if(temp_greater-temp_less <= greater){
			ans += (temp_greater-temp_less);
		}
		else ans = -1;
	}
	return ans;
}

int main(){
	freopen("input.txt","r",stdin);
	//freopen("output.txt","w",stdout);
	int t;
	cin >> t;
	while(t--){
		
		int n, q;
		cin >> n >> q;
		vector<int> arr(n);
		for(int i = 0; i < n; i++){
			cin >> arr[i];
		}
		while(q--){
			//cout<<"sw= "<<sw<<" "<<"fg= "<<fixed_greater<<" "<<"fl= "<<fixed_less<<" "<<"tg= "<<temp_greater<<" "<<"tl= "<<temp_less<<" ";
			//cout<<"l= "<<less<<" "<<"g= "<<greater<<" ";
			//cout<<"ans= ";
			int qi;
			cin>>qi;
			int ans = solve(arr,n,qi);
			cout<<ans;
			cout<<"\n";
		}
	}
}