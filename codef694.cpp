#include<bits/stdc++.h>
using namespace std;

#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long
#define TEST_CASES int t = 1; cin >> t; while(t--)
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define N 200009

void read_file(){
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

void init(){
	FAST_IO;
	read_file();
}

signed main(){
	init();
	//TEST_CASES
	{
		int n;
		cin >> n;
		string s;
		cin >> s;
		int l=0, r=0;
		int ans = 0;
		for (int i = 0; i < n; ++i)
		{
			for (int j = i+1; j < n; ++j)
			{
				swap(s[i],s[j]);
				int pre[n+1] = {0};
				int mn = INT_MAX;
				for(int k = 0; k < n; k++){
					pre[k+1] = pre[k] + (int)(s[k]==')'?-1:1);
					if(mn>pre[k+1]){
						mn = pre[k+1];
					}
				}
				if(pre[n]==0){
					int cntmn = 0;
					for (int k = 1; k <= n; ++k)
					{
						if(pre[k] == mn){
							cntmn++;
						}
					}		
					if(ans<cntmn){
						l = i;
						r = j;
						ans = cntmn;
					}
				}
				swap(s[j],s[i]);
			}
		}
		cout << ans << endl;
		cout << l + 1 << " " << r + 1 << endl;
	}
	return 0;
}