#include <bits/stdc++.h>
using namespace std;
const int N = 1e5 + 9, M = 255;
char s[N], d[5][M];
int n, q, len[5], f[M][M][M], nxt[N][31];
int main()
{
	freopen("input.txt","r",stdin);
	scanf("%d%d%s", &n, &q, s + 1);
	fill(nxt[n + 1], nxt[n + 1] + 26, n + 1);
	fill(nxt[n], nxt[n] + 26, n + 1);
	for (int i = n; i; --i)
	{
		copy(nxt[i], nxt[i] + 26, nxt[i - 1]);
		nxt[i - 1][s[i] - 'a'] = i;
	}
	for (int x; q--; printf(f[len[1]][len[2]][len[3]] <= n ? "YES\n" : "NO\n"))
	{
		scanf("%s%d", s, &x);
		if (s[0] == '-')
		{
			--len[x];
			continue;
		}
		scanf("%s", &d[x][++len[x]]);
		for (int i = x == 1 ? len[x] : 0; i <= len[1]; ++i)
			for (int j = x == 2 ? len[x] : 0; j <= len[2]; ++j)
				for (int k = x == 3 ? len[x] : 0; k <= len[3]; ++k)
				{
					f[i][j][k] = n + 1;
					if (i)
						f[i][j][k] = min(f[i][j][k], nxt[f[i - 1][j][k]][d[1][i] - 'a']);
					if (j)
						f[i][j][k] = min(f[i][j][k], nxt[f[i][j - 1][k]][d[2][j] - 'a']);
					if (k)
						f[i][j][k] = min(f[i][j][k], nxt[f[i][j][k - 1]][d[3][k] - 'a']);
				}
	}
}