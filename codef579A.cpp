#include<bits/stdc++.h>
using namespace std;
 
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define int long long

#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)

 
void read_file(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
}

 
 
signed main(){
	read_file();
 
	int t;
	t = 1;
	//cin >> t;
	
	while(t--){

		string s;
		string t;


		cin >> s >> t;
		

		int ss = s.length();
		int ts = t.length();

		int pre[ts];

		for (int i = 0, j = 0; i < ts && j < ss; ++j)
		{
			if(t[i]==s[j]){
				pre[i++] = j;
			}
		}

		int suf[ts];

		for (int i = ts-1, j = ss-1; i >= 0 && j >= 0; --j)
		{
			if(t[i]==s[j]){
				suf[i--] = j;
			}
		}

		int ans = 0;

		for (int i = 0; i < ts-1; ++i)
		{
			ans = max(ans, suf[i+1]-pre[i]-1);
		}

		ans = max(ans, ss-1-pre[ts-1]);
		ans = max(ans, suf[0]);
		cout << ans << endl;






	}
 
 
	
 
 
 
 
	return 0;
}
