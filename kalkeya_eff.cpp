#include<bits/stdc++.h>
using namespace std;

int main(){
	int t;
	scanf("%d", &t);
	while(t--){
		long int n, k;
		scanf("%ld %ld", &n, &k);
		if(k==0){
			for(long int i=1; i<n+1; i++)
				printf("%ld ", i);
			printf("\n");
		}
		
		else if(k>n/2)
			printf("-1\n");

		else{
			vector<long int> a(n, 0);
			long int count=1;
			for(long int i=0; i<(n/k)*k; i+=k){
				long int j=i;		
				if(count%2)
					while(j<i+k){
						a[j]=(j+1+k);
						j++;
					}
				
				else
					while(j<i+k){
						a[j]=(j+1-k);
						j++;
					}
				
				count++;
			}

			for(long int i=n-2*k; i<n-k; i++){
				a[i]=i+k+1;
			}

			for(long int i=0; i<n-k; i++)
				cout<<a[i]<<" ";

			vector<bool> flag(n+1, false);
			for(long int i=0; i<n-k; i++){
				flag[a[i]] = true;
			}

			for(long int i=1; i<n; i++)
				if(!flag[i])
					cout<<i<<" ";
		cout<<endl;
		}
		
	}
	return 0;
}