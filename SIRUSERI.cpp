#include<bits/stdc++.h>
using namespace std;

template<typename T>
void pop_front(std::vector<T>& vec)
{
    assert(!vec.empty());
    vec.erase(vec.begin());
}

int main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int t;
	cin >> t;
	while(t--){
		int a, b;
		cin >> a >> b;
		vector<pair<int,int> > awin;
		vector<pair<int,int> > bwin;
		int n = a*b;
		int aa[n];
		int bb[n];
		for(int i = 0; i < n; i++){
			cin >> aa[i];
		} 
		for(int i = 0; i < n; i++){
			cin >> bb[i];
		}

		for(int i = 0; i < n; i++){
			if(aa[i] > bb[i]){
				awin.push_back(make_pair(aa[i], bb[i]));
			}
			else{
				bwin.push_back(make_pair(bb[i], aa[i]));
			}
		}
		sort(awin.begin(), awin.end());
		sort(bwin.begin(), bwin.end());
		int ans = 0;

		// while(bwin.size()){
		// 	bwin.pop_back();
		// 	for (int i = 0; i < bwin.size(); ++i)
		// 	{
		// 		cout << bwin[i].first << " ";
		// 	}
		// 	cout << endl;
		// }

		int x = awin.size()-1;
		int y = bwin.size()-1;


		while(x >= 0 || y >= 0){
			int cnt = b;
			int mx = -1;
			while(cnt > 1 && y >= 0){
				mx = max(mx, bwin[y--].first);
				cnt--;
			}
			if(x >= 0){
				if(awin[x].first > mx){
					ans ++;
					while(cnt > 0 && x >= 0){
						x--;
						cnt--;
					}
				}
			}
			while(cnt > 0 && y >= 0){
				y--;
				cnt--;
			}
			while(cnt > 0 && x >= 0){
				x--;
				cnt--;
			}
		}

		cout << ans << endl;
	}
}