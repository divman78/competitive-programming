#include<bits/stdc++.h>
using namespace std;

int main(){
	//freopen("input.txt","r",stdin);
	int t;
	//cin >> t;
	t = 1;
	while(t--){
		string s;
		cin >> s;
		int n = s.size();
		int pal[n][n];
		for (int i = 0; i < n; ++i)
		{
			for (int j = 0; j < n; ++j)
			{
				pal[i][j] = 0;
			}
		}
		int cnt = 0;
		for (int i = 0; i < n; ++i)
		{
			pal[i][i] = 1;
			cnt++;
		}
		for (int i = 0; i < n-1; ++i)
		{
			if(s[i] == s[i+1]){
				pal[i][i+1] = 1;
				cnt++;
			}
		}

		for (int k = 3; k <= n; ++k)
		{
			for (int i = 0; i < n - k + 1; ++i)
			{
				int j = i + k - 1;
				if(pal[i+1][j-1] && s[i] == s[j]){
					pal[i+1][j-1] = 1;
					cnt++;
				}
			}
		}
		cout << cnt << endl;

	}
}

