#include<bits/stdc++.h>
using namespace std;
#define FAST_IO ios_base::sync_with_stdio(false); cin.tie(0);


double polygonArea(double X[], double Y[]) 
{ 
	int n = 3;
    double area = 0.0;  
    int j = n - 1; 
    for (int i = 0; i < n; i++) 
    { 
        area += (X[j] + X[i]) * (Y[j] - Y[i]); 
        j = i; 
    } 
    return abs(area / 2.0); 
} 


signed main(){
	//freopen("input.txt","r",stdin);
	FAST_IO;
	int t;
	cin >> t;
	while(t--){
		int n;
		cin >> n;
		std::vector<int> v[4];
		for(int i = 0;i < n; i++){
			int x, y;
			cin >> x >> y;
			v[x].push_back(y);
		}
		double ans = 0;
		for(int i = 0; i < v[1].size(); i++){
			for(int j = 0; j < v[2].size(); j++){
				for(int k = 0; k < v[3].size(); k++){
					double X[3], Y[3];
					X[0] = 1, X[1] = 2, X[2] = 3;
					Y[0] = v[1][i], Y[1] = v[2][j], Y[2] = v[3][k];
					ans += polygonArea(X,Y);
				}
			}
		}
		// for(int h = 1; h <= 3; h++){
		// 	for(int i = 0; i < v[h].size()-1; i++){
		// 		for(int j = i+1; j < v[h].size(); j++){
		// 			for(int e = (h+1)%4; e != h; e = (e+1)%4){
		// 				for(int k = 0; k < v[e].size(); k++){
		// 					double X[3], Y[3];
		// 					X[0] = h, X[1] = h, X[2] = e;
		// 					Y[0] = v[h][i], Y[1] = v[h][j], Y[2] = v[e][k];
		// 					ans += polygonArea(X,Y);
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		cout << fixed << setprecision(6);
		cout << ans << endl;

	}

}
