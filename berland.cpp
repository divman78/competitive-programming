#include<bits/stdc++.h>
using namespace std;
#define int unsigned long long
#define MOD 1000000007


signed main(){
	ios_base::sync_with_stdio(false); cin.tie(0);
	#ifndef ONLINE_JUDGE
	freopen("input.txt","r",stdin);
	#endif
	int t;
	cin >> t;
	while(t--){
		string s;
		cin >> s;
		int res = 0; 
		int n = s.length();
		int pre[n+1] = {0};
    	for (int i = 0; i < s.length(); i++) {
    		res = (res*10 + (int)s[i] - '0') % MOD, pre[i] = res;
    	}

        int ans = pre[n-1];

        int tenpow[n+1];
        tenpow[0] = 1;
        for (int i = 1; i <= n; ++i)
        {
        	tenpow[i] = (tenpow[i-1]*10)%MOD;
        }
        for(int i = 1; i < n; i++){
        	int p1 = (pre[n-1] - (tenpow[n-i]*((int)pre[i-1])%MOD)+MOD)%MOD;
        	int p2 = pre[i-1];
        	int x = (((p1*tenpow[i])%MOD) + ((p2)%MOD))%MOD;
        	ans = (((ans*tenpow[n])%MOD)+((x)%MOD))%MOD;
        }
        cout << ans << endl;

	}
}