#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "NO"; return 0;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

int bigMOD(int num,int n){
  if(n==0) return 1;
  int x=bigMOD(num,n/2);
  x=x*x%MO;
  if(n%2==1) x=x*num%MO;
  return x;
}

int vis[N];

int dfs(int v, vector<vector<pair<int,int > > >& adj){
	vis[v] = 1;
	int ret = 1;
	for(auto p: adj[v]){
		int u = p.first;
		if(!vis[u] && p.second==0){
			ret += dfs(u, adj);
		}
	}

	return ret;
}


signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	//cin >> t; 
	while(t--){

		int n, k;
		cin >> n >> k;

		vector<vector<pair<int,int > > > adj(n);

		for (int i = 0; i < n-1; ++i)
		{
			int u, v, x;
			cin >> u>> v>> x;
			adj[u-1].push_back(mp(v-1,x));
			adj[v-1].push_back(mp(u-1,x));
		}


		int ans = bigMOD(n,k);

		vector<int> v;

		for (int i = 0; i < n; ++i)
		{
			if(!vis[i]){
				v.push_back(dfs(i, adj));
			}
		}

		for (int i = 0; i < v.size(); ++i)
		{
			ans = (ans - bigMOD(v[i],k)+MO)%MO;
		}

		cout << ans << endl;










        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}




















