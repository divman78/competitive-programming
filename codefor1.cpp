#include<bits/stdc++.h>
using namespace std;
#define int long long



vector<int> apply(int n){
	vector<int> v;
	if(n == 0){
		v.push_back(0);
		return v;
	}
	if(n == 1){
		v.push_back(1);
		return v;
	}
	if(n == 2){
		v.push_back(1);
		v.push_back(0);
		v.push_back(1);
		return v;
	}
	int x = n%2;
	vector<int> v1 = apply(n/2);
	for (int i = 0; i < v1.size(); ++i)
	{
		v.push_back(v1[i]);
	}
	v.push_back(x);
	for (int i = 0; i < v1.size(); ++i)
	{
		v.push_back(v1[i]);
	}
	return v;
}



signed main(){
	int n, l, r;
	cin >> n >> l >> r;

	vector<int> v;
	v = apply(n);

	int cnt = 0;
	// for (int i = 0; i < v.size(); ++i)
	// {
	// 	cout << v[i] << " ";
	// }
	// cout << endl;
	for(int i = min((int)(v.size()-1), (int)(l-1)); i < min((int)(v.size()), (int)(r)); i++){
		if(v[i] == 1) cnt++;

	}
	cout << cnt << endl;
}