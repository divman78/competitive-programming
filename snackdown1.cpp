#include<stdio.h>
#include<string.h>
#include<bits/stdc++.h>
using namespace std;
#define int long long int
int arr[64];
int dp[4][64][64][64];
int a, b, c;
int n1, n2;
int i, j, k;
int sz;
void to_binary(int n){
	sz = 0;
	while(n){
		arr[sz++] = n%2;
		n/=2;
	}

}
int countones(int n){
	int cnt = 0;
	while(n){
		cnt += n%2;
		n/=2;
	}
	return cnt;
}

main(){
	int t;
	scanf("%lld",&t);
	while(t--){
		memset(dp, 0, sizeof(dp));
		scanf("%lld%lld%lld",&a,&b,&c);
		//printf("%d %d %d\n", a,b,c);
		n1 = countones(a), n2 = countones(b);
		//printf("%d %d\n", n1,n2);
		to_binary(c);
		// for (i = sz-1; i >= 0; --i)
		// {
		// 	printf("%d", arr[i]);;
		// }
		// printf("\n");
		for (i = 0; i < 4; ++i)
			for (j = 0; j < 3; ++j)
				for (k = 0; k < 3; ++k)
					dp[i][0][j][k] = 0;

		if(arr[0] == 0){
			dp[0][0][0][0] = 1;
			dp[3][0][1][1] = 1;
		}
		else{
			dp[1][0][0][1] = 1;
			dp[2][0][1][0] = 1;
		}

		for (i = 1; i < sz; i++){
			for(j = 0; j <= i+1; j++){
				for(k = 0; k <= i+1; k++){
					if(arr[i]){
						dp[0][i][j][k] = dp[3][i-1][j][k];
						if(k > 0) 
						dp[1][i][j][k] = dp[0][i-1][j][k-1] + dp[1][i-1][j][k-1] + dp[2][i-1][j][k-1];
					    if(j > 0)
					    dp[2][i][j][k] = dp[0][i-1][j-1][k] + dp[1][i-1][j-1][k] + dp[2][i-1][j-1][k];
					    if(j > 0 && k > 0)
					    dp[3][i][j][k] = dp[3][i-1][j-1][k-1];
					}
					else{
						dp[0][i][j][k] = dp[0][i-1][j][k] + dp[1][i-1][j][k] + dp[2][i-1][j][k];
						if(k > 0)
						dp[1][i][j][k] = dp[3][i-1][j][k-1];
					    if(j > 0)
					    dp[2][i][j][k] = dp[3][i-1][j-1][k];
					    if(j > 0 && k > 0)
					    dp[3][i][j][k] = dp[0][i-1][j-1][k-1] + dp[1][i-1][j-1][k-1] + dp[2][i-1][j-1][k-1];
					}
					cout<<"["<<i<<"]"<<"["<<j<<"]"<<"["<<k<<"] " << dp[0][i][j][k] <<" "<< dp[1][i][j][k] <<" "<< dp[2][i][j][k] <<" "<< dp[3][i][j][k] <<" "<< dp[0][i][j][k] + dp[1][i][j][k] + dp[2][i][j][k]<<"\n";
				}
			}
		}
		int ans =  dp[0][sz-1][n1][n2] + dp[1][sz-1][n1][n2] + dp[2][sz-1][n1][n2];
		//ans = dp[0][sz-1][n2][n1] + dp[1][sz-1][n2][n1] + dp[2][sz-1][n2][n1];
		if(sz > 1){
			ans -= (dp[3][sz-2][n1][n2-1]+dp[3][sz-2][n1-1][n2]);
		}
		printf("%lld\n", ans);


	}
}

//test

// 2
// 1 2 3
// 369 428 797

//test

// 2
// 1 2 3
// 369 428 797