#include<bits/stdc++.h>
using namespace std;
#define N 100000

long long arr[N];
long long arr1[N];
long long ans[N];
long long myans[N];
long long n;
long long cnt;

vector<long long> FindMaxSum(vector<long long>& arrx, long long ni)
{
  vector<long long> temp1,temp2,temp3;
  long long incl = arrx[0];
  long long excl = 0;
  long long excl_new;
  temp1.push_back(0);
 
  for (long long i = 1; i < ni; i++)
  {
     /* current max excluding i */
    if(incl > excl)
    {
      temp3 = temp1;
    }
    else
    {
      temp3 = temp2;
    }
    excl_new = max(incl,excl);
 
     /* current max including i */
     incl = excl + arrx[i];
     temp1 = temp2;
     temp1.push_back(i);
     excl = excl_new;
     temp2 = temp3;
  }
 
   /* return max of incl and excl */
  if(incl > excl)
    {
      return temp1;
    }
    else
    {
      return temp2;
    } 
}

void func(vector<long long>& push_vec, vector<bool>& flag, long long x){

	vector<long long> vec;

	for(long long i = 0; i < push_vec.size(); i++){
		vec.push_back(arr[push_vec[i]]);
	}
	vector<long long> vect1 = FindMaxSum(vec,vec.size());
	/*long long j = 0;
	for(long long i = 0; i< vec.size(); i++){
		if(vec[i]==vect1[j]){
			j++;
			flag[push_vec[i]] = true;
		}
	}*/
	//cout<<"dfsdgfss"<<endl;
	for(long long i = 0; i < vect1.size(); i++){
		    //cout<<vect1[i]<<" "<<x<<endl;
			flag[2*vect1[i]+x] = true;
	}
}

void solve(){
	/*code goes here*/
	if(n==1){
		myans[0] = 0;
		return;
	}
	if(n==2){
		myans[0]=(arr[0]>arr[1])?arr[0]:-arr[0];
		myans[1]=(arr[0]>arr[1])?-arr[1]:arr[1];
		if(arr[0]==arr[1]){
			myans[0]=arr[0];
			myans[1]=arr[1];
		}
		return;
	}
	vector<long long> mark;
	if(arr[0]<arr[1]){
		mark.push_back(0);
	}
	for(long long i = 1; i < n-1; i++){
		if(arr[i]<arr[i+1] && arr[i]<arr[i-1]){
			mark.push_back(i);
		}
	}
	if(arr[n-1]<arr[n-2]){
		mark.push_back(n-1);
	}
	//for(long long j = 0; j < mark.size(); j++) cout<<mark[j]<<" ";
	//cout<<"\n";
	vector<bool> flag(n);
	for (long long i = 0; i < n; i++) flag[i] = false;
	long long i = 0;
	while(i<mark.size()){
		long long p = mark[i];
		//cout<<"p = "<<p<<"\n";
		vector<long long> push_vec;
		push_vec.push_back(mark[i++]);
		while(i<mark.size()){
			if(mark[i]-mark[i-1]==2){
				if(arr[mark[i]-1]-arr[mark[i]] <= arr[mark[i-1]]){
					push_vec.push_back(mark[i++]);
				}
				else{
					break;
				}
			}
			else{
				break;
			}
		}
		/*cout<<"dfs  "<<i<<endl;
		for(long long j = 0; j < push_vec.size(); j++){
			cout<<arr[push_vec[j]]<<" ";
		}
		cout<<"\n";*/
		func(push_vec, flag, p);
	}
	for(long long i = 0; i < n; i++){
		if(flag[i]){
			myans[i] = -arr[i];
		}
		else{
			myans[i] = arr[i];
		}
	}
}

int main(){
	//freopen("input.txt","r",stdin);
	//freopen("output.txt","w",stdout);
	ios_base::sync_with_stdio(false);
    cin.tie(NULL);
	long long t;
	cin >> t;
	while(t--){
		cin >> n;
		for(long long i = 0; i < n; i++){
			cin >> arr[i];
		}
		solve();
		
		for(long long i = 0; i < n; i++) cout<<myans[i]<<" "; 
		cout<<"\n";
	}
	//trap(x);
	return 0;
}