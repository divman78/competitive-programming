#include<bits/stdc++.h>
using namespace std;

#define int long long
#define FAST_IO ios_base::sync_with_stdio(0); cin.tie(0)
#define endl "\n"
#define printarr(arr, n) for(int i = 0; i < n; i++) cout << arr[i] << " "; cout << endl;
#define pii pair<int,int>
#define mp(x, y) make_pair(x, y)
#define debug(x) cout << "#debug " << x << endl;
#define print2D(arr, n) cout << "\n_____________________\n";  for(int i = 1; i  <= n; i++){	for(int j = 1; j <= n; j++){cout << arr[i][j] << " ";}cout << endl;}cout << "\n_____________________\n";
#define mii map<int,int>
#define printmap(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++) cout << itr->first << "->" << itr->second << " "; cout << endl;
#define iterm(mm) for(map<int,int>::iterator itr = mm.begin(); itr!=mm.end(); itr++)
#define NO cout << "No answer\n"; continue;
#define MO 1000000007
#define N 500100
#define MX 2000000000

void preprocess(){
	
}

void makedistinct(string& vec){
  sort( vec.begin(), vec.end() );
  vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
}

bool check(string& s){
	for (int i = 1; i < s.length(); ++i)
	{
		if(abs(s[i]-s[i-1])==1){
			return false;
		}
	}
	return true;
}


signed main(){
	FAST_IO;
	#ifndef ONLINE_JUDGE
	freopen("/home/divakar/programs/input.txt","r",stdin);
	//freopen("/home/divakar/programs/output.txt","w",stdout);
	#endif
	clock_t clk = clock();

	preprocess();
	int t=1;
	//cout << " dsf ";
	cin >> t; 
	while(t--){
		cout << t+1 << ". ";
		string s;
		cin >> s;
		int n = s.length();

		int cnt[26] ={0};
		for (int i = 0; i < n; ++i)
		{
			cnt[s[i]-'a']++;
		}
		string ss = s;
		makedistinct(ss);

		n = ss.length();

		if(n%2==0){
			string ans = ss;
			for (int i = 0,  j = 0; i < n/2; ++i)
			{
				ans[j++] = ss[i+n/2];
				ans[j++] = ss[i];
			}

			string ans1 = s;
			for (int i = 0,  j = 0; i < n; ++i)
			{
				while(cnt[ans[i]-'a']--){
					ans1[j++] = ans[i];
				}
			}
			if(check(ans1)==false){
				NO;
			}
			cout << ans1 << endl;
		}
		else{
			string ans = ss;
			for (int i = 0,  j = 0; i < n/2+1; ++i)
			{
				ans[j++] = ss[i];
				ans[j++] = ss[i+n/2+1];
			}

			string ans1 = s;
			for (int i = 0,  j = 0; i < n; ++i)
			{
				while(cnt[ans[i]-'a']--){
					ans1[j++] = ans[i];
				}
			}
			if(check(ans1)==false){
				NO;
			}
			//cout << "Sdf";
			//cout << ss << endl;
			cout << ans1 << endl;
		}








        
		//cout << "ENDOFTEST "<< t << "\n";
	}









	#ifndef ONLINE_JUDGE
	clk = clock() - clk;
	cout << "Time Elapsed: " << fixed << setprecision(10) << ((long double)clk)/CLOCKS_PER_SEC << "\n";
	#endif
	return 0;
}